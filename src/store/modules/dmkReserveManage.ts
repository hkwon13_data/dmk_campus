// import { Map } from "immutable";

// const CHANGE_INPUT = "auth/CHANGE_INPUT"; // input 값 변경
// const INITIALIZE_FORM = "auth/INITIALIZE_FORM"; // form 초기화

// export const changeInput = createAction(CHANGE_INPUT); //  { form, name, value }
// export const initializeForm = createAction(INITIALIZE_FORM); // form

import types from 'types/types';

const date = new Date();
// eslint-disable-next-line consistent-return
const defaultWeekSetting = (): string | undefined => {
  if (date.getDay() === 1) {
    return '월';
    // eslint-disable-next-line no-else-return
  } else if (date.getDay() === 2) {
    return '화';
  } else if (date.getDay() === 3) {
    return '수';
  } else if (date.getDay() === 4) {
    return '목';
  } else if (date.getDay() === 5) {
    return '금';
  } else if (date.getDay() === 6) {
    return '토';
  } else if (date.getDay() === 7 || date.getDay() === 0) {
    return '일';
  }
}; // 현재 날짜로 default 설정

const initialState: types.DmkHallReservationTypes = {
  dmkHallReservation: {
    rating: 0,
    reserveTime: 0,
    people: 1,
    totalPrice: 0,
    discountPrice: 0,
    lastPrice: 0,
    year: `${date.getFullYear()}`,
    month: `${date.getMonth() + 1}`,
    day: `${date.getDate()}`,
    week: defaultWeekSetting(),
    reserveHour: ['0', '0'],
    responseData: ['10', '11'],
    username: '',
    groupname: '',
    phone: '',
    email: '',
    eventname: '',
    agreementKeep: false,
    keepTime: '',
    keepList: '',
    agreementPhoto: false,
    photoTime: '',
    agreementTotal: false,
    agreementCollectUserInfo: false,
    agreementHallRules: false,
  },
  initReservation: {
    rating: 0,
    reserveTime: 0,
    people: 1,
    totalPrice: 0,
    discountPrice: 0,
    lastPrice: 0,
    year: `${date.getFullYear()}`,
    month: `${date.getMonth() + 1}`,
    day: `${date.getDate()}`,
    week: defaultWeekSetting(),
    reserveHour: ['0', '0'],
    responseData: ['10', '11'],
    username: '',
    groupname: '',
    phone: '',
    email: '',
    eventname: '',
    agreementKeep: false,
    keepTime: '',
    keepList: '',
    agreementPhoto: false,
    photoTime: '',
    agreementTotal: false,
    agreementCollectUserInfo: false,
    agreementHallRules: false,
  },
};
const CHANGE_INPUT = 'dmkHall/CHANGE_INPUT';
// const RESET_STATE = "dmkHall/RESET_STATE";
const REFRESH_RESERVATION = 'dmkHall/REFRESH_RESERVATION';
export const setDmkHallReserveValue = (key: string, value: any) => {
  return { type: CHANGE_INPUT, payload: { key, value } };
};

export const setInitReserveValue = (value: object) => {
  return { type: REFRESH_RESERVATION, payload: value };
};

// export const resetDmkHallReserveValue = () => {
//   return
// }

export default function dmkHallReserveManages(
  state = initialState,
  action: types.DmkHallReserveAction
): types.DmkHallReservationTypes {
  switch (action.type) {
    case CHANGE_INPUT:
      return {
        ...state,
        dmkHallReservation: {
          ...state.dmkHallReservation,
          [action.payload.key]: action.payload.value,
        },
      };

    case REFRESH_RESERVATION:
      return {
        ...state,
        dmkHallReservation: action.payload,
      };
    default:
      return state;
  }
}

// export default handleActions(
//   {
//     [CHANGE_INPUT]: (state, action) => {
//       const { form, name, value } = action.payload;
//       return state.setIn([form, "form", name], value);
//     },
//     [INITIALIZE_FORM]: (state, action) => {
//       const initialForm = initialState.get(action.payload);
//       return state.set(action.payload, initialForm);
//     },
//   },
//   initialState
// );
