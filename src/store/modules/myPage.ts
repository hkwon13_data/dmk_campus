// import { createAction, handleActions } from 'redux-actions';
import {
  createPromiseThunk,
  reducerUtils,
  handleAsyncActions,
} from 'utils/asyncUtils';
import * as api from 'store/api/myPage';
import * as types from 'types/types';

const GET_PAYMENT_HEADER = 'myPage/GET_PAYMENT_HEADER'; // 요청 시작
const GET_PAYMENT_HEADER_SUCCESS = 'myPage/GET_PAYMENT_HEADER_SUCCESS'; // 요청 성공
const GET_PAYMENT_HEADER_ERROR = 'myPage/GET_PAYMENT_HEADER_ERROR'; // 요청 실패

// thunk 를 사용 할 때, 꼭 모든 액션들에 대하여 액션 생성함수를 만들 필요는 없습니다.
// 그냥 thunk 함수에서 바로 액션 객체를 만들어주어도 괜찮습니다.

// action creator(action)
export const getPaymentHeader = createPromiseThunk(
  GET_PAYMENT_HEADER,
  api.getPaymentHeader
);

// export const setValueText = (value) => {
// 	return {
// 		type: TEST_VALUE,
// 		payload: value,
// 	};
// };

const initialState: types.MyPage = {
  paymentHeader: reducerUtils.initial(),
};

const paymentHeader = (
  state = initialState,
  action: types.MyPageAction
): types.MyPage => {
  switch (action.type) {
    case GET_PAYMENT_HEADER:
    case GET_PAYMENT_HEADER_SUCCESS:
    case GET_PAYMENT_HEADER_ERROR:
      return handleAsyncActions(
        GET_PAYMENT_HEADER,
        'paymentHeader',
        true
      )(state, action);
    default:
      return state;
  }
};

export default paymentHeader;
