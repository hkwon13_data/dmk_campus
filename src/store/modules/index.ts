import { combineReducers } from 'redux';
import holidays from './dmkHall';
import community from './community';
import myPage from './myPage';
import authentication from './authentication';
import modalManages from './modalManages';
import dmkReserveManage from './dmkReserveManage';
// import auth from "./auth";

const reducers = combineReducers({
  holidays,
  community,
  myPage,
  authentication,
  modalManages,
  dmkReserveManage,
});

export default reducers;
export type RootState = ReturnType<typeof reducers>;
