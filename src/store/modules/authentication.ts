// import { Map } from 'immutable';
import { reducerUtils } from 'utils/asyncUtils';
import { setStorageItem, removeStorageItem } from 'utils/useLocalStorage';
// import { useSelector } from 'react-redux';
// import axios from 'axios';

import * as api from 'store/api/authentication';

import * as types from 'types/types';

const GET_TOKEN = 'auth/GET_TOKEN';
const GET_TOKEN_SUCCESS = 'auth/LOGIN_SUCCESS';
const GET_TOKEN_ERROR = 'auth/GET_TOKEN_ERROR';

const POST_SIGNUP = 'auth/POST_SIGNUP';
const POST_SIGNUP_SUCCESS = 'auth/POST_SIGNUP_SUCCESS';
const POST_SIGNUP_ERROR = 'auth/POST_SIGNUP_ERROR';

const SET_TOKEN = 'auth/SET_TOKEN';
const SET_REFRESH = 'auth/SET_REFRESH';
const SET_USERINFO = 'auth/SET_USERINFO';
const SET_LOGGEDIN = 'auth/SET_LOGGEDIN';
const DELETE_TOKEN = 'auth/DELETE_TOKEN';
const DELETE_AUTHINFO = 'auth/DELETE_AUTHINFO';

// action creator(action)
export const getToken = (data: any) => async (dispatch: Function) => {
  dispatch({ type: GET_TOKEN });
  try {
    const payload = await api.getLogin(data);
    dispatch({ type: GET_TOKEN_SUCCESS, payload });
  } catch (e) {
    dispatch({ type: GET_TOKEN_ERROR, error: e });
  }
};

export const postSignup = (data: any) => async (dispatch: Function) => {
  dispatch({ type: POST_SIGNUP });
  try {
    const payload = await api.postSignup(data);
    dispatch({ type: POST_SIGNUP_SUCCESS, payload });
  } catch (e) {
    dispatch({ type: POST_SIGNUP_ERROR, error: e });
  }
};

export const deleteToken = () => {
  removeStorageItem('jwtToken');
  removeStorageItem('refresh');
  return { type: DELETE_TOKEN };
};
export const deleteAuthInfo = () => {
  const value = { isAuthenticated: false, isAdmin: false, isActive: false };
  return { type: DELETE_AUTHINFO, payload: value };
};

export const setToken = (value: string) => {
  setStorageItem('jwtToken', value);
  return { type: SET_TOKEN, payload: value };
};

export const setRefresh = (value: string) => {
  setStorageItem('refresh', value);
  return { type: SET_REFRESH, payload: value };
};

export const setUserInfo = (value: types.SetUserInfoParameter) => {
  return { type: SET_USERINFO, payload: value };
};

export const setLoggedIn = (value: boolean) => {
  return { type: SET_LOGGEDIN, payload: value };
};

const initialState: types.Authentication = {
  tokenInfo: reducerUtils.initial(),
  signupInfo: reducerUtils.initial(),
  userInfo: {
    isAuthenticated: false,
    isAdmin: false,
    isActive: false,
    email: '',
    phone: '',
    username: '',
    category: '',
    company: '',
    department: '',
    position: '',
    dateJoined: '',
    agreementEmail: false,
    agreementSNS: false,
  },
  isLoggedIn: false,
  jwtToken: '',
  refresh: '',
};

export default function authentication(
  state = initialState,
  action: types.AuthenticationAction
): types.Authentication {
  switch (action.type) {
    case GET_TOKEN:
      return {
        ...state,
        tokenInfo: reducerUtils.loading(),
      };

    case GET_TOKEN_SUCCESS:
      return {
        ...state,
        tokenInfo: reducerUtils.success(action.payload),
      };

    case GET_TOKEN_ERROR:
      return {
        ...state,
        tokenInfo: reducerUtils.error(action.error),
      };
    case POST_SIGNUP:
      return {
        ...state,
        signupInfo: reducerUtils.loading(),
      };

    case POST_SIGNUP_SUCCESS:
      return {
        ...state,
        signupInfo: reducerUtils.success(action.payload),
      };

    case POST_SIGNUP_ERROR:
      return {
        ...state,
        signupInfo: reducerUtils.error(action.error),
      };
    case DELETE_TOKEN:
      return { ...state, jwtToken: '', refresh: '' };
    case SET_TOKEN:
      return { ...state, jwtToken: action.payload };
    case SET_REFRESH:
      return { ...state, refresh: action.payload };
    case SET_USERINFO:
      return { ...state, userInfo: action.payload };
    case SET_LOGGEDIN:
      return { ...state, isLoggedIn: action.payload };
    case DELETE_AUTHINFO:
      return { ...state, userInfo: action.payload };

    default:
      return state;
  }
}
