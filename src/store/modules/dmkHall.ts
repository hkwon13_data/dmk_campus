// import { createAction, handleActions } from "redux-actions";
// import { Map } from "immutable";
import {
  createPromiseThunk,
  reducerUtils,
  handleAsyncActions,
} from 'utils/asyncUtils';

import * as api from 'store/api/dmkHall';
// import { AxiosResponse } from 'axios';
import * as types from 'types/types';

const GET_HOLIDAYS = 'GET_HOLIDAYS';
const GET_HOLIDAYS_SUCCESS = 'GET_HOLIDAYS_SUCCESS';
const GET_HOLIDAYS_ERROR = 'GET_HOLIDAYS_ERROR';

const CLEAR_HOLIDAYS = 'CLEAR_HOLIDAYS';

// action creator(action)
export const getHolidays = createPromiseThunk(GET_HOLIDAYS, api.getHolidays);
export const clearHolidays = () => ({ type: CLEAR_HOLIDAYS });

const initialState: types.Holidays = {
  holidays: reducerUtils.initial(),
};

export default function holidays(
  state = initialState,
  action: types.DmkHallAction
): types.Holidays {
  switch (action.type) {
    case GET_HOLIDAYS:
    case GET_HOLIDAYS_SUCCESS:
    case GET_HOLIDAYS_ERROR:
      return handleAsyncActions(GET_HOLIDAYS, 'holidays', true)(state, action);
    case CLEAR_HOLIDAYS:
      return {
        ...state,
        holidays: reducerUtils.initial(),
      };

    default:
      return state;
  }
}
