// import { Map } from 'immutable';
import types from 'types/types';

const SET_MODAL_EMAIL_CHECK = 'authentication/SET_MODAL_EMAIL_CHECK' as const;

const SET_SIGNUP_SUCCESS_MODAL =
  'authentication/SET_SIGNUP_SUCCESS_MODAL' as const;

const SET_SEARCH_PASSWORD_POPUP_MODAL =
  'authentication/SET_SEARCH_PASSWORD_POPUP_MODAL' as const;

const SET_SEARCH_PASSWORD_SUCEESS_MODAL =
  'authentication/SET_SEARCH_PASSWORD_SUCEESS_MODAL' as const;

const SET_SEND_TEMPORARY_PASSWORD_MODAL =
  'authentication/SET_SEND_TEMPORARY_PASSWORD_MODAL' as const;

const SET_ERROR_EMAIL_INPUT_MODAL =
  'authentication/SET_ERROR_EMAIL_INPUT_MODAL' as const;

const SET_TERMS_MODAL = 'terms/SET_TERMS_MODAL' as const;

const SET_PERSNAL_INFO_MODAL = 'terms/SET_PERSNAL_INFO_MODAL' as const;

const SET_SEND_INQUIRY_SUCCESS_MODAL =
  'community/SET_SEND_INQUIRY_SUCCESS_MODAL' as const;

const SET_SEND_INQUIRY_ERROR_MODAL =
  'community/SET_SEND_INQUIRY_ERROR_MODAL' as const;

const SET_PASSWORD_UPDATE_MODAL = 'mypage/SET_PASSWORD_UPDATE_MODAL';

const SET_PASSWORD_UPDATE_SUCCESS_MODAL =
  'mypage/SET_PASSWORD_UPDATE_SUCCESS_MODAL';
// action creator(action)

export const setEmailCheckModal = (value: boolean): types.ReducerTypes => {
  return { type: SET_MODAL_EMAIL_CHECK, payload: value };
};

// action creator(action)
export const setSignUpSuccessModal = (value: boolean): types.ReducerTypes => {
  return { type: SET_SIGNUP_SUCCESS_MODAL, payload: value };
};

// action creator(action)
export const setSearchPasswordSuccessModal = (
  value: boolean
): types.ReducerTypes => {
  return { type: SET_SEARCH_PASSWORD_SUCEESS_MODAL, payload: value };
};

export const setSearchPasswordPopupModal = (
  value: boolean
): types.ReducerTypes => {
  return { type: SET_SEARCH_PASSWORD_POPUP_MODAL, payload: value };
};

export const setSendTemporaryPasswordModal = (
  value: boolean
): types.ReducerTypes => {
  return { type: SET_SEND_TEMPORARY_PASSWORD_MODAL, payload: value };
};

export const setErrorEmailInputModal = (value: boolean): types.ReducerTypes => {
  return { type: SET_ERROR_EMAIL_INPUT_MODAL, payload: value };
};

export const setTermsModal = (value: boolean): types.ReducerTypes => {
  return { type: SET_TERMS_MODAL, payload: value };
};

export const setPersnalInfoModal = (value: boolean): types.ReducerTypes => {
  return { type: SET_PERSNAL_INFO_MODAL, payload: value };
};

export const setSendInquirySuccessModal = (
  value: boolean
): types.ReducerTypes => {
  return { type: SET_SEND_INQUIRY_SUCCESS_MODAL, payload: value };
};

export const setSendInquiryErrorModal = (
  value: boolean
): types.ReducerTypes => {
  return { type: SET_SEND_INQUIRY_ERROR_MODAL, payload: value };
};

export const setPasswordUpdateModal = (value: boolean): types.ReducerTypes => {
  return { type: SET_PASSWORD_UPDATE_MODAL, payload: value };
};

export const setPasswordUpdateSuccessModal = (
  value: boolean
): types.ReducerTypes => {
  return { type: SET_PASSWORD_UPDATE_SUCCESS_MODAL, payload: value };
};

const initialState: types.ModalTypes = {
  authentication: {
    isOpenEmailCheck: false,
    isOpenSignUpSuccess: false,
    isOpenSearchPasswordSuccess: false,
    isSearchPasswordPopup: false,
    isSendTemporaryPasswordPopup: false,
    isErrorEmailInputPopup: false,
  },
  terms: {
    isTermsPopup: false,
    isPersnalInfoPopup: false,
  },
  community: {
    isSendInquirySuccessPopup: false,
    isSendInquiryErrorPopup: false,
  },
  myPage: {
    isPasswordUpdatePopup: false,
    isPasswordUpdateSuccessPopup: false,
  },
};

export default function modalManages(
  state = initialState,
  action: types.ModalAction
): types.ModalTypes {
  switch (action.type) {
    case SET_MODAL_EMAIL_CHECK:
      return {
        ...state,
        authentication: { isOpenEmailCheck: action.payload },
      };

    case SET_SIGNUP_SUCCESS_MODAL:
      return {
        ...state,
        authentication: { isOpenSignUpSuccess: action.payload },
      };

    case SET_SEARCH_PASSWORD_SUCEESS_MODAL:
      return {
        ...state,
        authentication: { isOpenSearchPasswordSuccess: action.payload },
      };

    case SET_SEARCH_PASSWORD_POPUP_MODAL:
      return {
        ...state,
        authentication: { isSearchPasswordPopup: action.payload },
      };

    case SET_SEND_TEMPORARY_PASSWORD_MODAL:
      return {
        ...state,
        authentication: { isSendTemporaryPasswordPopup: action.payload },
      };

    case SET_ERROR_EMAIL_INPUT_MODAL:
      return {
        ...state,
        authentication: { isErrorEmailInputPopup: action.payload },
      };

    case SET_TERMS_MODAL:
      return {
        ...state,
        terms: { isTermsPopup: action.payload },
      };

    case SET_PERSNAL_INFO_MODAL:
      return {
        ...state,
        terms: { isPersnalInfoPopup: action.payload },
      };

    case SET_SEND_INQUIRY_SUCCESS_MODAL:
      return {
        ...state,
        community: { isSendInquirySuccessPopup: action.payload },
      };

    case SET_SEND_INQUIRY_ERROR_MODAL:
      return {
        ...state,
        community: { isSendInquiryErrorPopup: action.payload },
      };

    case SET_PASSWORD_UPDATE_MODAL:
      return {
        ...state,
        myPage: { isPasswordUpdatePopup: action.payload },
      };

    case SET_PASSWORD_UPDATE_SUCCESS_MODAL:
      return {
        ...state,
        myPage: { isPasswordUpdateSuccessPopup: action.payload },
      };

    default:
      return state;
  }
}
