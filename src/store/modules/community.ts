// import { createAction, handleActions } from 'redux-actions';
import {
  createPromiseThunk,
  reducerUtils,
  handleAsyncActions,
} from 'utils/asyncUtils';
import * as api from 'store/api/community';
import * as types from 'types/types';

const GET_NOTICES = 'community/GET_NOTICES';
const GET_NOTICES_SUCCESS = 'community/GET_NOTICES_SUCCESS';
const GET_NOTICES_ERROR = 'community/GET_NOTICES_ERROR';

const GET_LECTURE_MATERIALS = 'community/GET_LECTURE_MATERIALS';
const GET_LECTURE_MATERIALS_SUCCESS = 'community/GET_LECTURE_MATERIALS_SUCCESS';
const GET_LECTURE_MATERIALS_ERROR = 'community/GET_LECTURE_MATERIALS_ERROR';

// const CLEAR_NOTICES = "community/CLEAR_NOTICES";
// const CLEAR_LECTURE_MATERIALS = "community/CLEAR_LECTURE_MATERIALS";

const SET_AUTH_LETCURE_POST = 'community/SET_AUTH_LETCURE_POST';
const SET_LETCURE_POST_PASSWORD = 'community/SET_LETCURE_POST_PASSWORD';

// action creator(action)
export const getNotices = createPromiseThunk(GET_NOTICES, api.getNotices);
export const getLectureMaterials = createPromiseThunk(
  GET_LECTURE_MATERIALS,
  api.getLectureMaterials
);
export const setAuthLecturePost = (value: boolean) => {
  return { type: SET_AUTH_LETCURE_POST, payload: value };
};
export const setLecturePostPassword = (value: number | undefined) => {
  return { type: SET_LETCURE_POST_PASSWORD, payload: value };
};

const initialState: types.Community = {
  notices: reducerUtils.initial(),
  lectureMaterials: reducerUtils.initial(),
  isPasswordCorrect: false,
  selectedPassword: '',
};

const notices = (
  state = initialState,
  action: types.CommunityAction
): types.Community => {
  switch (action.type) {
    case GET_NOTICES:
    case GET_NOTICES_SUCCESS:
    case GET_NOTICES_ERROR:
      return handleAsyncActions(GET_NOTICES, 'notices', true)(state, action);
    case GET_LECTURE_MATERIALS:
    case GET_LECTURE_MATERIALS_SUCCESS:
    case GET_LECTURE_MATERIALS_ERROR:
      return handleAsyncActions(
        GET_LECTURE_MATERIALS,
        'lectureMaterials',
        true
      )(state, action);
    case SET_AUTH_LETCURE_POST:
      return {
        ...state,
        isPasswordCorrect: action.payload,
      };
    case SET_LETCURE_POST_PASSWORD:
      return {
        ...state,
        selectedPassword: action.payload,
      };
    default:
      return state;
  }
};

export default notices;
