import { applyMiddleware, createStore } from 'redux';
import ReduxThunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly'; // 리덕스 개발자 도구
import reducers from './modules/index';
// import logger from "redux-logger";
// import loggerMiddleware from 'utils/loggerMiddleware';

// const logger = createLogger();

// import { routerMiddleware } from "react-router-redux";

// const createHistory = require("history").createBrowserHistory;

// const history = createHistory({
// 	basename: process.env.PUBLIC_URL,
// });

// const routeMiddleware = routerMiddleware(history);
const composeEnhancers = composeWithDevTools({});

// const middlewares = [ReduxThunk, loggerMiddleware];
const middlewares = [ReduxThunk];

// const configure = (preloadedState) =>
//   createStore(reducers, composeEnhancers(applyMiddleware(...middlewares)));
const configure = () =>
  createStore(reducers, composeEnhancers(applyMiddleware(...middlewares)));

export default configure;
