// import axios from 'axios';
import { API_HOST as host } from 'utils/Constant';
// import { getStorageItem } from 'utils/useLocalStorage';
import { axiosInstance } from 'api';

export const getNotices = () => axiosInstance.get(`${host}/community/notices/`);
export const getLectureMaterials = () =>
  axiosInstance.get(`${host}/community/lecture_materials/`);
