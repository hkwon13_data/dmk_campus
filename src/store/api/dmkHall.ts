/* eslint-disable @typescript-eslint/no-unused-vars */
// import axios from 'axios';
import { API_HOST as host } from 'utils/Constant';
import { axiosInstance } from 'api';

// eslint-disable-next-line import/prefer-default-export
export const getHolidays = (param: any) =>
  axiosInstance.get(`${host}/dmk_hall/holidays/`);

export const getCalendarEvents = (param: any) =>
  axiosInstance.get(`${host}/calendar/list/`);
