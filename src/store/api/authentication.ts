import axios from 'axios';
import { API_HOST as host } from 'utils/Constant';

export const getLogin = (data: any) =>
  axios.post(`${host}/accounts/token/`, data);
export const postSignup = (data: any) =>
  axios.post(`${host}/accounts/signup/`, data);
