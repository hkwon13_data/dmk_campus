import React from 'react';
import { useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
// import PropTypes from 'prop-types';
// import { notification } from "antd";

// JavaScript plugin that hides or shows a component based on your scroll
import Headroom from 'headroom.js';
// import { axiosInstance } from 'api';
import { RootState } from 'store/modules/index';

// reactstrap components
import {
  Collapse,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  DropdownToggle,
  // Media,
  NavbarBrand,
  Navbar,
  NavItem,
  NavLink,
  Nav,
  Container,
  Row,
  // Col,
} from 'reactstrap';

import imgA from 'assets/img/DataMarketingCampus_Logo_white.png';
// import ImgB from 'assets/img/brand/blue.png';
import imgC from 'assets/img/DataMarketingCampus_Logo.png';
import * as actions from 'store/modules/authentication';
// import { getStorageItem } from 'utils/useLocalStorage';

interface CommonNavbarProps {
  type?: 'dark' | 'transparent' | 'white' | 'primary';
  img?: string;
}

const CommonNavbar: React.FC<CommonNavbarProps> = (props) => {
  const { type, img } = props;
  const history = useHistory();
  const {
    userInfo: { isAdmin },
    isLoggedIn,
  } = useSelector((state: RootState) => state.authentication);
  const dispatch = useDispatch();
  // const access = getStorageItem('jwtToken');
  // const refresh = getStorageItem('refresh');

  const initLoginInfo = async () => {
    try {
      // const response = await axiosInstance.post('accounts/logout/', {
      //   refresh,
      // });
      dispatch(actions.deleteToken());
      dispatch(actions.deleteAuthInfo());
      dispatch(actions.setLoggedIn(false));
      history.push('/home');
    } catch (error) {
      if (error.response) {
        console.log('error.response: ', error);
      }
    }
  };

  const handleClick = (path: string) => {
    if (path === '/logout') {
      initLoginInfo();
    } else {
      history.push(path);
    }
  };
  const [scrollY, setScrollY] = React.useState(0);
  const [collapseOpen, toggleCollapse] = React.useState(false);
  React.useEffect(() => {
    const headroom = new Headroom(
      document.getElementById('navbar-main') as HTMLElement | Node
    );
    // initialise
    headroom.init();
  });

  let cnt = 0;

  const handleLogoChange = () => {
    if (document.documentElement.scrollTop === 0) {
      cnt = 0;
      setScrollY(0);
    } else if (document.documentElement.scrollTop !== 0) {
      if (cnt > 0) {
        return;
      }
      setScrollY(document.documentElement.scrollTop);
      cnt += 1;
    }
  };

  React.useEffect(() => {
    window.addEventListener('scroll', handleLogoChange, true);
    return () => {
      window.removeEventListener('scroll', handleLogoChange, true);
    };
  }, []);

  let navbarType = '';
  switch (type) {
    case 'dark':
      navbarType = 'bg-default navbar-dark';
      break;
    case 'transparent':
      navbarType = 'navbar-transparent';
      break;
    case 'primary':
      navbarType = 'bg-primary navbar-dark';
      break;
    case 'white':
      navbarType = 'bg-white';
      break;
    default:
      navbarType = 'bg-default navbar-dark';
      break;
  }

  return (
    <>
      <Navbar
        className={`navbar-main headroom ${navbarType}`}
        expand='lg'
        id='navbar-main'
      >
        <Container className='container-xl'>
          <NavbarBrand
            onClick={() => handleClick('/')}
            style={{ height: '60px', width: '180px' }}
          >
            <img
              alt='..'
              src={img === 'black' && scrollY === 0 ? imgC : imgA}
            />
          </NavbarBrand>
          <button
            className='navbar-toggler'
            data-toggle='collapse'
            type='button'
            onClick={() => toggleCollapse(!collapseOpen)}
          >
            <span className='navbar-toggler-icon' />
          </button>
          <Collapse
            className='ml-8 pl-5'
            id='navbar_global'
            navbar
            toggler='#navbar_global'
            isOpen={collapseOpen}
          >
            <div className='navbar-collapse-header '>
              <Row>
                {/* <Col className="collapse-brand" xs="6">
									<Link to="/home">
										<img alt="..." src={ImgB}></img>
									</Link>
								</Col> */}
                {/* <Col className="collapse-close" xs="6">
									<button
										className="navbar-toggler"
										onClick={() => toggleCollapse(!collapseOpen)}></button>
								</Col> */}
              </Row>
            </div>
            <Nav className='navbar-nav-hover align-items-lg-center' navbar>
              <UncontrolledDropdown nav>
                <DropdownToggle
                  data-toggle='dropdown'
                  role='button'
                  tag={NavLink}
                >
                  <i className='ni ni-ui-04 d-lg-none' />
                  <span className='nav-link-inner--text'>캠퍼스 소개</span>
                </DropdownToggle>
                <DropdownMenu className='dropdown-menu-md '>
                  <div className='dropdown-menu-footer'>
                    <DropdownItem onClick={() => handleClick('/intro')}>
                      데이터마케팅캠퍼스
                    </DropdownItem>
                    {/* <DropdownItem
											onClick={() => handleClick("/intro/dmcpeople")}>
											전임 교수진
										</DropdownItem> */}
                  </div>
                </DropdownMenu>
              </UncontrolledDropdown>

              <UncontrolledDropdown nav>
                <DropdownToggle
                  data-toggle='dropdown'
                  role='button'
                  tag={NavLink}
                >
                  <i className='ni ni-ui-04 d-lg-none' />
                  <span className='nav-link-inner--text'>기업 교육</span>
                </DropdownToggle>
                <DropdownMenu className='dropdown-menu-md'>
                  <div className='dropdown-menu-footer'>
                    <DropdownItem onClick={() => handleClick('/customEdu')}>
                      맞춤형 기업 교육
                    </DropdownItem>
                    <DropdownItem
                      onClick={() => handleClick('/customEdu/spclct')}
                    >
                      {/* <i className="ni ni-archive-2"></i> 기업 특강 */}
                      특강 및 세미나
                    </DropdownItem>
                  </div>
                </DropdownMenu>
              </UncontrolledDropdown>

              <UncontrolledDropdown nav>
                <DropdownToggle
                  data-toggle='dropdown'
                  role='button'
                  tag={NavLink}
                >
                  <i className='ni ni-ui-04 d-lg-none' />
                  <span className='nav-link-inner--text'>오픈 과정</span>
                </DropdownToggle>
                <DropdownMenu className='dropdown-menu-md'>
                  <div className='dropdown-menu-footer'>
                    <DropdownItem onClick={() => handleClick('/openProcess')}>
                      {/* <i className="ni ni-atom"></i> DM9 */}
                      DM9 정규 과정
                    </DropdownItem>
                    <DropdownItem
                      onClick={() => handleClick('/openProcess/mmc')}
                    >
                      {/* <i className="ni ni-ui-04"></i> 마대리멤버십클럽 */}
                      마대리멤버십클럽
                    </DropdownItem>
                    {/* <DropdownItem
                      onClick={() => handleClick("/openProcess/summit")}
                    >
                      <i className="ni ni-ui-04"></i> 서밋
                      데이터마케팅 서밋
                    </DropdownItem> */}
                  </div>
                </DropdownMenu>
              </UncontrolledDropdown>
              {/* <UncontrolledDropdown nav>
								<DropdownToggle
									data-toggle="dropdown"
									role="button"
									tag={NavLink}>
									<i className="ni ni-ui-04 d-lg-none"></i>
									<span className="nav-link-inner--text">온라인 강의</span>
								</DropdownToggle>
								<DropdownMenu className="dropdown-menu-md">
									<div className="dropdown-menu-footer">
										<DropdownItem onClick={() => handleClick("/onlineEdu")}>
											<i className="ni ni-atom"></i> DM9 Learning Contents
											DM9 온라인 강의
										</DropdownItem>
										<DropdownItem onClick={() => handleClick("/onlineEdu/mmc")}>
											<i className="ni ni-ui-04"></i> 마대리멤버십클럽
											마대리멤버십클럽
										</DropdownItem>
									</div>
								</DropdownMenu>
							</UncontrolledDropdown> */}
              <UncontrolledDropdown nav>
                <DropdownToggle
                  data-toggle='dropdown'
                  role='button'
                  tag={NavLink}
                >
                  <i className='ni ni-ui-04 d-lg-none' />
                  <span className='nav-link-inner--text'>데마코홀 대관</span>
                </DropdownToggle>
                <DropdownMenu className='dropdown-menu-md'>
                  <div className='dropdown-menu-footer'>
                    <DropdownItem onClick={() => handleClick('/dmkHall')}>
                      데마코홀 소개
                    </DropdownItem>
                    <DropdownItem
                      onClick={() => handleClick('/dmkHall/reservation')}
                    >
                      {/* <i className="ni ni-ui-04"></i> 대관 신청 */}
                      대관 예약
                    </DropdownItem>
                  </div>
                </DropdownMenu>
              </UncontrolledDropdown>
              <UncontrolledDropdown nav>
                <DropdownToggle
                  data-toggle='dropdown'
                  role='button'
                  tag={NavLink}
                >
                  <i className='ni ni-ui-04 d-lg-none' />
                  <span className='nav-link-inner--text'>커뮤니티</span>
                </DropdownToggle>
                <DropdownMenu className='dropdown-menu-md'>
                  <div className='dropdown-menu-footer'>
                    <DropdownItem onClick={() => handleClick('/community')}>
                      {/* <i className="ni ni-atom"></i> 공지사항 */}
                      공지사항
                    </DropdownItem>
                    <DropdownItem
                      onClick={() => handleClick('/community/materials')}
                    >
                      {/* <i className="ni ni-ui-04"></i> 강의자료 */}
                      강의자료
                    </DropdownItem>
                    <DropdownItem
                      onClick={() => handleClick('/community/inquiry')}
                    >
                      {/* <i className="ni ni-ui-04"></i> 1:1 문의 */}
                      1:1 문의
                    </DropdownItem>
                  </div>
                </DropdownMenu>
              </UncontrolledDropdown>

              {isLoggedIn && !isAdmin && (
                <>
                  <UncontrolledDropdown
                    nav
                    className='position-lg-absolute float-right right-lg-7 mr-lg-5'
                  >
                    <DropdownToggle
                      data-toggle='dropdown'
                      role='button'
                      tag={NavLink}
                    >
                      <i className='ni ni-ui-04 d-lg-none' />
                      <span className=''>마이페이지</span>
                    </DropdownToggle>
                    <DropdownMenu className='dropdown-menu-md'>
                      <div className='dropdown-menu-footer'>
                        {/* <DropdownItem onClick={() => handleClick("/mypage")}>
													결제이력
												</DropdownItem> */}
                        {/* <DropdownItem
													onClick={() => handleClick("/mypage/dm9Result")}>
													DM9 진단 결과 확인
												</DropdownItem> */}
                        <DropdownItem
                          onClick={() => {
                            handleClick('/mypage/userInfo');
                          }}
                        >
                          회원정보 수정
                        </DropdownItem>
                      </div>
                    </DropdownMenu>
                  </UncontrolledDropdown>
                  <span
                    className='position-lg-absolute float-right right-lg-2 mr-lg-8'
                    style={{ color: 'white' }}
                  >
                    |
                  </span>
                  <NavItem className='position-lg-absolute float-right right-lg-0 mr-lg-5'>
                    <NavLink onClick={() => handleClick('/logout')}>
                      로그아웃
                    </NavLink>
                  </NavItem>
                </>
              )}
              {isLoggedIn && isAdmin && (
                <>
                  <UncontrolledDropdown
                    nav
                    className='position-lg-absolute float-right right-lg-9 mr-lg-7'
                  >
                    <DropdownToggle
                      data-toggle='dropdown'
                      role='button'
                      tag={NavLink}
                    >
                      <i className='ni ni-ui-04 d-lg-none' />
                      <span className=''>마이페이지</span>
                    </DropdownToggle>
                    <DropdownMenu className='dropdown-menu-md'>
                      <div className='dropdown-menu-footer'>
                        {/* <DropdownItem onClick={() => handleClick("/mypage")}>
													결제이력
												</DropdownItem> */}
                        {/* <DropdownItem
													onClick={() => handleClick("/mypage/dm9Result")}>
													DM9 진단 결과 확인
												</DropdownItem> */}
                        <DropdownItem
                          onClick={() => {
                            handleClick('/mypage/userInfo');
                          }}
                        >
                          회원정보 수정
                        </DropdownItem>
                      </div>
                    </DropdownMenu>
                  </UncontrolledDropdown>
                  <span
                    className='position-lg-absolute float-right right-lg-2 mr-lg-8'
                    style={{ color: 'white' }}
                  >
                    |
                  </span>
                  <NavItem className='position-lg-absolute float-right right-lg-7 mr-lg-5'>
                    <NavLink onClick={() => handleClick('/admin/dashboard')}>
                      Dashboard
                    </NavLink>
                  </NavItem>
                  <span
                    className='position-lg-absolute float-right right-lg-2 mr-lg-8'
                    style={{ color: 'white' }}
                  >
                    |
                  </span>
                  <NavItem className='position-lg-absolute float-right right-lg-0 mr-lg-5'>
                    <NavLink onClick={() => handleClick('/logout')}>
                      로그아웃
                    </NavLink>
                  </NavItem>
                </>
              )}

              {!isLoggedIn && (
                <>
                  <NavItem className='position-lg-absolute float-right right-lg-7 mr-lg-5'>
                    <NavLink onClick={() => handleClick('/auth/login')}>
                      로그인
                    </NavLink>
                  </NavItem>
                  <span
                    className='position-lg-absolute float-right right-lg-2 mr-lg-8'
                    style={{ color: 'white' }}
                  >
                    |
                  </span>
                  <NavItem className='position-lg-absolute float-right right-lg-0 mr-lg-5'>
                    <NavLink onClick={() => handleClick('/auth/signup')}>
                      회원가입
                    </NavLink>
                  </NavItem>
                </>
              )}
            </Nav>
          </Collapse>
        </Container>
      </Navbar>
    </>
  );
};

CommonNavbar.defaultProps = {
  type: 'primary',
};

export default CommonNavbar;
