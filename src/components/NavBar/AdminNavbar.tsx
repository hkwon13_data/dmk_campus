/* eslint-disable @typescript-eslint/no-unused-vars */
/*!

=========================================================
* Argon Dashboard PRO React - v1.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-pro-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from 'react';
// nodejs library that concatenates classes
import classnames from 'classnames';
// nodejs library to set properties for components
import PropTypes from 'prop-types';
// reactstrap components
import {
  Collapse,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  DropdownToggle,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  ListGroupItem,
  ListGroup,
  Media,
  Navbar,
  NavItem,
  NavLink,
  Nav,
  Container,
  Row,
  Col,
} from 'reactstrap';

// import imgA from 'assets/img/faces/team-1.jpg';
// import imgB from 'assets/img/faces/team-2.jpg';
// import imgC from 'assets/img/faces/team-3.jpg';
// import imgD from 'assets/img/faces/team-4.jpg';
// import imgE from 'assets/img/faces/team-5.jpg';

// interface AdminNavbarPropsTypes: {
// 	toggleSidenav: ()=>void;
// 	sidenavOpen: boolean;
// 	brandText: () => string;
// }

// function AdminNavbar({ theme, sidenavOpen, toggleSidenav }) {
function AdminNavbar({ theme }: any) {
  // function that on mobile devices makes the search open
  const openSearch = () => {
    document.body.classList.add('g-navbar-search-showing');
    setTimeout(function () {
      document.body.classList.remove('g-navbar-search-showing');
      document.body.classList.add('g-navbar-search-show');
    }, 150);
    setTimeout(function () {
      document.body.classList.add('g-navbar-search-shown');
    }, 300);
  };
  // function that on mobile devices makes the search close
  const closeSearch = () => {
    document.body.classList.remove('g-navbar-search-shown');
    setTimeout(function () {
      document.body.classList.remove('g-navbar-search-show');
      document.body.classList.add('g-navbar-search-hiding');
    }, 150);
    setTimeout(function () {
      document.body.classList.remove('g-navbar-search-hiding');
      document.body.classList.add('g-navbar-search-hidden');
    }, 300);
    setTimeout(function () {
      document.body.classList.remove('g-navbar-search-hidden');
    }, 500);
  };

  return (
    <>
      <Navbar
        className={classnames(
          'navbar-top navbar-expand border-bottom',
          { 'navbar-light bg-secondary': theme === 'light' },
          { 'navbar-dark bg-info': theme === 'dark' }
        )}
      >
        <Container fluid>
          <Collapse navbar isOpen>
            <Form
              className={classnames(
                'navbar-search form-inline mr-sm-3',
                { 'navbar-search-light': theme === 'dark' },
                { 'navbar-search-dark': theme === 'light' }
              )}
            >
              <FormGroup className='mb-0'>
                <InputGroup className='input-group-alternative input-group-merge'>
                  <InputGroupAddon addonType='prepend'>
                    <InputGroupText>
                      <i className='fas fa-search' />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input placeholder='Search' type='text' />
                </InputGroup>
              </FormGroup>
            </Form>
          </Collapse>
        </Container>
      </Navbar>
    </>
  );
}

// AdminNavbar.defaultProps = {
//   toggleSidenav: () => {},
//   sidenavOpen: false,
//   theme: 'dark',
// };
// AdminNavbar.propTypes = {
//   toggleSidenav: PropTypes.func,
//   sidenavOpen: PropTypes.bool,
//   theme: PropTypes.oneOf(['dark', 'light']),
// };

export default AdminNavbar;
