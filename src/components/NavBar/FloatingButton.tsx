import React from 'react';
import { Button } from 'reactstrap';
import { useHistory } from 'react-router-dom';

import styled, { DefaultTheme } from 'styled-components';

const IconDiv = styled.div<DefaultTheme>`
  display: inline-block;
  position: relative;
  top: 2px;
`;

export default function FloatingButton() {
  const history = useHistory();
  const handleClick = (path: string) => {
    history.push(path);
  };

  const handleScrollBottom = () => {
    const clientHeight = document.body.scrollHeight;
    window.scrollTo(clientHeight, clientHeight);
  };

  return (
    <>
      <div
        style={{ right: '180px', bottom: '100px', zIndex: 80000 }}
        className='fixed-plugin'
      >
        <ul className='fixed-floating-menu'>
          <li style={{ width: '70%' }} className='button-container mb-3'>
            <Button
              style={{ backgroundColor: '#CED3D8', border: 'none' }}
              // color="primary"
              block
              className='btn-round py-3'
              onClick={() => handleClick('/community/inquiry')}
            >
              <IconDiv>
                <i className='ni ni-chat-round' />
              </IconDiv>{' '}
              교육 문의하기
            </Button>
          </li>
          <li style={{ width: '70%' }} className='button-container mb-3'>
            <a
              href='http://dm9test.marketingcampus.co.kr/'
              target='_blank'
              rel='noreferrer'
            >
              <Button
                style={{ backgroundColor: '#EBEDEF' }}
                // color="info"
                block
                className='btn-round py-3'
              >
                <IconDiv>
                  <i className='ni ni-zoom-split-in' />
                </IconDiv>{' '}
                DM9 역량 진단하기
              </Button>
            </a>
          </li>
          <li style={{ width: '70%' }} className='button-container mb-3'>
            <Button
              style={{ backgroundColor: '#F8F9FA' }}
              // color="light"
              block
              className='btn-round py-3'
              onClick={() => handleScrollBottom()}
            >
              <IconDiv>
                <i className='ni ni-archive-2' />
              </IconDiv>{' '}
              뉴스레터 구독하기
            </Button>
          </li>
        </ul>
      </div>
    </>
  );
}
