/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useState, useEffect } from 'react';
import validator from 'validator';
// import classnames from 'classnames';
// reactstrap components
import {
  Button,
  FormGroup,
  Form,
  Input,
  NavLink,
  Container,
  Row,
  Col,
} from 'reactstrap';

import { useSelector, useDispatch } from 'react-redux';

import * as actions from 'store/modules/modalManages';

// Core Components
// import imgA from "assets/img/brand/blue.png";
import imgA from 'assets/img/DataMarketingCampus_Logo_white.png';
import { RootState } from 'store/modules/index';
import PersnalInfoModal from '../FooterPopup/PersnalInfoModal';
import TermsModal from '../FooterPopup/TermsModal';

function MainFooter() {
  const dispatch = useDispatch();

  const [subscribe, setSubscribe] = React.useState('');

  const [emailValid, setEmailValid] = useState('');
  const [emailState, setEmailState] = useState<string | null>(null);
  const [focusedEmail, setFocusedEmail] = useState(false);

  const checkValidate = (e: React.ChangeEvent<HTMLInputElement>) => {
    const success = 'has-success';
    const danger = 'has-danger';
    switch (e.target.name) {
      case 'email':
        if (!validator.isEmail(e.target.value) || e.target.value === '') {
          setEmailValid(danger);
          setEmailState('invalid');
        } else {
          setEmailValid(success);
          setEmailState('valid');
        }
        break;

      default:
        break;
    }
  };

  const handleInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSubscribe(e.target.value);
    checkValidate(e);
  };

  const submitSubscribe = () => {
    console.log('subscribe: ', subscribe);
  };

  // 리덕스
  const { isTermsPopup } = useSelector(
    (state: RootState) => state.modalManages.terms
  );

  const { isPersnalInfoPopup } = useSelector(
    (state: RootState) => state.modalManages.terms
  );

  const handleTermsPopup = () => {
    dispatch(actions.setTermsModal(!isTermsPopup));
  };

  const handlePersnalInfoPopup = () => {
    dispatch(actions.setPersnalInfoModal(!isPersnalInfoPopup));
  };
  // 리덕스

  return (
    <>
      <footer className='footer footer-big bg-gradient-default'>
        <Container style={{ maxWidth: '1520px' }}>
          <div className='content'>
            <Row className='mb-4'>
              <div className='column mx-auto'>
                <img alt='...' className='logo logo-lg' src={imgA} />
                {/* <div style={{ fontSize: "1.3rem", color: "white" }}>
									데이터마케팅캠퍼스
								</div> */}
              </div>
            </Row>
            <Row>
              <Col md='5' xs='12' className='pl-lg-8'>
                <div className='column'>
                  <h4 className='mb-4'>COMPANY INFO</h4>
                  <ul
                    className='font-weight-700 text-white opacity-7'
                    style={{ fontSize: '0.875rem', color: 'white' }}
                  >
                    <li>
                      <span className=''>(주)데이터마케팅코리아</span>
                      <span className='mx-2'>|</span>
                      <span className='font-weight-300 '>대표이사</span>
                      <span className=''> 이진형</span>
                    </li>
                    <li>
                      <span className='font-weight-300 '>사업자등록번호 </span>
                      <span className=''> 394-88-00499</span>
                      <span className='mx-2'>|</span>
                      <span className='font-weight-300 '>통신판매업신고</span>
                      <span className=''> 제2018-서울강남-02041호</span>
                    </li>
                    <li>
                      <span className='font-weight-300 '>
                        대표메일 dataedu@datamarketing.co.kr
                      </span>
                      <span className='mx-2'>|</span>{' '}
                      <span className='font-weight-300 '>대표번호 </span>{' '}
                      <span className=''>02-6011-5407, 5418</span>
                    </li>
                    <li>
                      <span className='font-weight-300 '>주소</span>{' '}
                      <span className=''>
                        서울특별시 강남구 테헤란로 53길 16, 역삼동 예안빌딩
                      </span>
                    </li>
                    <li>
                      {' '}
                      <span className='font-weight-300 '>
                        개인정보관리책임자{' '}
                      </span>{' '}
                      <span className=''>이진형</span>
                    </li>
                    <li className=''>Copyright @ DataMKTKorea</li>
                  </ul>
                </div>
              </Col>

              <Col md='5'>
                <div className='column'>
                  <a
                    href='https://www.maderi.co.kr/maderitimes/'
                    target='_blank'
                    rel='noreferrer'
                  >
                    <h4 className='mb-4'>NEWSLETTER(Ma:deri TIMES)</h4>
                    <ul
                      className='font-weight-700 opacity-7'
                      style={{ fontSize: '0.875rem' }}
                    >
                      <li>
                        Ma:deri TIMES는 최신 마케팅 AI정보를 챙겨드리는
                        뉴스레터입니다.
                      </li>
                      <li>
                        평일 아침마다 인공지능이 픽한 가장 영향력 있는 뉴스를
                        메일로 편하게 받아보세요!
                      </li>
                    </ul>
                  </a>
                  <Form action='' className='form form-newsletter' method=''>
                    {/* <FormGroup className={classnames(emailValid)}>
											<Input
												placeholder="이메일 입력"
												type="email"
												name="email"
												value={subscribe}
												onChange={handleInput}
												onFocus={() => setFocusedEmail(true)}
												onBlur={() => setFocusedEmail(false)}
												valid={emailState === "valid"}
												invalid={emailState === "invalid"}
											/>
											<div className="invalid-feedback">
												이메일 형식을 확인해주세요
											</div>
										</FormGroup> */}
                    <a
                      href='https://www.maderi.co.kr/maderitimes/'
                      target='_blank'
                      rel='noreferrer'
                    >
                      <Button
                        className='w-50'
                        color='primary'
                        name='subscribe'
                        type='button'
                        // disabled
                        onClick={submitSubscribe}
                      >
                        구독하기
                      </Button>
                    </a>
                  </Form>
                </div>
              </Col>
              <Col md='2' xs='6'>
                <div className='column'>
                  <h4 className='mb-4'>FAMILY SITE</h4>
                  <ul
                    className='links-vertical font-weight-700 opacity-7'
                    style={{ fontSize: '0.875rem' }}
                  >
                    <li>
                      <a
                        href='http://datamarketing.co.kr/'
                        target='_blank'
                        rel='noreferrer'
                      >
                        <span style={{ color: 'white' }}>
                          데이터마케팅코리아
                        </span>
                      </a>
                    </li>
                    <li>
                      <a
                        href='https://www.maderi.co.kr/'
                        target='_blank'
                        rel='noreferrer'
                      >
                        <span style={{ color: 'white' }}>마대리(Ma:deri)</span>
                      </a>
                    </li>
                    <li>
                      <a
                        href='https://m.blog.naver.com/PostList.nhn?blogId=datamarketing'
                        target='_blank'
                        rel='noreferrer'
                      >
                        <span style={{ color: 'white' }}>
                          데이터마케팅코리아 블로그
                        </span>
                      </a>
                    </li>
                    <li>
                      <a
                        href='https://www.youtube.com/channel/UCRex91PJUeNnQoP7F77oizg'
                        target='_blank'
                        rel='noreferrer'
                      >
                        <span style={{ color: 'white' }}>
                          데이터맛집 유튜브
                        </span>
                      </a>
                    </li>
                  </ul>
                </div>
              </Col>
            </Row>
          </div>
          <hr className='bg-white opacity-4 pl-lg--8' />
          <div className='column'>
            <ul className='d-flex justify-content-center'>
              <li>
                <NavLink href='#pablo' onClick={() => handleTermsPopup()}>
                  <span className='text-white font-weight-800'>
                    서비스 이용약관
                  </span>
                </NavLink>
              </li>
              <li>
                <NavLink disabled>
                  <span className='text-white'>|</span>
                </NavLink>
              </li>
              <li>
                <NavLink href='#pablo' onClick={() => handlePersnalInfoPopup()}>
                  <span className='text-white font-weight-800'>
                    개인정보 처리방침
                  </span>
                </NavLink>
              </li>
            </ul>
          </div>
          {isTermsPopup && (
            <TermsModal handleTermsPopup={() => handleTermsPopup()} />
          )}
          {isPersnalInfoPopup && (
            <PersnalInfoModal
              handlePersnalInfoPopup={() => handlePersnalInfoPopup()}
            />
          )}
        </Container>
      </footer>
    </>
  );
}

export default MainFooter;
