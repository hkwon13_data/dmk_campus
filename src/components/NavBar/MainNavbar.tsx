// import React from "react";
// import { Link, useHistory, useLocation } from "react-router-dom";
// import PropTypes from "prop-types";
// // import { notification } from "antd";
export {};
// // JavaScript plugin that hides or shows a component based on your scroll
// import Headroom from "headroom.js";

// // reactstrap components
// import {
// 	Collapse,
// 	DropdownMenu,
// 	DropdownItem,
// 	UncontrolledDropdown,
// 	DropdownToggle,
// 	Media,
// 	NavbarBrand,
// 	Navbar,
// 	NavItem,
// 	NavLink,
// 	Nav,
// 	Container,
// 	Row,
// 	Col,
// } from "reactstrap";

// import ImgA from "assets/img/DataMarketingCampus_Logo.png";
// import ImgB from "assets/img/brand/blue.png";
// import { useAppContext, deleteToken } from "store";
// import { useSelector, useDispatch, shallowEqual } from "react-redux";
// import * as actions from "store/modules/authentication";

// function MainNavbar(props) {
// 	const dispatch = useDispatch();
// 	const location = useLocation();
// 	const history = useHistory();

// 	const { authInfo } = useSelector((state) => state.authentication);
// 	const { from: loginRedirectUrl } = location.state || {
// 		from: { pathname: "/home" },
// 	};

// 	const handleClick = (path) => {
// 		if (path === "/logout") {
// 			dispatch(actions.deleteToken());
// 			// notification.open({
// 			// 	message: "로그아웃 성공",
// 			// 	description: "Main 페이지로 이동합니다",
// 			// });
// 			authInfo.isAuthenticated = false;
// 			dispatch(actions.setAuthInfo(authInfo));
// 			history.goBack();
// 		} else {
// 			history.push(path);
// 		}
// 	};

// 	const [collapseOpen, toggleCollapse] = React.useState(false);
// 	React.useEffect(() => {
// 		let headroom = new Headroom(document.getElementById("navbar-main"));
// 		// initialise
// 		headroom.init();
// 	});
// 	let navbarType = "";
// 	switch (props.type) {
// 		case "dark":
// 			navbarType = "bg-default navbar-dark";
// 			break;
// 		case "transparent":
// 			navbarType = "navbar-transparent";
// 			break;
// 		case "primary":
// 			navbarType = "bg-primary navbar-dark";
// 			break;
// 		case "white":
// 			navbarType = "bg-white";
// 			break;
// 		default:
// 			navbarType = "bg-default navbar-dark";
// 			break;
// 	}

// 	return (
// 		<>
// 			<Navbar
// 				className={"navbar-main headroom " + navbarType}
// 				expand="lg"
// 				id="navbar-main">
// 				<Container className="container-lg text-xs1 font-weight-600">
// 					<NavbarBrand onClick={() => handleClick("/home")}>
// 						<img className="w-75" alt=".." src={ImgA} />
// 					</NavbarBrand>
// 					<button
// 						className="navbar-toggler"
// 						type="button"
// 						onClick={() => toggleCollapse(!collapseOpen)}>
// 						<span className="navbar-toggler-icon"></span>
// 					</button>
// 					<Collapse
// 						id="navbar_global"
// 						navbar
// 						toggler="#navbar_global"
// 						isOpen={collapseOpen}>
// 						<div className="navbar-collapse-header">
// 							<Row>
// 								<Col className="collapse-brand" xs="6">
// 									<Link to="/home">
// 										<img alt="..." src={ImgB}></img>
// 									</Link>
// 								</Col>
// 								<Col className="collapse-close" xs="6">
// 									<button
// 										className="navbar-toggler"
// 										onClick={() => toggleCollapse(!collapseOpen)}>
// 										<span></span>
// 										<span></span>
// 									</button>
// 								</Col>
// 							</Row>
// 						</div>
// 						<Nav
// 							className="navbar-nav-hover align-items-lg-center ml-lg-auto"
// 							navbar>
// 							<UncontrolledDropdown nav>
// 								<DropdownToggle
// 									data-toggle="dropdown"
// 									// onClick={() => handleClick("/home")}
// 									onClick
// 									role="button"
// 									tag={NavLink}>
// 									<i className="ni ni-ui-04 d-lg-none"></i>
// 									<span className="nav-link-inner--text">
// 										데이터마케팅캠퍼스 소개
// 									</span>
// 								</DropdownToggle>
// 								<DropdownMenu className="dropdown-menu-lg">
// 									<div className="dropdown-menu-footer">
// 										<DropdownItem onClick={() => handleClick("/intro")}>
// 											<i className="ni ni-atom"></i> 데이터마케팅캠퍼스 소개
// 										</DropdownItem>
// 										<DropdownItem
// 											onClick={() => handleClick("/intro/philosopy")}>
// 											<i className="ni ni-ui-04"></i> 과정개발 철학
// 										</DropdownItem>
// 										<DropdownItem
// 											onClick={() => handleClick("/intro/dmcpeople")}>
// 											<i className="ni ni-archive-2"></i> 운영진 및 강사진
// 										</DropdownItem>
// 									</div>
// 								</DropdownMenu>
// 							</UncontrolledDropdown>
// 							<NavItem>
// 								<NavLink onClick={() => handleClick("/customEdu")}>
// 									기업 맞춤 교육
// 								</NavLink>
// 							</NavItem>

// 							<UncontrolledDropdown nav>
// 								<DropdownToggle
// 									data-toggle="dropdown"
// 									onClick={() => handleClick("/home")}
// 									role="button"
// 									tag={NavLink}>
// 									<i className="ni ni-ui-04 d-lg-none"></i>
// 									<span className="nav-link-inner--text">오프라인</span>
// 								</DropdownToggle>
// 								<DropdownMenu className="dropdown-menu-md">
// 									<div className="dropdown-menu-footer">
// 										<DropdownItem onClick>
// 											<i className="ni ni-atom"></i> DM9
// 										</DropdownItem>
// 										<DropdownItem>
// 											<i className="ni ni-ui-04"></i> 서밋
// 										</DropdownItem>
// 										<DropdownItem>
// 											<i className="ni ni-archive-2"></i> 특강
// 										</DropdownItem>
// 									</div>
// 								</DropdownMenu>
// 							</UncontrolledDropdown>
// 							<UncontrolledDropdown nav>
// 								<DropdownToggle
// 									data-toggle="dropdown"
// 									onClick={() => handleClick("/home")}
// 									role="button"
// 									tag={NavLink}>
// 									<i className="ni ni-ui-04 d-lg-none"></i>
// 									<span className="nav-link-inner--text">온라인</span>
// 								</DropdownToggle>
// 								<DropdownMenu className="dropdown-menu-md">
// 									<div className="dropdown-menu-footer">
// 										<DropdownItem onClick>
// 											<i className="ni ni-atom"></i> 마멤클
// 										</DropdownItem>
// 										<DropdownItem>
// 											<i className="ni ni-ui-04"></i> DM9 온라인
// 										</DropdownItem>
// 										<DropdownItem>
// 											<i className="ni ni-archive-2"></i> 온라인 콘텐츠 구매
// 										</DropdownItem>
// 									</div>
// 								</DropdownMenu>
// 							</UncontrolledDropdown>
// 							<UncontrolledDropdown nav>
// 								<DropdownToggle
// 									data-toggle="dropdown"
// 									onClick={() => handleClick("/home")}
// 									role="button"
// 									tag={NavLink}>
// 									<i className="ni ni-ui-04 d-lg-none"></i>
// 									<span className="nav-link-inner--text">데마코홀 대관</span>
// 								</DropdownToggle>
// 								<DropdownMenu className="dropdown-menu-md">
// 									<div className="dropdown-menu-footer">
// 										<DropdownItem onClick>
// 											<i className="ni ni-atom"></i> 데마코홀 소개
// 										</DropdownItem>
// 										<DropdownItem>
// 											<i className="ni ni-ui-04"></i> 대관 안내
// 										</DropdownItem>
// 									</div>
// 								</DropdownMenu>
// 							</UncontrolledDropdown>
// 							<UncontrolledDropdown nav>
// 								<DropdownToggle
// 									data-toggle="dropdown"
// 									onClick={() => handleClick("/home")}
// 									role="button"
// 									tag={NavLink}>
// 									<i className="ni ni-ui-04 d-lg-none"></i>
// 									<span className="nav-link-inner--text">커뮤니티</span>
// 								</DropdownToggle>
// 								<DropdownMenu className="dropdown-menu-md">
// 									<div className="dropdown-menu-footer">
// 										<DropdownItem onClick>
// 											<i className="ni ni-atom"></i> 공지사항
// 										</DropdownItem>
// 										<DropdownItem>
// 											<i className="ni ni-ui-04"></i> 강의자료
// 										</DropdownItem>
// 									</div>
// 								</DropdownMenu>
// 							</UncontrolledDropdown>

// 							{authInfo.isAuthenticated && (
// 								<UncontrolledDropdown nav>
// 									<DropdownToggle
// 										data-toggle="dropdown"
// 										onClick={() => handleClick("/home")}
// 										role="button"
// 										tag={NavLink}>
// 										<i className="ni ni-ui-04 d-lg-none"></i>
// 										<span className="nav-link-inner--text">My Page</span>
// 									</DropdownToggle>
// 									<DropdownMenu className="dropdown-menu-md">
// 										<div className="dropdown-menu-footer">
// 											<DropdownItem onClick>
// 												<i className="ni ni-atom"></i> 결제이력
// 											</DropdownItem>
// 											<DropdownItem>
// 												<i className="ni ni-ui-04"></i> DM9 진단 결과 확인
// 											</DropdownItem>
// 											<DropdownItem>
// 												<i className="ni ni-ui-04"></i> 회원정보 수정
// 											</DropdownItem>
// 										</div>
// 									</DropdownMenu>
// 								</UncontrolledDropdown>
// 							)}
// 							{(authInfo.isAuthenticated && (
// 								<NavItem>
// 									<NavLink onClick={() => handleClick("/logout")}>
// 										로그아웃
// 									</NavLink>
// 								</NavItem>
// 							)) || (
// 								<NavItem>
// 									<NavLink onClick={() => handleClick("/login")}>
// 										{/* <i className='fas fa-grip-lines-vertical'></i> */}
// 										로그인
// 									</NavLink>
// 								</NavItem>
// 							)}
// 							{!authInfo.isAuthenticated && (
// 								<NavItem>
// 									<NavLink onClick={() => handleClick("/signup")}>
// 										회원가입
// 									</NavLink>
// 								</NavItem>
// 							)}
// 						</Nav>
// 					</Collapse>
// 				</Container>
// 			</Navbar>
// 		</>
// 	);
// }

// MainNavbar.defaultProps = {
// 	type: "dark",
// };

// MainNavbar.propTypes = {
// 	type: PropTypes.oneOf(["dark", "transparent", "primary", "white"]),
// };

// export default MainNavbar;
