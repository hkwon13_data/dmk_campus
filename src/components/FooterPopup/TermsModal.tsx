import React from 'react';

import { Modal } from 'reactstrap';

import { useSelector } from 'react-redux';

import { RootState } from 'store/modules/index';

import DemakoTerms from '../../variables/DemakoTerms';

function TermsModal({ handleTermsPopup }: { handleTermsPopup: () => void }) {
  //   const dispatch = useDispatch();

  const { isTermsPopup } = useSelector(
    (state: RootState) => state.modalManages.terms
  );

  return (
    <>
      {/* <Button
				block
				className="mb-3"
				color="primary"
				onClick={() => handleTermsPopup(!isTermsPopup)}
				type="button">
				Default
			</Button> */}
      <Modal
        isOpen={isTermsPopup}
        toggle={() => handleTermsPopup()}
        className='modal-dialog-centered modal-xl'
      >
        <div className='modal-header'>
          <div
            className='modal-title h5 font-weight-900 py-2'
            id='modal-title-default'
          >
            데이터마케팅캠퍼스 서비스 이용약관
          </div>
          <button
            aria-label='Close'
            className='close'
            onClick={() => handleTermsPopup()}
            type='button'
          >
            <span aria-hidden>×</span>
          </button>
        </div>
        <div className='modal-body'>
          <DemakoTerms />
        </div>
        <div className='text-center pb-4'>
          <button
            type='button'
            className='btn btn-primary'
            color='primary'
            onClick={() => handleTermsPopup()}
          >
            확인
          </button>
        </div>
      </Modal>
    </>
  );
}

export default TermsModal;
