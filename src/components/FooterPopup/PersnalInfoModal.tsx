import React from 'react';

import { Modal } from 'reactstrap';

import { useSelector } from 'react-redux';

import { RootState } from 'store/modules/index';
import PersnalInfo from '../../variables/PersnalInfo';

function PersnalInfoModal({
  handlePersnalInfoPopup,
}: {
  handlePersnalInfoPopup: () => void;
}) {
  //   const dispatch = useDispatch();

  const { isPersnalInfoPopup } = useSelector(
    (state: RootState) => state.modalManages.terms
  );

  return (
    <>
      {/* <Button
				block
				className="mb-3"
				color="primary"
				onClick={() => handlePersnalInfoPopup(!isPersnalInfoPopup)}
				type="button">
				Default
			</Button> */}
      <Modal
        isOpen={isPersnalInfoPopup}
        toggle={() => handlePersnalInfoPopup()}
        className='modal-dialog-centered modal-xl'
      >
        <div className='modal-header'>
          <h6
            className='modal-title h5 font-weight-900 py-2'
            id='modal-title-default'
          >
            데이터마케팅캠퍼스 개인정보 처리방침
          </h6>
          <button
            aria-label='Close'
            className='close'
            onClick={() => handlePersnalInfoPopup()}
            type='button'
          >
            <span aria-hidden>×</span>
          </button>
        </div>
        <div className='modal-body'>
          <PersnalInfo />
        </div>
        <div className='text-center pb-4'>
          <button
            type='button'
            className='btn btn-primary'
            color='primary'
            onClick={() => handlePersnalInfoPopup()}
          >
            확인
          </button>
        </div>
      </Modal>
    </>
  );
}

export default PersnalInfoModal;
