/* eslint-disable consistent-return */
import React from 'react';

// reactstrap components ...
import { Badge } from 'reactstrap';
import * as types from 'types/types';

interface NoticesPropsTypes {
  pagingData: {
    pagesCount: number;
    currentPage: number;
    pageSize: number;
  };
  data: types.CommunityData[] | undefined;
  setOpenNotice: React.Dispatch<React.SetStateAction<boolean>>;
  setClickedNotice: React.Dispatch<
    React.SetStateAction<{
      mapId: number;
      postId: number | undefined;
    }>
  >;
}

export default function Notices({
  pagingData,
  data,
  setOpenNotice,
  setClickedNotice,
}: NoticesPropsTypes) {
  const { pageSize, currentPage } = pagingData;

  const date = new Date();
  const today = Number(date.getDate());

  const renderBadge = (item: types.CommunityData): React.ReactNode => {
    if (today > Number(item.created_at.split('T')[0].split('-')[2])) {
      if (today - Number(item.created_at.split('T')[0].split('-')[2]) <= 6) {
        return <Badge color='info'>NEW</Badge>;
      }
    }
    if (today < Number(item.created_at.split('T')[0].split('-')[2])) {
      if (today - Number(item.created_at.split('T')[0].split('-')[2]) >= 24) {
        return <Badge color='info'>NEW</Badge>;
      }
    }
  };

  return (
    <>
      <thead className='text-primary text-center'>
        <tr className='bg-secondary' style={{ color: '#333333' }}>
          <th style={{ width: '7%' }}>번호</th>
          <th>제목</th>
          <th style={{ width: '8.33%' }}>작성일</th>
        </tr>
      </thead>
      <tbody>
        {data
          ?.slice(currentPage * pageSize, (currentPage + 1) * pageSize)
          .map((post, i) => {
            return (
              <tr
                key={post.id}
                onClick={() => {
                  setOpenNotice(true);
                  setClickedNotice({ mapId: i, postId: post.id });
                }}
              >
                <th className='text-center' scope='row'>
                  {post.id}
                </th>
                <td>
                  {post.title} &nbsp; {renderBadge(post)}
                </td>
                <td>{post.created_at.split('T')[0]}</td>
              </tr>
            );
          })}
        <tr style={{ pointerEvents: 'none' }}>
          <td colSpan={3} />
        </tr>
      </tbody>
    </>
  );
}
