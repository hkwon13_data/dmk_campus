/* eslint-disable @typescript-eslint/no-shadow */
/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, useState } from 'react';
import { RootState } from 'store/modules/index';
import { axiosInstance } from 'api';
import { useHistory } from 'react-router-dom';
// import { notification } from 'antd';
import {
  getStorageItem,
  setStorageItem,
  removeStorageItem,
} from 'utils/useLocalStorage';
import validator from 'validator';
// import axios from 'axios';
import classnames from 'classnames';
// import imgA from "assets/img/sections/unsplashs.jpg";
import imgA from 'assets/img/login/dmk_campus_ad.png';

// reactstrap components
import {
  Button,
  Card,
  CardBody,
  Container,
  Col,
  FormGroup,
  Form,
  Input,
  Row,
  InputGroup,
  CardHeader,
} from 'reactstrap';

import { useSelector, useDispatch } from 'react-redux';
import * as actions from 'store/modules/authentication';

import * as modalActions from 'store/modules/modalManages';

import SearchPasswordPopup from './SearchPasswordPopup';
import SendTemporaryPasswordPopup from './SendTemporaryPasswordPopup';
import ErrorEmailInputPopup from './ErrorEmailInputPopup';
import SendInquirtErrorModal from '../../pages/Community/SendInquiryErrorModal';

const PasswordFont = {
  fontFamily: 'sans-serif',
};

// Core Components
export default function LoginCard() {
  const dispatch = useDispatch();
  const history = useHistory();

  const isRemember = getStorageItem('remembered', '');
  const rememberedId = getStorageItem('userId', '');

  // const [username, setUsername] = useState(rememberedId);
  const [email, setEmail] = useState(rememberedId);
  const [password, setPassword] = useState('');

  const [emailFocus, setEmailFocus] = React.useState('');
  const [passwordFocus, setPasswordFocus] = React.useState('');

  const [remeberCheck, setRemeberCheck] = useState(isRemember);
  const [checkUserId, setCheckUserId] = useState(true);
  const [checkLogin, setCheckLogin] = useState(true);

  const [errorMessage, setErrorMessage] = useState('');

  const { isSearchPasswordPopup } = useSelector(
    (state: RootState) => state.modalManages.authentication
  );

  const { isSendTemporaryPasswordPopup } = useSelector(
    (state: RootState) => state.modalManages.authentication
  );

  const { isErrorEmailInputPopup } = useSelector(
    (state: RootState) => state.modalManages.authentication
  );

  const { isSendInquiryErrorPopup } = useSelector(
    (state: RootState) => state.modalManages.community
  );

  const handleSearchPasswordPopup = () => {
    dispatch(modalActions.setSearchPasswordPopupModal(!isSearchPasswordPopup));
  };

  const handleSendTemporaryPasswordModal = () => {
    dispatch(
      modalActions.setSearchPasswordPopupModal(!isSendTemporaryPasswordPopup)
    );
  };

  const handleErrorEmailInputModal = () => {
    dispatch(modalActions.setErrorEmailInputModal(!isErrorEmailInputPopup));
  };

  const handleEmailCheckModal = () => {
    dispatch(modalActions.setSendInquiryErrorModal(!isSendInquiryErrorPopup));
  };

  const handleSubmitForm = (event: React.FormEvent<HTMLButtonElement>) => {
    event.preventDefault();
    async function fn() {
      const data = { email, password };
      try {
        const response = await axiosInstance.post('/accounts/token/', data);
        const {
          data: {
            access: jwtToken,
            refresh,
            id,
            is_staff,
            is_active,
            email,
            date_joined,
            phone,
            username,
            company,
            department,
            category,
            marketing_contract_email,
            marketing_contract_sms,
            position,
            is_email_authentication,
            private_info,
            service_contract,
          },
        } = response;
        const userInfo = {
          id,
          isAdmin: is_staff,
          isAuthenticated: true,
          isActive: is_active,
          email,
          dateJoined: date_joined,
          phone,
          username,
          company,
          department,
          position,
          category,
          agreementEmail: marketing_contract_email,
          agreementSNS: marketing_contract_sms,
          privateInfo: private_info,
          serviceContract: service_contract,
          is_email_authentication,
        };

        if (!is_active || !is_email_authentication) {
          // return alert("이메일 인증을 진행해주세요.");
          setErrorMessage('이메일 인증을 진행해주세요.');
          handleEmailCheckModal();
          return;
        }
        // console.log("response: ", response);
        dispatch(actions.setToken(jwtToken));
        dispatch(actions.setRefresh(refresh));
        dispatch(actions.setLoggedIn(true));
        dispatch(actions.setUserInfo(userInfo));
        if (!isRemember) {
          removeStorageItem('userId');
          removeStorageItem('remembered');
          setRemeberCheck(false);
        } else {
          setStorageItem('userId', email);
        }
        if (is_staff) {
          history.push('/admin/dashboard');
        } else {
          //   notification.open({
          //     message: "로그인 성공",
          //     description: "메인 페이지로 이동합니다",
          //   });
          history.push('/');
        }
      } catch (error) {
        if (error.response) {
          console.log('error: ', error);
          // notification.open({
          // 	message: "로그인 실패",
          // 	description: "아이디/암호를 확인해주세요.",
          // });
          handleErrorEmailInputModal();
        }
      }
    }
    fn();
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.name === 'email') {
      setEmail(e.target.value);
    } else if (e.target.name === 'password') {
      setPassword(e.target.value);
    }
  };

  const handleRememberId = () => {
    setStorageItem('userId', email);
    setRemeberCheck((remeberCheck: boolean) => !remeberCheck);
    setStorageItem('remembered', !remeberCheck);
  };

  useEffect(() => {
    if (validator.isEmail(email)) {
      setCheckUserId(false);
    } else {
      setCheckUserId(true);
    }
    if (email !== '' && password !== '') {
      setCheckLogin(false);
    } else {
      setCheckLogin(true);
    }
  }, [email, password]);

  // const clicked = (value) => {
  //   console.log('clicked: ', value);
  // };
  return (
    <>
      <section>
        <div
          className='page-header page-header-small header-filter'
          style={{ minHeight: '28vh', maxHeight: '440px' }}
        >
          <div
            className='page-header-image'
            style={{
              // backgroundImage: "url(" + imgA + ")",
              backgroundColor: '#5d4ad2',
            }}
          />
        </div>

        <Container>
          <Col className='mx-auto' lg='5' md='8'>
            <Card
              className='bg-secondary shadow border-0 top--5 mt--6 mx-5'
              style={{ top: '-2.8rem' }}
            >
              <CardHeader>
                <div className='header-body text-center mt-2'>
                  <Row className='justify-content-center'>
                    <Col className='px-2' lg='10' md='8' xl='5'>
                      <h1>로그인</h1>
                    </Col>
                  </Row>
                </div>
              </CardHeader>
              <CardBody className='px-lg-5 py-lg-5'>
                <Form>
                  <FormGroup
                    className={classnames(passwordFocus, 'px-lg-2', 'mb-lg-3')}
                  >
                    <div className='font-weight-800'>이메일</div>

                    <InputGroup className='input-group-alternative'>
                      <Input
                        placeholder='이메일을 입력해주세요'
                        type='text'
                        name='email'
                        value={email}
                        onChange={handleChange}
                        onFocus={() => setEmailFocus('focused')}
                        onBlur={() => setEmailFocus('')}
                      />
                    </InputGroup>
                  </FormGroup>
                  <FormGroup className={classnames(passwordFocus, 'px-lg-2')}>
                    <div className='font-weight-800'>비밀번호</div>
                    <InputGroup className='input-group-alternative'>
                      <Input
                        style={PasswordFont}
                        placeholder='비밀번호를 입력해주세요'
                        type='password'
                        name='password'
                        onChange={handleChange}
                        onFocus={() => setPasswordFocus('focused')}
                        onBlur={() => setPasswordFocus('')}
                      />
                    </InputGroup>
                  </FormGroup>
                  <div className='custom-control custom-control-alternative custom-checkbox ml-lg-2'>
                    <input
                      className='custom-control-input'
                      id=' customCheckLogin2'
                      type='checkbox'
                      disabled={checkUserId}
                      checked={isRemember || remeberCheck}
                      onChange={(e) => handleRememberId()}
                    />
                    <label
                      className='custom-control-label'
                      htmlFor=' customCheckLogin2'
                    >
                      <span className='text-default opacity-5 font-weight-800'>
                        아이디 저장하기
                      </span>
                    </label>
                  </div>
                  <div className='text-center'>
                    <Button
                      className='my-4'
                      color='primary'
                      disabled={checkLogin}
                      onClick={handleSubmitForm}
                    >
                      로그인
                    </Button>
                    <div className='pl-4'>
                      <span
                        style={{ cursor: 'pointer' }}
                        onClick={() => history.push('/auth/signup')}
                        onKeyDown={() => {}}
                        role='button'
                        tabIndex={0}
                      >
                        회원가입
                      </span>
                      <span className='px-3'>|</span>
                      <span
                        style={{ cursor: 'pointer' }}
                        onClick={() => handleSearchPasswordPopup()}
                        onKeyDown={() => {}}
                        role='button'
                        tabIndex={0}
                      >
                        비밀번호 찾기
                      </span>
                    </div>
                  </div>
                </Form>
              </CardBody>
            </Card>
            <div className='mt--5 mb-5 mx-lg-5'>
              <img alt='panda' className='img-fluid shadow-lg' src={imgA} />
            </div>
          </Col>
          {isSearchPasswordPopup && (
            <SearchPasswordPopup
              handleSearchPasswordPopup={() => handleSearchPasswordPopup()}
            />
          )}
          {isSendTemporaryPasswordPopup && (
            <SendTemporaryPasswordPopup
              handleSendTemporaryPasswordModal={() =>
                handleSendTemporaryPasswordModal()
              }
            />
          )}
          {isErrorEmailInputPopup && (
            <ErrorEmailInputPopup
              handleErrorEmailInputModal={() => handleErrorEmailInputModal()}
            />
          )}
          {isSendInquiryErrorPopup && (
            <SendInquirtErrorModal
              handleInquiryErrorPopup={() => handleEmailCheckModal()}
              text={errorMessage}
            />
          )}
        </Container>
      </section>
    </>
  );
}
