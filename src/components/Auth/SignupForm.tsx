import React, { useState } from 'react';
import { axiosInstance } from 'api';
import { RootState } from 'store/modules/index';
import { notification } from 'antd';
// import { useHistory } from 'react-router-dom';

import classnames from 'classnames';
import validator from 'validator';
import { categoryList } from 'variables/updateUserInfo';

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  Collapse,
  FormGroup,
  Form,
  Label,
  Input,
  Container,
  Row,
  Col,
  Spinner,
} from 'reactstrap';

import { useSelector, useDispatch } from 'react-redux';
import * as actions from 'store/modules/modalManages';

// core components
import SignUpSuccessModal from './SignUpSuccessModal';
import Terms from '../../variables/DemakoTerms';
import UserInfoTerms from '../../variables/PersnalInfo';

const PasswordFont = {
  fontFamily: 'sans-serif',
};

export default function SignUp() {
  const dispatch = useDispatch();
  // const history = useHistory();
  const { isOpenSignUpSuccess } = useSelector(
    (state: RootState) => state.modalManages.authentication
  );

  const [emailAvailable, setEmailAvailable] = useState<string | boolean>('');
  const [values, setValues] = React.useState({
    email: '',
    password: '',
    repassword: '',
    username: '',
    phone: '',
    company: '',
    department: '',
    position: '',
    category: '',
    question: '',
  });

  const {
    email,
    password,
    repassword,
    username,
    phone,
    company,
    department,
    position,
    category,
    question,
  } = values;

  const [isLoading, setIsLoading] = useState(false);

  const [usernameValid, setUsernameValid] = useState('');
  const [phoneValid, setPhoneValid] = useState('');
  const [emailValid, setEmailValid] = useState('');
  const [companyValid, setCompanyValid] = useState('');
  const [passwordValid, setPasswordValid] = useState('');
  const [repasswordValid, setRepasswordValid] = useState('');

  const [usernameState, setUsernameState] = useState<string | null>(null);
  const [phoneState, setPhoneState] = useState<string | null>(null);
  const [emailState, setEmailState] = useState<string | null>(null);
  const [companyState, setCompanyState] = useState<string | null>(null);
  const [passwordState, setPasswordState] = useState<string | null>(null);
  const [repasswordState, setRepasswordState] = useState<string | null>(null);

  const [disabledButton, setDisabledButton] = useState(true);
  const [emailCheckedButton, setEmailCheckedButton] = useState(true);

  const [agreementService, setAgreementService] = useState<boolean>(false);
  const [agreementUserInfo, setAgreementUserInfo] = useState<boolean>(false);
  const [agreementMarketing, setAgreementMarketing] = useState<boolean>(false);
  const [focusedEmail, setFocusedEmail] = useState<string | boolean>(false);
  const [focusedUsername, setFocusedUsername] = useState<string | boolean>(
    false
  );
  const [focusedPassword, setFocusedPassword] = useState<string | boolean>(
    false
  );
  const [focusedRePassword, setFocusedRePassword] = useState<string | boolean>(
    false
  );
  const [focusedPhone, setFocusedPhone] = useState<string | boolean>(false);
  const [focusedCompany, setFocusedCompany] = useState<string | boolean>(false);
  const [focusedDepartment, setFocusedDepartment] = useState<string | boolean>(
    false
  );
  const [focusedPosition, setFocusedPosition] = useState<string | boolean>(
    false
  );
  const [focusedCategory, setFocusedCategory] = useState<string | boolean>(
    false
  );
  const [focusedQuestion, setFocusedQuestion] = useState<string | boolean>(
    false
  );
  const [openedCollapse, setOpenedCollapse] = useState('');
  const [openedCollapse2, setOpenedCollapse2] = useState('');

  const success = 'has-success';
  const danger = 'has-danger';

  const handleEmailCheck = () => {
    async function fn() {
      const data = {
        email,
      };
      try {
        const response = await axiosInstance.post(
          '/accounts/check/email/',
          data
        );
        if (response.data) {
          setEmailValid(danger);
          setEmailState('invalid');
        }
        setEmailAvailable(response.data);
      } catch (error) {
        if (error.response) {
          console.log('error: ', error.response);
        }
      }
    }
    if (email === '') {
      notification.open({
        message: '이메일 확인 실패',
        description: '아이디를 입력해주세요.',
      });
    } else if (emailState === 'invalid') {
      notification.open({
        message: '이메일 확인 실패',
        description: '아이디 형식을 다시 확인해주세요.',
      });
    } else {
      fn();
    }
  };

  const handleSignUpSuccessModal = () => {
    dispatch(actions.setSignUpSuccessModal(!isOpenSignUpSuccess));
  };

  const checkValidate = (e: React.ChangeEvent<HTMLInputElement>) => {
    switch (e.target.name) {
      case 'username':
        if (
          validator.matches(e.target.value, '[ !@#$%^&*(),.?":{}|<>]') ||
          e.target.value === ''
        ) {
          setUsernameValid(danger);
          setUsernameState('invalid');
          setDisabledButton(true);
        } else {
          setUsernameValid(success);
          setUsernameState('valid');
        }

        break;
      case 'phone':
        if (
          !validator.isMobilePhone(e.target.value, 'ko-KR') ||
          e.target.value === ''
        ) {
          setPhoneValid(danger);
          setPhoneState('invalid');
          setDisabledButton(true);
        } else {
          setPhoneValid(success);
          setPhoneState('valid');
        }

        break;

      case 'email':
        if (
          !validator.isEmail(e.target.value) ||
          emailAvailable === true ||
          e.target.value === ''
        ) {
          setEmailValid(danger);
          setEmailState('invalid');
          setDisabledButton(true);
          setEmailCheckedButton(true);
        } else {
          setEmailValid(success);
          setEmailState('valid');
          setEmailCheckedButton(false);
        }
        break;

      case 'password':
        if (
          !validator.isStrongPassword(e.target.value, {
            minLength: 8,
            minLowercase: 1,
            minUppercase: 0,
            minNumbers: 1,
            minSymbols: 1,
            returnScore: false,
          }) ||
          e.target.value === ''
        ) {
          setPasswordValid(danger);
          setPasswordState('invalid');
          setDisabledButton(true);
        } else {
          setPasswordValid(success);
          setPasswordState('valid');
        }
        break;

      case 'repassword':
        if (password !== e.target.value || e.target.value === '') {
          setRepasswordValid(danger);
          setRepasswordState('invalid');
          setDisabledButton(true);
        } else {
          setRepasswordValid(success);
          setRepasswordState('valid');
        }
        break;

      case 'company':
        if (e.target.value === '') {
          setCompanyValid(danger);
          setCompanyState('invalid');
          setDisabledButton(true);
        } else {
          setCompanyValid(success);
          setCompanyState('valid');
        }

        break;

      default:
        break;
    }
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    if (name === 'email') {
      setEmailAvailable('');
    } //  email이 바뀔때마다 이메일 체크 벨류를 빈 문자열로 바꿔 줌으로서 계속 사용중인 이메일 이라는 문구가 출력 되는 현상을 방지.
    const nextInputValue = {
      ...values,
      [name]: value,
    };
    setValues(nextInputValue);
    if (
      name === email ||
      password ||
      repassword ||
      username ||
      phone ||
      company
    ) {
      if (
        email !== '' &&
        emailState === 'valid' &&
        password !== '' &&
        passwordState === 'valid' &&
        repassword !== '' &&
        repasswordState === 'valid' &&
        username !== '' &&
        usernameState === 'valid' &&
        phone !== '' &&
        phoneState === 'valid' &&
        company !== '' &&
        companyState === 'valid' &&
        emailAvailable === false &&
        agreementService === true &&
        agreementUserInfo === true
      ) {
        setDisabledButton(false);
      } else {
        setDisabledButton(true);
      }
    }
    checkValidate(e);
  };

  const handleSubmitForm = (event: React.FormEvent<HTMLButtonElement>) => {
    event.preventDefault();
    async function fn() {
      const data = {
        email,
        password,
        username,
        phone,
        company,
        department,
        position,
        category,
        // question: question,
        agreementService,
        agreementUserInfo,
        agreementMarketing,
      };

      try {
        if (password !== repassword) {
          notification.open({
            message: '회원가입 실패',
            description: '비밀번호가 맞지 않습니다.',
          });
          return;
        }
        if (
          email === '' ||
          password === '' ||
          repassword === '' ||
          username === '' ||
          phone === '' ||
          company === ''
        ) {
          notification.open({
            message: '회원가입 실패',
            description: '필수입력 항목을 확인해주세요.',
          });
          return;
        }
        setIsLoading(true);
        await axiosInstance.post('/accounts/signup/', data);
        //   notification.open({
        //     message: "회원가입 성공",
        //     description: "이메일 인증을 진행해주세요.",
        //   });
        // history.push("/auth/login");
        handleSignUpSuccessModal();
      } catch (error) {
        if (error.response) {
          notification.open({
            message: '회원가입 실패',
            description: '아이디/암호를 확인해주세요.',
          });
        }
      }
    }
    fn();
  };

  const renderErrorMessage = (text: string) => {
    return <div className='invalid-feedback'>{text}</div>;
  }; // 에러 메세지를 출력하는 함수를 정의. 인자로는 에러메세지(문자열) 이 들어 오고
  // 에러메세지를 감싸는 블록 태그를 리턴한다.

  const renderSuccessMessage = (text: string) => {
    return <div className='valid-feedback'>{text}</div>;
  }; // 에러 메세지를 출력하는 함수를 정의. 인자로는 에러메세지(문자열) 이 들어 오고
  // 에러메세지를 감싸는 블록 태그를 리턴한다.

  React.useEffect(() => {
    // if(emailAvailable === "") {
    // 	setDisabledButton(true)
    // } else if(emailAvailable === true) {
    // 	setDisabledButton(true);
    // } else {
    // 	setDisabledButton(false);
    // }
    if (
      email !== '' &&
      emailState === 'valid' &&
      password !== '' &&
      passwordState === 'valid' &&
      repassword !== '' &&
      repasswordState === 'valid' &&
      username !== '' &&
      usernameState === 'valid' &&
      phone !== '' &&
      phoneState === 'valid' &&
      company !== '' &&
      companyState === 'valid' &&
      emailAvailable !== '' &&
      emailAvailable === false &&
      agreementService === true &&
      agreementUserInfo === true
    ) {
      setDisabledButton(false);
    } else {
      setDisabledButton(true);
    }
  }, [emailAvailable, agreementUserInfo, agreementService]);

  const renderEmailCheckValid = () => {
    if (emailAvailable === false) {
      return renderSuccessMessage('사용 가능한 이메일 입니다.');
    }
    if (email === '') {
      return renderErrorMessage('이메일은 필수 입력사항 입니다.');
    }
    if (emailAvailable) {
      return renderErrorMessage('사용중인 이메일 입니다.');
    }
    if (emailAvailable === '' && emailValid === 'has-success') {
      return renderSuccessMessage('이메일 중복확인을 해주시기 바랍니다.');
    }
    return renderErrorMessage('이메일 형식을 확인해 주시기 바랍니다.');
  };

  return (
    <>
      <Container className='pb-3 container-xl'>
        <Row className='justify-content-center'>
          <Col className='mx-auto' lg='8' md='10'>
            <div className='text-right'>
              <small className='text-red'>*필수입력사항</small>
              <hr className='w-100 mt-2' />
            </div>
            <Form role='form'>
              <FormGroup
                className={classnames(
                  {
                    focused: focusedEmail,
                  },
                  'row'
                )}
              >
                <Label
                  className='form-control-label font-weight-bold'
                  htmlFor='email'
                  md='2'
                >
                  이메일<span className='text-red'>*</span>
                </Label>
                <Col md='8' className={classnames(emailValid)}>
                  <Input
                    placeholder='이메일 주소를 입력해주세요'
                    type='email'
                    name='email'
                    onChange={handleChange}
                    onFocus={() => setFocusedEmail(true)}
                    onBlur={() => setFocusedEmail(false)}
                    valid={emailState === 'valid'}
                    invalid={emailState === 'invalid'}
                  />
                  {renderEmailCheckValid()}
                </Col>
                <Col md='2' className='text-right'>
                  <Button
                    className='btn-icon btn-3 '
                    color='primary'
                    type='button'
                    name='emailCheck'
                    disabled={emailCheckedButton}
                    onClick={handleEmailCheck}
                  >
                    중복 확인
                  </Button>
                </Col>
              </FormGroup>
              <FormGroup
                className={classnames(
                  {
                    focused: focusedPassword,
                  },
                  'row'
                )}
              >
                <Label
                  className='form-control-label font-weight-bold'
                  htmlFor='password'
                  md='2'
                >
                  비밀번호<span className='text-red'>*</span>
                </Label>
                <Col md='10' className={classnames(passwordValid)}>
                  <Input
                    style={PasswordFont}
                    placeholder='영문, 숫자, 특수문자 포함 8자 이상'
                    type='password'
                    name='password'
                    onChange={handleChange}
                    onFocus={() => setFocusedPassword(true)}
                    onBlur={() => setFocusedPassword(false)}
                    valid={passwordState === 'valid'}
                    invalid={passwordState === 'invalid'}
                  />
                  {password === ''
                    ? renderErrorMessage('비밀번호는 필수 입력사항 입니다.')
                    : renderErrorMessage(
                        '비밀번호 형식을 확인해 주시기 바랍니다.'
                      )}
                </Col>
              </FormGroup>
              <FormGroup
                className={classnames(
                  {
                    focused: focusedRePassword,
                  },
                  'row'
                )}
              >
                <Label
                  className='form-control-label font-weight-bold'
                  htmlFor='repassword'
                  md='2'
                >
                  비밀번호 확인<span className='text-red'>*</span>
                </Label>
                <Col md='10' className={classnames(repasswordValid)}>
                  <Input
                    style={PasswordFont}
                    placeholder='비밀번호를 다시 입력해주세요'
                    type='password'
                    name='repassword'
                    onChange={handleChange}
                    onFocus={() => setFocusedRePassword(true)}
                    onBlur={() => setFocusedRePassword(false)}
                    valid={repasswordState === 'valid'}
                    invalid={repasswordState === 'invalid'}
                  />
                  {repassword === ''
                    ? renderErrorMessage('비밀번호를 입력해주세요.')
                    : renderErrorMessage('비밀번호 입력이 잘못되었습니다..')}
                </Col>
              </FormGroup>
              <FormGroup
                className={classnames(
                  {
                    focused: focusedUsername,
                  },
                  'row'
                )}
              >
                <Label
                  className='form-control-label font-weight-bold'
                  htmlFor='username'
                  md='2'
                >
                  이름<span className='text-red'>*</span>
                </Label>
                <Col md='10' className={classnames(usernameValid)}>
                  <Input
                    placeholder='이름'
                    type='text'
                    name='username'
                    onChange={handleChange}
                    onFocus={() => setFocusedUsername(true)}
                    onBlur={() => setFocusedUsername(false)}
                    valid={usernameState === 'valid'}
                    invalid={usernameState === 'invalid'}
                  />
                  {username === ''
                    ? renderErrorMessage('성명은 필수 입력사항 입니다.')
                    : renderErrorMessage('입력 형식이 잘못 되었습니다.')}
                </Col>
              </FormGroup>
              <FormGroup
                className={classnames(
                  {
                    focused: focusedPhone,
                  },
                  'row'
                )}
              >
                <Label
                  className='form-control-label font-weight-bold'
                  htmlFor='phone'
                  md='2'
                >
                  휴대폰번호<span className='text-red'>*</span>
                </Label>

                <Col md='10' className={classnames(phoneValid)}>
                  <Input
                    placeholder='숫자만 입력하여 주세요'
                    type='tel'
                    name='phone'
                    maxLength={11}
                    minLength={10}
                    onChange={handleChange}
                    onFocus={() => setFocusedPhone(true)}
                    onBlur={() => setFocusedPhone(false)}
                    valid={phoneState === 'valid'}
                    invalid={phoneState === 'invalid'}
                  />
                  {phone === ''
                    ? renderErrorMessage('휴대폰 번호는 필수 입력사항 입니다.')
                    : renderErrorMessage('휴대폰 번호 형식이 잘못 되었습니다.')}
                </Col>
              </FormGroup>
              <FormGroup
                className={classnames(
                  {
                    focused: focusedCompany,
                  },
                  'row'
                )}
              >
                <Label
                  className='form-control-label font-weight-bold'
                  htmlFor='company'
                  md='2'
                >
                  소속<span className='text-red'>*</span>
                </Label>

                <Col md='10' className={classnames(companyValid)}>
                  <Input
                    placeholder='소속(회사, 학교, 연구소)를 입력하여 주세요'
                    type='text'
                    name='company'
                    onChange={handleChange}
                    onFocus={() => setFocusedCompany(true)}
                    onBlur={() => setFocusedCompany(false)}
                    valid={companyState === 'valid'}
                    invalid={companyState === 'invalid'}
                  />
                  {company === ''
                    ? renderErrorMessage('소속은 필수 입력사항 입니다.')
                    : renderErrorMessage('소속은 필수 입력사항 입니다..')}
                </Col>
              </FormGroup>
              <FormGroup
                className={classnames(
                  {
                    focused: focusedDepartment,
                  },
                  'row'
                )}
              >
                <Label
                  className='form-control-label font-weight-bold'
                  htmlFor='department'
                  md='2'
                >
                  부서
                </Label>
                <Col md='10'>
                  <Input
                    placeholder='부서 입력'
                    type='text'
                    name='department'
                    onChange={handleChange}
                    onFocus={() => setFocusedDepartment(true)}
                    onBlur={() => setFocusedDepartment(false)}
                  />
                </Col>
              </FormGroup>
              <FormGroup
                className={classnames(
                  {
                    focused: focusedPosition,
                  },
                  'row'
                )}
              >
                <Label
                  className='form-control-label font-weight-bold'
                  htmlFor='position'
                  md='2'
                >
                  직급
                </Label>

                <Col md='10'>
                  <Input
                    placeholder='직급 입력'
                    type='text'
                    name='position'
                    onChange={handleChange}
                    onFocus={() => setFocusedPosition(true)}
                    onBlur={() => setFocusedPosition(false)}
                  />
                </Col>
              </FormGroup>
              <FormGroup
                className={classnames(
                  {
                    focused: focusedCategory,
                  },
                  'row'
                )}
              >
                <Label
                  className='form-control-label font-weight-bold'
                  htmlFor='category'
                  md='2'
                >
                  업종선택
                </Label>

                <Col md='10'>
                  <Input
                    // defaultValue={values.category}
                    id='category'
                    name='category'
                    type='select'
                    onChange={handleChange}
                    onFocus={() => setFocusedCategory('focused')}
                    onBlur={() => setFocusedCategory('')}
                  >
                    {categoryList.map((item, idx) => {
                      // eslint-disable-next-line react/no-array-index-key
                      return <option key={idx}>{item}</option>;
                    })}
                  </Input>
                </Col>
              </FormGroup>
              <FormGroup
                className={classnames({
                  focused: focusedQuestion,
                })}
              >
                <div>마케팅관련 궁금하신 점을 물어보세요</div>
                <Input
                  id='contact-us-message-1'
                  placeholder='데이터, 마케팅과 관련하여 궁금하신 점을 입력해주세요 궁금증들을 해결할 수 있는 콘텐츠를 개발하겠습니다'
                  name='question'
                  rows='4'
                  type='textarea'
                  value={question}
                  onChange={handleChange}
                  onFocus={() => setFocusedQuestion('focused')}
                  onBlur={() => setFocusedQuestion('')}
                />
              </FormGroup>
              <div className='mb-3'>
                <span className='text-lg font-weight-800 mb-3'>약관동의</span>
              </div>
              <Card style={{ boxShadow: 'none' }}>
                <div className='accordion-1'>
                  <Container>
                    <Row>
                      <Col className='ml-auto' md='12'>
                        <div
                          className='accordion my-1 custom-checkbox'
                          id='accordionExample'
                        >
                          <Card>
                            <CardHeader id='headingOne'>
                              <h5 className='mb-0'>
                                <Button
                                  aria-expanded={
                                    openedCollapse === 'collapse-1'
                                  }
                                  onClick={() =>
                                    setOpenedCollapse(
                                      openedCollapse === 'collapse-1'
                                        ? ''
                                        : 'collapse-1'
                                    )
                                  }
                                  className='w-100 text-primary text-left'
                                  color='link'
                                  type='button'
                                >
                                  <input
                                    className='custom-control-input'
                                    id='agreement1'
                                    type='checkbox'
                                    name='agreementService'
                                    checked={agreementService}
                                    onChange={() =>
                                      setAgreementService(!agreementService)
                                    }
                                  />
                                  <label
                                    className='custom-control-label left--1'
                                    htmlFor='agreement1'
                                  />
                                  서비스이용약관을 확인하였고, 이에
                                  동의합니다(필수)
                                  <i className='ni ni-bold-down float-right pt-1' />
                                </Button>
                              </h5>
                            </CardHeader>
                            <Collapse isOpen={openedCollapse === 'collapse-1'}>
                              <CardBody className='opacity-8'>
                                {/* <span className="font-weight-700">
																	[필수] 개인정보보호법 제 15조 제1항의 의거
																	개인정보 수집 및 이용에 동의합니다.
																</span>
																<ul
																	className="pl-4"
																	style={{
																		fontSize: "14px",
																		lineHeight: "1.5rem",
																	}}>
																	<li>
																		개인 정보 수집 목적 및 이용 목적 : 교육
																		서비스 제공에 관한 계약 이행 및 서비스
																		제공에 따른 대관비 정산, 정보 및 콘텐츠
																		제공, 신청 확인, 영수증 발급, 화면 이력관리
																	</li>
																	<li>
																		수집하는 항목 : 회사명, 회사 주소/전화번호,
																		사업자등록번호, 성명, 주민등록번호(수료증
																		발급과정 시), 부서, 직위, 핸드폰, 이메일
																	</li>
																	<li>
																		개인정보의 보유 및 이용기간 : 대관 신청자
																		개인정보는 추후 이력관리 및 원활한 서비스
																		지원을 위해 최소 3년간 보유합니다.
																	</li>
																	<li>
																		다만, 이용자가 메일/전화 등을 통해 정보
																		삭제를 요청한 경우 수집된 개인 정보는 재생할
																		수 없는 방법에 의하여 완전히 삭제되면 어떠한
																		용도로도 열람 또는 이용할 수 없도록
																		처리됩니다.
																	</li>
																</ul> */}
                                <Terms />
                              </CardBody>
                            </Collapse>
                          </Card>
                          <Card>
                            <CardHeader id='headingTwo'>
                              <h5 className='mb-0'>
                                <Button
                                  aria-expanded={
                                    openedCollapse2 === 'collapse-2'
                                  }
                                  onClick={() =>
                                    setOpenedCollapse2(
                                      openedCollapse2 === 'collapse-2'
                                        ? ''
                                        : 'collapse-2'
                                    )
                                  }
                                  className='w-100 text-primary text-left collapsed'
                                  color='link'
                                  type='button'
                                >
                                  <input
                                    className='custom-control-input'
                                    id='agreement2'
                                    type='checkbox'
                                    name='agreementUserInfo'
                                    checked={agreementUserInfo}
                                    onChange={() =>
                                      setAgreementUserInfo(!agreementUserInfo)
                                    }
                                  />
                                  <label
                                    className='custom-control-label left--1'
                                    htmlFor='agreement2'
                                  />
                                  개인정보처리방침을 확인하였고, 이에
                                  동의합니다(필수)
                                  <i className='ni ni-bold-down float-right pt-1' />
                                </Button>
                              </h5>
                            </CardHeader>
                            <Collapse isOpen={openedCollapse2 === 'collapse-2'}>
                              <CardBody className='opacity-8'>
                                {/* <span className="font-weight-700">
																	본인은 대관 이용 약관을 모두 이해하였으며,
																	해당 내용을 준수할 것에 동의합니다.
																</span> */}
                                <UserInfoTerms />
                              </CardBody>
                            </Collapse>
                          </Card>
                          <Card>
                            <CardHeader id='headingTwo'>
                              <h5 className='mb-0'>
                                <Button
                                  className='w-100 text-primary text-left collapsed'
                                  color='link'
                                  type='button'
                                >
                                  <input
                                    className='custom-control-input'
                                    id='agreement3'
                                    type='checkbox'
                                    name='agreementMarketing'
                                    checked={agreementMarketing}
                                    onChange={() =>
                                      setAgreementMarketing(!agreementMarketing)
                                    }
                                  />
                                  <label
                                    className='custom-control-label left--1'
                                    htmlFor='agreement3'
                                  />
                                  마케팅메시지 수신에 동의합니다. 유용한 정보와
                                  혜택을 받을 수 있습니다(선택)
                                </Button>
                              </h5>
                            </CardHeader>
                          </Card>
                        </div>
                      </Col>
                    </Row>
                  </Container>
                </div>
              </Card>

              <div className='text-center'>
                {isLoading ? (
                  <Spinner style={{ width: '3rem', height: '3rem' }} />
                ) : (
                  <Button
                    className='mt-4 btn-round px-5'
                    color='primary'
                    type='button'
                    onClick={(e: React.FormEvent<HTMLButtonElement>) => {
                      handleSubmitForm(e);
                    }}
                    disabled={disabledButton}
                  >
                    가입하기
                  </Button>
                )}
              </div>
            </Form>
          </Col>
          {isOpenSignUpSuccess && <SignUpSuccessModal />}
        </Row>
      </Container>
    </>
  );
}
