import React from 'react';
import { useSelector } from 'react-redux';
import { RootState } from 'store/modules/index';
import { Button, Modal, Row } from 'reactstrap';

function SendTemporaryPasswordPopup({
  handleSendTemporaryPasswordModal,
}: {
  handleSendTemporaryPasswordModal: () => void;
}) {
  const { isSendTemporaryPasswordPopup } = useSelector(
    (state: RootState) => state.modalManages.authentication
  );

  return (
    <>
      {/* <Button
				block
				className="mb-3"
				color="primary"-
				onClick={() => handleSendTemporaryPasswordModal(!isSendTemporaryPasswordPopup)}
				type="button">
				Default
			</Button> */}
      <Modal
        isOpen={isSendTemporaryPasswordPopup}
        toggle={() => handleSendTemporaryPasswordModal()}
        className='modal-dialog-centered modal-sm'
      >
        <div className='modal-body'>
          <div className='font-weight-700 text-center h3 mt-3'>
            <i className='ni ni-email-83' />
          </div>
          <div className='text-center mb-4 h5'>
            등록된 이메일로 <br />
            임시 비밀번호가 전송되었습니다.
          </div>
          <Row className='justify-content-center mb-3'>
            <div>
              <Button
                className='btn px-4'
                color='primary'
                onClick={() => handleSendTemporaryPasswordModal()}
              >
                확인
              </Button>
            </div>
          </Row>
        </div>
      </Modal>
    </>
  );
}

export default SendTemporaryPasswordPopup;
