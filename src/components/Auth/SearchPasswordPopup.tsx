/* eslint-disable @typescript-eslint/no-unused-vars */
import classNames from 'classnames';
import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { getStorageItem } from 'utils/useLocalStorage';
import styled from 'styled-components';
import * as actions from 'store/modules/modalManages';
import { notification } from 'antd';
import {
  Button,
  Modal,
  Row,
  Input,
  InputGroup,
  FormGroup,
  Spinner,
} from 'reactstrap';
// import classnames from "classnames";
import { axiosInstance } from 'api';
import { useHistory } from 'react-router-dom';
import { RootState } from 'store/modules/index';

const TollTip = styled.div<{ hidden: boolean }>`
  display: ${(props) => (props.hidden ? 'none' : 'block')};
  font-size: 14px;
`;

function SearchPasswordPopup({
  handleSearchPasswordPopup,
}: {
  handleSearchPasswordPopup: () => void;
}) {
  // const history = useHistory();
  const dispatch = useDispatch();

  const { isSearchPasswordPopup } = useSelector(
    (state: RootState) => state.modalManages.authentication
  );

  const { isSendTemporaryPasswordPopup } = useSelector(
    (state: RootState) => state.modalManages.authentication
  );

  const [emailFocus, setEmailFocus] = useState<string | null>(null);

  const [email, setEmail] = useState('');

  const [toolTipHidden, setToolTipHidden] = useState(true);

  const [isLoading, setIsLoading] = useState(false);

  const submitPasswordReset = () => {
    const fn = async () => {
      const formData = new FormData();
      formData.append('email', email);
      try {
        setIsLoading(true);
        const response = await axiosInstance.post(
          'accounts/password/reset/',
          formData
        );
        dispatch(actions.setSearchPasswordPopupModal(!isSearchPasswordPopup));
        dispatch(
          actions.setSendTemporaryPasswordModal(!isSendTemporaryPasswordPopup)
        );
        // setEmail(false);
      } catch (error) {
        // console.log("error: ", error);
        setToolTipHidden(false);
        setIsLoading(false);
      }
    };
    fn();
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setEmail(e.target.value);
    setToolTipHidden(true);
  };

  return (
    <>
      {/* <Button
				block
				className="mb-3"

				color="primary"-
				onClick={() => handleSearchPasswordPopup(!isSearchPasswordPopup)}
				type="button">
				Default
			</Button> */}
      <Modal
        isOpen={isSearchPasswordPopup}
        toggle={() => handleSearchPasswordPopup()}
        className='modal-dialog-centered modal-md'
      >
        <div className='modal-header align-items-center pl-5 pr-4'>
          <div
            className='modal-title h3 font-weight-900'
            id='modal-title-default'
          >
            비밀번호 찾기
          </div>
          <button
            aria-label='Close'
            className='close'
            onClick={() => handleSearchPasswordPopup()}
            type='button'
          >
            <div className='h3' aria-hidden>
              ×
            </div>
          </button>
        </div>
        <div className='modal-body bg-secondary px-5'>
          <div className='font-weight-700 mb-5'>
            가입 이메일 주소를 입력해주시면 임시 비밀번호가 전달됩니다
          </div>
          <FormGroup className={(classNames(emailFocus), 'mb-0')}>
            <div className='font-weight-700'>이메일</div>
            <InputGroup className='input-group-alternative'>
              <Input
                placeholder='가입한 이메일을 입력해주세요'
                type='text'
                name='email'
                value={email}
                onChange={(e) => handleChange(e)}
                onFocus={() => setEmailFocus('focused')}
                onBlur={() => setEmailFocus('')}
              />
            </InputGroup>
          </FormGroup>
          <TollTip className='text-red' hidden={toolTipHidden}>
            입력하신 이메일을 확인해 주세요.
          </TollTip>
          <div className='mt-3'>
            가입 이메일을 잊으신 경우, 데이터마케팅캠퍼스 고객센터로
            <br /> 문의해주세요
          </div>
          <div style={{ color: 'blue', textDecoration: 'underline' }}>
            (dataedu@datamarketing.co.kr)
          </div>
          {isLoading ? (
            <Row className='justify-content-center mt-3'>
              <Spinner style={{ width: '3rem', height: '3rem' }} />
            </Row>
          ) : (
            <Row className='justify-content-end pr-3'>
              <div className=''>
                <Button
                  className='btn'
                  color='white'
                  onClick={() => handleSearchPasswordPopup()}
                >
                  취소
                </Button>
              </div>
              <div>
                <Button
                  className='btn ml-3'
                  color='primary'
                  onClick={() => submitPasswordReset()}
                >
                  확인
                </Button>
              </div>
            </Row>
          )}
        </div>
      </Modal>
    </>
  );
}

export default SearchPasswordPopup;
