/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-shadow */
/* eslint-disable @typescript-eslint/naming-convention */
import React from 'react';
import { axiosInstance } from 'api';
import * as actions from 'store/modules/authentication';
import { useSelector, useDispatch } from 'react-redux';
import { getStorageItem } from 'utils/useLocalStorage';
import { RootState } from 'store/modules/index';

export default function RefreshToken() {
  const dispatch = useDispatch();
  const isRefresh = getStorageItem('refresh') ? getStorageItem('refresh') : '';
  const { jwtToken, refresh } = useSelector(
    (state: RootState) => state.authentication
  );

  const initLoginInfo = async () => {
    try {
      const response = await axiosInstance.post('/accounts/token/refresh/', {
        refresh: isRefresh,
      });
      const {
        data: {
          access: jwtToken,
          id,
          is_staff,
          is_active,
          email,
          date_joined,
          phone,
          username,
          company,
          department,
          category,
          marketing_contract_email,
          marketing_contract_sms,
          position,
          is_email_authentication,
          private_info,
          service_contract,
        },
      } = response;

      const userInfo = {
        id,
        isAdmin: is_staff,
        isAuthenticated: true,
        isActive: is_active,
        email,
        dateJoined: date_joined,
        phone,
        username,
        company,
        department,
        position,
        category,
        agreementEmail: marketing_contract_email,
        agreementSNS: marketing_contract_sms,
        privateInfo: private_info,
        serviceContract: service_contract,
        is_email_authentication,
      };
      dispatch(actions.setToken(jwtToken));
      dispatch(actions.setLoggedIn(true));
      dispatch(actions.setUserInfo(userInfo));
    } catch (error) {
      //   if (error.response) {
      //   }
    }
  };
  initLoginInfo();
  return null;
}
