/* eslint-disable @typescript-eslint/no-unused-vars */
// import React from 'react';
import { useDispatch } from 'react-redux';
import * as actions from 'store/modules/authentication';
import { getStorageItem } from 'utils/useLocalStorage';
import { axiosAuth } from 'api';
import { useHistory } from 'react-router-dom';

export default function UserLogout() {
  const dispatch = useDispatch();
  const history = useHistory();
  const isRefresh = getStorageItem('refresh');
  const initLoginInfo = async () => {
    try {
      const response = await axiosAuth.post('accounts/logout/', {
        refresh: isRefresh,
      });
      dispatch(actions.deleteToken());
      dispatch(actions.deleteAuthInfo());
      dispatch(actions.setLoggedIn(false));
      history.push('/home');
    } catch (error) {
      if (error.response) {
        console.log('error.response: ', error);
      }
    }
  };
  initLoginInfo();
  return null;
}
