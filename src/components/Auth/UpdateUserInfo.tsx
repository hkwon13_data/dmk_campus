// /* eslint-disable @typescript-eslint/naming-convention */
// import React, { useState } from 'react';
// import { axiosInstance } from 'api';
// // import { useHistory, useLocation } from 'react-router-dom';
// import { useAppContext, setToken, setIsStaff } from 'store';
// // import { notification } from "antd";

// // import googleImg from 'assets/icons/common/google.svg';
// // import gitHubImg from 'assets/icons/common/github.svg';
export {};
// // reactstrap components
// import {
//   Button,
//   Card,
//   CardBody,
//   Container,
//   Col,
//   FormGroup,
//   Form,
//   Input,
//   InputGroupAddon,
//   InputGroupText,
//   InputGroup,
// } from 'reactstrap';

// // Core Components
// export default function LoginCard() {
//   const { dispatch } = useAppContext();
//   //   const location = useLocation();
//   //   const history = useHistory();
//   const [username, setUsername] = useState('');
//   const [password, setPassword] = useState('');

//   const [emailFocus, setEmailFocus] = React.useState('');
//   const [passwordFocus, setPasswordFocus] = React.useState('');

//   //   const { from: loginRedirectUrl } = location.state || {
//   //     from: { pathname: '/admin/dashboard' },
//   //   };

//   const handleSubmitForm = (event: React.FormEvent<HTMLButtonElement>) => {
//     event.preventDefault();
//     async function fn() {
//       const data = { username, password };
//       try {
//         const response = await axiosInstance.post('/accounts/token/', data);
//         const {
//           data: { access: jwtToken, is_staff },
//         } = response;
//         dispatch(setToken(jwtToken));
//         dispatch(setIsStaff(is_staff));

//         // notification.open({
//         // 	message: "로그인 성공",
//         // 	description: "Admin 페이지로 이동합니다",
//         // });
//         document.location.href = '/admin/dashboard';
//       } catch (error) {
//         if (error.response) {
//           // notification.open({
//           // 	message: "로그인 실패",
//           // 	description: "아이디/암호를 확인해주세요.",
//           // });
//         }
//       }
//     }
//     fn();
//   };

//   const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
//     if (e.target.name === 'username') {
//       setUsername(e.target.value);
//     } else if (e.target.name === 'password') {
//       setPassword(e.target.value);
//     }
//   };

//   return (
//     <>
//       <section className='upper'>
//         <Container>
//           <Col className='mx-auto' lg='5' md='8'>
//             <Card className='bg-secondary shadow border-0'>
//               <CardBody className='px-lg-5 py-lg-5'>
//                 <div className='text-center text-muted mb-4'>
//                   <small>Login with credentials</small>
//                 </div>
//                 <Form>
//                   <FormGroup className={`mb-3 ${emailFocus}`}>
//                     <InputGroup className='input-group-alternative'>
//                       <InputGroupAddon addonType='prepend'>
//                         <InputGroupText>
//                           <i className='ni ni-email-83' />
//                         </InputGroupText>
//                       </InputGroupAddon>
//                       <Input
//                         placeholder='Email'
//                         type='text'
//                         name='username'
//                         onChange={handleChange}
//                         onFocus={() => setEmailFocus('focused')}
//                         onBlur={() => setEmailFocus('')}
//                       />
//                     </InputGroup>
//                   </FormGroup>
//                   <FormGroup className={passwordFocus}>
//                     <InputGroup className='input-group-alternative'>
//                       <InputGroupAddon addonType='prepend'>
//                         <InputGroupText>
//                           <i className='ni ni-lock-circle-open' />
//                         </InputGroupText>
//                       </InputGroupAddon>
//                       <Input
//                         placeholder='Password'
//                         type='password'
//                         name='password'
//                         onChange={handleChange}
//                         onFocus={() => setPasswordFocus('focused')}
//                         onBlur={() => setPasswordFocus('')}
//                       />
//                     </InputGroup>
//                   </FormGroup>
//                   <div className='custom-control custom-control-alternative custom-checkbox'>
//                     <input
//                       className='custom-control-input'
//                       id=' customCheckLogin2'
//                       type='checkbox'
//                     />
//                     <label
//                       className='custom-control-label'
//                       htmlFor=' customCheckLogin2'
//                     >
//                       <span className='text-default opacity-5'>
//                         Remember me
//                       </span>
//                     </label>
//                   </div>
//                   <div className='text-center'>
//                     <Button
//                       className='my-4'
//                       color='primary'
//                       onClick={handleSubmitForm}
//                     >
//                       Login
//                     </Button>
//                   </div>
//                 </Form>
//               </CardBody>
//             </Card>
//           </Col>
//         </Container>
//       </section>
//     </>
//   );
// }
