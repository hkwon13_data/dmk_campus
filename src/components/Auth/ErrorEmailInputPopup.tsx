import React from 'react';
import { RootState } from 'store/modules/index';
import { useSelector } from 'react-redux';
import { Button, Modal, Row } from 'reactstrap';

function ErrorEmailInputPopup({
  handleErrorEmailInputModal,
}: {
  handleErrorEmailInputModal: () => void;
}) {
  const { isErrorEmailInputPopup } = useSelector(
    (state: RootState) => state.modalManages.authentication
  );

  return (
    <>
      {/* <Button
				block
				className="mb-3"
				color="primary"-
				onClick={() => handleErrorEmailInputModal(!isErrorEmailInputPopup)}
				type="button">
				Default
			</Button> */}
      <Modal
        isOpen={isErrorEmailInputPopup}
        toggle={() => handleErrorEmailInputModal()}
        className='modal-dialog-centered modal-sm'
      >
        <div className='modal-body'>
          <div className='font-weight-700 text-center h3 mt-3'>
            <i className='ni ni-bell-55' />
          </div>
          <div className='text-center mb-4 h5'>
            입력하신 정보가 잘못되었습니다.
          </div>
          <Row className='justify-content-center pb-3'>
            <div>
              <Button
                className='btn px-4'
                color='primary'
                onClick={() => handleErrorEmailInputModal()}
              >
                확인
              </Button>
            </div>
          </Row>
        </div>
      </Modal>
    </>
  );
}

export default ErrorEmailInputPopup;
