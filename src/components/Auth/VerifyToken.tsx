/* eslint-disable @typescript-eslint/no-unused-vars */
import React from 'react';
import { RootState } from 'store/modules/index';
import { useSelector } from 'react-redux';
import { axiosAuth } from 'api';

export default function VerifyToken() {
  const { jwtToken } = useSelector((state: RootState) => state.authentication);

  const initLoginInfo = async () => {
    const formData = new FormData();
    formData.append('token', jwtToken);
    try {
      const response = await axiosAuth.post(
        '/accounts/token/verify/',
        formData
      );
    } catch (error) {
      if (error.response) {
        alert('로그인을 다시 해주시기 바랍니다.');
      }
    }
  };
  initLoginInfo();
  return null;
}
