import React from 'react';

// reactstrap components
import { Container, Row, Col } from 'reactstrap';
// import styled from 'styled-components';
// Core Components
// import LoginCard from 'components/Auth/LoginForm.js';

// import imgA from "assets/img/sections/unsplashs.jpg"
// import imgA from 'assets/img/sections/unsplashs.jpg';

export default function SignupHeader() {
  return (
    <>
      <div
        className='page-header page-header-small header-filter '
        style={{ minHeight: '28vh', maxHeight: '440px' }}
      >
        <Container>
          <div className='header-body text-center mt-9'>
            <Row className='justify-content-center'>
              <Col className='px-5' lg='6' md='8' xl='5'>
                <h1>회원가입</h1>
                <p className='text-lead'>
                  데이터마케팅캠퍼스 가입을 환영합니다
                  <br />
                  입력해주신 정보는 안전하게 처리됩니다
                </p>
              </Col>
            </Row>
          </div>
        </Container>
      </div>
    </>
  );
}
