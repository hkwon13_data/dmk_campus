import React from 'react';
import { useSelector } from 'react-redux';
// reactstrap components
import { Button, Modal, Row } from 'reactstrap';
import { RootState } from 'store/modules/index';
// Core Components

function SignUpSuccessModal() {
  //   const [modalOpen, handleSignUpSuccessModal] = React.useState(false);

  const { isOpenSignUpSuccess } = useSelector(
    (state: RootState) => state.modalManages.authentication
  );

  return (
    <>
      <Modal
        isOpen={isOpenSignUpSuccess}
        // toggle={() => handleSignUpSuccessModal(!isOpenSignUpSuccess)}
        className='modal-dialog-centered modal-md'
      >
        <div className='modal-body'>
          <div className='font-weight-700 text-center h3 m3-5'>
            <i className='ni ni-email-83' />
          </div>
          <div className='text-center mb-3 h5'>회원가입에 성공하였습니다.</div>
          <div className='text-center mb-3 h5'>
            이메일 인증을 진행해 주세요.
          </div>
          <div className='text-center mb-4 h5'>
            확인을 누르시면 로그인 페이지로 이동 합니다.
          </div>
          <Row className='justify-content-center'>
            <div>
              <Button
                className='btn px-4'
                color='primary'
                onClick={() => {
                  window.localStorage.clear();
                  document.location.href = '/auth/login';
                }}
              >
                확인
              </Button>
            </div>
          </Row>
        </div>
      </Modal>
    </>
  );
}

export default SignUpSuccessModal;
