/* eslint-disable react/no-array-index-key */
import React from 'react';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';

interface PagenationTypes {
  pagingData: {
    pagesCount: number;
    currentPage: number;
    pageSize: number;
  };
  handlePage: (e: number) => void;
}

function CommonPagination({ pagingData, handlePage }: PagenationTypes) {
  const { pagesCount, currentPage } = pagingData;

  const handleCurrentPage = (e: any, idx: number) => {
    e.preventDefault();
    handlePage(idx);
  };

  return (
    <>
      <div className='pagination justify-content-center'>
        <Pagination aria-label='Page navigation'>
          <PaginationItem disabled={currentPage <= 0}>
            <PaginationLink
              aria-label='Previous'
              onClick={(e) => handleCurrentPage(e, currentPage - 1)}
            >
              <span aria-hidden>
                <i aria-hidden className='fa fa-angle-left' />
              </span>
            </PaginationLink>
          </PaginationItem>
          {[...Array(pagesCount)].map((page, i) => (
            <PaginationItem active={i === currentPage} key={i}>
              <PaginationLink
                onClick={(e: any) => handleCurrentPage(e, i)}
                href='#'
              >
                {i + 1}
              </PaginationLink>
            </PaginationItem>
          ))}
          <PaginationItem disabled={currentPage >= pagesCount - 1}>
            <PaginationLink
              aria-label='Next'
              onClick={(e) => handleCurrentPage(e, currentPage + 1)}
            >
              <span aria-hidden>
                <i aria-hidden className='fa fa-angle-right' />
              </span>
            </PaginationLink>
          </PaginationItem>
        </Pagination>
      </div>
    </>
  );
}

export default CommonPagination;
