/* eslint-disable import/prefer-default-export */
import React from 'react';
import { Button } from 'reactstrap';

function ScrollTopButton() {
  return (
    <>
      <Button
        className='btn-icon-only back-to-top show'
        color='primary'
        name='button'
        type='button'
        onClick={() => {
          window.scrollTo(0, 0);
          document.body.scrollTop = 0;
        }}
      >
        <i className='ni ni-bold-up' />
      </Button>
    </>
  );
}

export { ScrollTopButton };
