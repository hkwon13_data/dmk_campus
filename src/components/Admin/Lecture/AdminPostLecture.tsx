import React from 'react';
import 'react-quill/dist/quill.snow.css';
import { FormGroup, Form, Input, Row, Col } from 'reactstrap';

import AdminLectureEditor from './AdminLectureEditor';

interface AdminPostNoticeTypes {
  title: string;
  desc: string;
  onEditorChange: (value: string) => void;
  onTitleChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
}

export default function AdminPostNotice({
  title,
  desc,
  onEditorChange,
  onTitleChange,
}: AdminPostNoticeTypes) {
  return (
    <>
      <Form id='contact-form-1' method='post' role='form'>
        <Row>
          <Col md='12'>
            <label className='form-control-label font-weight-700 h5'>
              강의자료 제목
            </label>
            <FormGroup>
              <Input
                className='form-control'
                placeholder='강의자료 제목을 입력해주세요'
                type='text'
                name='title'
                value={title}
                onChange={onTitleChange}
              />
            </FormGroup>
          </Col>
        </Row>
      </Form>
      <AdminLectureEditor desc={desc} onEditorChange={onEditorChange} />
    </>
  );
}
