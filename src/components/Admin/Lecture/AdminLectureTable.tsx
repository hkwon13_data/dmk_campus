import React from 'react';

import * as types from 'types/types';

interface NoticeInfoPropTypes {
  pagingData: {
    pagesCount: number;
    currentPage: number;
    pageSize: number;
  };
  data: types.CommunityData[] | undefined;
  setOpenNotice: React.Dispatch<React.SetStateAction<boolean>>;
  setClickedNotice: React.Dispatch<
    React.SetStateAction<{
      mapId: number;
      postId: number | undefined;
    }>
  >;
}

export default function AdminLectureMaterials({
  pagingData,
  data,
  setOpenNotice,
  setClickedNotice,
}: NoticeInfoPropTypes) {
  const { pageSize, currentPage } = pagingData;

  return (
    <>
      <thead className='text-primary text-center'>
        <tr>
          <th style={{ width: '7%' }}>번호</th>
          <th>제목</th>
          <th style={{ width: '8.33%' }}>작성일</th>
        </tr>
      </thead>
      <tbody>
        {data
          ?.slice(currentPage * pageSize, (currentPage + 1) * pageSize)
          .map((post, i) => (
            <tr
              key={post.id}
              onClick={() => {
                setOpenNotice(true);
                setClickedNotice({ mapId: i, postId: post.id });
              }}
            >
              <th className='text-center' scope='row'>
                {post.id}
              </th>
              <td>{post.title}</td>
              <td>{post.created_at.split('T')[0]}</td>
            </tr>
          ))}
        <tr style={{ pointerEvents: 'none' }}>
          <td colSpan={3} />
        </tr>
      </tbody>
    </>
  );
}
