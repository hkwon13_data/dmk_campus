import React from 'react';
import ReactQuill from 'react-quill'; // ES6
import 'react-quill/dist/quill.snow.css';

interface AdminPostNitocePropsTypes {
  desc: string;
  onEditorChange: (value: string) => void;
}

export default function AdminPostNotice({
  desc,
  onEditorChange,
}: AdminPostNitocePropsTypes) {
  const formats = [
    'header',
    'bold',
    'italic',
    'underline',
    'strike',
    'blockquote',
    'size',
    'color',
    'list',
    'bullet',
    'indent',
    'link',
    'image',
    'video',
    'align',
  ];

  return (
    <>
      <ReactQuill
        theme='snow'
        style={{ height: '30rem' }}
        value={desc || ''}
        onChange={(content, delta, source, editor) =>
          onEditorChange(editor.getHTML())
        }
        modules={{
          toolbar: [
            [{ size: ['small', false, 'large', 'huge'] }], // custom dropdown
            [{ header: [1, 2, 3, 4, 5, 6, false] }],

            ['bold', 'italic', 'underline', 'strike'], // toggled buttons
            ['link', 'image'],
            ['blockquote', 'code-block'],

            [{ header: 1 }, { header: 2 }], // custom button values
            [{ list: 'ordered' }, { list: 'bullet' }],
            [{ indent: '-1' }, { indent: '+1' }], // outdent/indent
            [{ direction: 'rtl' }], // text direction

            [{ color: [] }, { background: [] }], // dropdown with defaults from theme
            [{ align: [] }],

            ['clean'], // remove formatting button
          ],
        }}
        formats={formats}
      />
    </>
  );
}
