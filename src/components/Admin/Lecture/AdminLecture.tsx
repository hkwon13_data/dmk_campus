/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-shadow */
import React, { useState } from 'react';

// reactstrap components
import { Table, Button, Container, Row, Col } from 'reactstrap';

import CommonPagination from 'components/utils/Pagination';
import { useSelector } from 'react-redux';
import { axiosAuth } from 'api';
import { RootState } from 'store/modules/index';
import AdminLectureTable from './AdminLectureTable';
import AdminPostLecture from './AdminPostLecture';
import AdminLectureInfo from './AdminLectureInfo';

interface ClickedNoticeTypes {
  mapId: number;
  postId: number | undefined;
}

export default function AdminLecture() {
  const { data } = useSelector(
    (state: RootState) => state.community.lectureMaterials
  );
  const lectureList = data;

  const [pageSize, setPageSize] = useState(10);
  const dataLength = !data ? 1 : data.data.length;
  const pages = Math.ceil(dataLength / pageSize);
  const [pagesCount, setPageCount] = useState(pages);
  const [currentPage, setCurrentPage] = useState(0);

  const [postPage, setPostPage] = useState(false);
  const [buttonName, setButtonName] = useState('강의자료 작성');

  const [openNotice, setOpenNotice] = useState(false);
  const [clickedNotice, setClickedNotice] = useState<ClickedNoticeTypes>({
    mapId: 0,
    postId: 0,
  });

  const pagingData = {
    pagesCount,
    currentPage,
    pageSize,
  };
  const [desc, setDesc] = useState('');

  const onEditorChange = (value: string) => {
    setDesc(value);
  };

  const [title, setTitle] = useState('');

  const onTitleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setTitle(e.target.value);
  };

  const postSubmit = (e: React.FormEvent<HTMLButtonElement>) => {
    e.preventDefault();
    const formData = new FormData();
    formData.append('title', title);
    formData.append('content', desc);
    formData.append('etc', 'React에서 etc 테스트');
    formData.append('user', '1');

    try {
      const response = axiosAuth.post('/community/notices/post', formData);
      console.log('response: ', response);
    } catch (error) {
      console.log('error: ', error.response);
    }
  };

  React.useEffect(() => {
    if (dataLength > 10) {
      setPageCount(pages);
    }
  }, [dataLength, pages]);

  const handlePage = (e: number) => {
    setCurrentPage(e);
  };

  const handlePosting = () => {
    setPostPage((postPage) => !postPage);
    setOpenNotice(false);
  };

  React.useEffect(() => {
    if (postPage) {
      setButtonName('목록');
    } else {
      setButtonName('강의자료 작성');
    }
  }, [postPage, buttonName]);

  return (
    <>
      <Container className='container-lg p-0'>
        <Row className='align-items-right py-4'>
          <Col lg='6' xs='7'>
            강의자료
          </Col>
          <Col className='text-right' lg='6' xs='5'>
            <Button
              className='btn-neutral'
              color='default'
              onClick={() => handlePosting()}
              size='sm'
            >
              {buttonName}
            </Button>
            {openNotice && (
              <Button
                className='btn-neutral'
                color='default'
                onClick={() => console.log('test')}
                size='sm'
              >
                수정
              </Button>
            )}
            {postPage && (
              <Button
                className='btn-neutral'
                color='default'
                onClick={(e) => postSubmit(e)}
                size='sm'
              >
                저장
              </Button>
            )}
          </Col>
        </Row>
        {!postPage && openNotice && (
          <AdminLectureInfo
            data={lectureList?.data}
            openNotice={setOpenNotice}
            clickedNotice={clickedNotice}
            setClickedNotice={setClickedNotice}
          />
        )}
        {!postPage && !openNotice && (
          <div className='table'>
            <Table responsive hover>
              {data && (
                <AdminLectureTable
                  pagingData={pagingData}
                  data={lectureList?.data}
                  setOpenNotice={setOpenNotice}
                  setClickedNotice={setClickedNotice}
                />
              )}
            </Table>
          </div>
        )}
        {data && !postPage && !openNotice && (
          <CommonPagination
            pagingData={pagingData}
            handlePage={(e) => handlePage(e)}
          />
        )}

        {/* Post 등록 */}
        {postPage && !openNotice && (
          <AdminPostLecture
            title={title}
            desc={desc}
            onEditorChange={onEditorChange}
            onTitleChange={onTitleChange}
          />
        )}
      </Container>
    </>
  );
}
