import React from 'react';
import { useSelector } from 'react-redux';
import Root from 'pages';
// import VerifyToken from 'components/Auth/VerifyToken';
import RefreshToken from 'components/Auth/RefreshToken';
import { getStorageItem } from 'utils/useLocalStorage';
import { RootState } from 'store/modules/index';

export default function Authenticated() {
  const { isLoggedIn } = useSelector(
    (state: RootState) => state.authentication
  );
  const refresh = getStorageItem('refresh');

  return (
    <>
      {!isLoggedIn && refresh && (
        <>
          <RefreshToken />
          <Root />
        </>
      )}
      {(isLoggedIn || !refresh) && <Root />}
    </>
  );
}
