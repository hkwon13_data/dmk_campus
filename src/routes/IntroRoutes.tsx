import IntroPage from 'pages/Intro/_IntroPage';
import HistoryPage from 'pages/Intro/_HistoryPage';
import PeoplePage from 'pages/Intro/_DMCPeoplePage';
import types from 'types/types';

// import LoginRequiredRoute from "utils/LoginRequiredRoute";

const layout = '/intro';
const routes: types.RoutesTypes[] = [
  {
    collapse: true,
    name: 'Intro Page',
    icon: 'ni ni-ungroup text-orange',
    state: '',
    views: [
      {
        path: '/',
        name: 'Intro',
        miniName: 'I',
        component: IntroPage,
        layout,
      },
      {
        path: '/history',
        name: 'History',
        miniName: 'P',
        component: HistoryPage,
        layout,
      },
      {
        path: '/dmcpeople',
        name: 'DMCPeople',
        miniName: 'D',
        component: PeoplePage,
        layout,
      },
    ],
  },
];

export default routes;
