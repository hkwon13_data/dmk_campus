import DMKHallPage from 'pages/DMKHall/_DMKHallPage';
import DMKHallReservePage from 'pages/DMKHall/_DMKHallReservePage';
import DMKHallReserveInfo from 'pages/DMKHall/DMKHallReserveInfo';
// import LoginRequiredRoute from "utils/LoginRequiredRoute";
import types from 'types/types';

const layout = '/dmkHall';
const routes: types.RoutesTypes[] = [
  {
    collapse: true,
    name: 'DMKHall Page',
    icon: 'ni ni-ungroup text-orange',
    // state: "examplesCollapse",
    state: '',
    views: [
      {
        path: '/',
        name: 'dmkHallPage',
        miniName: 'I',
        component: DMKHallPage,
        layout,
      },
      {
        path: '/reservation',
        name: 'dmkHallReservation',
        miniName: 'P',
        component: DMKHallReservePage,
        layout,
      },
      {
        path: '/reservation/payment',
        name: 'dmkHallReservationPayment',
        miniName: 'P',
        component: DMKHallReserveInfo,
        layout,
      },
      // {
      // 	path: "/payment",
      // 	name: "dmkHallPayment",
      // 	miniName: "K",
      // 	component: DMKHallReserveInfo,
      // 	layout: layout,
      // },
    ],
  },
];

export default routes;
