import DM9Page from 'pages/OpenProcess/_DM9Page';
import SummitPage from 'pages/OpenProcess/_SummitPage';
import MMCPage from 'pages/OpenProcess/_MaderiMembershipClubPage';
import DM9DetailPage1 from 'pages/OpenProcess/DM9DetailPage1/DM9DetailPage';
import DM9DetailPage2 from 'pages/OpenProcess/DM9DetailPage2/DM9DetailPage';
import DM9DetailPage3 from 'pages/OpenProcess/DM9DetailPage3/DM9DetailPage';
import DM9DetailPage4 from 'pages/OpenProcess/DM9DetailPage4/DM9DetailPage';
import DM9DetailPage5 from 'pages/OpenProcess/DM9DetailPage5/DM9DetailPage';
import DM9DetailPage6 from 'pages/OpenProcess/DM9DetailPage6/DM9DetailPage';
import DM9DetailPage7 from 'pages/OpenProcess/DM9DetailPage7/DM9DetailPage';
import DM9DetailPage8 from 'pages/OpenProcess/DM9DetailPage8/DM9DetailPage';
import DM9DetailPage9 from 'pages/OpenProcess/DM9DetailPage9/DM9DetailPage';
import types from 'types/types';

const layout = '/openProcess';
const routes: types.RoutesTypes[] = [
  {
    collapse: true,
    name: 'Open Process Page',
    icon: 'ni ni-ungroup text-orange',
    // state: "examplesCollapse",
    state: '',
    views: [
      {
        path: '/',
        name: 'DM9',
        miniName: 'L',
        component: DM9Page,
        layout,
      },
      {
        path: '/summit',
        name: 'Summit',
        miniName: 'S',
        component: SummitPage,
        layout,
      },
      {
        path: '/summit/lineUp',
        name: 'Summit lineUp',
        miniName: 'S',
        component: SummitPage,
        layout,
      },
      {
        path: '/summit/advantages',
        name: 'Summit Advantages',
        miniName: 'S',
        component: SummitPage,
        layout,
      },
      {
        path: '/summit/inquiry',
        name: 'Summit Inquiry',
        miniName: 'S',
        component: SummitPage,
        layout,
      },
      {
        path: '/mmc',
        name: 'Maderi Membership Club',
        miniName: 'MMC',
        component: MMCPage,
        layout,
      },
      {
        path: '/mmc/lineUp',
        name: 'Maderi Membership Club lineUp',
        miniName: 'MMC',
        component: MMCPage,
        layout,
      },
      {
        path: '/mmc/info',
        name: 'Maderi Membership Club info',
        miniName: 'MMC',
        component: MMCPage,
        layout,
      },
      {
        path: '/dm9/detailPage1',
        name: 'Maderi Membership Club info',
        miniName: 'DetailPage',
        component: DM9DetailPage1,
        layout,
      },
      {
        path: '/dm9/detailPage2',
        name: 'Maderi Membership Club info',
        miniName: 'DetailPage',
        component: DM9DetailPage2,
        layout,
      },
      {
        path: '/dm9/detailPage3',
        name: 'Maderi Membership Club info',
        miniName: 'DetailPage',
        component: DM9DetailPage3,
        layout,
      },
      {
        path: '/dm9/detailPage4',
        name: 'Maderi Membership Club info',
        miniName: 'DetailPage',
        component: DM9DetailPage4,
        layout,
      },
      {
        path: '/dm9/detailPage5',
        name: 'Maderi Membership Club info',
        miniName: 'DetailPage',
        component: DM9DetailPage5,
        layout,
      },
      {
        path: '/dm9/detailPage6',
        name: 'Maderi Membership Club info',
        miniName: 'DetailPage',
        component: DM9DetailPage6,
        layout,
      },
      {
        path: '/dm9/detailPage7',
        name: 'Maderi Membership Club info',
        miniName: 'DetailPage',
        component: DM9DetailPage7,
        layout,
      },
      {
        path: '/dm9/detailPage8',
        name: 'Maderi Membership Club info',
        miniName: 'DetailPage',
        component: DM9DetailPage8,
        layout,
      },
      {
        path: '/dm9/detailPage9',
        name: 'Maderi Membership Club info',
        miniName: 'DetailPage',
        component: DM9DetailPage9,
        layout,
      },
    ],
  },
];

export default routes;
