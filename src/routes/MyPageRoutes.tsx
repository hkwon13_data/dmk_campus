import DM9ResultPage from 'pages/MyPage/_DM9ResultPage';
import PaymentHistPage from 'pages/MyPage/_PaymentHistoryPage';
import UserInfoPage from 'pages/MyPage/_UserInfoPage';
import types from 'types/types';

// import LoginRequiredRoute from "utils/LoginRequiredRoute";

const layout = '/mypage';
const routes: types.RoutesTypes[] = [
  {
    collapse: true,
    name: 'My Page',
    icon: 'ni ni-ungroup text-orange',
    // state: "examplesCollapse",
    state: '',
    views: [
      {
        path: '/',
        name: 'Payment History',
        miniName: 'L',
        component: PaymentHistPage,
        layout,
      },
      {
        path: '/dm9result',
        name: 'DM9 Result',
        miniName: 'L',
        component: DM9ResultPage,
        layout,
      },
      {
        path: '/userInfo',
        name: 'User Info',
        miniName: 'L',
        component: UserInfoPage,
        layout,
      },
    ],
  },
];

export default routes;
