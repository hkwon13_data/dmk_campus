import Home from 'pages/Main/_MainPage';
import types from 'types/types';

// import LoginRequiredRoute from "utils/LoginRequiredRoute";

const layout = '/main';
const routes: types.RoutesTypes[] = [
  {
    collapse: true,
    name: 'Main Page',
    icon: 'ni ni-ungroup text-orange',
    // state: "examplesCollapse",
    state: '',
    views: [
      {
        path: '/',
        name: 'Main Home',
        miniName: 'M',
        component: Home,
        layout,
      },
      // {
      //   path: '/help',
      //   name: 'Main Help',
      //   miniName: 'H',
      //   component: HelpPage,
      //   layout,
      // },
    ],
  },
];

export default routes;
