import LoginPage from 'pages/Auth/_LoginPage';
import SignupPage from 'pages/Auth/_SignupPage';
import types from 'types/types';

// import LoginRequiredRoute from "utils/LoginRequiredRoute";

const layout = '/auth';
const routes: types.RoutesTypes[] = [
  {
    collapse: true,
    name: 'Auth Page',
    icon: 'ni ni-ungroup text-orange',
    // state: "examplesCollapse",
    state: '',
    views: [
      {
        path: '/login',
        name: 'Login',
        miniName: 'L',
        component: LoginPage,
        layout,
      },
      {
        path: '/signup',
        name: 'Signup',
        miniName: 'L',
        component: SignupPage,
        layout,
      },
    ],
  },
];

export default routes;
