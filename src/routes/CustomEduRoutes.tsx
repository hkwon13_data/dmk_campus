import CustomEduPage from 'pages/CustomEdu/_CustomEduPage';
import SpcLctPage from 'pages/CustomEdu/_SpecialLecturePage';
import types from 'types/types';

const layout = '/customEdu';
const routes: types.RoutesTypes[] = [
  {
    collapse: true,
    name: 'Custom Education Page',
    icon: 'ni ni-ungroup text-orange',
    state: '',
    views: [
      {
        path: '/',
        name: 'CustomEducation',
        miniName: 'P',
        component: CustomEduPage,
        layout,
      },
      {
        path: '/spclct',
        name: 'Special Lecture',
        miniName: 'SL',
        component: SpcLctPage,
        layout,
      },
    ],
  },
];

export default routes;
