import NoticePage from 'pages/Community/_NoticePage';
// import LctMtrPage from 'pages/Community/_LectureMaterialPage';
import NoticeInfo from 'pages/Community/NoticeInfo';
import types from 'types/types';

// import LoginRequiredRoute from "utils/LoginRequiredRoute";

const layout = '/community';
const routes: types.RoutesTypes[] = [
  {
    collapse: true,
    name: 'Community Page',
    icon: 'ni ni-ungroup text-orange',
    // state: "examplesCollapse",
    state: '',
    views: [
      {
        path: '/',
        name: 'Notice',
        miniName: 'N',
        component: NoticePage,
        layout,
      },
      {
        path: '/materials',
        name: 'Lecture Materials',
        miniName: 'L',
        component: NoticePage,
        layout,
      },
      {
        path: '/inquiry',
        name: '1:1 Inquiry',
        miniName: 'Q',
        component: NoticePage,
        layout,
      },
      {
        path: '/info',
        name: 'notice-info',
        miniName: 'Q',
        component: NoticeInfo,
        layout,
      },
    ],
  },
];

export default routes;
