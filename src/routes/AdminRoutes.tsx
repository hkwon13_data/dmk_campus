import AdminNotices from 'pages/Admin/AdminNotices';
import AdminLecture from 'pages/Admin/AdminLectureMaterials';
// import * as types from 'types/types';
// import types from "types/types";

const layout = '/admin';
const routes: any = [
  {
    collapse: true,
    name: '공지사항',
    icon: 'ni ni-shop text-primary',
    state: 'dashboardsCollapse',
    views: [
      {
        path: '/dashboard',
        name: 'Notices',
        miniName: 'D',
        component: AdminNotices,
        layout,
      },
      {
        path: '/dashboard/materials',
        name: 'Lecture Materials',
        miniName: 'L',
        component: AdminLecture,
        layout,
      },
    ],
  },
  {
    collapse: true,
    name: '결제내역',
    icon: 'ni ni-ungroup text-orange',
    state: 'examplesCollapse',
    views: [
      {
        path: '/timeline',
        name: 'Timeline',
        miniName: 'T',
        // component: Timeline,
        component: () => {},
        layout: '/admin',
      },
      {
        path: '/profile',
        name: 'Profile',
        miniName: 'P',
        // component: Profile,
        component: () => {},
        layout: '/admin',
      },
    ],
  },
  {
    collapse: true,
    name: '문의',
    icon: 'ni ni-ui-04 text-info',
    state: 'componentsCollapse',
    views: [
      {
        path: '/buttons',
        name: 'Buttons',
        miniName: 'B',
        // component: Buttons,
        component: () => {},
        layout: '/admin',
      },
      {
        path: '/cards',
        name: 'Cards',
        miniName: 'C',
        // component: Cards,
        component: () => {},
        layout: '/admin',
      },
      {
        path: '/grid',
        name: 'Grid',
        miniName: 'G',
        // component: Grid,
        component: () => {},
        layout: '/admin',
      },
    ],
  },
  {
    collapse: true,
    name: 'Users 정보',
    icon: 'ni ni-single-copy-04 text-pink',
    state: 'formsCollapse',
    views: [
      {
        path: '/elements',
        name: 'Elements',
        miniName: 'E',
        // component: Elements,
        component: () => {},
        layout: '/admin',
      },
      {
        path: '/components',
        name: 'Components',
        miniName: 'C',
        // component: Components,
        component: () => {},
        layout: '/admin',
      },
    ],
  },
];

export default routes;
