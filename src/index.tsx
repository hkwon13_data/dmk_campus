import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
// import { AppProvider } from 'store';
import Authenticated from 'authenticated';
import { Provider } from 'react-redux';
import reportWebVitals from './reportWebVitals';
import configure from './store/configure';

export const store = configure();

ReactDOM.render(
  <BrowserRouter>
    {/* <AppProvider> */}
    <Provider store={store}>
      <Authenticated />
    </Provider>
    {/* </AppProvider> */}
  </BrowserRouter>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
export type AppDispatch = typeof store.dispatch;
