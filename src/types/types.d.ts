// export interface RoutesTypes {
//  path : string,
//  name : string,
//  minName : string
//  componrnt : React.ElementType,
//  layout : string
// }
import React from 'react';

declare global {
  interface Window {
    IMP: any;
  }
}

// 라우트 타입
export interface RoutesTypes {
  collapse: boolean;
  name: string;
  icon: string;
  state: string;
  views: View[] | View;
}

export interface View {
  path: string;
  name: string;
  miniName: string;
  component: React.ReactNode;
  layout: string;
}
// 라우트 타입

// 리덕스 타입
export interface ReducerTypes {
  type: string;
  payload: boolean;
}

// 모달
export type ModalAction =
  | ReturnType<typeof setEmailCheckModal>
  | ReturnType<typeof setSignUpSuccessModal>
  | ReturnType<typeof setSearchPasswordSuccessModal>
  | ReturnType<typeof setSearchPasswordPopupModal>
  | ReturnType<typeof setSendTemporaryPasswordModal>
  | ReturnType<typeof setErrorEmailInputModal>
  | ReturnType<typeof setTermsModal>
  | ReturnType<typeof setPersnalInfoModal>
  | ReturnType<typeof setSendInquirySuccessModal>
  | ReturnType<typeof setSendInquiryErrorModal>
  | ReturnType<typeof setPasswordUpdateModal>
  | ReturnType<typeof setPasswordUpdateSuccessModal>;

export interface ModalTypes {
  authentication: {
    isOpenEmailCheck?: boolean;
    isOpenSignUpSuccess?: boolean;
    isOpenSearchPasswordSuccess?: boolean;
    isSearchPasswordPopup?: boolean;
    isSendTemporaryPasswordPopup?: boolean;
    isErrorEmailInputPopup?: boolean;
  };
  terms: {
    isTermsPopup?: boolean;
    isPersnalInfoPopup?: boolean;
  };
  community: {
    isSendInquirySuccessPopup?: boolean;
    isSendInquiryErrorPopup?: boolean;
  };
  myPage: {
    isPasswordUpdatePopup?: boolean;
    isPasswordUpdateSuccessPopup?: boolean;
  };
}
// 모달

// 데마코홀
export type DmkHallReserveAction =
  | ReturnType<typeof setDmkHallReserveValue>
  | ReturnType<typeof setInitReserveValue>;

export interface DmkHallReservationTypes {
  dmkHallReservation: DmkHallReserveInitValues;
  initReservation: DmkHallReserveInitValues;
}

export interface DmkHallReserveInitValues {
  rating: number;
  reserveTime: number;
  people: number;
  totalPrice: number;
  discountPrice: number;
  lastPrice: number;
  year: string;
  month: string;
  day: string;
  week: string | undefined;
  reserveHour: string[];
  responseData: string[];
  username: string;
  groupname: string;
  phone: string;
  email: string;
  eventname: string;
  agreementKeep: boolean;
  keepTime: string;
  keepList: string;
  agreementPhoto: boolean;
  photoTime: string;
  agreementTotal: boolean;
  agreementCollectUserInfo: boolean;
  agreementHallRules: boolean;
}
// 데마코홀

// 데마코홀 캘린더
export type DmkHallAction =
  | ReturnType<typeof getHolidays>
  | ReturnType<typeof clearHolidays>;

export type Holidays = {
  holidays: ReducerUtils<HolidaysInitial, Error>;
};

export interface HolidaysInitial {
  data: string[];
  status: number;
  statusText: string;
}

// 데마코홀 캘린더

// 커뮤니티
export type CommunityAction =
  | ReturnType<typeof getNotices>
  | ReturnType<typeof getLectureMaterials>
  | ReturnType<typeof setAuthLecturePost>
  | ReturnType<typeof setLecturePostPassword>;

export interface Community {
  notices: ReducerUtils<CommunityInitial, Error>;
  lectureMaterials: ReducerUtils<CommunityInitial, Error>;
  isPasswordCorrect: boolean;
  selectedPassword: string;
}

export interface CommunityInitial {
  data: CommunityData[];
  status: number;
  statusText: string;
}

export interface CommunityData {
  id: number;
  title: string;
  content: string;
  etc: string;
  user: string;
  password?: number;
  created_at: string;
  updated_at: string;
}

// 커뮤니티

// 마이페이지
export type MyPageAction = ReturnType<typeof getPaymentHeader>;

export interface MyPage {
  paymentHeader: ReducerUtils<MyPageInitial[], Error>;
}

export interface MyPageInitial {
  id: number;
  rating: number;
  payMethod: string;
  reserveTime: number;
  people: number;
  totalPrice: number;
  discountPrice: number;
  lastPrice: number;
  year: string;
  month: string;
  day: string;
  week: string;
  reserveHour: string[];
  responseTime: string[];
  username: string;
  groupname: string;
  phone: string;
  email: string;
  eventname: string;
  greementKeek: boolean;
  keepTime: string;
  keepList: string;
  agreementPhoto: boolean;
  photoTime: string;
  agreementTotal: boolean;
  agreementCollectUserInfo: boolean;
  agreementHallRules: boolean;
  hallComment: string;
}
// 마이페이지

// 인증
export type AuthenticationAction =
  | ReturnType<typeof getToken>
  | ReturnType<typeof postSignup>
  | ReturnType<typeof deleteToken>
  | ReturnType<typeof deleteAuthInfo>
  | ReturnType<typeof setRefresh>
  | ReturnType<typeof setUserInfo>
  | ReturnType<typeof setLoggedIn>;

export interface Authentication {
  tokenInfo: ReducerUtils<any, Error>;
  signupInfo: ReducerUtils<any, Error>;
  userInfo: {
    isAuthenticated: boolean;
    isAdmin: boolean;
    isActive: boolean;
    email: string;
    phone: string;
    username: string;
    category: string;
    company: string;
    department: string;
    position: string;
    dateJoined: Date | string;
    agreementEmail: boolean;
    agreementSNS: boolean;
  };
  isLoggedIn: boolean;
  jwtToken: string;
  refresh: string;
}

export interface SetUserInfoParameter {
  id: number;
  isAdmin: boolean;
  isAuthenticated: boolean;
  isActive: boolean;
  email: string;
  dateJoined: Date | string;
  phone: string;
  username: string;
  company: string;
  department: string;
  position: string;
  category: string;
  agreementEmail: boolean;
  agreementSNS: boolean;
  privateInfo: boolean;
  serviceContract: boolean;
  is_email_authentication: boolean;
}

// 인증

// asyncUtils
export type ReducerUtils<T, E = any> = {
  data: T | null;
  loading: boolean;
  error: E | null;
};
// asyncUtils

// 리덕스 타입
