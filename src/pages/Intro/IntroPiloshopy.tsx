/* eslint-disable no-irregular-whitespace */
import React from 'react';

// reactstrap components
import { Card, CardBody, CardTitle, Container, Row, Col } from 'reactstrap';

import iconA from 'assets/icons/intro_growth.png';
import iconB from 'assets/icons/intro_sight.png';
import iconC from 'assets/icons/intro_marketing.png';

// Core Components
export default function IntroPhiloshopy() {
  return (
    <>
      <div className='project-2 mb-8 mt-3'>
        <Container>
          <Row>
            <Col className='mx-auto text-center mb-2 mt-0' lg='8'>
              <div className='display-5'>교육 목표</div>
              <p className='h7 py-4'>
                마케터가 데이터로 사고하고,
                <br />
                데이터로 일하는 방법을알게한다
              </p>
            </Col>
          </Row>
          <Row className='justify-content-center'>
            <Col lg='4' md='6'>
              <Card className='card-project'>
                <a href='#pablo' onClick={(e) => e.preventDefault()}>
                  <div className='icon icon-lg icon-shape icon-shape-primary shadow rounded-circle mx-auto cursor-default'>
                    <div className='icon-numbers'>1</div>
                  </div>
                </a>
                <CardBody>
                  <CardTitle className='mt-3 text-center' tag='h4'>
                    <div className='icon icon-shape icon-xl mx-auto '>
                      <img src={iconA} alt='...' />
                    </div>
                  </CardTitle>
                  {/* <p className="card-description text-muted text-lg font-weight-600 text-center pt-5"> */}
                  <div className='card-desc'>
                    <br />
                    <span className='font-weight-900'>
                      고객은 항상 흔적을 남깁니다
                    </span>
                    <br />
                    디지털상에서 일어나는 고객 행동 데이터를 기반으로 마케팅
                    전략을 도출합니다.
                    <br />
                    <br />
                  </div>
                </CardBody>
              </Card>
            </Col>
            <Col lg='4' md='6'>
              <Card className='card-project'>
                <a href='#pablo' onClick={(e) => e.preventDefault()}>
                  <div className='icon icon-lg icon-shape icon-shape-primary shadow rounded-circle mx-auto cursor-default'>
                    <div className='icon-numbers'>2</div>
                  </div>
                </a>
                <CardBody>
                  <CardTitle className='mt-3 text-center' tag='h4'>
                    <div className='icon icon-shape icon-xl mx-auto'>
                      <img src={iconB} alt='...' />
                    </div>
                  </CardTitle>
                  <div
                    className='card-desc'
                    style={{
                      letterSpacing: '-0.0925rem',
                    }}
                  >
                    <br />
                    <span className='font-weight-900'>
                      교육·데이터·마케팅 전문가가 <br />
                      모였습니다.
                    </span>
                    <br />
                    ​다년간 수많은 프로젝트와 교육 경험이 축적된 전문가 그룹
                    입니다.​
                    <br />
                    <br />
                  </div>
                </CardBody>
              </Card>
            </Col>
            <Col lg='4' md='6'>
              <Card className='card-project'>
                <a href='#pablo' onClick={(e) => e.preventDefault()}>
                  <div className='icon icon-lg icon-shape icon-shape-primary shadow rounded-circle mx-auto cursor-default'>
                    <div className='icon-numbers'>3</div>
                  </div>
                </a>
                <CardBody>
                  <CardTitle className='mt-3 text-center' tag='h4'>
                    <div className='icon icon-shape icon-xl mx-auto '>
                      <img src={iconC} alt='...' />
                    </div>
                  </CardTitle>
                  <div
                    className='card-desc'
                    style={{
                      letterSpacing: '-0.0625rem',
                    }}
                  >
                    <br />
                    <span className='font-weight-900'>
                      가상의 실습데이터가 아닙니다.
                    </span>
                    <br /> 실시간으로 쌓이는 소셜데이터를 활용한 실습이
                    가능합니다. <br />
                    <br />
                    <br />
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
}
