import React from 'react';

// reactstrap components
import {
  Badge,
  Card,
  CardBody,
  Container,
  Row,
  Col,
  NavItem,
  NavLink,
  Nav,
  TabContent,
  TabPane,
} from 'reactstrap';

// Core Components
// import imgA from 'assets/img/presentation-page/harvard.jpg';
import imgB from 'assets/img/clients_logo/lg_enterprise.png';
import imgC from 'assets/img/clients_logo/md_enterprise.png';
import imgD from 'assets/img/clients_logo/public.png';
import imgE from 'assets/img/clients_logo/university.png';

export default function HistCustomerHeader() {
  const [hTabsIcons, setHTabsIcons] = React.useState('hTabsIcons-1');

  return (
    <>
      <Container className='mt-9'>
        <Row>
          <Col className='ml-auto mr-auto text-center' md='8'>
            <h3>
              <Badge pill color='primary' className='badge-sm' />
            </h3>
            <br />

            <h3 className='display-3'>고객사</h3>
            <p className='lead text-muted font-weight-800'>
              데이터마케팅캠퍼스의 교육을 수강하는 고객사를 소개합니다
            </p>
          </Col>
        </Row>
      </Container>

      <Container className='mb-7'>
        <div className='nav-wrapper'>
          <Nav
            className='nav-fill flex-column flex-md-row'
            pills
            role='tablist'
          >
            <NavItem>
              <NavLink
                className={`mb-sm-3 mb-md-0 ${
                  hTabsIcons === 'hTabsIcons-1' ? 'active' : ''
                }`}
                href='#pablo'
                onClick={(e) => {
                  e.preventDefault();
                  setHTabsIcons('hTabsIcons-1');
                }}
              >
                대기업
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={`mb-sm-3 mb-md-0 ${
                  hTabsIcons === 'hTabsIcons-2' ? 'active' : ''
                }`}
                href='#pablo'
                onClick={(e) => {
                  e.preventDefault();
                  setHTabsIcons('hTabsIcons-2');
                }}
              >
                중견기업
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={`mb-sm-3 mb-md-0 ${
                  hTabsIcons === 'hTabsIcons-3' ? 'active' : ''
                }`}
                href='#pablo'
                onClick={(e) => {
                  e.preventDefault();
                  setHTabsIcons('hTabsIcons-3');
                }}
              >
                공공기간
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={`mb-sm-3 mb-md-0 ${
                  hTabsIcons === 'hTabsIcons-4' ? 'active' : ''
                }`}
                href='#pablo'
                onClick={(e) => {
                  e.preventDefault();
                  setHTabsIcons('hTabsIcons-4');
                }}
              >
                대학
              </NavLink>
            </NavItem>
          </Nav>
        </div>
        <Card className='shadow'>
          <CardBody>
            <TabContent id='myTabContent' activeTab={hTabsIcons}>
              <TabPane tabId='hTabsIcons-1' role='tabpanel'>
                <Row className='justify-content-md-center'>
                  <Col lg='12' md='12' xs='12'>
                    <img src={imgB} className='img-fluid ' alt='...' />
                  </Col>
                </Row>
              </TabPane>
              <TabPane tabId='hTabsIcons-2' role='tabpanel'>
                <Row className='justify-content-md-center'>
                  <Col lg='12' md='12' xs='12'>
                    <div className='image'>
                      <img src={imgC} className='img-fluid' alt='...' />
                    </div>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tabId='hTabsIcons-3' role='tabpanel'>
                <Row className='justify-content-md-center'>
                  <Col lg='12' md='12' xs='12'>
                    <div className='image'>
                      <img src={imgD} className='img-fluid' alt='...' />
                    </div>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tabId='hTabsIcons-4' role='tabpanel'>
                <Row className='justify-content-md-center'>
                  <Col lg='12' md='12' xs='12'>
                    <div className='image'>
                      <img src={imgE} className='img-fluid' alt='...' />
                    </div>
                  </Col>
                </Row>
              </TabPane>
            </TabContent>
          </CardBody>
        </Card>
      </Container>
    </>
  );
}
