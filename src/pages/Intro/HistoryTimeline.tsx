/* eslint-disable react/no-unescaped-entities */
import React from 'react';

// reactstrap components
import { CardBody, CardHeader, Container, Row, Col } from 'reactstrap';

export default function HistoryTimeline() {
  return (
    <>
      <Container>
        <Row className='justify-content-center mt-7'>
          <Col lg='6'>
            <CardHeader className='bg-transparent'>
              <div className='display-5 text-center'>
                데이터마케팅캠퍼스 연혁
              </div>
              <div className='h7 text-center'>
                데이터마케팅캠퍼스가 걸어온 길
              </div>
            </CardHeader>
            <CardBody className='mt-5 container'>
              <div
                className='timeline timeline-one-side'
                data-timeline-axis-style='dashed'
                data-timeline-content='axis'
              >
                <div className='timeline-block'>
                  <span className='timeline-step badge-info'>
                    <i className='ni ni-bulb-61' />
                  </span>
                  <div className='timeline-content'>
                    <h3 className='font-weight-800'>2021</h3>
                    <p className='text-sm mt-1 mb-0 time-line-text-length'>
                      04 &nbsp;&nbsp;&nbsp;&nbsp;삼성카드 디지털 실무자 특강
                      <br />
                      04 &nbsp;&nbsp;&nbsp;&nbsp;Real MBA Digital Transformation
                      오프라인 특강
                      <br />
                      03 &nbsp;&nbsp;&nbsp;&nbsp;성균관대학교 2021-1학기
                      '데이터마케팅' 강의
                      <br />
                      03 &nbsp;&nbsp;&nbsp;&nbsp;KB 국민은행 개인 마케터 양성
                      기업 맞춤 교육
                      <br />
                      03 &nbsp;&nbsp;&nbsp;&nbsp;파리크라상 데이터 마케팅 기업
                      맞춤 교육
                      <br />
                      03 &nbsp;&nbsp;&nbsp;&nbsp;암웨이 마케팅솔루션 온라인 교육
                      <br />
                      03 &nbsp;&nbsp;&nbsp;&nbsp;세종대학교 데이터리터러시 특강
                      <br />
                      02 &nbsp;&nbsp;&nbsp;&nbsp;삼성금융연수원 데이터마케팅
                      특강
                      <br />
                      01 &nbsp;&nbsp;&nbsp;&nbsp;KDB 신입행원 과정개발 및 교육
                      <br />
                      01 &nbsp;&nbsp;&nbsp;&nbsp;SK 이노베이션 온라인 교육
                      콘텐츠 공급
                      <br />
                      01 &nbsp;&nbsp;&nbsp;&nbsp;레오버넷㈜ 데이터마케팅 9대
                      영역(DM9) 교육
                    </p>
                  </div>
                </div>
                <div className='timeline-block'>
                  <span className='timeline-step badge-info'>
                    <i className='ni ni-bulb-61' />
                  </span>
                  <div className='timeline-content'>
                    <h3 className='font-weight-800'>2020</h3>
                    <p className='text-sm mt-1 mb-0 time-line-text-length'>
                      11 &nbsp;&nbsp;&nbsp;&nbsp;데이터 마케팅 서밋 "데이터는
                      사람을 행동하게 한다" 개최 <br />
                      11 &nbsp;&nbsp;&nbsp;&nbsp;CJ 제일제당 데이터마케팅 특강{' '}
                      <br />
                      11 &nbsp;&nbsp;&nbsp;&nbsp;현대자동차 공통직무역량
                      고객분석의 시작, 빅데이터 따라하기 과정 교육 <br />
                      11 &nbsp;&nbsp;&nbsp;&nbsp;인사혁신처 디지털 역량강화 교육
                      <br />
                      11 &nbsp;&nbsp;&nbsp;&nbsp;한국문화정보원 2020 문화데이터
                      전문인력 양성교육
                      <br />
                      10 &nbsp;&nbsp;&nbsp;&nbsp;CJ오쇼핑 데이터활용 특강
                      <br />
                      10 &nbsp;&nbsp;&nbsp;&nbsp;오픈놀 미니인턴 과정 교육 진행
                      <br />
                      10 &nbsp;&nbsp;&nbsp;&nbsp;한국관광공사 데이터리터러시
                      교육
                      <br />
                      10 &nbsp;&nbsp;&nbsp;&nbsp;서강대학교 데이터마케팅 특강
                      <br />
                      09 &nbsp;&nbsp;&nbsp;&nbsp;팜스코 데이터마케팅 교육
                      <br />
                      09 &nbsp;&nbsp;&nbsp;&nbsp;다우엔터프라이즈 데이터마케팅
                      특강
                      <br />
                      09 &nbsp;&nbsp;&nbsp;&nbsp;농심인재원 데이터마케팅 특강
                      <br />
                      09 &nbsp;&nbsp;&nbsp;&nbsp;한독 HTP 프로그램_브랜드 경쟁력
                      분석, 콘텐츠 제작 및 성과측정 교육 과정
                      <br />
                      08 &nbsp;&nbsp;&nbsp;&nbsp;연세대학교 연세영CEO 과정 교육
                      <br />
                      07 &nbsp;&nbsp;&nbsp;&nbsp;한국문화정보원 2020 문화데이터
                      전문인력 양성교육
                      <br />
                      07 &nbsp;&nbsp;&nbsp;&nbsp;예술경영지원센터 빅데이터
                      심화과정
                      <br />
                      06 &nbsp;&nbsp;&nbsp;&nbsp;KGC_정관장/데이터마케팅 교육
                      <br />
                      06 &nbsp;&nbsp;&nbsp;&nbsp;한독 HTP 프로그램_고객데이터
                      결합분석
                      <br />
                      06 &nbsp;&nbsp;&nbsp;&nbsp;토니모리 데이터마케팅
                      9대영역(DM9) 교육
                      <br />
                      05 &nbsp;&nbsp;&nbsp;&nbsp;신한은행 데이터마케팅 강의
                      <br />
                      05 &nbsp;&nbsp;&nbsp;&nbsp;한국섬유산업연합회
                      디지털마케팅전략수립 교육
                      <br />
                      01 &nbsp;&nbsp;&nbsp;&nbsp;오라클 데이터마케팅 특강
                      <br />
                      01 &nbsp;&nbsp;&nbsp;&nbsp;DB그룹 우수인재 특강
                      <br />
                      01 &nbsp;&nbsp;&nbsp;&nbsp;DB그룹 신입사원 특강
                      <br />
                      01 &nbsp;&nbsp;&nbsp;&nbsp;이마트 관리자 코칭 교육 과정
                      <br />
                      01 &nbsp;&nbsp;&nbsp;&nbsp;부경대학교 데이터마케팅 입문
                      과정
                      <br />
                      01 &nbsp;&nbsp;&nbsp;&nbsp;KOBACO 데이터마케팅 특강
                    </p>
                  </div>
                </div>
                <div className='timeline-block'>
                  <span className='timeline-step badge-info'>
                    <i className='ni ni-bulb-61' />
                  </span>
                  <div className='timeline-content'>
                    <h3 className='font-weight-800'>2019</h3>
                    <p className='text-sm mt-1 mb-0 time-line-text-length'>
                      11 &nbsp;&nbsp;&nbsp;&nbsp;데이터마케팅 방법론 ‘DM9’ 런칭
                      <br />
                      10 &nbsp;&nbsp;&nbsp;&nbsp;한국문화정보원 빅데이터 분석 및
                      활용 교육
                      <br />
                      05 &nbsp;&nbsp;&nbsp;&nbsp;서울과학종합대학원 SNS 데이터
                      분석 및 마케팅 교육
                      <br />
                      05 &nbsp;&nbsp;&nbsp;&nbsp;엠티엠 주식회사 '빅데이터로
                      마케팅 혁신' 교육
                      <br />
                      04 &nbsp;&nbsp;&nbsp;&nbsp;KCC 데이터 마케팅 이노베이션
                      교육
                      <br />
                      05 &nbsp;&nbsp;&nbsp;&nbsp;사옥 지하에 자체 강연장
                      '데마코홀’ 오픈
                      <br />
                      01 &nbsp;&nbsp;&nbsp;&nbsp;농협중앙회 데이터 마케팅
                      이노베이션 교육
                      <br />
                    </p>
                  </div>
                </div>
                <div className='timeline-block'>
                  <span className='timeline-step badge-info'>
                    <i className='ni ni-bulb-61' />
                  </span>
                  <div className='timeline-content'>
                    <h3 className='font-weight-800'>2018</h3>
                    <p className='text-sm mt-1 mb-0 time-line-text-length'>
                      12 &nbsp;&nbsp;&nbsp;&nbsp;국립극장 마케팅 데이터 분석 및
                      디지털 마케팅 교육
                      <br />
                      12 &nbsp;&nbsp;&nbsp;&nbsp;육군본부 외부데이터 분석, 실습
                      Workshop
                      <br />
                      04 &nbsp;&nbsp;&nbsp;&nbsp;교육서비스 ‘데이터마케팅캠퍼스’
                      런칭 <br />
                    </p>
                  </div>
                </div>
                <div className='timeline-block'>
                  <span className='timeline-step badge-info'>
                    <i className='ni ni-bulb-61' />
                  </span>
                  <div className='timeline-content'>
                    <h3 className='font-weight-800'>2017</h3>
                    <p className='text-sm mt-1 mb-0 time-line-text-length'>
                      11 &nbsp;&nbsp;&nbsp;&nbsp;예술경영지원센터
                      예술경영아카데미 정규과정 ‘데이터 마케팅 전략 수립’ 과정
                      교육
                      <br />
                      04 &nbsp;&nbsp;&nbsp;&nbsp;강원대학교, 상명대학교 산학협력
                      체결
                      <br />
                    </p>
                  </div>
                </div>
              </div>
            </CardBody>
          </Col>
        </Row>
      </Container>
    </>
  );
}
