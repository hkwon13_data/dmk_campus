import React, { useEffect } from 'react';

import CommonFooter from 'components/NavBar/CommonFooter';
import CommonNavbar from 'components/NavBar/CommonNavbar';
import { ScrollTopButton } from 'components/utils/ScrollTopButton';
import FloatingButton from '../../components/NavBar/FloatingButton';

import DMCHeader from './DMCHeader';
import DMCPeople from './DMCPeople';

export default function PeoplePage() {
  useEffect(() => {
    // let classList = document.body.classList;
    // document.body.classList.remove(...classList);
    // document.body.classList.add('index-page');
    window.scrollTo(0, 0);
    // return function cleanup() {
    //   document.body.classList.remove('index-page');
    // };
  }, []);

  return (
    <>
      <ScrollTopButton />
      <CommonNavbar type='primary' />
      <FloatingButton />
      <div className='wrapper'>
        <DMCHeader />
        <DMCPeople />
      </div>
      <CommonFooter />
    </>
  );
}
