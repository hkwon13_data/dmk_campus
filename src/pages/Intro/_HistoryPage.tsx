import React, { useEffect } from 'react';
// import { Container, Row, Button } from 'reactstrap';

import CommonFooter from 'components/NavBar/CommonFooter';
import CommonNavbar from 'components/NavBar/CommonNavbar';
import { ScrollTopButton } from 'components/utils/ScrollTopButton';

import HistoryHeader from './HistoryHeader';
import CustomerHeader from './HistoryCustomersHeader';
import FloatingButton from '../../components/NavBar/FloatingButton';

export default function HistoryPage() {
  useEffect(() => {
    // let classList = document.body.classList;
    // document.body.classList.remove(...classList);
    // document.body.classList.add('index-page');
    window.scrollTo(0, 0);
    // return function cleanup() {
    //   document.body.classList.remove('index-page');
    // };
  }, []);

  return (
    <>
      <ScrollTopButton />
      <CommonNavbar type='transparent' />
      <FloatingButton />
      {/* <div className="wrapp/r"/ */}
      <HistoryHeader />
      {/* <HistFeatures /> */}
      {/* <HistoryDM9 /> */}
      <CustomerHeader />
      {/* </div/ */}
      <CommonFooter />
    </>
  );
}
