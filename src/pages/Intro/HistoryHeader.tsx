import React from 'react';

// reactstrap components
import { Container, Row } from 'reactstrap';

// import videoA from "https://s3.ap-northeast-2.amazonaws.com/media.marketingcampus.co.kr/DM9_%ED%99%8D%EB%B3%B4%EC%98%81%EC%83%81/DM9+%ED%99%8D%EB%B3%B4%EC%98%81%EC%83%81_+1201+%EC%B5%9C%EC%A2%85.mp4";
// import videoA from "assets/videos/dm9_into_history_video.mp4";

export default function HistoryHeader() {
  return (
    <>
      <div>
        <div
          style={{
            position: 'absolute',
            top: '15%',
            left: '50%',
            transform: 'translate(-50%, 0)',
            width: '80%',
            textAlign: 'center',
          }}
          className='h2 font-weight-900 text-white'
        >
          세상에 없던 데이터 마케팅 교육,{' '}
          <span className='h1 text-white font-weight-900'>
            데이터 마케팅의 답을
          </span>{' '}
          제시하다
        </div>
        <div
          className='page-header-image'
          style={{
            background: '#000000',
            height: '90vh',
            backgroundSize: '100% 100%',
            zIndex: -123,
          }}
        >
          <div className='overlay'>
            <Container>
              <Row className='justify-content-center'>
                <div
                  style={{
                    position: 'absolute',
                    top: '55%',
                    left: '50%',
                    transform: 'translate(-50%, -50%)',
                  }}
                >
                  <video
                    className='container-video mt-5'
                    onLoadedMetadata={(e: any) =>
                      e.target.currentTime === e.target.currentTime + 3
                    }
                    loop
                    controls
                    muted
                    autoPlay
                    playsInline
                  >
                    <source
                      src='https://s3.ap-northeast-2.amazonaws.com/media.marketingcampus.co.kr/DM9_%ED%99%8D%EB%B3%B4%EC%98%81%EC%83%81/dm9_into_history_video.mp4'
                      type='video/mp4'
                    />
                    <track
                      src='captions_en.vtt'
                      kind='captions'
                      label='english_captions'
                    />
                  </video>
                </div>
              </Row>
            </Container>
          </div>
        </div>
      </div>
      {/* <Container>
				<Row className="my-5 mt-9 content-center ">
					<Col lg="12" md="8">
						<div className="display-5 text-center text-size-custom">
							세상에 없던 데이터 마케팅 교육 <br />
							데이터 마케팅의 답을 제시하다
						</div>
					</Col>
				</Row>
			</Container> */}
    </>
  );
}
