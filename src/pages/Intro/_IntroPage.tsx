import React, { useEffect } from 'react';
// import { Container, Row, Button } from 'reactstrap';

import CommonFooter from 'components/NavBar/CommonFooter';
import CommonNavbar from 'components/NavBar/CommonNavbar';
import { ScrollTopButton } from 'components/utils/ScrollTopButton';

// import IntroHeader from './IntroHeader';
import IntroPiloshopy from './IntroPiloshopy';
import Feature from './IntroFeatures';
import ContactUs from './IntroContactUs';
// import HistoryTimeline from './HistoryTimeline.js';
// import HistoryInterview from './HistoryInterview';
import CustomerHeader from './HistoryCustomersHeader';
import HistoryHeader from './HistoryHeader';
import FloatingButton from '../../components/NavBar/FloatingButton';

export default function IntroPage() {
  useEffect(() => {
    // let classList = document.body.classList;
    // document.body.classList.remove(...classList);
    // document.body.classList.add('index-page');
    window.scrollTo(0, 0);
    // return function cleanup() {
    //   document.body.classList.remove('index-page');
    // };
  }, []);

  return (
    <>
      <ScrollTopButton />
      <CommonNavbar type='transparent' />
      <div className='wrapper'>
        {/* <IntroHeader /> */}
        <HistoryHeader />
        <FloatingButton />
        <IntroPiloshopy />
        {/* <HistoryInterview /> */}
        <Feature />
        {/* <HistoryTimeline /> */}
        <CustomerHeader />

        <ContactUs />
      </div>
      <CommonFooter />
    </>
  );
}
