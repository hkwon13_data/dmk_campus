import React from 'react';

// reactstrap components
import { Container, Row, Col } from 'reactstrap';

// Core Components

function ContactUs() {
  return (
    <>
      <div className='contactus-3'>
        <Container className='my-5'>
          <Row>
            <Col className='text-center my-6' md='12'>
              <h1 className='display-5'>Contact us</h1>
            </Col>
          </Row>
          <Row>
            <Col lg='4' md='4' xs='12'>
              <div className='info info-hover'>
                <div className='icon icon-shape icon-shape-primary icon-md rounded-circle text-white'>
                  <i className='ni ni-email-83' />
                </div>
                <h4 className='info-title'>Email</h4>
                <p className='description px-0 text-muted'>
                  dataedu@datamarketing.co.kr
                </p>
              </div>
            </Col>
            <Col lg='4' md='4' xs='12'>
              <div className='info info-hover'>
                <div className='icon icon-shape icon-shape-primary icon-md rounded-circle text-white'>
                  <i className='ni ni-mobile-button' />
                </div>
                <h4 className='info-title'>Phone</h4>
                <p className='description px-0 text-muted'>
                  02-6011-5407, 5418
                </p>
              </div>
            </Col>
            <Col lg='4' md='4' xs='12'>
              <div className='info info-hover'>
                <div className='icon icon-shape icon-shape-primary icon-md rounded-circle text-white'>
                  <i className='ni ni-square-pin' />
                </div>
                <h4 className='info-title'>Address</h4>
                <p className='description px-0 text-muted'>
                  서울특별시 강남구 테헤란로 53길 16 <br />
                  3층(역삼동, 예안빌딩)
                </p>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
}

export default ContactUs;
