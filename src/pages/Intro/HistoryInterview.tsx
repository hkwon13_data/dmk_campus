import React from 'react';

// reactstrap components
import { Row, Col, Container } from 'reactstrap';

// Core Components

// import imgA from 'assets/img/intro_hist_bg.png';
import ProfileCard from './HistoryInterviewProfileCard';

interface InterviewItemsTypes {
  company: string;
  position: string;
  desc: string;
}

const items: InterviewItemsTypes[] = [
  {
    company: 'IT 회사',
    position: '마케팅 7년차 A님',
    desc: '변화하는 트렌드와 데이터 변화에 적응하는 발 빠른 능력은 어떻게 기를 수 있나요?',
  },
  {
    company: '금융',
    position: '컨설팅 1년차 H님',
    desc: '수 많은 데이터에서 어떻게 최적의 시장에 맞는 인사이트를 도출 할 수 있나요?',
  },
  {
    company: '스포츠의류',
    position: '마케팅 5년차 A님',
    desc: '사람들이 우리 브랜드에 대해 어떻게 생각하는지 알 수 있는 방법은 무엇인가요?',
  },
];

export default function HistoryInterview() {
  return (
    <>
      <div className='content-center'>
        <Container>
          <Row className='pt-3 mt-2 mb-3'>
            <Col className='mx-auto text-center' md='6'>
              <h3 className='title font-weight-800'>
                데이터마케팅의 A to Z!
                <br /> 마케터의 난제점을 <br />
                9가지 영역 안에서 모두 해결해드립니다
              </h3>
              <div className='lead'>실제 현업에 있는 마케터의 Real Voice</div>
            </Col>
          </Row>
          <Row className='justify-content-center'>
            {items.map((item, key) => {
              return (
                // eslint-disable-next-line react/no-array-index-key
                <Col lg='3' md='6' className='mt-4 mb-lg-8' key={key}>
                  <ProfileCard
                    company={item.company}
                    position={item.position}
                    desc={item.desc}
                  />
                </Col>
              );
            })}
          </Row>
        </Container>
      </div>
    </>
  );
}
