import React from 'react';

// reactstrap components
import { Container, Row, Col } from 'reactstrap';

// Core Components
import imgA from 'assets/img/intro_professors/jin-hyung-lee.png';
// import imgB from 'assets/img/intro_professors/jang-jung-kim.png';
import imgC from 'assets/img/intro_professors/yung-chan-yun.png';
// import imgD from 'assets/img/intro_professors/hyung-su-kang.png';
// import imgE from 'assets/img/intro_professors/seok-hm-choi.png';
import imgF from 'assets/img/intro_professors/sang-hyun-park.png';
// import imgG from 'assets/img/intro_professors/woo-jeong-kim.png';

export default function DMCPeople() {
  return (
    <>
      <section className='mt-9'>
        <Container>
          <Row>
            <Col className='ml-auto mr-auto text-center' md='8'>
              <p className='display-5'>전임 교수진</p>
              <div className='h7 font-weight-300'>
                데이터마케팅 캠퍼스를 이끄는 교육 전문가
              </div>
            </Col>
          </Row>
          <Row className='justify-content-center my-8'>
            <Col
              className='mt-5 mb-7 mt-sm-0 px-sm-5 px-md-5'
              sm='6'
              xs='6'
              lg='4'
            >
              <img
                alt='...'
                className=' img-fluid rounded-circle shadow-lg'
                src={imgA}
              />
              <div className='lecture-name mt-5'>이진형</div>
            </Col>
            <Col
              className='mt-5 mb-7 mt-sm-0 px-sm-5 px-md-5'
              sm='6'
              xs='6'
              lg='4'
            >
              <img
                alt='...'
                className=' img-fluid rounded-circle shadow-lg'
                src={imgF}
              />
              <div className='lecture-name mt-5'>박상현</div>
            </Col>
            <Col
              className='mt-5 mb-7 mt-sm-0 px-sm-5 px-md-5'
              sm='6'
              xs='6'
              lg='4'
            >
              <img
                alt='...'
                className=' img-fluid rounded-circle shadow-lg'
                src={imgC}
              />
              <div className='lecture-name mt-5'>윤용찬</div>
            </Col>
          </Row>
        </Container>
      </section>
    </>
  );
}
