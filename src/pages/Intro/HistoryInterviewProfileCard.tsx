import React from 'react';

// reactstrap components
import { Card, CardBody, CardTitle } from 'reactstrap';

// Core Components
// import imgA from 'assets/img/faces/team-1.jpg';
interface InterviewItemsTypes {
  company: string;
  position: string;
  desc: string;
}

export default function HistProfileCard(props: InterviewItemsTypes) {
  const { company, position, desc } = props;
  return (
    <>
      <Card className='card-profile' data-image='img-rounded'>
        <div className='card-avatar my-3 shadow-none'>
          <div className='icon-shape icon-xl mx-auto '>
            <i className='ni ni-circle-08' />
          </div>
        </div>
        <CardBody>
          <h6 className='card-category text-gray font-weight-800'>{company}</h6>
          <CardTitle className='font-weight-700' tag='h4'>
            {position}
          </CardTitle>
          <p
            className='card-description font-weight-300 p-4 text-sm text-left'
            style={{ fontSize: '1.3rem' }}
          >
            {desc}
            <br />
            <br />
            <br />
          </p>
        </CardBody>
      </Card>
    </>
  );
}
