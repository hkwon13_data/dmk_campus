import React from 'react';

// reactstrap components
import { Button, Card, CardBody, Container, Row, Col } from 'reactstrap';

import { useHistory } from 'react-router-dom';

// Core Components

function Feature() {
  const history = useHistory();

  return (
    <>
      <div className='section features-7 bg-secondary'>
        <Container className='my-6'>
          <Row>
            <Col className='text-center mx-auto' md='8'>
              <div className='display-5'>사업 소개</div>
              <p className='h7 py-4'>
                고객의 필요에 맞춰 데이터마케팅에 대한 <br />
                최적의 교육과 인프라를 제공합니다
              </p>
            </Col>
          </Row>
          <Row className='mt-5'>
            <Col lg='12'>
              <Row className='row-grid justify-content-center'>
                <Col lg='3' className='text-left'>
                  <Card className='card-lift--hover shadow border-0'>
                    <CardBody className='py-5'>
                      <div className='icon icon-shape icon-shape-primary rounded-circle mb-4'>
                        <i className='ni ni-check-bold' />
                      </div>
                      <p className='h5 font-weight-700'>[기업교육]</p>
                      <p className='intro-card-desc font-weight-500'>
                        마케팅 의사결정, 왜 데이터로 <br />
                        해야 할까요?
                        <br />
                        그 해답을 DMK에서 알려드립니다.
                        <br />
                        수많은 최상위 기업들이 이미 함께 하고 있습니다.
                        <br />
                        <br />
                      </p>
                      <div className='text-center'>
                        <Button
                          className='mt-4 px-4'
                          color='primary'
                          onClick={() => history.push('/customEdu/')}
                        >
                          기업 교육 자세히 보기
                        </Button>
                      </div>
                    </CardBody>
                  </Card>
                </Col>
                <Col lg='3' className='text-left'>
                  <Card className='card-lift--hover shadow border-0'>
                    <CardBody className='py-5'>
                      <div className='icon icon-shape icon-shape-primary rounded-circle mb-4'>
                        <i className='ni ni-check-bold' />
                      </div>
                      <p className='h5 font-weight-700'>[오픈 과정]</p>
                      <p className='intro-card-desc font-weight-500'>
                        마케팅 실무자들의 고민을 담아
                        <br />
                        만들었습니다.
                        <br />
                        마케팅 건강검진부터 기획, 이행, <br />
                        성과측정까지 데이터마케팅
                        <br />전 영역에 걸쳐 체계적인 교육 커리큘럼을
                        제공합니다.
                      </p>
                      <div className='text-center'>
                        <Button
                          className='mt-4'
                          color='primary'
                          onClick={() => history.push('/openProcess/')}
                        >
                          DM9 정규 과정 자세히 보기
                        </Button>
                      </div>
                    </CardBody>
                  </Card>
                </Col>
                <Col lg='3' className='text-left'>
                  <Card className='card-lift--hover shadow border-0'>
                    <CardBody className='py-5'>
                      <div className='icon icon-shape icon-shape-primary rounded-circle mb-4'>
                        <i className='ni ni-check-bold' />
                      </div>
                      <p className='h5 font-weight-700'>[데마코홀 대관]</p>
                      <p className='intro-card-desc font-weight-500'>
                        대형 LED 스크린으로 ON · OFF <br />
                        모두 생생하게!
                        <br />
                        선릉역 초역세권 다목적 공간!
                        <br />
                        <br />
                        <br />
                        <br />
                      </p>
                      <div className='text-center'>
                        <Button
                          className='mt-4 px-5'
                          color='primary'
                          onClick={() => history.push('/dmkHall/reservation')}
                        >
                          대관 예약 바로가기
                        </Button>
                      </div>
                    </CardBody>
                  </Card>
                </Col>
                {/* <Col lg="3" className="text-left">
									<Card className="card-lift--hover shadow border-0">
										<CardBody className="py-5">
											<div className="icon icon-shape icon-shape-primary rounded-circle mb-4">
												<i className="ni ni-check-bold"></i>
											</div>
											<p className="h5 font-weight-700">[데마코홀 대관]</p>
											<p className="intro-card-desc font-weight-500">
												대형 LED 스크린으로 <br />
												ON-OFF 모두 생생하게! <br />
												선릉역 초역세권 다목적 공간! <br />
												<br />
											</p>
											<Button
												className="mt-4"
												color="primary"
												href="#pablo"
												onClick={(e) => console.log("clicked 데마코홀 대관")}>
												확인하기
											</Button>
										</CardBody>
									</Card>
								</Col> */}
              </Row>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
}

export default Feature;
