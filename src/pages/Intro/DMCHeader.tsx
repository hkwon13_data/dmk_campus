import React from 'react';

// Core Components

import imgA from 'assets/img/intro_people_bg.png';

function DMCHeader() {
  return (
    <>
      <header className='header-2 '>
        <div className='page-header'>
          <div
            className='page-header-image'
            style={{
              backgroundImage: `url(${imgA})`,
            }}
          />
        </div>
      </header>
    </>
  );
}

export default DMCHeader;
