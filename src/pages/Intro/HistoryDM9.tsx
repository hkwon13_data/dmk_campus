/* eslint-disable react/no-unescaped-entities */
import React from 'react';

// reactstrap components
import { Container, Row, Col, Card, CardBody, Badge } from 'reactstrap';
// import imgA from 'assets/img/ill/inn.svg';
// Core Components

// interface itemsTypes {
//   id: number;
//   title: string;
//   desc: string;
// }

// const items: itemsTypes = [
//   {
//     id: 1,
//     title: '시장/트렌드 분석',
//     desc: '자사 제품부터<br />라이프 스타일까지! <br /> 시장을 분석하고 <br /> 예측할 수 있다',
//   },
// ];

export default function HistoryDM9() {
  return (
    <>
      <Container className='mt-6'>
        <Row>
          <Col className='text-center mx-auto' md='8'>
            <h4>
              <Badge pill color='primary' className='badge-sm'>
                Primary
              </Badge>
            </h4>
            <br />
            <h3 className='display-3'>데이터마케팅의 9대 영역을 소개합니다</h3>
          </Col>
        </Row>

        <Row className='justify-content-center'>
          <Col lg='3' md='6' className='mt-5'>
            <Card className='card-profile' data-image='img-rounded'>
              <div className='h3 mb-2 text-left pl-3 pt-2 font-weight-900'>
                1 <span className='text-sm font-weight-900'>영역</span>
              </div>
              <div className='h3 mb-2 text-primary font-weight-800 px-2'>
                시장/트렌드 분석
              </div>
              <CardBody>
                <p className='card-description text-center text-muted font-weight-800'>
                  자사 제품부터
                  <br />
                  라이프 스타일까지! <br /> 시장을 분석하고 <br /> 예측할 수
                  있다
                </p>
              </CardBody>
            </Card>
          </Col>
          <Col lg='3' md='6' className='mt-5'>
            <Card className='card-profile' data-image='img-rounded'>
              <div className='h3 mb-2 text-left pl-3 pt-2 font-weight-900'>
                2 <span className='text-sm font-weight-900'>영역</span>
              </div>
              <div className='h3 mb-2 text-primary font-weight-800 px-2'>
                고객 분석
              </div>
              <CardBody>
                <p className='card-description text-center text-muted font-weight-800'>
                  고객이 남긴
                  <br />
                  디지털 발자국을 보면 <br /> 진짜 내 고객을 <br /> 알 수 있다!
                </p>
              </CardBody>
            </Card>
          </Col>
          <Col lg='3' md='6' className='mt-5'>
            <Card className='card-profile' data-image='img-rounded'>
              <div className='h3 mb-2 text-left pl-3 pt-2 font-weight-900'>
                3 <span className='text-sm font-weight-900'>영역</span>
              </div>
              <div className='h3 mb-2 text-primary font-weight-800 px-2'>
                브랜드 경쟁력 분석
              </div>
              <CardBody>
                <p className='card-description text-center text-muted font-weight-800'>
                  내가 생각하는
                  <br />
                  우리 브랜드 VS 고객이 생각
                  <br />
                  하는 우리 브랜드 Gap을 <br />
                  좁혀야 경쟁에서 승리한다!
                </p>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row className='justify-content-center'>
          <Col lg='3' md='6'>
            <Card className='card-profile' data-image='img-rounded'>
              <div className='h3 mb-2 text-left pl-3 pt-2 font-weight-900'>
                4 <span className='text-sm font-weight-900'>영역</span>
              </div>
              <div className='h3 mb-2 text-primary font-weight-800 px-2'>
                채널 경쟁력 분석
              </div>
              <CardBody>
                <p className='card-description text-center text-muted font-weight-800'>
                  자사 마케팅 채널의
                  <br />
                  건강상태(DHC)를 <br />
                  파악할 수 있다 <br />
                  <br />
                </p>
              </CardBody>
            </Card>
          </Col>
          <Col lg='3' md='6'>
            <Card className='card-profile' data-image='img-rounded'>
              <div className='h3 mb-2 text-left pl-3 pt-2 font-weight-900'>
                5 <span className='text-sm font-weight-900'>영역</span>
              </div>
              <div className='h3 mb-2 text-primary font-weight-800 px-2'>
                콘텐츠 경쟁력 분석
              </div>
              <CardBody>
                <p className='card-description text-center text-muted font-weight-800'>
                  내 고객이 반응하는 <br />
                  대박 콘텐츠의 비밀, <br />
                  콘텐츠 DPI* 속에 있다 <br />
                  <span className='text-sm'>*DPI: Digital Power Index</span>
                </p>
              </CardBody>
            </Card>
          </Col>
          <Col lg='3' md='6'>
            <Card className='card-profile' data-image='img-rounded'>
              <div className='h3 mb-2 text-left pl-3 pt-2 font-weight-900'>
                6 <span className='text-sm font-weight-900'>영역</span>
              </div>
              <div className='h3 mb-2 text-primary font-weight-800 px-2'>
                퍼보먼스 마케팅
              </div>
              <CardBody>
                <p className='card-description text-center text-muted font-weight-800'>
                  적은 예산으로
                  <br />
                  최적의 광고를
                  <br />
                  기획해보자 <br />
                  <br />
                </p>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row className='justify-content-center'>
          <Col lg='3' md='6'>
            <Card className='card-profile' data-image='img-rounded'>
              <div className='h3 mb-2 text-left pl-3 pt-2 font-weight-900'>
                7 <span className='text-sm font-weight-900'>영역</span>
              </div>
              <div className='h3 mb-2 text-primary font-weight-800 px-2'>
                데이터리터러시
              </div>
              <CardBody>
                <p className='card-description text-center text-muted font-weight-800'>
                  내게 필요한 데이터,
                  <br />
                  읽고 쓰고 맛보고 싶다면? <br /> 데이터 리터러시 배우러 <br />{' '}
                  GoGo
                </p>
              </CardBody>
            </Card>
          </Col>
          <Col lg='3' md='6'>
            <Card className='card-profile' data-image='img-rounded'>
              <div className='h3 mb-2 text-left pl-3 pt-2 font-weight-900'>
                8 <span className='text-sm font-weight-900'>영역</span>
              </div>
              <div className='h3 mb-2 text-primary font-weight-800 px-2'>
                데이터마케팅 방법론
              </div>
              <CardBody>
                <p className='card-description text-center text-muted font-weight-800'>
                  어려운 머신러닝과 인공지능 <br />
                  간단한 개념만 알아도 <br />
                  데이터 기술자와 <br />
                  대화할 수 있다!
                </p>
              </CardBody>
            </Card>
          </Col>
          <Col lg='3' md='6'>
            <Card className='card-profile' data-image='img-rounded'>
              <div className='h3 mb-2 text-left pl-3 pt-2 font-weight-900'>
                9 <span className='text-sm font-weight-900'>영역</span>
              </div>
              <div className='h3 mb-2 text-primary font-weight-800 px-2'>
                데이터 윤리
              </div>
              <CardBody>
                <p className='card-description text-center text-muted font-weight-800'>
                  '아' 다르고
                  <br />
                  '어' 다른 데이터?
                  <br />
                  데이터 속지 말고 <br />
                  속이지도 말자!
                </p>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
}
