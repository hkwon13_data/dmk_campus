import React from 'react';

// reactstrap components
import { Container, Row, Col, Badge } from 'reactstrap';

// Core Components

import imgA from 'assets/img/intro_main_header.png';

export default function IntroHeader() {
  return (
    <>
      {/* <div className="page-header header-filter page-header-small skew-separator skew-mini"> */}
      <div className='page-header header-filter'>
        <div
          className='page-header-image'
          style={{
            backgroundImage: 'url(' + imgA + ')',
          }}
        ></div>
        <div className='separator separator-bottom separator-skew'>
          <svg
            xmlns='http://www.w3.org/2000/svg'
            preserveAspectRatio='none'
            version='1.1'
            viewBox='0 0 2560 100'
            x='0'
            y='0'
          >
            <polygon
              className='fill-white'
              points='2560 0 2560 100 0 100'
            ></polygon>
          </svg>
        </div>
      </div>
    </>
  );
}
