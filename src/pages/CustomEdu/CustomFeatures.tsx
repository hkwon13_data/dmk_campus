import React from 'react';

// reactstrap components
import { Card, CardTitle, CardBody, Container, Row, Col } from 'reactstrap';

// Core Components

// import imgA from 'assets/img/custom_edu/edu_features.png';
// import imgB from 'assets/img/custom_edu/edu_process.png';

import Img1 from 'assets/img/custom_edu/edu_glass.png';
import Img2 from 'assets/img/custom_edu/edu_led.png';
import Img3 from 'assets/img/custom_edu/edu_document.png';
import Img4 from 'assets/img/custom_edu/edu_evaluate.png';

const style = { width: '18rem' };
// const cardHeight = { height: '668px' };
const imgwh = { width: '100px', height: '100px' };

export default function CustomFeatures() {
  return (
    <>
      <div className='section features features-5'>
        <Container>
          <Row className='mt-lg-5'>
            <Col className='mx-auto text-center mt-lg-5' lg='8'>
              <p className='display-5'>기업 맞춤 교육의 강점</p>
              <p className='h7'>
                데이터마케팅캠퍼스의 기업 맞춤 교육은 다양한 Needs에 맞게 적용
                가능합니다
              </p>
            </Col>
          </Row>

          <Row className='justify-content-md-center mt-4'>
            <Col lg='3' md='6' xs='12'>
              {/* <div className="image">
								<img src={imgA} className="img-fluid" alt="..." />
							</div> */}
              <Card style={style} className='text-center'>
                <CardBody className='px-5' style={{ background: '#efeded' }}>
                  <CardTitle className='font-weight-900 m-0 p-1' tag='h3'>
                    마케터 대상 <br />
                    직무 교육
                  </CardTitle>
                </CardBody>
                <CardBody className='py-5'>
                  <div className='mb-3 h5'>
                    전문 마케터의{' '}
                    <span style={{ fontWeight: 800 }}>데이터마케팅</span> 역량
                    강화
                  </div>
                  <div>- 데이터 기반 마케팅 기획</div>
                  <div>- 디지털마케팅 이행 및 성과 측정</div>
                </CardBody>
              </Card>
            </Col>
            <Col lg='3' md='6' xs='12'>
              {/* <div className="image">
								<img src={imgA} className="img-fluid" alt="..." />
							</div> */}
              <Card style={style} className='text-center'>
                <CardBody className='px-5' style={{ background: '#efeded' }}>
                  <CardTitle className='font-weight-900 m-0 p-1' tag='h3'>
                    전사 DT 역량 강화 교육
                  </CardTitle>
                </CardBody>
                <CardBody className='py-5'>
                  <div className='mb-3 h5'>
                    데이터 문맹탈출을 위한 전사{' '}
                    <span style={{ fontWeight: 800 }}>Data Literacy</span> 역량
                    강화
                  </div>
                  <div>- 목적에 따른 데이터 분석</div>
                  <div>- 데이터 기반 문제해결</div>
                </CardBody>
              </Card>
            </Col>
            <Col lg='3' md='12' xs='12'>
              {/* <div className="image">
								<img src={imgA} className="img-fluid" alt="..." />
							</div> */}
              <Card style={style} className='text-center'>
                <CardBody
                  className='px-5 py-1'
                  style={{ background: '#efeded' }}
                >
                  <CardTitle
                    className='font-weight-900 m-0'
                    tag='h3'
                    style={{ lineHeight: 1.6 }}
                  >
                    임원대상 <br />
                    Data Driven
                    <br /> 의사결정 교육
                  </CardTitle>
                </CardBody>
                <CardBody className='py-5'>
                  <div className='mb-3 h5'>
                    감이 아닌{' '}
                    <span style={{ fontWeight: 800 }}>Data를 근거로</span>{' '}
                    방향을 제시하는 리더십 강화
                  </div>
                  <div>- 디지털 환경 및 데이터 유형 이해</div>
                  <div>- 데이터 해석 및 인사이트 도출</div>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Row className='mt-9'>
            <Col className='mx-auto text-center mt-5' lg='8'>
              <p className='display-5'>체계적인 맞춤 교육</p>
              <p className='h7'>
                DM9 베이스 체계적인 맞춤 교육 Process을 경험해보세요
              </p>
            </Col>
          </Row>
          <Row className='justify-content-center mt-4'>
            {/* <div className="image">
							<img src={imgB} className="img-fluid" alt="..." />
						</div> */}
            <Col lg='3' md='6' xs='12'>
              <Card className='px-3 py-5' style={{ height: '600px' }}>
                <div className='text-center mb-4'>
                  <img style={imgwh} src={Img1} alt='img' />
                </div>
                <div className='h2 font-weight-900 text-center text-primary'>
                  고객사
                  <br />
                  요구분석
                </div>
                <CardBody className='h4' style={{ fontSize: '1.3rem' }}>
                  · 교육 목적 및 배경,
                  <br />
                  &nbsp;&nbsp;&nbsp;교육대상 분석
                </CardBody>
              </Card>
            </Col>
            <Col lg='3' md='6' xs='12'>
              <Card className='px-3 py-5' style={{ height: '600px' }}>
                <div className='text-center mb-4'>
                  <img style={imgwh} src={Img2} alt='img' />
                </div>
                <div className='h2 font-weight-900 text-center text-primary'>
                  과정설계
                  <br />및 제안
                </div>
                <CardBody className='h4' style={{ fontSize: '1.3rem' }}>
                  · 필요 과정 도출/제안 <br />· 고객사 데이터 기반
                  &nbsp;&nbsp;&nbsp;내용 및 실습 구성
                </CardBody>
              </Card>
            </Col>
            <Col lg='3' md='6' xs='12'>
              <Card className='px-3 py-5' style={{ height: '600px' }}>
                <div className='text-center mb-4'>
                  <img style={imgwh} src={Img3} alt='img' />
                </div>
                <div className='h2 font-weight-900 text-center text-primary'>
                  과정운영<div>&nbsp;</div>
                </div>
                <CardBody className='h4' style={{ fontSize: '1.3rem' }}>
                  · 실무 전문가 강사진 <br />· 데이터마케팅 적용도
                  &nbsp;&nbsp;&nbsp;자가진단 <br />· 자사 및 외부데이터
                  &nbsp;&nbsp;&nbsp;활용 전략 도출 워크샵
                </CardBody>
              </Card>
            </Col>
            <Col lg='3' md='6' xs='12'>
              <Card className='px-3 py-5' style={{ height: '600px' }}>
                <div className='text-center mb-4'>
                  <img style={imgwh} src={Img4} alt='img' />
                </div>
                <div className='h2 font-weight-900 text-center text-primary'>
                  과정평가<div>&nbsp;</div>
                </div>
                <CardBody className='h4' style={{ fontSize: '1.3rem' }}>
                  · 교육 만족도 측정 <br />· 교육 결과 보고서 제공
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
}
