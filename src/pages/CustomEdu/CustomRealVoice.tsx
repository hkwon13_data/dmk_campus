import React from 'react';

// reactstrap components
import { Container, Row, Col } from 'reactstrap';

// Core Components

import imgA from 'assets/img/custom_edu/edu_realvoice.png';

export default function CustomRealVoice() {
  return (
    <>
      <div className='section features features-5'>
        <Container>
          <Row>
            <Col className='mx-auto text-center mt-lg-0' lg='8'>
              <p className='display-5'>현장의 Real Voice 해결</p>
              <p className='h7'>
                실제 현업에서 근무하는 사람들의 궁금증을 해결해드립니다
              </p>
            </Col>
          </Row>

          <Row className='justify-content-md-center'>
            <Col lg='8' md='12' xs='12'>
              <div className='image'>
                <img src={imgA} className='img-fluid' alt='...' />
              </div>
            </Col>
          </Row>
          <Row className='align-items-center'>
            <Col className='mt-md-5' lg='12'>
              <Row>
                <Col lg='6'>
                  <div className='info float-right'>
                    <div className='pr-lg-5'>
                      <div className='icon icon-shape icon-shape-primary shadow rounded-circle mb-4'>
                        <i className='ni ni-atom' />
                      </div>
                      <p className='h4 font-weight-700'>인사이트 교육</p>
                      <p
                        className='h5 font-weight-400 py-2'
                        style={{ fontSize: '1.125rem' }}
                      >
                        풍부한 실제 컨설팅 사례를 활용하여 인사이트를 얻을 수
                        있습니다
                      </p>
                      <ul className='list-unstyled'>
                        <li>
                          <div className='d-flex align-items-center'>
                            <div>
                              <div className='mr-3'>
                                <i className='ni ni-check-bold' />
                              </div>
                            </div>
                            <div>
                              <p
                                className='mb-0 font-weight-300'
                                style={{
                                  fontSize: '1.1rem',
                                }}
                              >
                                300건 이상의 다양한 컨설팅 사례
                              </p>
                            </div>
                          </div>
                        </li>
                        <li>
                          <div className='d-flex align-items-top'>
                            <div>
                              <div className='mr-3 pt-1'>
                                <i className='ni ni-check-bold' />
                              </div>
                            </div>
                            <div>
                              <p
                                className='mb-0 font-weight-300 width-lg-max-content'
                                style={{ fontSize: '1.125rem' }}
                              >
                                데이터마케팅 기획, 이행, 성과측정까지 폭 넓은
                                내용
                              </p>
                            </div>
                          </div>
                        </li>
                        <li>
                          <div className='d-flex align-items-center'>
                            <div>
                              <div className='mr-3'>
                                <i className='ni ni-check-bold' />
                              </div>
                            </div>
                            <div>
                              <p
                                className='mb-0 font-weight-300'
                                style={{ fontSize: '1.125rem' }}
                              >
                                가장 최신 트렌드를 반영한 사례
                              </p>
                            </div>
                          </div>
                        </li>
                      </ul>
                    </div>
                  </div>
                </Col>

                <Col lg='6'>
                  <div className='info'>
                    <div className='pr-lg-5'>
                      <div className='icon icon-shape icon-shape-primary shadow rounded-circle mb-4'>
                        <i className='ni ni-money-coins' />
                      </div>
                      <p className='h4 font-weight-700'>다양한 Data 활용</p>
                      <p
                        className='h5 font-weight-400 py-2'
                        style={{ fontSize: '1.125rem' }}
                      >
                        다양한 데이터를 결합하여 더욱 유의미한 인사이트를 도출
                        할 수 있습니다
                      </p>
                      <ul className='list-unstyled'>
                        <li>
                          <div className='d-flex align-items-center'>
                            <div>
                              <div className='mr-3'>
                                <i className='ni ni-check-bold' />
                              </div>
                            </div>
                            <div>
                              <p
                                className='mb-0 font-weight-300'
                                style={{
                                  fontSize: '1.1rem',
                                }}
                              >
                                CRM Data
                              </p>
                            </div>
                          </div>
                        </li>
                        <li>
                          <div className='d-flex align-items-top'>
                            <div>
                              <div className='mr-3 pt-1'>
                                <i className='ni ni-check-bold' />
                              </div>
                            </div>
                            <div>
                              <p
                                className='mb-0 font-weight-300'
                                style={{ fontSize: '1.125rem' }}
                              >
                                WEB & SNS채널 Data
                              </p>
                            </div>
                          </div>
                        </li>
                        <li>
                          <div className='d-flex align-items-center'>
                            <div>
                              <div className='mr-3'>
                                <i className='ni ni-check-bold' />
                              </div>
                            </div>
                            <div>
                              <p
                                className='mb-0 font-weight-300'
                                style={{ fontSize: '1.125rem' }}
                              >
                                검색 & Buzz Data
                              </p>
                            </div>
                          </div>
                        </li>
                      </ul>
                    </div>
                  </div>
                </Col>
              </Row>
              <Row className='mt-lg-5'>
                <Col lg='6'>
                  <div className='info float-right'>
                    <div className='pr-lg-5'>
                      <div className='icon icon-shape icon-shape-primary shadow rounded-circle mb-4'>
                        <i className='ni ni-support-16' />
                      </div>
                      <p className='h4 font-weight-700'>이슈 해결</p>
                      <p
                        className='h5 font-weight-400 py-2'
                        style={{ fontSize: '1.125rem' }}
                      >
                        통합 마케팅 전략 도출 워크샵을 통해 실제 Biz 이슈를
                        해결할 수 있습니다
                      </p>
                      <ul className='list-unstyled'>
                        <li>
                          <div className='d-flex align-items-center'>
                            <div>
                              <div className='mr-3'>
                                <i className='ni ni-check-bold' />
                              </div>
                            </div>
                            <div>
                              <p
                                className='mb-0 font-weight-300 width-lg-max-content'
                                style={{
                                  fontSize: '1.1rem',
                                }}
                              >
                                자사 제품/브랜드 및 채널의 경쟁력 분석
                              </p>
                            </div>
                          </div>
                        </li>
                        <li>
                          <div className='d-flex align-items-top'>
                            <div>
                              <div className='mr-3 pt-1'>
                                <i className='ni ni-check-bold' />
                              </div>
                            </div>
                            <div>
                              <p
                                className='mb-0 font-weight-300'
                                style={{ fontSize: '1.125rem' }}
                              >
                                전략 캔버스 작성
                              </p>
                            </div>
                          </div>
                        </li>
                        <li>
                          <div className='d-flex align-items-center'>
                            <div>
                              <div className='mr-3'>
                                <i className='ni ni-check-bold' />
                              </div>
                            </div>
                            <div>
                              <p
                                className='mb-0 font-weight-300'
                                style={{ fontSize: '1.125rem' }}
                              >
                                데이터마케팅 이행 아이디어 도출
                              </p>
                            </div>
                          </div>
                        </li>
                      </ul>
                    </div>
                  </div>
                </Col>
                <Col lg='6'>
                  <div className='info'>
                    <div className='pr-lg-5'>
                      <div className='icon icon-shape icon-shape-primary shadow rounded-circle mb-4'>
                        <i className='ni ni-laptop' />
                      </div>
                      <p className='h4 font-weight-700'>
                        인공지능 마케팅 솔루션
                      </p>
                      <p
                        className='h5 font-weight-400 py-2'
                        style={{ fontSize: '1.125rem' }}
                      >
                        마대리로 마케팅 데이터를 한눈에 확인할 수 있습니다
                      </p>
                      <ul className='list-unstyled'>
                        <li>
                          <div className='d-flex align-items-center'>
                            <div>
                              <div className='mr-3'>
                                <i className='ni ni-check-bold' />
                              </div>
                            </div>
                            <div>
                              <p
                                className='mb-0 font-weight-300'
                                style={{
                                  fontSize: '1.1rem',
                                }}
                              >
                                마대리 활용 실습
                              </p>
                            </div>
                          </div>
                        </li>
                        <li>
                          <div className='d-flex align-items-top'>
                            <div>
                              <div className='mr-3 pt-1'>
                                <i className='ni ni-check-bold' />
                              </div>
                            </div>
                            <div>
                              <p
                                className='mb-0 font-weight-300 width-lg-max-content'
                                style={{ fontSize: '1.125rem' }}
                              >
                                월 30만원 솔루션 3개월 무료제공
                              </p>
                            </div>
                          </div>
                        </li>
                        <li>
                          <div className='d-flex align-items-center'>
                            <div>
                              <div className='mr-3'>
                                <i className='ni ni-check-bold' />
                              </div>
                            </div>
                            <div>
                              <p
                                className='mb-0 font-weight-300 width-lg-max-content'
                                style={{ fontSize: '1.125rem' }}
                              >
                                분석, 해석, 결과 예측까지 한번에 확인
                              </p>
                            </div>
                          </div>
                        </li>
                      </ul>
                    </div>
                  </div>
                </Col>
              </Row>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
}
