import React from 'react';

// reactstrap components
import { Button, CardBody, CardTitle, Container, Row, Col } from 'reactstrap';

import imgA from 'assets/img/custom_edu/edu_bg.png';
// import imgB from 'assets/img/faces/team-4.jpg';

import { useHistory } from 'react-router-dom';

export default function CustomEduHeader() {
  const history = useHistory();

  return (
    <>
      <div
        className='page-header-image d-flex align-items-center pt-5'
        style={{
          backgroundImage: `url(${imgA})`,
          backgroundSize: '100% 100%',
          height: '80vh',
          zIndex: -123,
        }}
      >
        <Container>
          <Row>
            <Col className='mx-auto' lg='10'>
              <CardBody className='mb-4 text-center'>
                <div className='content-bottom'>
                  <CardTitle
                    tag='h1'
                    className='text-white font-weight-700 mb-5'
                  >
                    내 서랍 속 Data와 Big Data의 결합
                  </CardTitle>
                  <div className='h7 text-white font-weight-700 my-3'>
                    흩어진 업무 데이터를 모으면 마케팅 전략이 도출됩니다
                    <br />
                    자사 맞춤형 교육 프로그램을 만나보세요
                  </div>
                  <Button
                    color='white'
                    className='mt-5 px-5 font-weight-700'
                    onClick={() => history.push('/community/inquiry/')}
                  >
                    문의하기
                  </Button>
                </div>
              </CardBody>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
}
