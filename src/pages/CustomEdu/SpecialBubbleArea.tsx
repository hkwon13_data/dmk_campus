import React from 'react';

import { Container, Row, Col } from 'reactstrap';

import styled from 'styled-components';

interface BubbleBlockPropsTypes {
  width: string;
  height: string;
  border: string;
  bgColor: string;
}

const BubbleBlock = styled.div<BubbleBlockPropsTypes>`
  display: flex;
  align-items: center;
  justify-content: center;
  width: ${(props) => props.width};
  height: ${(props) => props.height};
  border: ${(props) => props.border};
  border-radius: 50%;
  :hover {
    background: ${(props) => props.bgColor};
  }
  text-align: center;
  transition: all 0.2s ease-in-out;
`;

function SpecialBubbleArea() {
  return (
    <Container className='mb-8 mt-3 pb-5'>
      <h2 className='title mb-1 text-center font-weight-900 mb-4'>
        기업 맞춤 특강
      </h2>
      <h4 className='title text-center'>
        막막하기만 한 Data + Marketing, 기업 임원진 부터 실무진, 예비 마케터까지
      </h4>
      <h4 className='title text-center mb-3'>
        맞춤형으로 데이터마케팅 트렌드가 무엇인지 인사이트를 얻어가세요!
      </h4>
      <Row className='align-items-end justify-content-md-center'>
        <Col md='auto' lg='auto'>
          <BubbleBlock
            width='150px'
            height='150px'
            border='1px solid rgb(51, 51, 51)'
            bgColor='rgba(51, 51, 51, 0.3)'
          >
            <div className='font-weight-900 h5'>
              <div>마케팅 가치</div> <div>측정능력</div>
            </div>
          </BubbleBlock>
        </Col>
        <Col md='auto' lg='auto'>
          <BubbleBlock
            width='200px'
            height='200px'
            border='1px solid rgb(51, 102, 255)'
            bgColor='rgba(51, 102, 255, 0.3)'
          >
            <div className='font-weight-900 h5'>
              <div>디지털 영향력</div>
              <div>분석</div>
            </div>
          </BubbleBlock>
        </Col>
      </Row>
      <Row className='align-items-center justify-content-md-center'>
        <Col md='auto' lg='auto'>
          <BubbleBlock
            width='150px'
            height='150px'
            border='1px solid rgb(51, 102, 255)'
            bgColor='rgba(51, 102, 255, 0.3)'
          >
            <div className='font-weight-900 h5'>
              <div>데이터 분서 및</div> <div>활용 선진 사례</div>
            </div>
          </BubbleBlock>
        </Col>
        <Col md='auto' lg='auto'>
          <BubbleBlock
            width='300px'
            height='300px'
            border='1px solid rgb(102, 255, 255)'
            bgColor='rgba(102, 255, 255, 0.3)'
          >
            <div className='font-weight-900 h5'>
              <div>AI,</div>
              <div>마케팅을 바꾸다</div>
            </div>
          </BubbleBlock>
        </Col>
        <Col md='auto' lg='auto'>
          <BubbleBlock
            width='200px'
            height='200px'
            border='1px solid rgb(102, 255, 51)'
            bgColor='rgba(102, 255, 51, 0.3)'
          >
            <div className='font-weight-900 h5'>
              <div>데이터 의사결정</div>
            </div>
          </BubbleBlock>
        </Col>
      </Row>
      <Row className='align-items-start justify-content-md-center'>
        <Col md='auto' lg='auto'>
          <BubbleBlock
            width='200px'
            height='200px'
            border='1px solid rgb(102, 255, 51)'
            bgColor='rgba(102, 255, 51, 0.3)'
          >
            <div className='font-weight-900 h5'>
              <div>데이터마케팅</div> <div>거버넌스 체계</div>
            </div>
          </BubbleBlock>
        </Col>
        <Col md='auto' lg='auto'>
          <BubbleBlock
            width='250px'
            height='250px'
            border='1px solid rgb(51, 51, 51)'
            bgColor='rgba(51, 51, 51, 0.3)'
          >
            <div className='font-weight-900 h5'>
              <div>시장 트랜드 예측과</div>
              <div>변화</div>
            </div>
          </BubbleBlock>
        </Col>
      </Row>
    </Container>
  );
}
export default SpecialBubbleArea;
