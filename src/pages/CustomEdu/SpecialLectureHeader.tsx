import React from 'react';

// reactstrap components
import { Button, Container } from 'reactstrap';

import ImgA from 'assets/img/spcialEdu/specialHeader.png';

import { useHistory } from 'react-router-dom';

function SpeacialLectureHeader() {
  const history = useHistory();

  return (
    <>
      <div
        className='page-header-image d-flex align-items-center pt-5'
        style={{
          background: '#ebedef',
          backgroundSize: '100% 100%',
          height: '80vh',
          zIndex: -123,
        }}
      >
        <Container>
          <div
            style={{ display: 'flex' }}
            className='align-items-center justify-content-center pl-6'
          >
            <div className=''>
              <div className='h1 font-weight-900'>
                Data + Marketing,
                <br />
                그리고 Insight!
              </div>
              <div className='h5'>
                데이터를 활용한 미래 비즈니스 발전 방향은 어디로 향하는가?
                <br />
                최신 DX 사례에서 그 해답을 찾으세요!
              </div>
              <Button
                color='white'
                className='mt-5 px-5 font-weight-700'
                onClick={() => history.push('/community/inquiry/')}
              >
                문의하기
              </Button>
            </div>
            <div>
              <img src={ImgA} width='500px' height='500px' alt='right-img' />
            </div>
          </div>
        </Container>
      </div>
    </>
  );
}

export default SpeacialLectureHeader;
