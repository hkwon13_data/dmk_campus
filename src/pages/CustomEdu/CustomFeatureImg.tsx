import React from 'react';

// reactstrap components
import { Card, CardImg } from 'reactstrap';

// Core Components

// import imgA from 'assets/img/theme/img-1-1200x1000.jpg';

export default function CustomFeatureImg() {
  return (
    <>
      <Card className='bg-dark text-white border-0'>
        <CardImg
          alt='...'
          src='https://demos.creative-tim.com/argon-design-system-pro/assets/img/faces/alejandro-escamilla.jpg'
        />
      </Card>
    </>
  );
}
