import React from 'react';

import CommonNavbar from 'components/NavBar/CommonNavbar';
import CommonFooter from 'components/NavBar/CommonFooter';
import { ScrollTopButton } from 'components/utils/ScrollTopButton';
import FloatingButton from '../../components/NavBar/FloatingButton';

import CustomEduHeader from './CustomEduHeader';
import CustomRealVoice from './CustomRealVoice';
import CustomFeatures from './CustomFeatures';

import CustomFeedbackVideos from './CustomFeedbackVideos';

export default function CustomEduPage() {
  React.useEffect(() => {
    // let classList = document.body.classList;
    // document.body.classList.remove(...classList);
    // document.body.classList.add('index-page');
    window.scrollTo(0, 0);
    // return function cleanup() {
    //   document.body.classList.remove('index-page');
    // };
  }, []);

  return (
    <>
      <ScrollTopButton />
      <CommonNavbar type='transparent' />
      <FloatingButton />
      <CustomEduHeader />
      <CustomRealVoice />
      <CustomFeatures />
      <CustomFeedbackVideos />
      {/* <CustomClientLogos /> */}
      <CommonFooter />
    </>
  );
}
