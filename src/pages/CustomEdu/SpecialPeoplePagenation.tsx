/* eslint-disable react/no-array-index-key */
import React, { useState, useEffect } from 'react';

// reactstrap components
import { Card, CardTitle, Container, Row, Col } from 'reactstrap';

// import ImgA from '../../assets/img/spcialEdu/cardImg.png';
import CommonPagination from 'components/utils/Pagination';
import leeJinHyung from '../../assets/img/spcialEdu/leeJinHyung.png';
import parkSangHyun from '../../assets/img/spcialEdu/parkSangHyun.png';
import hwangBoHyun from '../../assets/img/spcialEdu/hwangBoHyun.png';
import joYongMin from '../../assets/img/spcialEdu/joYongMin.png';
import goJiHyun from '../../assets/img/spcialEdu/goJiHyun.png';
import gooBumJoon from '../../assets/img/spcialEdu/gooBumJoon.png';
import kimJungRyum from '../../assets/img/spcialEdu/kimJungRyum.png';
import limHyunJae from '../../assets/img/spcialEdu/limHyunJae.png';
import limSeongHee from '../../assets/img/spcialEdu/limSeongHee.png';
// import limJaeHyun from "../../assets/img/spcialEdu/limJaeHyun.png"
import seoGwonSeok from '../../assets/img/spcialEdu/seoGwonSeok.png';
import seoYeongSeok from '../../assets/img/spcialEdu/seoYeongSeok.png';
import YoonJungGen from '../../assets/img/spcialEdu/YoonJungGen.png';
import yoonYongChan from '../../assets/img/spcialEdu/yoonYongChan.png';
import kimHyungTaek from '../../assets/img/spcialEdu/kimHyungTaek.jpg';
import kimJinHo from '../../assets/img/spcialEdu/kimJinHo.jpg';
import kimOhgKi from '../../assets/img/spcialEdu/kimOhgKi.jpg';
import leeGeonHo from '../../assets/img/spcialEdu/leeGeonHo.jpg';
import parkJunGyu from '../../assets/img/spcialEdu/parkJunGyu.jpg';
import hongHeeDae from '../../assets/img/spcialEdu/hongHeeDae.jpg';
import leeJinGook from '../../assets/img/spcialEdu/leeJinGook.jpg';

interface SpecialPeopleTypes {
  id: number;
  img: string;
  text: string;
  name: string;
  position: string;
}

interface PagingDataTypes {
  pagesCount: number;
  currentPage: number;
  pageSize: number;
}

const data: SpecialPeopleTypes[] = [
  {
    id: 0,
    img: leeJinHyung,
    text: '모든 마케팅 ROI는 정량화할 수 있어야 합니다',
    name: '이진형',
    position: '데이터마케팅코리아CEO',
  },
  {
    id: 1,
    img: parkSangHyun,
    text: '데이터와 인사이트, 크리에이티브의 또다른 시작입니다',
    name: '박상현',
    position: '데이터마케팅코리아 부사장',
  },
  {
    id: 2,
    img: hwangBoHyun,
    text: '논리가 궁극에 닿는 순간 창의가 열린다',
    name: '황보현',
    position: '솔트룩스CCO',
  },
  {
    id: 3,
    img: joYongMin,
    text: '데이터와 머신러닝이 마케팅을 더 강하게 합니다',
    name: '조용민',
    position: '구글코리아 매니저',
  },
  // {
  // 	id: 4,
  // 	img: ImgA,
  // 	text: "경영철학자. 미래 인문경영을 요하다",
  // 	name: "서진영",
  // 	position: "자의누리경영연구원 원장",
  // },
  {
    id: 5,
    img: gooBumJoon,
    text: '우리의 스토리로 세상을 바꿉니다',
    name: '구범준',
    position: '세상을바꾸는 시간15분 대표',
  },
  {
    id: 6,
    img: leeJinGook,
    text: 'B2B, 이미 하면서도 당신만 모르는 서비스',
    name: '이진국',
    position: 'JKL컴퍼니 대표',
  },
  {
    id: 7,
    img: kimHyungTaek,
    text: '디지털 마케팅, 트렌드 쫓지말고 전략에 집중하라',
    name: '김형택',
    position: '마켓캐스트 대표',
  },
  {
    id: 8,
    img: kimJinHo,
    text: '데이터분석을 통하여 시장을 더 잘 이해하는 것, 그것이 데이터 기반 경영입니다',
    name: '김진호',
    position: '서울과학종합대학원 이사',
  },
  {
    id: 9,
    img: kimOhgKi,
    text: 'B2B 기업, 데이터 과학 마케팅시대를 준비하라',
    name: '김옥기',
    position: '엔코아 상무',
  },
  {
    id: 10,
    img: seoGwonSeok,
    text: '콘텐츠는 더이상 조언이 아닙니다',
    name: '서권석',
    position: '전)72초 TV CMO',
  },
  {
    id: 11,
    img: limSeongHee,
    text: '새로운 기술은 언제나 마케터를 설레게 합니다',
    name: '임성희',
    position: 'SK텔레콤 부장',
  },
  // {
  // 	id: 12,
  // 	img: ImgA,
  // 	text: "마케팅과 기술의 융합, 전략은 좀 더 전략적 일 수 있습니다",
  // 	name: "권순우",
  // 	position: "기아자동차 차장",
  // },
  {
    id: 13,
    img: kimJungRyum,
    text: '브랜드 가치를 높이는 놀라운 Insight를 추구합니다',
    name: '김정렴',
    position: '세종정사 디지털소통기확과 과장',
  },
  // {
  // 	id: 14,
  // 	img: ImgA,
  // 	text: "모바일 퍼포먼스, 마케팅의 새로운 전환",
  // 	name: "임현균",
  // 	position: "이미저블 대표",
  // },
  {
    id: 15,
    img: YoonJungGen,
    text: '럭셔리 브랜드를 위한 럭셔리마케팅은 다릅니다',
    name: '윤정근',
    position: 'TEADS.TV 대표',
  },
  // {
  // 	id: 16,
  // 	img: ImgA,
  // 	text: "TMI자율롭게 소통하는 새로운 브랜드 입니다",
  // 	name: "이수진",
  // 	position: "이노션 수석 국장",
  // },
  // {
  // 	id: 17,
  // 	img: ImgA,
  // 	text:
  // 		"책상에서 배운것과 현실에서 배운것은 다릅니다 야생의 경험을 전해 드립니다",
  // 	name: "허원석",
  // 	position: "MLB 상무",
  // },
  // {
  // 	id: 18,
  // 	img: ImgA,
  // 	text: "아무것도 하지 않으면, 아무일도 일어나지 않는다",
  // 	name: "이대복",
  // 	position: "(주)메세이상 대표",
  // },
  // {
  // 	id: 19,
  // 	img: ImgA,
  // 	text: "준비 없는 창업은 학도병이나 다름없다",
  // 	name: "임완",
  // 	position: "엑셀인베스트먼트 대표",
  // },
  // {
  // 	id: 20,
  // 	img: ImgA,
  // 	text: "플랫폼, 미디어, 콘텐츠 시장을 독점하기 위한 경제 전쟁이 시작되었다",
  // 	name: "김조한",
  // 	position: "New BREND 대표",
  // },
  {
    id: 21,
    img: goJiHyun,
    text: '이미 모호한 경게를 더 이상 경계하지 말라',
    name: '고지현',
    position: '취향관, 킷스튜디오 대표',
  },
  {
    id: 22,
    img: limHyunJae,
    text: '1세대 구글 마케터의 디지털 성공전략을 전합니다',
    name: '임현재',
    position: '글링크미디어 대표',
  },
  // {
  // 	id: 23,
  // 	img: ImgA,
  // 	text: "1부 끝, 2부 시작 당신의 삶에 2부를 시작하세요",
  // 	name: "딴트공",
  // 	position: "1인크리에이터 유튜버",
  // },
  {
    id: 24,
    img: seoYeongSeok,
    text: '데이터라는 바다에서 가치를 찾아내는 여정에 초대합니다',
    name: '서영석',
    position: '데이터마케팅코리아 이사',
  },
  {
    id: 25,
    img: yoonYongChan,
    text: '상상력이 데이터를 만날때 꿈은 현실이 됩니다',
    name: '윤용찬',
    position: '데이터마케팅코리아 이사',
  },
  // {
  // 	id: 26,
  // 	img: ImgA,
  // 	text: "뉴미디어 바다에서 길을 찾는 마케팅 기획자를위한 안내서",
  // 	name: "문두열",
  // 	position: "필라멘트리 대표",
  // },
  {
    id: 27,
    img: parkJunGyu,
    text: '거꾸로 매달린 박쥐처럼, 180도 다르게 바라봐라',
    name: '박준규',
    position: 'B.A.T 대표',
  },
  {
    id: 28,
    img: leeGeonHo,
    text: 'EAT, Smart!',
    name: '이건호',
    position: '샐러디 대표',
  },
  // {
  // 	id: 29,
  // 	img: ImgA,
  // 	text: "인공지능 기반의 미래를 위한 생태게를 구상하라",
  // 	name: "이주석",
  // 	position: "인텔 전무",
  // },
  {
    id: 30,
    img: hongHeeDae,
    text: '4차산업을 퍼실리테이션으로 이야기하다',
    name: '홍희대',
    position: 'DellEMC KOREA 컨설턴트',
  },
];

function SpecialPeoplePagenation() {
  const [pageSize] = useState<number>(10);
  const dataLength: number = !data ? 1 : data.length;
  const pages: number = Math.ceil(dataLength / pageSize);
  const [pagesCount, setPageCount] = useState<number>(pages);
  const [currentPage, setCurrentPage] = useState<number>(0);

  const pagingData: PagingDataTypes = {
    pagesCount,
    currentPage,
    pageSize,
  };

  useEffect(() => {
    if (dataLength > 10) {
      setPageCount(pages);
    }
  }, [dataLength, pages]);

  const handlePage = (e: number) => {
    setCurrentPage(e);
  };

  return (
    <>
      <h2 className='title mb-1 text-center font-weight-900'>
        마케팅 명사 특강
      </h2>
      <h4 className='title text-center mb-7'>
        자신만의 인사이트로 세상을 혁신하는 분들을 모십니다
      </h4>
      <Container className='mb-4'>
        <Row className='justify-content-start'>
          {data
            .slice(currentPage * pageSize, (currentPage + 1) * pageSize)
            .map((item, index) => {
              if (
                data.slice(currentPage * pageSize, (currentPage + 1) * pageSize)
                  .length > 1
              ) {
                return (
                  <Col className='mx-auto' lg='6' md='8' key={index}>
                    <Card className='card-blog card-plain blog-horizontal mb-5'>
                      <Row className='align-items-end'>
                        <Col lg='4'>
                          <div className='card-image shadow'>
                            <img
                              alt='...'
                              width='188px'
                              height='188px'
                              className='img rounded'
                              src={item.img}
                            />
                          </div>
                        </Col>
                        <Col lg='8'>
                          <div
                            style={{
                              display: 'flex',
                              flexDirection: 'column',
                            }}
                          >
                            <CardTitle tag='h5' className='font-weight-900'>
                              {item.text}
                            </CardTitle>
                            <p className='card-description m-0'>
                              <span className='h2 font-weight-900 mr-3'>
                                {item.name}
                              </span>
                              <span
                                style={{ borderLeft: '3px solid #333333' }}
                                className='pl-4 font-weight-900'
                              >
                                {item.position}
                              </span>
                            </p>
                          </div>
                        </Col>
                      </Row>
                    </Card>
                  </Col>
                );
              }
              return (
                <Col className='mx-auto' lg='12' md='12' key={index}>
                  <Card className='card-blog card-plain blog-horizontal mb-5'>
                    <Row className='align-items-end'>
                      <Col lg='2'>
                        <div className='card-image shadow'>
                          <img
                            alt='...'
                            height='188px'
                            className='img rounded'
                            src={item.img}
                          />
                        </div>
                      </Col>
                      <Col lg='8'>
                        <div
                          style={{
                            display: 'flex',
                            flexDirection: 'column',
                          }}
                        >
                          <CardTitle tag='h5' className='font-weight-900'>
                            {item.text}
                          </CardTitle>
                          <p className='card-description m-0'>
                            <span className='h2 font-weight-900 mr-3'>
                              {item.name}
                            </span>
                            <span
                              style={{ borderLeft: '3px solid #333333' }}
                              className='pl-4 font-weight-900'
                            >
                              {item.position}
                            </span>
                          </p>
                        </div>
                      </Col>
                    </Row>
                  </Card>
                </Col>
              );
            })}
        </Row>
      </Container>
      {data && (
        <CommonPagination
          pagingData={pagingData}
          handlePage={(e: number) => handlePage(e)}
        />
      )}
      <div className='py-5' />
    </>
  );
}

export default SpecialPeoplePagenation;
