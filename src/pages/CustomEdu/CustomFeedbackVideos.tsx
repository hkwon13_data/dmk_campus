/* eslint-disable react/no-array-index-key */
import React from 'react';

// reactstrap components
import { Container, Row, Col, Carousel, CarouselItem } from 'reactstrap';

// Core Components
// import videoA from "assets/customEdu/handok_review.mp4";
// import videoB from "assets/customEdu/farmsco_review.mp4";
// import videoC from "assets/customEdu/kgc_review.mp4";

export default function CustomFeedbackVideos() {
  const [activeIndex, setActiveIndex] = React.useState(0);
  const [animating, setAnimating] = React.useState(false);
  const [videoTest, setVideoTest] = React.useState(0);
  const items = [
    {
      content: (
        <Row>
          <Col className='px-1'>
            <div className='p-lg-4'>
              <video
                width='100%'
                height='100%'
                controls
                muted
                onLoadedMetadata={(e: any) =>
                  e.target.currentTime === e.target.currentTime + 0.8
                }
                className='embed-responsive video'
              >
                <source
                  src='https://s3.ap-northeast-2.amazonaws.com/media.marketingcampus.co.kr/%EA%B8%B0%EC%97%85%EB%A7%9E%EC%B6%A4%EA%B5%90%EC%9C%A1_%ED%9B%84%EA%B8%B0%EC%98%81%EC%83%81/0728+%ED%95%9C%EB%8F%85+DT%EA%B5%90%EC%9C%A1%EA%B3%BC%EC%A0%95+Review+%EC%98%81%EC%83%81.mp4'
                  type='video/mp4'
                />
              </video>
            </div>
          </Col>
        </Row>
      ),
      src: '0',
      altText: 'Slide 1',
      caption: '',
      header: '',
      key: '1',
    },
    {
      content: (
        <Row>
          <Col className='px-1'>
            <div className='p-lg-4'>
              <video
                width='100%'
                height='100%'
                controls
                muted
                onLoadedMetadata={(e: any) =>
                  e.target.currentTime === e.target.currentTime + 0.8
                }
                className='embed-responsive video'
              >
                <source
                  src='https://s3.ap-northeast-2.amazonaws.com/media.marketingcampus.co.kr/%EA%B8%B0%EC%97%85%EB%A7%9E%EC%B6%A4%EA%B5%90%EC%9C%A1_%ED%9B%84%EA%B8%B0%EC%98%81%EC%83%81/%ED%8C%9C%EC%8A%A4%EC%BD%94+%EB%8D%B0%EC%9D%B4%ED%84%B0%EB%A7%88%EC%BC%80%ED%8C%85+%EA%B5%90%EC%9C%A1%EA%B3%BC%EC%A0%95+Review.mp4'
                  type='video/mp4'
                />
              </video>
            </div>
          </Col>
        </Row>
      ),
      src: '0',
      altText: 'Slide 2',
      caption: '',
      header: '',
      key: '2',
    },

    {
      content: (
        <Row>
          <Col className='px-1'>
            <div className='p-lg-4'>
              <video
                width='100%'
                height='100%'
                controls
                muted
                onLoadedMetadata={(e: any) =>
                  e.target.currentTime === e.target.currentTime + 0.8
                }
                className='embed-responsive video'
              >
                <source
                  src='https://s3.ap-northeast-2.amazonaws.com/media.marketingcampus.co.kr/%EA%B8%B0%EC%97%85%EB%A7%9E%EC%B6%A4%EA%B5%90%EC%9C%A1_%ED%9B%84%EA%B8%B0%EC%98%81%EC%83%81/%ED%95%9C%EA%B5%AD%EC%9D%B8%EC%82%BC%EA%B3%B5%EC%82%AC+%EB%8D%B0%EC%9D%B4%ED%84%B0%EB%A7%88%EC%BC%80%ED%8C%85+%EA%B5%90%EC%9C%A1+Review.mp4'
                  type='video/mp4'
                />
              </video>
            </div>
          </Col>
        </Row>
      ),
      src: '0',
      altText: 'Slide 3',
      caption: '',
      header: '',
      key: '3',
    },
  ];

  React.useEffect(() => {
    if (videoTest !== 0) {
      const videoList = document.querySelectorAll('video');
      videoList.forEach((video) => {
        video.pause();
      });
    }
  }, [activeIndex, videoTest]);

  const next = () => {
    setVideoTest(videoTest + 1);
    if (animating) return;
    const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
    setActiveIndex(nextIndex);
  };

  const previous = () => {
    setVideoTest(videoTest + 1);
    if (animating) return;
    const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
    setActiveIndex(nextIndex);
  };

  return (
    <>
      <Container className='mt-7'>
        <Row>
          <Col className='mx-auto text-center mt-5' lg='8'>
            <p className='display-5 mb-0'>생생한 현장 VOICE</p>
            <p className='h7'>
              교육을 통해 데이터마케팅 역량을 강화할 수 있었습니다
            </p>
          </Col>
        </Row>
        <Row className='my-3'>
          <Col className='mx-auto'>
            <Carousel
              activeIndex={activeIndex}
              next={next}
              previous={previous}
              className='carousel-inner '
              id='carouselControls'
              //   ride="carousel"
              interval={false}
            >
              {items.map((item, key) => {
                return (
                  <CarouselItem
                    onExiting={() => setAnimating(true)}
                    onExited={() => setAnimating(false)}
                    key={key}
                  >
                    {item.content}
                  </CarouselItem>
                );
              })}
              <a
                className='carousel-control-prev h-50 align-items-end'
                data-slide='prev'
                href='#pablo'
                onClick={(e) => {
                  e.preventDefault();
                  previous();
                }}
                role='button'
              >
                <i className='ni ni-bold-left' />
                <span className='sr-only'>Previous</span>
              </a>
              <a
                className='carousel-control-next h-50 align-items-end'
                data-slide='next'
                href='#pablo'
                onClick={(e) => {
                  e.preventDefault();
                  next();
                }}
                role='button'
              >
                <i className='ni ni-bold-right' />
                <span className='sr-only'>Next</span>
              </a>
            </Carousel>
          </Col>
        </Row>
      </Container>
    </>
  );
}
