import React from 'react';

// reactstrap components
import { Container, Row, Col, Button } from 'reactstrap';

// Core Components
import imgA from 'assets/img/custom_edu/edu_clients.png';

export default function CustomClientLogos() {
  return (
    <>
      <Container className='mt-9 pt-3'>
        <Row>
          <Col className='mx-auto text-center mt-5' lg='8'>
            <p className='display-5'>데마캠과 함께한 기업</p>
            <p className='h7'>
              데이터마케팅캠퍼스와 기업은 교육을 통해 동반 성장 중입니다
            </p>
          </Col>
        </Row>
      </Container>
      <Container className='mb-7'>
        <Row className='justify-content-md-center'>
          <Col lg='10' md='12' xs='12'>
            <div className='image'>
              <img src={imgA} className='img-fluid' alt='...' />
            </div>
          </Col>
        </Row>
        <Row className='justify-content-md-center py-5'>
          <Button
            className='btn-icon btn-3'
            color='primary'
            type='button'
            size='lg'
          >
            <span className='btn-inner--text px-4'>
              <span className='btn-inner--icon'>
                <i
                  className='ni ni-single-copy-04 pr-3'
                  style={{ color: '#ffffff' }}
                />
              </span>
              표준제안서 다운로드
            </span>
          </Button>
        </Row>
      </Container>
    </>
  );
}
