import React from 'react';

import CommonNavbar from 'components/NavBar/CommonNavbar';
import CommonFooter from 'components/NavBar/CommonFooter';
import { ScrollTopButton } from 'components/utils/ScrollTopButton';
import FloatingButton from '../../components/NavBar/FloatingButton';
import SpecialLectureHeader from './SpecialLectureHeader';
import SpecialBubbleArea from './SpecialBubbleArea';
import SpecialPeoplePagenation from './SpecialPeoplePagenation';

export default function SpcLctPage() {
  React.useEffect(() => {
    // let classList = document.body.classList;
    // document.body.classList.remove(...classList);
    // document.body.classList.add('index-page');
    window.scrollTo(0, 0);
    // return function cleanup() {
    //   document.body.classList.remove('index-page');
    // };
  }, []);
  return (
    <>
      <ScrollTopButton />
      <CommonNavbar type='primary' />
      <FloatingButton />
      <SpecialLectureHeader />
      <SpecialBubbleArea />
      <SpecialPeoplePagenation />
      <CommonFooter />
    </>
  );
}
