/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { Route, Switch, Redirect, RouteComponentProps } from 'react-router-dom';
// import PropTypes from 'prop-types';

import 'antd/dist/antd.css';

// plugins styles from node_modules
import 'assets/css/nucleo-svg.css';
import 'assets/css/nucleo-icons.css';

// plugins styles from node_modules
import 'react-notification-alert/dist/animate.css';

// plugins styles downloaded
import 'react-perfect-scrollbar/dist/css/styles.css';
import '@fullcalendar/common/main.min.css';
import '@fullcalendar/daygrid/main.min.css';
import 'sweetalert2/dist/sweetalert2.min.css';
import 'select2/dist/css/select2.min.css';
import '@fortawesome/fontawesome-free/css/all.min.css';

// import 'assets/scss/dmc-styles.scss'; // 개발
// import "assets/css/dmc-styles.min.css"; // 배포
import 'assets/css/dmc-styles.css'; // 배포

import MainLayout from 'layouts/MainLayout';
import AdminLayout from 'layouts/AdminLayout';
import IntroLayout from 'layouts/IntroLayout';
import DMKHallLayout from 'layouts/DMKHallLayout';
import CommunityLayout from 'layouts/CommunityLayout';
import AuthLayout from 'layouts/AuthLayout';
import CustomEduLayout from 'layouts/CustomEduLayout';
import MyPageLayout from 'layouts/MyPageLayout';
import OpenProcessLayout from 'layouts/OpenProcessLayout';

function Root() {
  return (
    <>
      <Switch>
        <Route
          path='/intro'
          render={(props: RouteComponentProps<any>) => (
            <IntroLayout {...props} />
          )}
        />
        <Route
          path='/openProcess' // offlineEdue
          render={(props: RouteComponentProps<any>) => (
            <OpenProcessLayout {...props} />
          )} // OfflineEdue
        />
        {/* <Route
					path="/onlineEdu"
					render={(props) => <OnlineEduLayout {...props} />}
				/> */}
        <Route
          path='/dmkHall'
          render={(props: RouteComponentProps<any>) => (
            <DMKHallLayout {...props} />
          )}
        />
        <Route
          path='/community'
          render={(props: RouteComponentProps<any>) => (
            <CommunityLayout {...props} />
          )}
        />
        <Route
          path='/customEdu'
          render={(props: RouteComponentProps<any>) => (
            <CustomEduLayout {...props} />
          )}
        />
        <Route
          path='/mypage'
          render={(props: RouteComponentProps<any>) => (
            <MyPageLayout {...props} />
          )}
        />
        <Route
          path='/auth'
          render={(props: RouteComponentProps<any>) => (
            <AuthLayout {...props} />
          )}
        />

        <Route
          path='/admin'
          render={(props: any) => <AdminLayout {...props} />}
        />
        <Route
          path='/main'
          render={(props: RouteComponentProps<any>) => (
            <MainLayout {...props} />
          )}
        />
        <Route
          path='/'
          render={(props: RouteComponentProps<any>) => (
            <MainLayout {...props} />
          )}
        />
        <Redirect from='*' to='/' />
      </Switch>
    </>
  );
}

export default Root;
