import React from 'react';
// nodejs library to set properties for components
// import PropTypes from 'prop-types';
// reactstrap components
import { Button, Container, Row, Col } from 'reactstrap';

function AdminHeader() {
  return (
    <>
      <div className='header bg-secondary'>
        <Container fluid>
          <div className='header-body'>
            <Row className='align-items-right py-4'>
              <Col lg='6' xs='7' />
              <Col className='text-right' lg='6' xs='5'>
                <Button
                  className='btn-neutral'
                  color='default'
                  href='#pablo'
                  onClick={(e) => e.preventDefault()}
                  size='sm'
                >
                  New
                </Button>
                <Button
                  className='btn-neutral'
                  color='default'
                  href='#pablo'
                  onClick={(e) => e.preventDefault()}
                  size='sm'
                >
                  Filters
                </Button>
              </Col>
            </Row>
          </div>
        </Container>
      </div>
    </>
  );
}

// AdminHeader.propTypes = {
//   name: PropTypes.string,
//   parentName: PropTypes.string,
// };

export default AdminHeader;
