/* eslint-disable prefer-destructuring */
import React from 'react';
import { Container } from 'reactstrap';

import AdminLectureMaterials from 'pages/Admin/AdminLectureMaterials';

export default function Admin() {
  React.useEffect(() => {
    const classList = document.body.classList;
    document.body.classList.remove(...classList);
    document.body.classList.add('g-sidenav-hidden');
    return function cleanup() {
      document.body.classList.remove('g-sidenav-hidden');
    };
  });

  return (
    <div>
      <div className='section'>
        <Container>
          <AdminLectureMaterials />
        </Container>
      </div>
    </div>
  );
}
