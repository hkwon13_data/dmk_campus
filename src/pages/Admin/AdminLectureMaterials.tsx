import React from 'react';

// reactstrap components
import { Container } from 'reactstrap';

// core components
import AdminLecture from 'components/Admin/Lecture/AdminLecture';

export default function AdminLectureMaterials() {
  return (
    <>
      <Container className='bg-secondary pb-9' fluid>
        <AdminLecture />
      </Container>
    </>
  );
}
