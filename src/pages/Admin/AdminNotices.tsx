import React from 'react';

// reactstrap components
import { Container } from 'reactstrap';

// core components
import AdminNotice from 'components/Admin/Notice/AdminNotice';

export default function AdminNotices() {
  return (
    <>
      <Container className='bg-secondary pb-9' fluid>
        <AdminNotice />
      </Container>
    </>
  );
}
