/* eslint-disable consistent-return */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useState } from 'react';

// reactstrap components
import { Table, Container, Row, Spinner } from 'reactstrap';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from 'store/modules/index';
import LectureMaterials from 'components/Community/Notice/LectureMaterials';
import CommonPagination from 'components/utils/Pagination';
import * as actions from 'store/modules/community';
import NoticeLectureInfo from './NoticeLectureInfo';
import PasswordPopup from './NoticePasswordPopup';

interface PagingDataTypes {
  pagesCount: number;
  currentPage: number;
  pageSize: number;
}

interface ClickLectureInitState {
  mapId: number;
  postId: number | undefined;
}

export default function NoticeLectureMaterialsTable() {
  const dispatch = useDispatch();
  const { data, loading, error } = useSelector(
    (state: RootState) => state.community.lectureMaterials
  );
  const { isPasswordCorrect } = useSelector(
    (state: RootState) => state.community
  );
  const lectureList = data;

  const [pageSize, setPageSize] = useState(10);
  const dataLength = !data ? 1 : data.data.length;
  const pages = Math.ceil(dataLength / pageSize);
  const [pagesCount, setPageCount] = useState(pages);
  const [currentPage, setCurrentPage] = useState(0);

  const pagingData: PagingDataTypes = {
    pagesCount,
    currentPage,
    pageSize,
  };

  const [openLecture, setOpenLecture] = useState(false);
  const [clickedLecture, setClickedLecture] = useState<ClickLectureInitState>({
    mapId: 0,
    postId: 0,
  });

  const [passedPassword, setPassedPassword] = useState(false);
  const [openPasswordPopup, setOpenPasswordPopup] = useState(false);

  //   React.useEffect(() => {
  //     dispatch(actions.setAuthLecturePost(false));
  //     setPassedPassword(false);
  //   }, []);

  React.useEffect(() => {
    dispatch(actions.setAuthLecturePost(false));
    if (dataLength > 10) {
      setPageCount(pages);
    }
  }, [dataLength, pages, dispatch, isPasswordCorrect]);

  const handlePage = (e: number) => {
    setCurrentPage(e);
  };

  React.useEffect(() => {
    if (isPasswordCorrect) {
      setOpenPasswordPopup(true);
    }
  }, [isPasswordCorrect]);

  React.useEffect(() => {
    if (lectureList !== null) {
      const selectedPost = lectureList.data.filter(
        (val) => val.id === clickedLecture.postId
      );
      if (selectedPost.length !== 0) {
        dispatch(actions.setLecturePostPassword(selectedPost[0].password));
      }
    }
  }, [clickedLecture, lectureList, dispatch]);

  const PostListContainer = () => {
    if (loading)
      return (
        <Row className='justify-content-center'>
          <Spinner style={{ width: '3rem', height: '3rem' }} />
        </Row>
      );
    if (error) return <div>에러 발생!</div>;
    if (data)
      return (
        <div className='table'>
          <Table responsive hover>
            {data && (
              <LectureMaterials
                pagingData={pagingData}
                data={lectureList?.data}
                setOpenNotice={setOpenLecture}
                setClickedNotice={setClickedLecture}
                setOpenPasswordPopup={setOpenPasswordPopup}
              />
            )}
          </Table>
        </div>
      );
  };

  return (
    <>
      <Container className='container-lg mt-3 p-0'>
        {openPasswordPopup && (
          <PasswordPopup
            setPassedPassword={setPassedPassword}
            isModalOpen
            // setOpenPasswordPopup={setOpenPasswordPopup}
          />
        )}
        {openLecture && passedPassword && (
          <NoticeLectureInfo
            data={lectureList?.data}
            setOpenLecture={setOpenLecture}
            clickedLecture={clickedLecture}
            setClickedLecture={setClickedLecture}
            setOpenPasswordPopup={setOpenPasswordPopup}
            setPassedPassword={setPassedPassword}
          />
        )}
        {!openLecture && PostListContainer()}

        {!openLecture && data && (
          <CommonPagination
            pagingData={pagingData}
            handlePage={(e: number) => handlePage(e)}
          />
        )}
      </Container>
    </>
  );
}
