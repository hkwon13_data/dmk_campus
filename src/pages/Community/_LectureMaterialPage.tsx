import React from 'react';
import { Container } from 'reactstrap';

import CommonNavbar from 'components/NavBar/CommonNavbar';
import CommonFooter from 'components/NavBar/CommonFooter';
import { ScrollTopButton } from 'components/utils/ScrollTopButton';

export default function NoticePage() {
  return (
    <>
      <ScrollTopButton />
      <CommonNavbar type='primary' />
      <div className='section section-content-areas'>
        <Container>
          <h2 className='mt-lg mb-5'>Lecture Material Content Areas</h2>
        </Container>
      </div>
      <CommonFooter />
    </>
  );
}
