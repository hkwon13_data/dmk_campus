/* eslint-disable consistent-return */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useState } from 'react';

// reactstrap components
import { Table, Row, Container, Spinner } from 'reactstrap';

import { RootState } from 'store/modules/index';
import { useSelector } from 'react-redux';
import Notices from 'components/Community/Notice/Notices';
import CommonPagination from 'components/utils/Pagination';
import NoticeInfo from './NoticeInfo';

interface PagingDataTypes {
  pagesCount: number;
  currentPage: number;
  pageSize: number;
}

interface ClickLectureInitState {
  mapId: number;
  postId: number | undefined;
}
function NoticeTable() {
  const { data, loading, error } = useSelector(
    (state: RootState) => state.community.notices
  );
  const noticeList = data;

  const [pageSize, setPageSize] = useState(10);
  const dataLength = !data ? 1 : data.data.length;
  const pages = Math.ceil(dataLength / pageSize);
  const [pagesCount, setPageCount] = useState(pages);
  const [currentPage, setCurrentPage] = useState(0);

  const pagingData: PagingDataTypes = {
    pagesCount,
    currentPage,
    pageSize,
  };

  const [openNotice, setOpenNotice] = useState(false);
  const [clickedNotice, setClickedNotice] = useState<ClickLectureInitState>({
    mapId: 0,
    postId: 0,
  });

  React.useEffect(() => {
    if (dataLength > 10) {
      setPageCount(pages);
    }
  }, [dataLength, pages]);

  const handlePage = (e: number) => {
    setCurrentPage(e);
  };

  const PostListContainer = () => {
    if (loading)
      return (
        <Row className='justify-content-center'>
          <Spinner style={{ width: '3rem', height: '3rem' }} />
        </Row>
      );
    if (error) return <div>에러 발생!</div>;
    if (data)
      return (
        <div className='table'>
          <Table responsive hover>
            {data && (
              <Notices
                pagingData={pagingData}
                data={noticeList?.data}
                setOpenNotice={setOpenNotice}
                setClickedNotice={setClickedNotice}
              />
            )}
          </Table>
        </div>
      );
  };

  return (
    <>
      <Container className='container-lg mt-3 p-0'>
        {openNotice && (
          <NoticeInfo
            data={noticeList?.data}
            openNotice={setOpenNotice}
            clickedNotice={clickedNotice}
            setClickedNotice={setClickedNotice}
          />
        )}
        {!openNotice && PostListContainer()}
        {!openNotice && data && (
          <CommonPagination
            pagingData={pagingData}
            handlePage={(e: number) => handlePage(e)}
          />
        )}
      </Container>
    </>
  );
}

export default NoticeTable;
