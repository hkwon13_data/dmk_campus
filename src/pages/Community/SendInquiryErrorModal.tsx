/* eslint-disable @typescript-eslint/no-shadow */
import React from 'react';
import { useSelector } from 'react-redux';
import { RootState } from 'store/modules/index';
import { Button, Modal, Row } from 'reactstrap';

interface InquiryErrorModalPropsTypes {
  handleInquiryErrorPopup: () => void;
  text: string;
}

function SendInquiryErrorModal({
  handleInquiryErrorPopup,
  text,
}: InquiryErrorModalPropsTypes) {
  const { isSendInquiryErrorPopup } = useSelector(
    (state: RootState) => state.modalManages.community
  );

  const renderErrorMessage = (text: string) => {
    return text.split('\n').map((line: string) => {
      return (
        <span>
          {line}
          <br />
        </span>
      );
    });
  };

  return (
    <>
      <Modal
        isOpen={isSendInquiryErrorPopup}
        toggle={() => handleInquiryErrorPopup()}
        className='modal-dialog-centered modal-sm'
      >
        <div className='modal-body'>
          <div className='font-weight-700 text-center h3 mt-3'>
            <i className='ni ni-bell-55' />
          </div>
          <div className='text-center mb-4 h5'>{renderErrorMessage(text)}</div>
          <Row className='justify-content-center pb-3'>
            <div>
              <Button
                className='btn px-4'
                color='primary'
                onClick={() => handleInquiryErrorPopup()}
              >
                확인
              </Button>
            </div>
          </Row>
        </div>
      </Modal>
    </>
  );
}

export default SendInquiryErrorModal;
