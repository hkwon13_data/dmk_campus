import React from 'react';
import { useHistory } from 'react-router-dom';

// reactstrap components
import {
  Container,
  TabPane,
  TabContent,
  Nav,
  NavItem,
  NavLink,
} from 'reactstrap';

import NoticeTable from './NoticeTable';
import NoticeLectureTable from './NoticeLectureTables';
import NoticeInquiry from './NoticeInquiry';

export default function NoticeTabs(props: { tabs: string }) {
  const { tabs } = props;
  const history = useHistory();
  const handleClick = (path: string) => {
    history.push(path);
  };

  const selectedTabs = tabs;

  return (
    <>
      <Container className='container-lg'>
        <div className='mt-5'>
          <p className='display-5'>공지사항</p>
          <p className='h7'>
            고객의 필요에 맞춰 데이터마케팅에 대한 최적의 교육과 인프라를
            제공합니다
          </p>
        </div>

        <div className='nav-wrapper'>
          <Nav className='nav-fill flex-row' pills role='tablist'>
            <NavItem>
              <NavLink
                className={`mb-sm-0 mb-md-0 ${
                  selectedTabs === '/community/' ? 'active' : ''
                }`}
                href='#pablo'
                onClick={(e) => {
                  e.preventDefault();
                  handleClick('/community');
                }}
              >
                공지사항
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={`mb-sm-0 mb-md-0 ${
                  selectedTabs === '/community/materials' ? 'active' : ''
                }`}
                href='#pablo'
                onClick={(e) => {
                  e.preventDefault();
                  handleClick('/community/materials');
                }}
              >
                강의자료
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={`mb-sm-0 mb-md-0 ${
                  selectedTabs === '/community/inquiry' ? 'active' : ''
                }`}
                href='#pablo'
                onClick={(e) => {
                  e.preventDefault();
                  handleClick('/community/inquiry');
                }}
              >
                1:1 문의
              </NavLink>
            </NavItem>
          </Nav>
        </div>
        <TabContent id='myTabContent' activeTab={selectedTabs}>
          {selectedTabs === '/community/' && (
            <TabPane tabId='/community/' role='tabpanel'>
              <NoticeTable />
            </TabPane>
          )}
          {selectedTabs === '/community/materials' && (
            <TabPane tabId='/community/materials' role='tabpanel'>
              <NoticeLectureTable />
            </TabPane>
          )}
          {selectedTabs === '/community/inquiry' && (
            <TabPane tabId='/community/inquiry' role='tabpanel'>
              <NoticeInquiry />
            </TabPane>
          )}
        </TabContent>
      </Container>
    </>
  );
}
