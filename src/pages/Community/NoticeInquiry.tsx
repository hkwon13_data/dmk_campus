/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useState, useEffect } from 'react';
import { RootState } from 'store/modules/index';
import styled, { DefaultTheme } from 'styled-components';
import classnames from 'classnames';
import validator from 'validator';
// import { notification } from 'antd';
import { axiosInstance } from 'api';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import * as actions from 'store/modules/modalManages';
import {
  Button,
  FormGroup,
  Form,
  Input,
  Container,
  Row,
  Col,
  Label,
} from 'reactstrap';
import TermsModal from '../../components/FooterPopup/TermsModal';
import SendInquirySuccessModal from './SendInquirySuccessModal';
import SendInquiryErrorModal from './SendInquiryErrorModal';
// reactstrap components

// import NoticeQuill from './NoticeQuill';

// Core Components
// import imgA from 'assets/img/ill/1.svg';

const DivideLine = styled.div<DefaultTheme>`
  height: 0;
  margin: 1.5rem 0;
  overflow: hidden;
  border-top: 1px solid #333333;
`;

const DivideCustom = styled(DivideLine)<DefaultTheme>`
  margin: 0 0 1.5rem 0;
`;

const SetCenter = styled.div`
  text-align: center;
  margin-top: 2em;
`;

// const ToolTipVaild = styled.div<blockState: string>`
//   display: ${(props) => (props.blockStete === 'valid' ? 'none' : 'block')};
//   width: 100%;
//   margin-top: 0.25rem;
//   font-size: 80%;
//   color: #fc693a;
// `;

// const ToggleDiv = styled.div<{ toggleProps: boolean }>`
//   position: ${(props) =>
//     props.toggleProps === true ? 'relative' : 'absolute'};
//   visibility: ${(props) => (props.toggleProps === true ? 'visible' : 'hidden')};
//   opacity: ${(props) => (props.toggleProps === true ? '1' : '0')};
//   transition: ${(props) =>
//     props.toggleProps === true ? 'all 0.2s ease-in-out' : 'all 0s ease-in-out'};
// `;

export default function NoticeInquiry() {
  const dispatch = useDispatch();

  // 리덕스
  const { isTermsPopup } = useSelector(
    (state: RootState) => state.modalManages.terms
  );

  const handleTermsPopup = () => {
    dispatch(actions.setTermsModal(!isTermsPopup));
  };

  const { isSendInquirySuccessPopup } = useSelector(
    (state: RootState) => state.modalManages.community
  );

  const handleInquiryPopup = () => {
    dispatch(actions.setSendInquirySuccessModal(!isSendInquirySuccessPopup));
  };

  const { isSendInquiryErrorPopup } = useSelector(
    (state: RootState) => state.modalManages.community
  );

  const handleInquiryErrorPopup = () => {
    dispatch(actions.setSendInquiryErrorModal(!isSendInquiryErrorPopup));
  };

  const [errorMessage, setErrorMessage] = useState('');

  // 리덕스

  // const history = useHistory();
  const [userNameFocus, setUserNameFocus] = useState<string>('');
  const [partValueFocus, setPartValueFocus] = useState<string>('');
  const [focusedCompany, setFocusedCompany] = useState<string>('');
  const [eduObjectFocus, setEduObjectFocus] = useState<string>('');
  const [phoneFocus, setPhoneFocus] = useState<string>('');
  const [emailFocus, setEmailFocus] = useState<string>('');
  const [pathFromWhereFocus, setpathFromWhereFocus] = useState<string>('');
  const [textAreaFocus, setTextAreaFocus] = useState<string>('');

  const [userNameValid, setUserNameValid] = useState<string>('');
  const [partValueValid, setPartValueValid] = useState<string>('');
  const [companyValid, setCompanyValid] = useState<string>('');
  const [eduObjectValid, setEduObjectValid] = useState<string>('');
  const [phoneValid, setPhoneValid] = useState<string>('');
  const [emailValid, setEmailValid] = useState<string>('');
  // const [pathFromWhereValid, setpathFromWhereValid] = useState('');
  const [textAreaValid, setTextAreaValid] = useState('');

  const [userNameState, setUserNameState] = useState<string | null>(null);
  const [partValueState, setPartValueState] = useState<string | null>(null);
  const [companyState, setCompanyState] = useState<string | null>(null);
  const [eduObjectState, setEduObjectState] = useState<string | null>(null);
  const [phoneState, setPhoneState] = useState<string | null>(null);
  const [emailState, setEmailState] = useState<string | null>(null);
  // const [pathFromWhereState, setpathFromWhereState] = useState(null);
  // const [textAreaState, setTextAreaState] = useState(null);

  const [isDisablePath, setIsDisablePath] = useState<boolean>(true); // A component is changing a controlled input to be uncontrolled. 이슈로 인하여 초기값을 빈 문자열(false) 로 설정
  const [consent, setConsent] = useState<boolean>(false);
  const [btnSubmit, setBtnSubmit] = useState<boolean>(false);

  const [toggleProps, seTtoggleProps] = useState(false);

  const [inquiryType, setInquiryType] = useState('DM9');

  const [values, setValues] = useState({
    userName: '',
    partValue: '',
    company: '',
    eduObject: '',
    phone: '',
    email: '',
    pathFromWhere: '',
    textArea: '',
  });

  const {
    userName,
    partValue,
    company,
    eduObject,
    phone,
    email,
    pathFromWhere,
    textArea,
  } = values;

  const checkValidate = (e: React.ChangeEvent<HTMLInputElement>) => {
    const success = 'has-success';
    const danger = 'has-danger';
    switch (e.target.name) {
      case 'userName':
        if (
          validator.matches(e.target.value, '[ !@#$%^&*(),.?":{}|<>]') ||
          e.target.value === ''
        ) {
          setUserNameValid(danger);
          setUserNameState('invalid');
        } else {
          setUserNameValid(success);
          setUserNameState('valid');
        }
        break;

      case 'partValue':
        if (e.target.value === '') {
          setPartValueValid(danger);
          setPartValueState('invalid');
        } else {
          setPartValueValid(success);
          setPartValueState('valid');
        }

        break;

      case 'company':
        if (e.target.value === '') {
          setCompanyValid(danger);
          setCompanyState('invalid');
        } else {
          setCompanyValid(success);
          setCompanyState('valid');
        }

        break;

      case 'eduObject':
        if (e.target.value === '') {
          setEduObjectValid(danger);
          setEduObjectState('invalid');
        } else {
          setEduObjectValid(success);
          setEduObjectState('valid');
        }

        break;

      case 'phone':
        if (!validator.isMobilePhone(e.target.value, 'ko-KR')) {
          setPhoneValid(danger);
          setPhoneState('invalid');
        } else {
          setPhoneValid(success);
          setPhoneState('valid');
        }

        break;

      case 'email':
        if (!validator.isEmail(e.target.value)) {
          setEmailValid(danger);
          setEmailState('invalid');
        } else {
          setEmailValid(success);
          setEmailState('valid');
        }

        break;

      default:
        break;
    }
  };

  const handleDivToggle = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setInquiryType(value);
    if (
      value === '특강' ||
      value === '기업맞춤교육' ||
      value === '온라인 콘텐츠 임차'
    ) {
      seTtoggleProps(true);
    } else {
      seTtoggleProps(false);
    } // 문의유형 블럭 토글용

    if (value === '특강') {
      setValues({
        ...values,
        textArea: '- 교육 내용 : \n- 교육 일정 : \n- 교육 장소 : ',
      });
    } else if (value === '기업맞춤교육') {
      setValues({ ...values, textArea: '- 교육 일정 : \n- 교육 장소 : ' });
    } else if (value === '온라인 콘텐츠 임차') {
      setValues({ ...values, textArea: '- 임차시간(예상) : ' });
    } else {
      setValues({ ...values, textArea: '' });
    }
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setValues({ ...values, [name]: value });
    checkValidate(e);
  };

  const handleRadioChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value, id } = e.target;

    if (id === 'customRadio6') {
      setValues({ ...values, pathFromWhere: '' });
      setIsDisablePath(false);
    } else {
      setValues({ ...values, pathFromWhere: value });
      setIsDisablePath(true);
    }
  };

  const renderErrorMessage = (text: string) => {
    return <div className='invalid-feedback'>{text}</div>;
  };

  useEffect(() => {
    setBtnSubmit(!consent);
  }, [consent, values.userName]);

  const handleSubmitForm = (e: React.FormEvent<HTMLButtonElement>) => {
    e.preventDefault();

    if (
      inquiryType === '' ||
      email === '' ||
      userName === '' ||
      phone === '' ||
      company === '' ||
      partValue === '' ||
      eduObject === '' ||
      pathFromWhere === '' ||
      textArea === ''
    ) {
      //   notification.open({
      //     message: "문의하기 실패",
      //     description: "모든 항목은 필수입력사항 입니다.",
      //   });
      setErrorMessage('모든 항목은 필수입력사항 입니다.');
      handleInquiryErrorPopup();
      return;
    }
    if (
      emailState === 'invalid' ||
      phoneState === 'invalid' ||
      userNameState === 'invalid'
    ) {
      //   notification.open({
      //     message: "문의하기 실패",
      //     description:
      //       "잘못된 입력 항목이 있는지 다시한번 확인해 주시기 바랍니다.",
      //   });
      setErrorMessage(
        `잘못된 입력 항목이 있는지\n 다시한번 확인해 주시기 바랍니다.`
      );
      handleInquiryErrorPopup();
      return;
    }

    const formData = new FormData();

    formData.append('inquiry_type', inquiryType);
    formData.append('username', userName);
    formData.append('company', company);
    formData.append('department', partValue);
    formData.append('phone', phone);
    formData.append('email', email);
    formData.append('education_target', eduObject);
    formData.append('from_path', pathFromWhere);
    formData.append('inquiry_content', textArea);

    const response = axiosInstance.post('/community/inquiry/', formData);
    handleInquiryPopup();
  };

  return (
    <>
      <Container className='container-md mt-3 p-0'>
        <Form id='contact-form-1' method='post' role='form'>
          <div className='font-weight-900 h4 text-center'>1:1 문의</div>
          <div className='text-center mb-1'>
            데이터마케팅캠퍼스에 관심을가져 주셔서 감사합니다
          </div>
          <div className='text-center mb-5'>
            문의사항을 남겨 주시면 빠른 시일 내에 답변 드리겠습니다
          </div>
          <div className='text-right text-red'>
            <small>모든 항목을 입력해주셔야 문의 접수가 가능합니다.</small>
          </div>
          <div className='text-right text-red'>
            <small>보내주신 정보는 문의확인을 위한 용도로만 이용됩니다.</small>
          </div>
          <DivideCustom />
          <Row>
            <Col md='12'>
              <label className='form-control-label font-weight-700 h5'>
                문의자 이름
              </label>
              <FormGroup className={classnames(userNameValid)}>
                <Input
                  className='form-control'
                  aria-label='이름'
                  placeholder='이름을 입력해주세요'
                  type='text'
                  name='userName'
                  value={userName}
                  onChange={handleChange}
                  onFocus={() => setUserNameFocus('focused')}
                  onBlur={() => setUserNameFocus('')}
                  valid={userNameState === 'valid'}
                  invalid={userNameState === 'invalid'}
                />
                {userName === ''
                  ? renderErrorMessage('성명은 필수 입력사항 입니다.')
                  : renderErrorMessage('이름 입력이 잘못 되었습니다.')}
              </FormGroup>
            </Col>
            <Col md='6'>
              <label className='form-control-label font-weight-700 h5'>
                소속
              </label>
              <FormGroup className={classnames(companyValid)}>
                <Input
                  className='form-control'
                  aria-label='소속'
                  placeholder='소속(회사, 학교, 연구소)를 입력하여 주세요'
                  type='text'
                  name='company'
                  value={company}
                  onChange={handleChange}
                  onFocus={() => setFocusedCompany('focused')}
                  onBlur={() => setFocusedCompany('')}
                  valid={companyState === 'valid'}
                  invalid={companyState === 'invalid'}
                />
                <div className='invalid-feedback'>
                  소속은 필수 입력사항 입니다.
                </div>
              </FormGroup>
            </Col>
            <Col md='6'>
              <label className='form-control-label font-weight-700 h5'>
                부서
              </label>
              <FormGroup className={classnames(partValueValid)}>
                <Input
                  className='form-control'
                  aria-label='부서'
                  placeholder='부서명을 입력해 주세요'
                  type='text'
                  name='partValue'
                  value={partValue}
                  onChange={handleChange}
                  onFocus={() => setPartValueFocus('focused')}
                  onBlur={() => setPartValueFocus('')}
                  valid={partValueState === 'valid'}
                  invalid={partValueState === 'invalid'}
                />
                <div className='invalid-feedback'>
                  부서명은 필수 입력사항 입니다.
                </div>
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col md='12'>
              <Label className='form-control-label font-weight-700 h5'>
                이메일
              </Label>
              <FormGroup className={classnames(emailValid)}>
                <Input
                  // defaultValue={values.username}
                  className='form-control'
                  name='email'
                  value={email}
                  type='text'
                  autoComplete='off'
                  onFocus={() => setEmailFocus('focused')}
                  onBlur={() => setEmailFocus('')}
                  onChange={handleChange}
                  valid={emailState === 'valid'}
                  invalid={emailState === 'invalid'}
                  placeholder='이메일 주소를 입력해주세요'
                />
                {email === ''
                  ? renderErrorMessage('이메일은 필수 입력사항 입니다.')
                  : renderErrorMessage('이메일 주소가 잘못 되었습니다.')}
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col md='12'>
              <Label className='form-control-label font-weight-700 h5'>
                연락처
              </Label>
              <FormGroup className={classnames(phoneValid)}>
                <Input
                  // defaultValue={values.username}
                  className='form-control'
                  maxLength={11}
                  minLength={10}
                  name='phone'
                  value={phone}
                  type='text'
                  autoComplete='off'
                  onFocus={() => setPhoneFocus('focused')}
                  onBlur={() => setPhoneFocus('')}
                  onChange={handleChange}
                  valid={phoneState === 'valid'}
                  invalid={phoneState === 'invalid'}
                  placeholder="핸드폰 11자리를 '-' 없이 입력해주세요"
                />
                {phone === ''
                  ? renderErrorMessage(
                      '휴대폰 번호는 은 필수 입력사항 입니다..'
                    )
                  : renderErrorMessage('휴대폰 번호 형식이 잘못 되었습니다.')}
              </FormGroup>
            </Col>
          </Row>
          <DivideCustom />
          <Row className=''>
            <Col md='4'>
              <FormGroup>
                <Label
                  for='exampleSelect'
                  className='form-control-label font-weight-700 h5'
                >
                  문의 유형
                </Label>
                <Input
                  type='select'
                  name='inquiryType'
                  id='exampleSelect'
                  onChange={handleDivToggle}
                >
                  <option value='DM9'>DM9</option>
                  <option value='특강'>특강</option>
                  <option value='기업맞춤교육'>기업맞춤교육</option>
                  <option value='온라인 콘텐츠 임차'>온라인 콘텐츠 임차</option>
                  <option value='기타'>기타</option>
                </Input>
              </FormGroup>
            </Col>
          </Row>
          <Row className='align-items-center'>
            <Col md='12'>
              {/* <ToggleDiv toggleProps={toggleProps}> */}
              <label className='form-control-label'>교육대상</label>
              <FormGroup className={classnames(eduObjectValid)}>
                <Input
                  className='form-control'
                  aria-label='교육대상'
                  placeholder='교육대상을 입력해 주세요.'
                  type='text'
                  name='eduObject'
                  value={eduObject}
                  onChange={handleChange}
                  onFocus={() => setEduObjectFocus('focused')}
                  onBlur={() => setEduObjectFocus('')}
                  valid={eduObjectState === 'valid'}
                  invalid={eduObjectState === 'invalid'}
                />
                <div className='invalid-feedback'>
                  교육대상은 필수 입력사항 입니다..
                </div>
              </FormGroup>
              {/* </ToggleDiv> */}
            </Col>
          </Row>
          <DivideLine />
          <Row className='mb-3 align-items-end'>
            <Col>
              <div className='h5 font-weight-700 mb-0'>문의내용</div>
            </Col>
            <Col>
              <div className='text-red text-right'>
                <small>
                  교육 일정 및 내용에 대해 기재해주시면,
                  <br />
                  더욱 빠르고 정확한 안내를 도와드릴 수 있습니다.
                </small>
              </div>
            </Col>
          </Row>
          <FormGroup className={textAreaValid}>
            <Input
              id='contact-us-message-1 form-control'
              name='textArea'
              rows='4'
              type='textarea'
              value={textArea}
              onChange={handleChange}
              onFocus={() => setTextAreaFocus('focused')}
              onBlur={() => setTextAreaFocus('')}
            />
          </FormGroup>
          <DivideCustom />
          <Row className='mb-5'>
            <Col md='12'>
              <div className='font-weight-700 h5'>방문경로 선택</div>
            </Col>
            <Col md='3'>
              <div className='custom-control custom-radio mb-3'>
                <input
                  type='radio'
                  id='customRadio1'
                  name='customRadio'
                  value='인터넷 검색'
                  className='custom-control-input'
                  onChange={handleRadioChange}
                />
                <label className='custom-control-label' htmlFor='customRadio1'>
                  인터넷 검색
                </label>
              </div>
            </Col>
            <Col md='3'>
              <div className='custom-control custom-radio mb-3'>
                <input
                  type='radio'
                  id='customRadio2'
                  name='customRadio'
                  className='custom-control-input'
                  value='지인 추천'
                  onChange={handleRadioChange}
                />
                <label className='custom-control-label' htmlFor='customRadio2'>
                  지인 추천
                </label>
              </div>
            </Col>
            <Col md='3'>
              <div className='custom-control custom-radio mb-3'>
                <input
                  type='radio'
                  id='customRadio3'
                  name='customRadio'
                  className='custom-control-input'
                  value='각종 매체 광고'
                  onChange={handleRadioChange}
                />
                <label className='custom-control-label' htmlFor='customRadio3'>
                  각종 매체 광고
                </label>
              </div>
            </Col>
            <Col md='3'>
              <div className='custom-control custom-radio mb-3'>
                <input
                  type='radio'
                  id='customRadio4'
                  name='customRadio'
                  className='custom-control-input'
                  value='언론/뉴스기사'
                  onChange={handleRadioChange}
                />
                <label className='custom-control-label' htmlFor='customRadio4'>
                  언론/뉴스기사
                </label>
              </div>
            </Col>
            <Col md='3' style={{ display: 'flex', alignItems: 'center' }}>
              <div className='custom-control custom-radio'>
                <input
                  type='radio'
                  id='customRadio5'
                  name='customRadio'
                  value='SNS 광고'
                  className='custom-control-input'
                  onChange={handleRadioChange}
                />
                <label className='custom-control-label' htmlFor='customRadio5'>
                  SNS 광고
                </label>
              </div>
            </Col>
            <Col md='9'>
              <Row className='align-items-center'>
                <Col md='2'>
                  <div className='custom-control custom-radio'>
                    <input
                      type='radio'
                      id='customRadio6'
                      name='customRadio'
                      // value = "기타"
                      className='custom-control-input'
                      onChange={handleRadioChange}
                    />
                    <label
                      className='custom-control-label'
                      htmlFor='customRadio6'
                    >
                      기타
                    </label>
                  </div>
                </Col>
                <Col md='9'>
                  <Input
                    className='form-control'
                    id='pathFromWhere'
                    disabled={isDisablePath}
                    placeholder='방문경로를 입력해주세요'
                    name='pathFromWhere'
                    onChange={handleChange}
                    type='text'
                    value={pathFromWhere}
                    onFocus={() => setpathFromWhereFocus('focused')}
                    onBlur={() => setpathFromWhereFocus('')}
                  />
                </Col>
              </Row>
            </Col>
          </Row>
          <div>
            <div className='h5 font-weight-700'>
              개인정보 수집 및 이용에 대한 안내
            </div>
            <div>
              문의를 접수하고 회신하기 위해 반드시 필요한 범위 내에서 이용자의
              개인정보(이름, 소속, 및 부서, 이메일, 휴대폰번호) 를 수집하여
              후속조치를 위해 1년간 보관합니다. 개인정보 수집 및 이용을 거부할
              수 있으며, 이 경우 답변이 제한됩니다. 더 자세한 내용은&nbsp;
              <span
                onClick={() => handleTermsPopup()}
                className='font-weight-900'
                style={{ textDecoration: 'underline', cursor: 'pointer' }}
                onKeyDown={() => {}}
                role='button'
                tabIndex={0}
              >
                개인정보처리방침
              </span>
              을 참고하시기 바랍니다.
            </div>
            <Col>
              <SetCenter>
                <Button
                  className='btn btn-round'
                  color='primary'
                  type='submit'
                  // disabled={btnSubmit}
                  onClick={handleSubmitForm}
                >
                  <span className='h5 text-white font-weight-500 px-3'>
                    문의하기
                  </span>
                </Button>
              </SetCenter>
            </Col>
          </div>
        </Form>
        {isTermsPopup && (
          <TermsModal handleTermsPopup={() => handleTermsPopup()} />
        )}
        {/* <NoticeQuill /> */}
        {isSendInquirySuccessPopup && <SendInquirySuccessModal />}
        {isSendInquiryErrorPopup && (
          <SendInquiryErrorModal
            text={errorMessage}
            handleInquiryErrorPopup={() => handleInquiryErrorPopup()}
          />
        )}
      </Container>
    </>
  );
}
