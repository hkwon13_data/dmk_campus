/* eslint-disable consistent-return */
/* eslint-disable array-callback-return */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useState, useEffect } from 'react';
import * as types from 'types/types';
import { RootState } from 'store/modules/index';
import styled from 'styled-components';
import iconA from 'assets/icons/file_clip.png';
import { Row, Button, Col } from 'reactstrap';
import { useSelector, useDispatch } from 'react-redux';
import * as actions from 'store/modules/community';

const DivideLine = styled.div`
  height: 0;
  margin: 1.5rem 0;
  overflow: hidden;
  border-top: 2px solid #f4f5f7;
`;

// const DivideCustom = styled(DivideLine)`
//   margin: 0 0 1.5rem 0;
// `;

const DivideLineOne = styled(DivideLine)`
  margin: 0;
  border-top: 1px solid #3bd6ce;
`;
const DivideLineTwo = styled(DivideLine)`
  margin: 0;
  border-top: 2px solid #3bd6ce;
`;

interface NoticeInfoPropTypes {
  data: types.CommunityData[] | undefined;
  setOpenLecture: React.Dispatch<React.SetStateAction<boolean>>;
  clickedLecture: {
    mapId: number;
    postId: number | undefined;
  };
  setClickedLecture: React.Dispatch<
    React.SetStateAction<{
      mapId: number;
      postId: number | undefined;
    }>
  >;
  setOpenPasswordPopup: React.Dispatch<React.SetStateAction<boolean>>;
  setPassedPassword: React.Dispatch<React.SetStateAction<boolean>>;
}

function NoticeInfo({
  data,
  setOpenLecture,
  clickedLecture,
  setClickedLecture,
  setOpenPasswordPopup,
  setPassedPassword,
}: NoticeInfoPropTypes) {
  const dispatch = useDispatch();
  const { isPasswordCorrect } = useSelector(
    (state: RootState) => state.community
  );
  const postMaxLength = data?.length;
  const detail = data?.filter((value) => value.id === clickedLecture.postId)[0];
  const [hideNextPost, setHideNextPost] = useState(false);
  const [hidePrevPost, setHidePrevPost] = useState(false);

  const [selectedPost, setSelectedPost] = useState(detail);
  const htmlTag =
    '<p class="ql-align-center"><strong>2021년도 1학기 우월 김활란 장학회 장학금 신청안내</strong></p><p class="ql-align-center"><br></p><p class="ql-align-center">- 아래 -</p><p class="ql-align-center"><br></p><p>1. 선발인원: 2명</p><p><br></p><p>2. 지원내용: 200만원(1회) 학업보조비 성격</p><p><br></p><p>3. 지원자격(아래 내용을 모두 충족하는 학생에 한하여 신청 가능함)</p><ul><li>21-1학기 정규등록 학부 학생</li><li>북한이탈주민 본인 또는 자녀인 학생</li><li>학업성적이 우수하고, 품행이 단정하며 다른 학생에게 모범이 되는 학생</li><li>경제적 사정이 어려워 장학금을 필요로 하는 학생</li></ul><p><br></p><p>4. 구비서류</p><p>     가. 성적증명서 1부</p><p>     나. 신청서 1부</p><p>     다. 서약서 1부</p><p>     라. 자기소개서 1부</p><p>     마. 개인정보이용제공동의서 1부</p><p><br></p><p>5. 접수기한 및 접수처: 2021년 4월 26일(월) / 장학복지팀(학생문화관203호)</p>';

  useEffect(() => {
    setSelectedPost(
      data?.filter((value) => value.id === clickedLecture.postId)[0]
    );
    if (clickedLecture.postId === (data && data[0].id)) setHideNextPost(true);
    if (clickedLecture.postId === (data && data[data.length - 1]?.id))
      setHidePrevPost(true);
  }, [data, clickedLecture]);

  const handleNoticeNext = (clickedPostId: any, clickedMapId?: any) => {
    const tempMapIdx =
      (data?.findIndex((val, idx) => {
        if (val.id === clickedPostId) return idx;
      }) as number) - 1;

    const tempMapId = tempMapIdx < 0 ? 0 : tempMapIdx;
    const tempPostId = data?.filter((val, idx) => idx === tempMapId)[0].id;

    if (tempMapId <= 0) {
      setClickedLecture({ mapId: 0, postId: tempPostId });
      setHideNextPost(true);
    } else if (tempMapId < (postMaxLength as number) - 1) {
      setClickedLecture({ mapId: tempMapId, postId: tempPostId });
      setHidePrevPost(false);
      setHideNextPost(false);
    } else {
      setHidePrevPost(true);
    }
  };

  const handleNoticePrev = (clickedPostId: any, clickedMapId?: any) => {
    const tempMapIdx =
      (data?.findIndex((val, idx) => {
        if (val.id === clickedPostId) return idx;
      }) as number) + 1;
    const tempMapId =
      tempMapIdx > (postMaxLength as number) - 1
        ? (postMaxLength as number) - 1
        : tempMapIdx;

    const tempPostId = data?.filter((val, idx) => idx === tempMapId)[0].id;
    if (tempMapId === 0) {
      const tempPostIdx = data?.filter((val, idx) => idx === 1)[0].id;
      setClickedLecture({ mapId: 1, postId: tempPostIdx });
      setHideNextPost(false);
    } else if (tempMapId > (postMaxLength as number)) {
      setClickedLecture({ mapId: 0, postId: tempPostId });
      setHideNextPost(true);
    } else if (tempMapId < (postMaxLength as number)) {
      setClickedLecture({ mapId: tempMapId, postId: tempPostId });
      setHidePrevPost(false);
      setHideNextPost(false);
      if (tempMapId === (postMaxLength as number) - 1) {
        setHidePrevPost(true);
      }
    } else {
      setHidePrevPost(true);
    }
  };
  return (
    <div>
      <div className='section section-content-areas'>
        <DivideLineOne />
        <Row className='w-100 mx-0 py-3 pl-3' style={{ borderTop: '1px' }}>
          <div className='pl-3'>{detail?.title}</div>
        </Row>
        <DivideLineTwo />
        <Row>
          <Col lg='6' className='pl-lg-5 py-3'>
            데이터마케팅코리아 | {detail?.created_at.toString().split('T')[0]}
          </Col>
          <Col lg='6' className='pl-lg-5 py-3 text-right'>
            <img src={iconA} alt='file' />
            <small className='px-2'>첨부파일1</small>
            <br />
            <img src={iconA} alt='file' />
            <small className='px-2'>첨부파일2</small>
            <br />
            <img src={iconA} alt='file' />
            <small className='px-2'>첨부파일3</small>
          </Col>
        </Row>
        <DivideLineOne />
        <Row>
          <Col className='px-7 pt-3 pb-3'>
            <div dangerouslySetInnerHTML={{ __html: htmlTag }} />
          </Col>
        </Row>
        <DivideLineOne />
        <Row className='align-items-right py-4'>
          <Col lg='6' xs='7' />
          <Col className='text-right' lg='6' xs='5'>
            <Button
              className='btn-neutral'
              // outline
              style={{
                width: '5rem',
                boxShadow: 'none',
                borderColor: 'darkgray',
              }}
              color='default'
              onClick={() => {
                setOpenPasswordPopup(false);
                setOpenLecture(false);
                dispatch(actions.setAuthLecturePost(false));
                setPassedPassword(false);
              }}
              size='sm'
            >
              목록
            </Button>
          </Col>
        </Row>
      </div>
    </div>
  );
}

export default NoticeInfo;
