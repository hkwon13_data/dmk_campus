import React from 'react';
import { useSelector } from 'react-redux';
import { RootState } from 'store/modules/index';
import { Button, Modal, Row } from 'reactstrap';

function SendInquirySuccessModal() {
  const { isSendInquirySuccessPopup } = useSelector(
    (state: RootState) => state.modalManages.community
  );

  return (
    <>
      <Modal
        isOpen={isSendInquirySuccessPopup}
        // toggle={() => handleInquiryPopup(!isSendInquirySuccessPopup)}
        className='modal-dialog-centered modal-md'
      >
        <div className='modal-body'>
          <div className='font-weight-700 text-center h3 mb-2'>
            <i className='ni ni-email-83' />
          </div>
          <div className='text-center mb-32 h5'>
            성공적으로 문의가 접수 되었습니다.
          </div>
          <div className='text-center mb-4 h5'>
            확인을 누르시면 메인 페이지로 이동 합니다.
          </div>
          <Row className='justify-content-center'>
            <div>
              <Button
                className='btn px-4'
                color='primary'
                onClick={() => {
                  window.localStorage.clear();
                  document.location.href = '/';
                }}
              >
                확인
              </Button>
            </div>
          </Row>
        </div>
      </Modal>
    </>
  );
}

export default SendInquirySuccessModal;
