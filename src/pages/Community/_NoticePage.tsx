import React from 'react';
import { useRouteMatch } from 'react-router-dom';
import CommonNavbar from 'components/NavBar/CommonNavbar';
import CommonFooter from 'components/NavBar/CommonFooter';
import { ScrollTopButton } from 'components/utils/ScrollTopButton';
import { useDispatch } from 'react-redux';
import { getNotices, getLectureMaterials } from 'store/modules/community';
import FloatingButton from '../../components/NavBar/FloatingButton';

import NoticeTabs from './NoticeTabs';

export default function NoticePage() {
  const dispatch = useDispatch();
  const match = useRouteMatch();
  React.useEffect(() => {
    dispatch(getNotices(undefined));
    dispatch(getLectureMaterials(undefined));
  });

  React.useEffect(() => {
    // let classList = document.body.classList;
    // document.body.classList.remove(...classList);
    // document.body.classList.add('index-page');
    window.scrollTo(0, 0);
    // return function cleanup() {
    //   document.body.classList.remove('index-page');
    // };
  }, []);

  const selectedTabs = match.path;
  return (
    <>
      <ScrollTopButton />
      <CommonNavbar type='primary' />
      <FloatingButton />
      <div className='section section-content-areas'>
        <NoticeTabs tabs={selectedTabs} />
      </div>
      <CommonFooter />
    </>
  );
}
