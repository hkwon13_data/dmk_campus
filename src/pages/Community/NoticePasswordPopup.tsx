/* eslint-disable @typescript-eslint/no-shadow */
import React from 'react';

// reactstrap components
import {
  Button,
  Card,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Modal,
} from 'reactstrap';
import { RootState } from 'store/modules/index';
import * as actions from 'store/modules/community';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import styled from 'styled-components';

// Core Components

interface LecturePasswordPopupPropTypes {
  setPassedPassword: React.Dispatch<React.SetStateAction<boolean>>;
  isModalOpen: boolean;
  // setOpenPasswordPopup: React.Dispatch<React.SetStateAction<boolean>>;
}

const HiddenDiv = styled.div<{ hidden: boolean }>`
  display: ${(props) => (props.hidden ? 'none' : 'block')};
  width: 100%;
  margin-top: 0.25rem;
  font-size: 80%;
  color: #fc693a;
`;
export default function PasswordPopup({
  setPassedPassword,
  isModalOpen,
}: // setOpenPasswordPopup,
LecturePasswordPopupPropTypes) {
  const dispatch = useDispatch();
  const { selectedPassword } = useSelector(
    (state: RootState) => state.community
  );
  const history = useHistory();
  const [modalOpen, setModalOpen] = React.useState(isModalOpen);
  const [passwordFocus, setPasswordFocus] = React.useState('');
  const [isPassword, setIsPassword] = React.useState('');
  const [isHidden, setIsHidden] = React.useState(true);

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setIsHidden(true);
    setIsPassword(e.target.value);
  };

  const handlePassword = () => {
    if (isPassword === selectedPassword) {
      setModalOpen(false);
      dispatch(actions.setAuthLecturePost(true));
      setPassedPassword(true);
    } else {
      setIsHidden(false);
    }
  };

  return (
    <>
      <Modal
        isOpen={modalOpen}
        toggle={() => setModalOpen(!modalOpen)}
        className='modal-dialog-centered modal-sm'
      >
        <div className='modal-body p-0'>
          <Card className='bg-secondary shadow border-0 mb-0'>
            <CardBody className='px-lg-5 py-lg-5'>
              <Form role='form'>
                <FormGroup className={passwordFocus}>
                  발급받으신 비밀번호를 입력해주세요
                  <InputGroup className='input-group-alternative'>
                    <InputGroupAddon addonType='prepend'>
                      <InputGroupText>
                        <i className='ni ni-lock-circle-open' />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input
                      style={{ fontFamily: 'sans-serif' }}
                      placeholder='Password'
                      type='password'
                      value={isPassword}
                      onFocus={() => setPasswordFocus('focused')}
                      onBlur={() => setPasswordFocus('')}
                      onChange={(e) => handleChange(e)}
                    />
                  </InputGroup>
                  <HiddenDiv hidden={isHidden}>
                    비밀번호를 다시 입력해주세요
                  </HiddenDiv>
                </FormGroup>
                <div className='text-center'>
                  <Button
                    className='my-4'
                    color='secondary'
                    type='button'
                    onClick={(modalOpen) => {
                      setModalOpen(!modalOpen);
                      history.push('/community');
                    }}
                  >
                    닫기
                  </Button>
                  <Button
                    className='my-4'
                    color='primary'
                    type='button'
                    onClick={handlePassword}
                  >
                    확인
                  </Button>
                </div>
              </Form>
            </CardBody>
          </Card>
        </div>
      </Modal>
    </>
  );
}
