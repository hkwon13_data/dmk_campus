import React, { useState } from "react";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";

export default function NoticeQuill() {
	let defaultSetting =
		"<p>이메일: <br/></p><p><br/> 문의내용: <br/><br/></p>      ";
	const [value, setValue] = useState(defaultSetting);

	return (
		<ReactQuill
			theme="snow"
			value={value}
			onChange={setValue}
			modules={{
				toolbar: [
					[{ size: ["small", false, "large", "huge"] }], // custom dropdown
					[{ header: [1, 2, 3, 4, 5, 6, false] }],

					["bold", "italic", "underline", "strike"], // toggled buttons
					["link", "image"],
					["blockquote", "code-block"],

					[{ header: 1 }, { header: 2 }], // custom button values
					[{ list: "ordered" }, { list: "bullet" }],
					[{ indent: "-1" }, { indent: "+1" }], // outdent/indent
					[{ direction: "rtl" }], // text direction

					[{ color: [] }, { background: [] }], // dropdown with defaults from theme
					[{ align: [] }],

					["clean"], // remove formatting button
				],
			}}
		/>
	);
}
