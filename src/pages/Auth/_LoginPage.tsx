import React from 'react';
import { Button } from 'reactstrap';

import CommonNavbar from 'components/NavBar/CommonNavbar';
import CommonFooter from 'components/NavBar/CommonFooter';
import LoginForm from 'components/Auth/LoginForm';

export default function LoginPage() {
  React.useEffect(() => {
    document.body.classList.add('login-page');
    window.scrollTo(0, 0);
    document.body.scrollTop = 0;
    return function cleanup() {
      document.body.classList.remove('login-page');
    };
  });
  return (
    <>
      <Button
        className='btn-icon-only back-to-top show'
        color='primary'
        name='button'
        type='button'
        onClick={() => {
          window.scrollTo(0, 0);
          document.body.scrollTop = 0;
        }}
      >
        <i className='ni ni-bold-up' />
      </Button>
      <CommonNavbar type='transparent' />
      <LoginForm />
      <CommonFooter />
    </>
  );
}
