import React from 'react';

// reactstrap components
import { Button } from 'reactstrap';

import imgA from 'assets/img/main/mainHeader.jpg';

import { useHistory } from 'react-router-dom';

function MainCompanyEdu() {
  const history = useHistory();
  return (
    <>
      <div className='text-center pt-5'>
        <div className='h1 font-weight-900 text-dark'>기업교육</div>
        <p className='text-dark h5 font-weight-500'>
          마케팅 의사결정, 왜 데이터로 해야 할까요?
        </p>
        <p className='text-dark h5 font-weight-500'>
          그 해답을 DMK에서 알려드립니다.
        </p>
        <p className='text-dark h5 font-weight-500 mb-3'>
          수많은 최상위 기업들이 이미 함께하고 있습니다.
        </p>
        <div className='mb-3'>
          <Button
            type='button'
            className='btn btn-round'
            onClick={() => history.push('/customEdu/')}
          >
            <span className='font-weight-900'>기업 교육 자세히 보기</span>
          </Button>
        </div>
        <div>
          <Button
            type='button'
            color='primary'
            className='btn btn-round btn-primary px-5 mb-5'
            onClick={() => history.push('/community/inquiry/')}
          >
            <span className='font-weight-900'>교육 문의하기</span>
          </Button>
        </div>
        <img src={imgA} alt='..' width='100%' />
      </div>
    </>
  );
}

export default MainCompanyEdu;
