import React from 'react';
import { Row, Col, Container } from 'reactstrap';

import imgA from 'assets/img/main/youtube_ad.png';

// import { useHistory } from 'react-router-dom';

export default function MainBottomBanners() {
  return (
    <>
      <Container className='my-5'>
        <Row className='pt-2 align-item-center'>
          <Col lg='12' md='6' className='mx-auto'>
            <a
              href='https://www.youtube.com/channel/UCRex91PJUeNnQoP7F77oizg'
              target='_blank'
              rel='noreferrer'
            >
              <div className='image p-2'>
                <img
                  src={imgA}
                  className='img-fluid rounded shadow'
                  alt='...'
                />
              </div>
            </a>
          </Col>
        </Row>
      </Container>
    </>
  );
}
