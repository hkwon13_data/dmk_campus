/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React from 'react';
import { Row, Col, Container } from 'reactstrap';

import Carousel from 'react-bootstrap/Carousel';

import { useHistory } from 'react-router-dom';

import imgA from 'assets/img/main/carousel_one.png';
import imgB from 'assets/img/main/carousel_two.png';
import imgC from 'assets/img/main/carousel_three.png';
// import imgC from "assets/img/dmkhall/main_3.png";
// import imgD from "assets/img/dmkhall/main_4.png";

// const items = [
//   {
//     src: imgB,
//     altText: 'Slide 1',
//     caption: '',
//     header: '',
//     key: '1',
//   },
//   {
//     src: imgA,
//     altText: 'Slide 2',
//     caption: '',
//     header: '',
//     key: '2',
//   },
//   {
//     src: imgC,
//     altText: 'Slide 3',
//     caption: '',
//     header: '',
//     key: '3',
//   },
// ];

const MainCarousel = () => {
  const [index, setIndex] = React.useState(0);

  const handleSelect = (selectedIndex: any) => {
    setIndex(selectedIndex);
  };

  const history = useHistory();

  return (
    <Container fluid>
      <Row className='pt-7'>
        <Col lg='12' md='12' sm='12' className='mx-auto p-0'>
          {/* <UncontrolledCarousel className="" items={items} /> */}
          <Carousel activeIndex={index} onSelect={handleSelect}>
            <Carousel.Item>
              <img
                className='d-block w-100'
                src={imgB}
                alt='First slide'
                onClick={() => history.push('/intro/')}
                style={{ cursor: 'pointer' }}
              />
            </Carousel.Item>
            <Carousel.Item>
              <img
                className='d-block w-100'
                src={imgA}
                alt='Second slide'
                onClick={() => history.push('/openProcess/')}
                style={{ cursor: 'pointer' }}
              />
            </Carousel.Item>
            <Carousel.Item>
              <img
                className='d-block w-100'
                src={imgC}
                alt='Third slide'
                onClick={() => history.push('/openProcess/mmc/')}
                style={{ cursor: 'pointer' }}
              />
            </Carousel.Item>
          </Carousel>
        </Col>
      </Row>
    </Container>
  );
};

export default MainCarousel;
