import React from 'react';

// import { Container, Row, Button } from 'reactstrap';

import CommonFooter from 'components/NavBar/CommonFooter';
import CommonNavbar from 'components/NavBar/CommonNavbar';
import { ScrollTopButton } from 'components/utils/ScrollTopButton';

import MainCarousel from 'pages/Main/MainCarousel';
import MainServiceDesc from 'pages/Main/MainServiceDesc';
// import MainLectureApply from "pages/Main/MainLectureApply";
import MainBottomBanners from 'pages/Main/MainBottomBanners';
import MinSectionCarousel from './MinSectionCarousel';
import MainCompanyEdu from './MainCompanyEdu';
import FloatingButton from '../../components/NavBar/FloatingButton';

export default function Home() {
  // const scrollRef = createRef();

  // const handleScrollFooter = () => {
  // 	scrollRef.current.scrollIntoView({ behavior: 'smooth', block: 'end', inline: 'nearest' });
  // }

  React.useEffect(() => {
    // const { classList } = document.body;
    // document.body.classList.remove(...classList);
    // document.body.classList.add('index-page');
    window.scrollTo(0, 0);
    // return function cleanup() {
    //   document.body.classList.remove('index-page');
    // };
  }, []);
  const test_sangbong = 'genious';
  return (
    <>
      <ScrollTopButton />
      <CommonNavbar type='white' img='black' />
      <FloatingButton />
      <MainCarousel />
      <MainCompanyEdu />
      <MainServiceDesc />
      {/* <MainLectureApply /> 이달의 강의 */}
      <MinSectionCarousel />
      <MainBottomBanners />
      <CommonFooter />
    </>
  );
}
