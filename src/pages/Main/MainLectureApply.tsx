import React from 'react';

import {
  Button,
  Card,
  CardImg,
  CardBody,
  CardTitle,
  Container,
  Row,
  Col,
} from 'reactstrap';

// import imgA from 'assets/img/theme/masha-rostovskaya.jpg';
// import imgB from 'assets/img/theme/ali-pazani.jpg';
// import imgC from 'assets/img/theme/willy-dade.jpg';
import imgD from 'assets/img/main/prepare_lecture.png';

const style = { width: '18rem' };

interface MainLectureApplyTypes {
  subtitle: string;
  title: string;
  period: string;
  day: string;
  time: string;
}

export default function MainLectureApply() {
  const onlineData: MainLectureApplyTypes[] = [
    {
      subtitle: 'DM9_2영역 현업필수 트렌드 시장분석',
      title: '한 눈에 보는 브랜드트렌드 분석 - DM9필수영역',
      period: '2021.05.13 ~ 2021.05.30',
      day: '2021.06.11(금)',
      time: '오후 13:00 ~ 16:00',
    },
    {
      subtitle: 'DM9_2영역 현업필수 트렌드 시장분석',
      title: '한 눈에 보는 브랜드트렌드 분석 - DM9필수영역',
      period: '2021.05.13 ~ 2021.05.30',
      day: '2021.06.11(금)',
      time: '오후 13:00 ~ 16:00',
    },
    {
      subtitle: 'DM9_2영역 현업필수 트렌드 시장분석',
      title: '한 눈에 보는 브랜드트렌드 분석 - DM9필수영역',
      period: '2021.05.13 ~ 2021.05.30',
      day: '2021.06.11(금)',
      time: '오후 13:00 ~ 16:00',
    },
    {
      subtitle: 'DM9_2영역 현업필수 트렌드 시장분석',
      title: '한 눈에 보는 브랜드트렌드 분석 - DM9필수영역',
      period: '2021.05.13 ~ 2021.05.30',
      day: '2021.06.11(금)',
      time: '오후 13:00 ~ 16:00',
    },
  ];

  const offlineData: MainLectureApplyTypes[] = [
    {
      subtitle: 'DM9_2영역 현업필수 트렌드 시장분석',
      title: '한 눈에 보는 브랜드트렌드 분석 - DM9필수영역',
      period: '2021.05.13 ~ 2021.05.30',
      day: '2021.06.11(금)',
      time: '오후 13:00 ~ 16:00',
    },
    {
      subtitle: 'DM9_2영역 현업필수 트렌드 시장분석',
      title: '한 눈에 보는 브랜드트렌드 분석 - DM9필수영역',
      period: '2021.05.13 ~ 2021.05.30',
      day: '2021.06.11(금)',
      time: '오후 13:00 ~ 16:00',
    },
    {
      subtitle: 'DM9_2영역 현업필수 트렌드 시장분석',
      title: '한 눈에 보는 브랜드트렌드 분석 - DM9필수영역',
      period: '2021.05.13 ~ 2021.05.30',
      day: '2021.06.11(금)',
      time: '오후 13:00 ~ 16:00',
    },
    {
      subtitle: 'DM9_2영역 현업필수 트렌드 시장분석',
      title: '한 눈에 보는 브랜드트렌드 분석 - DM9필수영역',
      period: '2021.05.13 ~ 2021.05.30',
      day: '2021.06.11(금)',
      time: '오후 13:00 ~ 16:00',
    },
  ];

  const renderEduCard = (data: MainLectureApplyTypes[]) => {
    return data.map((item, index) => {
      return (
        // eslint-disable-next-line react/no-array-index-key
        <Col lg='3' md='6' xs='12' key={index}>
          <Card className='p-0' style={style}>
            <CardImg alt='카드' src={imgD} top />
            <CardBody>
              <small className='font-weight-700 text-primary'>
                {item.subtitle}
              </small>
              <CardTitle className='font-weight-900 mt-3' tag='h5'>
                {item.title}
              </CardTitle>
              <div className='mb-4'>
                <Row>
                  <div className='text-muted float-left pl-3'>
                    <small>수강신청기간</small>
                  </div>
                  <div style={{ alignSelf: 'center' }}>
                    <small className='float-right pl-3'>{item.period}</small>
                  </div>
                </Row>
                <Row>
                  <div className='text-muted float-left pl-3'>
                    <small>교육일</small>
                  </div>
                  <div
                    className='float-right pl-5'
                    style={{ alignSelf: 'center' }}
                  >
                    <small style={{ paddingLeft: '2px' }}>{item.day}</small>
                  </div>
                </Row>
                <Row>
                  <div className='text-muted float-left pl-3'>
                    <small>교육 시간</small>
                  </div>
                  <div
                    style={{ alignSelf: 'center' }}
                    className='float-right pl-4'
                  >
                    <small style={{ paddingLeft: '10px' }}>{item.time}</small>
                  </div>
                </Row>
              </div>
              <Row className='justify-content-center'>
                <Button
                  className='col-10'
                  color='primary'
                  href='#pablo'
                  onClick={(e) => e.preventDefault()}
                >
                  오픈 알림 신청하기
                </Button>
              </Row>
            </CardBody>
          </Card>
        </Col>
      );
    });
  };

  return (
    <>
      <Container className='my-5'>
        <div className='ml-auto mr-auto text-center'>
          <div className='display-5'>이달의 과정</div>
        </div>
        <div className='mt-5 mb-4'>
          <div className='h7 font-weight-800 mb-3'>오프라인 강의</div>
        </div>
        <Row className=''>{renderEduCard(onlineData)}</Row>
        <div className='mt-5 mb-4'>
          <div className='h7 font-weight-800 mb-3'>온라인 강의</div>
        </div>
        <Row>{renderEduCard(offlineData)}</Row>
      </Container>
    </>
  );
}
