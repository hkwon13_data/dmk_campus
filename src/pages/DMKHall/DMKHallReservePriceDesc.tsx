import React from 'react';

// reactstrap components
import { Col, Row, Container, Table } from 'reactstrap';
import DMKHallReserveDatePicker from './DMKHallReserveDatePicker';
import DMKHallReserveDiscount from './DMKHallReserveDiscount';
import DMKHallReserveConsent from './DMKHallReserveConsent';

interface HiddenPropTypes {
  hidden: boolean;
}

export default function DMKHallReservePriceDesc(prop: HiddenPropTypes) {
  const { hidden } = prop;
  return (
    <>
      <Container className='my-5'>
        <Row className='justify-content-center'>
          <Col lg='8'>
            <div className='mb-3'>
              <span className='text-lg font-weight-800 mb-3'>대관요금안내</span>
            </div>
            <div className='table'>
              <Table responsive striped>
                <thead className='text-muted text-center'>
                  <tr
                    style={{
                      fontSize: '2rem',
                      lineHeight: 2,
                    }}
                  >
                    <th className='text-sm'>대관 가능 시간/요일</th>
                    <th className='text-sm'>평일</th>
                    <th className='text-sm'>주말, 공휴일</th>
                  </tr>
                </thead>
                <tbody>
                  <tr className='text-center font-weight-800'>
                    <td className='text-sm'>04:00 ~ 09:00</td>
                    <td className='text-sm'>180,000원 / 1hour</td>
                    <td className='text-sm'>200,000원 / 1hour</td>
                  </tr>
                  <tr className='text-center font-weight-800'>
                    <td className='text-sm'>09:00 ~ 18:00</td>
                    <td className='text-sm'>150,000원 / 1hour</td>
                    <td className='text-sm'>180,000원 / 1hour</td>
                  </tr>
                  <tr className='text-center font-weight-800'>
                    <td className='text-sm'>18:00 ~ 24:00</td>
                    <td className='text-sm'>180,000원 / 1hour</td>
                    <td className='text-sm'>200,000원 / 1hour</td>
                  </tr>
                </tbody>
              </Table>
            </div>
            <DMKHallReserveDiscount />
            <DMKHallReserveConsent hidden={hidden} />
          </Col>
          <Col lg='4' md='12'>
            <DMKHallReserveDatePicker />
          </Col>
          {/* Todo: 달력 및 인원 시간 로직 추가 */}
        </Row>
      </Container>
    </>
  );
}

// const tableHeader = {
//   fontSize: '2rem',
//   lineHeight: 2,
// };

// const tableTRow = {
//   fontWeight: 'bold',
// };

// const tableTitle = {
//   fontWeight: 'bold',
//   color: 'black',
// };
