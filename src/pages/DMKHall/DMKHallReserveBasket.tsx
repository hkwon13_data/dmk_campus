// import Item from 'antd/lib/list/Item';
import React from 'react';
import { RootState } from 'store/modules/index';
import { Row, Container, Col, Toast, Button } from 'reactstrap';
import { withRouter, useHistory } from 'react-router-dom';
import { useSelector } from 'react-redux';

function BasketButton() {
  const history = useHistory();
  // const [selected, setSelected] = useState('예약인원');
  const dmkHallData = useSelector(
    (state: RootState) => state.dmkReserveManage.dmkHallReservation
  );
  // const handleChange = (data) => {
  //   setSelected(data);
  // };

  // eslint-disable-next-line consistent-return
  const reserveClick = (): Function | void => {
    if (dmkHallData.reserveHour[0] === '0') {
      return alert('시간대를 선택 해주세요');
    }
    history.push('/dmkHall/reservation/payment');
  };

  const Divier = {
    height: 0,
    margin: '0 1rem 1rem',
    overflow: 'hidden',
    borderTop: '2px solid #e9ecef',
  };

  return (
    <>
      <div className='text-primary text-sm font-weight-800'>예약 정보</div>
      <Container>
        <Row className='justify-content-center mt-2'>
          <Toast className='bg-white' style={{ width: '320px' }}>
            <Row className='mt-3'>
              <Col lg='3'>
                <small className='text-muted pl-4 font-weight-900'>일시</small>
              </Col>
              <Col lg='9'>
                <small className='text-left font-weight-900'>
                  {`${dmkHallData.year}년 ${dmkHallData.month}월 ${dmkHallData.day}일 ${dmkHallData.week}요일 `}{' '}
                  {`${dmkHallData.reserveHour[0]}-${
                    dmkHallData.reserveHour[dmkHallData.reserveHour.length - 1]
                  }`}
                  시({dmkHallData.reserveTime}시간)
                </small>
              </Col>
            </Row>
            <Row>
              <Col lg='3'>
                <small className='text-muted pl-4 font-weight-900'>인원</small>
              </Col>
              <Col lg='9'>
                <small className='text-left font-weight-900'>
                  {dmkHallData.people}명
                </small>
              </Col>
            </Row>
            <div style={Divier} className='my-3' />
            <Row className='mt-4'>
              <Col lg='6'>
                <p className='text-left pl-3 font-weight-900'>총 공간사용료</p>
              </Col>
              <Col lg='6'>
                <p className='text-right pr-3 font-weight-900'>
                  {`${dmkHallData.totalPrice
                    .toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                  원`}
                </p>
              </Col>
            </Row>
            <Row>
              <Col lg='6'>
                <p className='text-left pl-3 font-weight-900'>할인금액</p>
              </Col>
              <Col lg='6'>
                <p className='text-right pr-3 font-weight-900'>
                  {dmkHallData.reserveTime >= 10
                    ? dmkHallData.discountPrice
                        .toString()
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ',')
                    : '0'}
                  원
                </p>
              </Col>
            </Row>
            <div style={Divier} />
            <div className='text-primary text-right pr-3'>
              <small className='font-weight-800'>
                {dmkHallData.reserveTime >= 10
                  ? '할인 적용!'
                  : `${
                      10 - dmkHallData.reserveTime
                    }시간 더 추가예약시, 10% 할인!`}
                {/* 1시간 더 추가예약시, 10% 할인 */}
              </small>
            </div>
            <Row className='mt-1'>
              <Col lg='6'>
                <p className='text-left pl-3 font-weight-900'>결제예정금액</p>
              </Col>
              <Col lg='6'>
                <p className='text-right pr-3 font-weight-900'>
                  {dmkHallData.reserveTime >= 10
                    ? dmkHallData.lastPrice
                        .toString()
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ',')
                    : dmkHallData.totalPrice
                        .toString()
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                  원
                </p>
              </Col>
            </Row>
          </Toast>
        </Row>
        <Row className='p-2 my-2'>
          <Button
            type='button'
            color='primary'
            style={{ minWidth: '-webkit-fill-available' }}
            onClick={reserveClick}
          >
            예약 하기
          </Button>
        </Row>
      </Container>
    </>
  );
}
// onClick={prop.setMatchPath("/dmkHall/payment")}>

export default withRouter(BasketButton);
