/* eslint-disable consistent-return */
/* eslint-disable array-callback-return */
import React, { useState, useEffect } from 'react';
import { RootState } from 'store/modules/index';
import ScrollContainer from 'react-indiana-drag-scroll';
import { Button, ButtonGroup } from 'reactstrap';
import { setDmkHallReserveValue } from 'store/modules/dmkReserveManage';
import { useDispatch, useSelector } from 'react-redux';

interface ButtonStateItemTypes {
  id?: string;
  disabled?: boolean;
  active?: boolean;
  time?: string;
  price?: '200,000' | '180,000' | '150,000';
}

interface IsHolidayPropTypes {
  isHoliday: boolean;
}

export default function TimeButton({ isHoliday }: IsHolidayPropTypes) {
  const holiday = isHoliday;
  const fourToNine = holiday ? '200,000' : '180,000';
  const nineToEightTeen = holiday ? '180,000' : '150,000';
  const eightTeenToZero = holiday ? '200,000' : '180,000';

  // const [isDisabled, setIsDisabled] = useState(false);s

  const dispatch = useDispatch();

  const dmkHallData = useSelector(
    (state: RootState) => state.dmkReserveManage.dmkHallReservation
  );

  const [buttonState, setButtonState] = useState<ButtonStateItemTypes[]>([
    {
      id: '0',
      disabled: false,
      active: false,
      time: '04',
      price: fourToNine,
    },
    {
      id: '1',
      disabled: false,
      active: false,
      time: '05',
      price: fourToNine,
    },
    {
      id: '2',
      disabled: false,
      active: false,
      time: '06',
      price: fourToNine,
    },
    {
      id: '3',
      disabled: false,
      active: false,
      time: '07',
      price: fourToNine,
    },
    {
      id: '4',
      disabled: false,
      active: false,
      time: '08',
      price: fourToNine,
    },
    {
      id: '5',
      disabled: false,
      active: false,
      time: '09',
      price: nineToEightTeen,
    },
    {
      id: '6',
      disabled: false,
      active: false,
      time: '10',
      price: nineToEightTeen,
    },
    {
      id: '7',
      disabled: false,
      active: false,
      time: '11',
      price: nineToEightTeen,
    },
    {
      id: '8',
      disabled: false,
      active: false,
      time: '12',
      price: nineToEightTeen,
    },
    {
      id: '9',
      disabled: false,
      active: false,
      time: '13',
      price: nineToEightTeen,
    },
    {
      id: '10',
      disabled: false,
      active: false,
      time: '14',
      price: nineToEightTeen,
    },
    {
      id: '11',
      disabled: false,
      active: false,
      time: '15',
      price: nineToEightTeen,
    },
    {
      id: '12',
      disabled: false,
      active: false,
      time: '16',
      price: nineToEightTeen,
    },
    {
      id: '13',
      disabled: false,
      active: false,
      time: '17',
      price: nineToEightTeen,
    },
    {
      id: '14',
      disabled: false,
      active: false,
      time: '18',
      price: eightTeenToZero,
    },
    {
      id: '15',
      disabled: false,
      active: false,
      time: '19',
      price: eightTeenToZero,
    },
    {
      id: '16',
      disabled: false,
      active: false,
      time: '20',
      price: eightTeenToZero,
    },
    {
      id: '17',
      disabled: false,
      active: false,
      time: '21',
      price: eightTeenToZero,
    },
    {
      id: '18',
      disabled: false,
      active: false,
      time: '22',
      price: eightTeenToZero,
    },
    {
      id: '19',
      disabled: false,
      active: false,
      time: '23',
      price: eightTeenToZero,
    },
  ]);

  const isDisabled = () => {
    let disabledArray: any = [];
    if (dmkHallData.responseData.length === 0) {
      disabledArray = buttonState.map((item) => {
        for (let i = 0; i < dmkHallData.responseData.length; i += 1) {
          return {
            ...item,
            disabled: false,
            active: false,
          };
        }
      });
    } else {
      disabledArray = buttonState.map((item) => {
        for (let i = 0; i < dmkHallData.responseData.length; i += 1) {
          if (item.time === dmkHallData.responseData[i]) {
            return {
              ...item,
              disabled: true,
              active: false,
            };
          }
          if (i === dmkHallData.responseData.length - 1) {
            return {
              ...item,
              disabled: false,
              active: false,
            };
          }
        }
      });
    }
    setButtonState(disabledArray);
  }; // 날짜에 따른 예약 상태를 가져오고 상태에 따라 disabled를 true로 바꿔주는 함수

  // const isActive = () => {
  //   let activedArray = {};
  //   activedArray = buttonState.map((item, index) => {
  //     return {
  //       ...item,
  //       active: false,
  //     };
  //   });
  //   setButtonState(activedArray);
  // }; // 날짜가 바뀌면 모든 버튼의 active를 false로

  useEffect(() => {
    isDisabled();
    // isActive();
  }, [dmkHallData.day]);

  const clickPriceButton = (e: any) => {
    let totalPriceProps: any = 0;
    let totalTimeProps: any = 0;
    let totalDateProps: string[] = [];

    let activeButtonCount: number = 0;

    buttonState.forEach((item) => {
      if (item.active) {
        activeButtonCount += 1;
      }
    }); // 액티브 상태 버튼 갯수를 카운트

    let changeStateArray: ButtonStateItemTypes[] = []; // map 메소드를 이용하여 객체를 담을 변수

    if (activeButtonCount >= 2) {
      // 만약 버튼이 두개 이상이 액티브 라면 클릭 이벤트가 일어난 객체 외에 모두 active = false
      changeStateArray = buttonState.map((item) => {
        return item.id === e.target.id
          ? {
              ...item,
              active: true,
            }
          : {
              ...item,
              active: false,
            };
      });
    } else if (activeButtonCount === 0) {
      // 만약 액티브 된 버튼의 갯수가 0개라면 클릭 이벤트가 일어난 객체 외에 모두 false
      changeStateArray = buttonState.map((item) => {
        return item.id === e.target.id
          ? {
              ...item,
              active: !item.active,
            }
          : item;
      });
    } else if (activeButtonCount < 2) {
      // 그리고 두개보다 작다면? 즉 버튼이 1개가 액티브 상태일때 다른 한개가 클릭 되었을때 중간에 모든 버튼을 active 시켜줄 부분
      let trueButtonIndex = 0;
      buttonState.forEach((item) => {
        // 클릭 이벤트가 실행되고 난 후 버튼이 1개가 액티브라면 이쪽 if문으로 넘어 온 상태일 것이니 액티브 되어 있는 버튼의 index 번호 즉 id를 변수에 저장.
        if (item.active) {
          trueButtonIndex = Number(item.id);
        }
      });
      changeStateArray = buttonState.map((item, index) => {
        // 리턴 결과가 담길 변수에 buttonState에서 map 메소드를 이용하여 순회 한다.
        if (e.target.id > trueButtonIndex) {
          // 만약 클릭된 버튼의 인덱스가 이전에 클릭된 버튼의 인덱스 보다 크다면
          if (trueButtonIndex <= index && e.target.id >= index) {
            // 인덱스가 인덱스가 이전 클릭된 인덱스보다 크고 지금 클릭된 버튼의 인덱스보다 작을동안은
            return {
              ...item,
              active: true, // active를 true로 리턴 시키고
            };
          }
          return {
            ...item,
            active: false, // 그 외에는 false를 리턴 시킨다
          };
        }
        if (Number(e.target.id) === trueButtonIndex) {
          // 만약 active버튼이 한개인 상태에서 이쪽으로 넘어 왔을때 같은 버튼을 클릭 했다면 토글
          totalDateProps = ['0', '0'];
          return item.id === e.target.id
            ? {
                ...item,
                active: !item.active,
              }
            : item;
        }
        // 위의 반대
        if (trueButtonIndex >= index && e.target.id <= index) {
          return {
            ...item,
            active: true,
          };
        }
        return {
          ...item,
          active: false,
        };
      });
    }
    console.log(changeStateArray);
    let isMiddleCheck: boolean = false;
    changeStateArray.forEach((item: ButtonStateItemTypes) => {
      if (item.active) {
        if (item.disabled) {
          isMiddleCheck = true;
        }
        totalPriceProps += Number((item.price as string).replace(',', ''));
        totalTimeProps += 1;
        totalDateProps.push(item.time as string);
      }
    });
    if (isMiddleCheck) {
      return alert('중간에 예약이 되어 있는 시간대가 있습니다.');
    }
    // setReservationValues({
    //   ...reservationValues,
    //   totalPrice: totalPriceProps,
    //   reserveTime: totalTimeProps,
    //   reserveHour: totalDateProps,
    // });

    dispatch(setDmkHallReserveValue('totalPrice', totalPriceProps));
    dispatch(setDmkHallReserveValue('reserveTime', totalTimeProps));
    dispatch(setDmkHallReserveValue('reserveHour', totalDateProps));
    if (totalTimeProps >= 10) {
      dispatch(setDmkHallReserveValue('discountPrice', totalPriceProps * 0.1));
      dispatch(setDmkHallReserveValue('lastPrice', totalPriceProps * 0.9));
    } else {
      dispatch(setDmkHallReserveValue('lastPrice', totalPriceProps));
    }
    setButtonState(changeStateArray);
  };

  return (
    <>
      <ScrollContainer className='scroll-container'>
        <ButtonGroup style={{ pointerEvents: 'none' }}>
          <Button
            color='secondary'
            outline
            size='sm'
            style={{
              outlineColor: 'white',
              borderColor: 'white',
            }}
          >
            04:00
          </Button>
          <Button
            color='secondary'
            outline
            size='sm'
            style={{
              outlineColor: 'white',
              borderColor: 'white',

              marginLeft: '22.5px',
            }}
          >
            05:00
          </Button>
          <Button
            color='secondary'
            outline
            size='sm'
            style={{
              outlineColor: 'white',
              borderColor: 'white',

              marginLeft: '22px',
            }}
          >
            06:00
          </Button>
          <Button
            color='secondary'
            outline
            size='sm'
            style={{
              outlineColor: 'white',
              borderColor: 'white',

              marginLeft: '22px',
            }}
          >
            07:00
          </Button>
          <Button
            color='secondary'
            outline
            size='sm'
            style={{
              outlineColor: 'white',
              borderColor: 'white',

              marginLeft: '23px',
            }}
          >
            08:00
          </Button>
          <Button
            color='secondary'
            outline
            size='sm'
            style={{
              outlineColor: 'white',
              borderColor: 'white',

              marginLeft: '23px',
            }}
          >
            09:00
          </Button>
          <Button
            color='secondary'
            outline
            size='sm'
            style={{
              outlineColor: 'white',
              borderColor: 'white',

              marginLeft: '22.5px',
            }}
          >
            10:00
          </Button>
          <Button
            color='secondary'
            outline
            size='sm'
            style={{
              outlineColor: 'white',
              borderColor: 'white',

              marginLeft: '22px',
            }}
          >
            11:00
          </Button>
          <Button
            color='secondary'
            outline
            size='sm'
            style={{
              outlineColor: 'white',
              borderColor: 'white',

              marginLeft: '22px',
            }}
          >
            12:00
          </Button>
          <Button
            color='secondary'
            outline
            size='sm'
            style={{
              outlineColor: 'white',
              borderColor: 'white',

              marginLeft: '22px',
            }}
          >
            13:00
          </Button>
          <Button
            color='secondary'
            outline
            size='sm'
            style={{
              outlineColor: 'white',
              borderColor: 'white',

              marginLeft: '23px',
            }}
          >
            14:00
          </Button>
          <Button
            color='secondary'
            outline
            size='sm'
            style={{
              outlineColor: 'white',
              borderColor: 'white',

              marginLeft: '23px',
            }}
          >
            15:00
          </Button>
          <Button
            color='secondary'
            outline
            size='sm'
            style={{
              outlineColor: 'white',
              borderColor: 'white',

              marginLeft: '23px',
            }}
          >
            16:00
          </Button>
          <Button
            color='secondary'
            outline
            size='sm'
            style={{
              outlineColor: 'white',
              borderColor: 'white',

              marginLeft: '23px',
            }}
          >
            17:00
          </Button>
          <Button
            color='secondary'
            outline
            size='sm'
            style={{
              outlineColor: 'white',
              borderColor: 'white',

              marginLeft: '22px',
            }}
          >
            18:00
          </Button>
          <Button
            color='secondary'
            outline
            size='sm'
            style={{
              outlineColor: 'white',
              borderColor: 'white',

              marginLeft: '22px',
            }}
          >
            19:00
          </Button>
          <Button
            color='secondary'
            outline
            size='sm'
            style={{
              outlineColor: 'white',
              borderColor: 'white',

              marginLeft: '22px',
            }}
          >
            20:00
          </Button>
          <Button
            color='secondary'
            outline
            size='sm'
            style={{
              outlineColor: 'white',
              borderColor: 'white',

              marginLeft: '23px',
            }}
          >
            21:00
          </Button>
          <Button
            color='secondary'
            outline
            size='sm'
            style={{
              outlineColor: 'white',
              borderColor: 'white',

              marginLeft: '23px',
            }}
          >
            22:00
          </Button>
          <Button
            color='secondary'
            outline
            size='sm'
            style={{
              outlineColor: 'white',
              borderColor: 'white',

              marginLeft: '22px',
            }}
          >
            23:00
          </Button>
          <Button
            color='secondary'
            outline
            size='sm'
            style={{
              outlineColor: 'white',
              borderColor: 'white',

              marginLeft: '18px',
            }}
          >
            24:00
          </Button>
        </ButtonGroup>
        <ButtonGroup style={{ marginLeft: '24px' }}>
          {/* {buttonState.map((item, index) => {
						return (
							<Button
								id={index}
								key={index}
								name="active"
								className="m-1"
								outline
								color="primary"
								size="sm"
								disabled={item.disabled}
								active={item.active}
								value={item.price}
								style={{ borderRadius: "4px" }}
								//   aria-pressed="true"
								onClick={clickPriceButton}>
								{item.price}
							</Button>
						)
					})} */}
          <Button
            id={buttonState[0].id}
            className='m-1'
            outline
            color='primary'
            size='sm'
            // disabled={true}
            active={buttonState[0].active}
            disabled={buttonState[0].disabled}
            value={fourToNine}
            style={{ borderRadius: '4px' }}
            //   aria-pressed="true"
            onClick={clickPriceButton}
          >
            {fourToNine}
          </Button>
          <Button
            id={buttonState[1].id}
            className='m-1'
            outline
            color='primary'
            size='sm'
            active={buttonState[1].active}
            disabled={buttonState[1].disabled}
            value={fourToNine}
            style={{ borderRadius: '4px' }}
            //   aria-pressed="true"
            onClick={clickPriceButton}
          >
            {fourToNine}
          </Button>
          <Button
            id={buttonState[2].id}
            className='m-1'
            outline
            color='primary'
            size='sm'
            active={buttonState[2].active}
            disabled={buttonState[2].disabled}
            value={fourToNine}
            style={{ borderRadius: '4px' }}
            //   aria-pressed="true"
            onClick={clickPriceButton}
          >
            {fourToNine}
          </Button>
          <Button
            id={buttonState[3].id}
            className='m-1'
            outline
            color='primary'
            size='sm'
            active={buttonState[3].active}
            disabled={buttonState[3].disabled}
            value={fourToNine}
            style={{ borderRadius: '4px' }}
            //   aria-pressed="true"
            onClick={clickPriceButton}
          >
            {fourToNine}
          </Button>
          <Button
            id={buttonState[4].id}
            className='m-1'
            outline
            color='primary'
            size='sm'
            active={buttonState[4].active}
            disabled={buttonState[4].disabled}
            value={fourToNine}
            style={{ borderRadius: '4px' }}
            //   aria-pressed="true"
            onClick={clickPriceButton}
          >
            {fourToNine}
          </Button>
          <Button
            id={buttonState[5].id}
            className='m-1'
            outline
            color='primary'
            style={{ borderRadius: '4px' }}
            disabled={buttonState[5].disabled}
            size='sm'
            active={buttonState[5].active}
            value={nineToEightTeen}
            //   aria-pressed="true"
            onClick={clickPriceButton}
          >
            {nineToEightTeen}
          </Button>
          <Button
            id={buttonState[6].id}
            className='m-1'
            outline
            // disabled
            color='primary'
            size='sm'
            active={buttonState[6].active}
            disabled={buttonState[6].disabled}
            value={nineToEightTeen}
            style={{ borderRadius: '4px' }}
            //   aria-pressed="true"
            onClick={clickPriceButton}
          >
            {nineToEightTeen}
          </Button>
          <Button
            id={buttonState[7].id}
            className='m-1'
            outline
            color='primary'
            size='sm'
            active={buttonState[7].active}
            disabled={buttonState[7].disabled}
            value={nineToEightTeen}
            style={{ borderRadius: '4px' }}
            //   aria-pressed="true"
            onClick={clickPriceButton}
          >
            {nineToEightTeen}
          </Button>
          <Button
            id={buttonState[8].id}
            className='m-1'
            outline
            color='primary'
            style={{ borderRadius: '4px' }}
            size='sm'
            active={buttonState[8].active}
            disabled={buttonState[8].disabled}
            value={nineToEightTeen}
            //   aria-pressed="true"
            onClick={clickPriceButton}
          >
            {nineToEightTeen}
          </Button>
          <Button
            id={buttonState[9].id}
            className='m-1'
            outline
            color='primary'
            size='sm'
            active={buttonState[9].active}
            disabled={buttonState[9].disabled}
            value={nineToEightTeen}
            style={{ borderRadius: '4px' }}
            //   aria-pressed="true"
            onClick={clickPriceButton}
          >
            {nineToEightTeen}
          </Button>
          <Button
            id={buttonState[10].id}
            className='m-1'
            outline
            color='primary'
            style={{ borderRadius: '4px' }}
            size='sm'
            active={buttonState[10].active}
            disabled={buttonState[10].disabled}
            value={nineToEightTeen}
            //   aria-pressed="true"
            onClick={clickPriceButton}
          >
            {nineToEightTeen}
          </Button>
          <Button
            id={buttonState[11].id}
            className='m-1'
            outline
            color='primary'
            size='sm'
            active={buttonState[11].active}
            disabled={buttonState[11].disabled}
            value={nineToEightTeen}
            style={{ borderRadius: '4px' }}
            //   aria-pressed="true"
            onClick={clickPriceButton}
          >
            {nineToEightTeen}
          </Button>
          <Button
            id={buttonState[12].id}
            className='m-1'
            outline
            color='primary'
            style={{ borderRadius: '4px' }}
            size='sm'
            active={buttonState[12].active}
            disabled={buttonState[12].disabled}
            value={nineToEightTeen}
            //   aria-pressed="true"
            onClick={clickPriceButton}
          >
            {nineToEightTeen}
          </Button>
          <Button
            id={buttonState[13].id}
            className='m-1'
            outline
            color='primary'
            size='sm'
            active={buttonState[13].active}
            disabled={buttonState[13].disabled}
            value={nineToEightTeen}
            style={{ borderRadius: '4px' }}
            //   aria-pressed="true"
            onClick={clickPriceButton}
          >
            {nineToEightTeen}
          </Button>
          <Button
            id={buttonState[14].id}
            className='m-1'
            outline
            color='primary'
            style={{ borderRadius: '4px' }}
            size='sm'
            active={buttonState[14].active}
            disabled={buttonState[14].disabled}
            value={eightTeenToZero}
            //   aria-pressed="true"
            onClick={clickPriceButton}
          >
            {eightTeenToZero}
          </Button>
          <Button
            id={buttonState[15].id}
            className='m-1'
            outline
            color='primary'
            size='sm'
            active={buttonState[15].active}
            disabled={buttonState[15].disabled}
            value={eightTeenToZero}
            style={{ borderRadius: '4px' }}
            //   aria-pressed="true"
            onClick={clickPriceButton}
          >
            {eightTeenToZero}
          </Button>
          <Button
            id={buttonState[16].id}
            className='m-1'
            outline
            color='primary'
            style={{ borderRadius: '4px' }}
            size='sm'
            active={buttonState[16].active}
            disabled={buttonState[16].disabled}
            value={eightTeenToZero}
            //   aria-pressed="true"
            onClick={clickPriceButton}
          >
            {eightTeenToZero}
          </Button>
          <Button
            id={buttonState[17].id}
            className='m-1'
            outline
            color='primary'
            size='sm'
            active={buttonState[17].active}
            disabled={buttonState[17].disabled}
            value={eightTeenToZero}
            style={{ borderRadius: '4px' }}
            //   aria-pressed="true"
            onClick={clickPriceButton}
          >
            {eightTeenToZero}
          </Button>
          <Button
            id={buttonState[18].id}
            className='m-1'
            outline
            color='primary'
            style={{ borderRadius: '4px' }}
            size='sm'
            active={buttonState[18].active}
            disabled={buttonState[18].disabled}
            value={eightTeenToZero}
            //   aria-pressed="true"
            onClick={clickPriceButton}
          >
            {eightTeenToZero}
          </Button>
          <Button
            id={buttonState[19].id}
            className='m-1'
            outline
            color='primary'
            size='sm'
            active={buttonState[19].active}
            disabled={buttonState[19].disabled}
            value={eightTeenToZero}
            style={{ borderRadius: '4px' }}
            //   aria-pressed="true"
            onClick={clickPriceButton}
          >
            {eightTeenToZero}
          </Button>
        </ButtonGroup>
      </ScrollContainer>
    </>
  );
}

// const TimeLine = {
//   top: '20px',
//   paddingTop: '3rem',
// };
