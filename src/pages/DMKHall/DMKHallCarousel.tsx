import React from 'react';
import { UncontrolledCarousel, Row, Col, Container } from 'reactstrap';

import imgA from 'assets/img/dmkhall/main_1.png';
import imgB from 'assets/img/dmkhall/main_2.png';
import imgC from 'assets/img/dmkhall/main_3.png';
import imgD from 'assets/img/dmkhall/main_4.png';

const items = [
  {
    src: imgA,
    altText: 'Slide 1',
    caption: '',
    header: '',
    key: '1',
  },
  {
    src: imgB,
    altText: 'Slide 2',
    caption: '',
    header: '',
    key: '2',
  },
  {
    src: imgC,
    altText: 'Slide 3',
    caption: '',
    header: '',
    key: '3',
  },
  {
    src: imgD,
    altText: 'Slide 4',
    caption: '',
    header: '',
    key: '4',
  },
];

export default function DMKHallMainCarousel() {
  return (
    <>
      <Container>
        <Row className='pt-7'>
          <Col lg='10' md='6' className='mx-auto'>
            <UncontrolledCarousel items={items} />
          </Col>
        </Row>
      </Container>
    </>
  );
}
