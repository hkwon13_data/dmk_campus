/* eslint-disable react/no-unescaped-entities */
import React from 'react';

// reactstrap components
import { Container, Row, Col } from 'reactstrap';

export default function DMKHallDesc() {
  return (
    <>
      <section className='blogs-1 '>
        <Container>
          <Row className='mt-0 pt-8'>
            <Col className='ml-auto mr-auto text-center' md='8'>
              <h3 className='display-5'>
                연평균 3천 여명, 45개 기업이 선택한 <br />
                국내 최고의 프리미엄 다목적 공간 '데마코홀'
              </h3>
              <p className='h7'>
                선릉역 도보3분 거리에 위치한 데마코 홀은 감각적 인테리어와
                230인치의 대형 LED 스크린,
                <br />
                최고급 음향 설비를 겸비한 강남 유일의 프리미엄 다목적
                공간입니다.
              </p>
            </Col>
          </Row>
        </Container>
      </section>
    </>
  );
}
