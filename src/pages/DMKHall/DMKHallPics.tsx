import React from 'react';

// reactstrap components
import { Card, CardBody, CardTitle, Container, Row, Col } from 'reactstrap';

import imgA from 'assets/img/theme/josh-appel.jpg';
import imgB from 'assets/img/theme/john-hoang.jpg';
import imgC from 'assets/img/theme/kit-suman.jpg';
import imgD from 'assets/img/sections/damian.jpg';
import imgE from 'assets/img/sections/ashim.jpg';
import imgF from 'assets/img/sections/odin.jpg';

export default function MainPictures() {
  return (
    <>
      <section className='blogs-1'>
        <Container>
          <Row className='align-items-center'>
            <Col lg='3'>
              <Card
                className='card-blog card-background'
                data-animation='zooming'
              >
                <div
                  className='full-background'
                  style={{
                    backgroundImage: `url(${imgA})`,
                  }}
                />
                <a href='#pablo' onClick={(e) => e.preventDefault()}>
                  <CardBody>
                    <div className='content-bottom'>
                      <h6 className='card-category text-white opacity-8'>
                        New Challenges
                      </h6>
                      <CardTitle tag='h5'>Touch on a trend</CardTitle>
                    </div>
                  </CardBody>
                </a>
              </Card>
            </Col>
            <Col lg='3'>
              <Card
                className='card-blog card-background'
                data-animation='zooming'
              >
                <div
                  className='full-background'
                  style={{
                    backgroundImage: `url(${imgB})`,
                  }}
                />
                <a href='#pablo' onClick={(e) => e.preventDefault()}>
                  <CardBody>
                    <div className='content-bottom'>
                      <h6 className='card-category text-white opacity-8'>
                        New Opportunities
                      </h6>
                      <CardTitle tag='h5'>Constantly growing</CardTitle>
                    </div>
                  </CardBody>
                </a>
              </Card>
            </Col>
            <Col lg='6'>
              <Card
                className='card-blog card-background'
                data-animation='zooming'
              >
                <div
                  className='full-background'
                  style={{
                    backgroundImage: `url(${imgC})`,
                  }}
                />
                <a href='#pablo' onClick={(e) => e.preventDefault()}>
                  <CardBody>
                    <div className='content-bottom'>
                      <h6 className='card-category text-white opacity-8'>
                        Sales Leads
                      </h6>
                      <CardTitle tag='h5'>
                        Configure Blockchain Technology
                      </CardTitle>
                    </div>
                  </CardBody>
                </a>
              </Card>
            </Col>
          </Row>
          <Row className='align-items-center'>
            <Col lg='6'>
              <Card
                className='card-blog card-background'
                data-animation='zooming'
              >
                <div
                  className='full-background'
                  style={{
                    backgroundImage: `url(${imgD})`,
                  }}
                />
                <a href='#pablo' onClick={(e) => e.preventDefault()}>
                  <CardBody>
                    <div className='content-bottom'>
                      <h6 className='card-category text-white opacity-8'>
                        AI at the Edge
                      </h6>
                      <CardTitle tag='h5'>Research Byte</CardTitle>
                    </div>
                  </CardBody>
                </a>
              </Card>
            </Col>
            <Col lg='3'>
              <Card
                className='card-blog card-background'
                data-animation='zooming'
              >
                <div
                  className='full-background'
                  style={{
                    backgroundImage: `url(${imgE})`,
                  }}
                />
                <a href='#pablo' onClick={(e) => e.preventDefault()}>
                  <CardBody>
                    <div className='content-bottom'>
                      <h6 className='card-category text-white opacity-8'>
                        Spectrum
                      </h6>
                      <CardTitle tag='h5'>Data Virtualization</CardTitle>
                    </div>
                  </CardBody>
                </a>
              </Card>
            </Col>
            <Col lg='3'>
              <Card
                className='card-blog card-background'
                data-animation='zooming'
              >
                <div
                  className='full-background'
                  style={{
                    backgroundImage: `url(${imgF})`,
                  }}
                />
                <a href='#pablo' onClick={(e) => e.preventDefault()}>
                  <CardBody>
                    <div className='content-bottom'>
                      <h6 className='card-category text-white opacity-8'>
                        Touch on a trend
                      </h6>
                      <CardTitle tag='h5'>New Challenges</CardTitle>
                    </div>
                  </CardBody>
                </a>
              </Card>
            </Col>
          </Row>
        </Container>
      </section>
    </>
  );
}
