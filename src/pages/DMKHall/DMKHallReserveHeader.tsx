import React from 'react';

// reactstrap components
import {} from 'reactstrap';

import imgA from 'assets/img/dmkhall/dmk_guide_main.jpg';

export default function DMKHallReserveHeader() {
  return (
    <>
      <div className='page-header page-header-small header-filter'>
        <div
          className='page-header-image'
          style={{
            backgroundImage: `url(${imgA})`,
          }}
        />
      </div>
    </>
  );
}
