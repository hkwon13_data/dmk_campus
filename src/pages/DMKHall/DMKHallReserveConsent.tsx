import React from 'react';

// reactstrap components
import { Button, Card, Container, Row, Col, CardTitle } from 'reactstrap';

// Core Components
// import imgA from "assets/img/ill/p51.svg";

import styled, { DefaultTheme } from 'styled-components';

import { useHistory } from 'react-router-dom';

interface HiddenPropsTypes {
  hidden: boolean;
}
const HiddenDiv = styled.div<DefaultTheme>`
  display: ${(props) => (props.hidden ? 'none' : 'block')};
`;

export default function DMKHallReserveConsent({ hidden }: HiddenPropsTypes) {
  // const [openedCollapse, setOpenedCollapse] = React.useState('collapse-1');

  const history = useHistory();

  return (
    <HiddenDiv hidden={hidden}>
      <Container>
        <div className='accordion-1'>
          <Row>
            <Col lg='12' md='8'>
              <Card className='bg-white p-5'>
                <div className='content'>
                  <CardTitle className='h6 my-2 text-primary font-weight-700'>
                    예약 전 꼭 읽어주시기 바랍니다
                  </CardTitle>
                  <hr className='w-100 px--3' />
                  <div
                    className='font-weight-900'
                    style={{ fontSize: '0.875rem' }}
                  >
                    입퇴실
                  </div>
                  <p>
                    <small>
                      · 입실, 퇴실 시간을 준수해 주십시오. 행사 당일 30분 전에
                      미리 오셔서 대관 담당자의 안내를 받으십시오.
                    </small>
                    <br />
                    <small>
                      · 주차 공간은 제공하지 않습니다. 대중교통을 이용해 주시길
                      바랍니다.
                    </small>
                    <br />
                    <small>
                      · 책상 의자 등 사전, 사후 세팅을 직접 해 주셔야 하며, 대관
                      종료 후 사전 세팅 전 원상태로 정리해야 합니다.
                    </small>
                    <br />
                    <small>
                      · 현수막 등 안내 게시물은 사전 신청 시, 승인 후 허락된
                      곳에만 부착할 수 있습니다.
                    </small>
                    <br />
                    <small>
                      · 다과와 음료 외 음식물 반입은 불가합니다. 필요 시 사전
                      문의 및 승인을 받으셔야 합니다.
                    </small>
                    <br />
                    <small>
                      · 진행 중 발생한 쓰레기 (일반/ 재활용/ 음식물 등)는 반드시
                      수거 및 분리 배출해야 하며 (쓰레기봉투 지원), 퇴실 시 모든
                      쓰레기는 수거해야 합니다.
                    </small>
                    <br />
                  </p>
                  <br />
                  <div
                    className='font-weight-900'
                    style={{ fontSize: '0.875rem' }}
                  >
                    예약
                  </div>
                  <p>
                    <small>
                      · 취소 수수료는 행사 7일 전까지 대관료의 50%, 6일 전부터
                      행사 당일까지는 전액 100% 입니다.
                    </small>
                    <br />
                    <small>
                      · 업무 시간 외 새벽, 저녁 시간 대관 등을 원할 경우 원활한
                      출입을 위해 담당자에게 연락하시길 바랍니다.
                    </small>
                    <br />
                    <small>
                      · 대관 예약 전 사전 답사를 원할 경우 담당자에게 연락해
                      시간을 조율하시길 바랍니다.
                    </small>
                    <br />
                    <small>
                      · 정치, 종교, 판촉 목적의 행사를 위한 대관을 금지합니다.
                    </small>
                    <br />
                  </p>
                  <br />
                  <div
                    className='font-weight-900'
                    style={{ fontSize: '0.875rem' }}
                  >
                    주차
                  </div>
                  <p>
                    <small>
                      · 주차 공간은 제공하지 않습니다. 대중교통을 이용해 주시길
                      바랍니다.
                    </small>
                  </p>
                  <br />
                  <div
                    className='font-weight-900'
                    style={{ fontSize: '0.875rem' }}
                  >
                    내부시설
                  </div>
                  <p>
                    <small>
                      · 책상 의자 등 사전, 사후 세팅을 직접 해 주셔야 하며, 대관
                      종료 후 사전 세팅 전 원상태로 정리해야 합니다.
                    </small>
                    <br />
                    <small>
                      · 현수막 등 안내 게시물은 사전 신청 시, 승인 후 허락된
                      곳에만 부착할 수 있습니다.
                    </small>
                    <br />
                    <small>
                      · 다과와 음료 외 음식물 반입은 불가합니다. 필요 시 사전
                      문의 및 승인을 받으셔야 합니다.
                    </small>
                    <br />
                    <small>
                      · 강의장 기자재와 시설 이상, 분실, 파손 시 공급가액
                      기준으로 지불해야 하며, 이후 대관 이용이 불가할 수
                      있습니다.
                    </small>
                  </p>
                  <br />
                  <div
                    className='font-weight-900'
                    style={{ fontSize: '0.875rem' }}
                  >
                    기타
                  </div>
                  <p>
                    <small>
                      · 사무용품 대여 및 인쇄는 별도로 지원하지 않습니다. (사전
                      개별 준비)
                    </small>
                    <br />
                    <small>
                      · LED 화면 사용 시 노트북 지참 바랍니다. (별도 대여 불가)
                    </small>
                    <br />
                  </p>
                </div>
              </Card>
            </Col>
          </Row>
        </div>
        <div className='text-center'>
          <Button
            className='btn px-4'
            color='white'
            onClick={() => history.push('/community/inquiry/')}
          >
            <span className='text-dark'>대관 문의하기</span>
          </Button>
        </div>
      </Container>
    </HiddenDiv>
  );
}
