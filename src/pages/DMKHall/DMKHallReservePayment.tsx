// import Item from 'antd/lib/list/Item';
import React, { useState } from 'react';
import { RootState } from 'store/modules/index';
import { Row, Container, Col, Toast, Button } from 'reactstrap';

import { IAMPORT_IMP } from 'utils/Constant';
import { useSelector, useDispatch } from 'react-redux';
import * as actions from 'store/modules/modalManages';
import SendInquiryErrorModal from '../Community/SendInquiryErrorModal';

interface BasketPropsTypes {
  emailState: string | null;
  phoneState: string | null;
}

export default function BasketButton(props: BasketPropsTypes) {
  const { emailState, phoneState } = props;

  // const [nine, setNine] = useState(false);
  const [errorMessage, setErrorMessage] = useState<string>('');
  const dispatch = useDispatch();

  const { isSendInquiryErrorPopup } = useSelector(
    (state: RootState) => state.modalManages.community
  );
  const dmkHallData = useSelector(
    (state: RootState) => state.dmkReserveManage.dmkHallReservation
  );

  const handleInquiryErrorPopup = () => {
    dispatch(actions.setSendInquiryErrorModal(!isSendInquiryErrorPopup));
  };

  const requestPay = (price: number, text: string) => {
    if (
      dmkHallData.username === '' ||
      dmkHallData.phone === '' ||
      dmkHallData.email === '' ||
      dmkHallData.eventname === ''
    ) {
      setErrorMessage('모든 입력사항은 필수 입력사항 입니다');
      handleInquiryErrorPopup();
    } else if (phoneState === 'invalid' || emailState === 'invalid') {
      setErrorMessage(
        '잘못된 입력 항목이 있는지 \n 다시한번 확인해 주시기 바랍니다'
      );
      handleInquiryErrorPopup();
    } else if (
      dmkHallData.agreementTotal ||
      dmkHallData.agreementCollectUserInfo ||
      dmkHallData.agreementHallRules
    ) {
      setErrorMessage('주의사항 및 약관을 확인해주세요');
      handleInquiryErrorPopup();
    } else {
      const { IMP } = window;
      IMP.init(IAMPORT_IMP);
      IMP.request_pay(
        {
          // param
          pg: 'html5_inicis',
          pay_method: text,
          // merchant_uid: "ORD20180131-0000011",
          name: '데마코홀 대관',
          // amount: `${price}`,
          amount: price,
          buyer_email: dmkHallData.email,
          buyer_name: dmkHallData.username,
          buyer_tel: dmkHallData.phone,
          buyer_addr: '서울특별시 강남구 신사동',
          buyer_postcode: '01181',
        },
        (rsp: any) => {
          // callback
          if (rsp.success) {
            const formData = new FormData();
            formData.append('payMethod', text);
            formData.append('reserveTime', dmkHallData.reserveTime.toString());
            formData.append('people', dmkHallData.people.toString());
            formData.append('totalPrice', dmkHallData.totalPrice.toString());
            formData.append(
              'discountPrice',
              dmkHallData.discountPrice.toString()
            );
            formData.append('lastPrice', dmkHallData.lastPrice.toString());
            formData.append('year', dmkHallData.year);
            formData.append('month', dmkHallData.month);
            formData.append('day', dmkHallData.day);
            formData.append('week', dmkHallData.week as string);
            for (let i = 0; i < dmkHallData.reserveHour.length; i += 1) {
              formData.append('reserveHour', dmkHallData.reserveHour[i]);
            }
            // formData.append('reserveHour', dmkHallData.reserveHour);
            formData.append('username', dmkHallData.username);
            formData.append('groupname', dmkHallData.groupname);
            formData.append('phone', dmkHallData.phone);
            formData.append('email', dmkHallData.email);
            formData.append('eventname', dmkHallData.eventname);
            formData.append(
              'agreementKeep',
              dmkHallData.agreementKeep.toString()
            );
            formData.append('keepTime', dmkHallData.keepTime);
            formData.append('keepList', dmkHallData.keepList);
            formData.append(
              'agreementPhoto',
              dmkHallData.agreementPhoto.toString()
            );
            formData.append('photoTime', dmkHallData.photoTime);
            formData.append(
              'agreementTotal',
              dmkHallData.agreementTotal.toString()
            );
            formData.append(
              'agreementCollectUserInfo',
              dmkHallData.agreementCollectUserInfo.toString()
            );
            formData.append(
              'agreementHallRules',
              dmkHallData.agreementHallRules.toString()
            );

            // 결제 성공 시 로직,
          } else {
            console.log('Failed', rsp);
            // 결제 실패 시 로직,
          }
        }
      );
    }
  };

  const renderButton = () => {
    if (dmkHallData.totalPrice <= 2000000) {
      return (
        <>
          <Col>
            <Button
              type='button'
              color='primary'
              style={{ minWidth: '-webkit-fill-available' }}
              onClick={() => requestPay(dmkHallData.lastPrice, 'card')}
            >
              카드결제
            </Button>
          </Col>
          <Col>
            <Button
              type='button'
              color='primary'
              style={{ minWidth: '-webkit-fill-available' }}
              onClick={() => requestPay(dmkHallData.lastPrice, 'trans')}
            >
              계좌이체
            </Button>
          </Col>
        </>
      );
    }
    return (
      <Col>
        <Button
          type='button'
          color='primary'
          style={{ minWidth: '-webkit-fill-available' }}
          onClick={() => requestPay(dmkHallData.lastPrice, 'card')}
        >
          카드결제
        </Button>
      </Col>
    );

    // }
  };

  return (
    <>
      <div
        className='text-sm font-weight-800'
        style={{
          position: 'sticky',
          top: 80,
          right: 0,
          left: 0,
          zIndex: 1,
          border: '1px',
        }}
      >
        <div className='h3 font-weight-500 mb-2'>결제 예정 금액</div>
        <Toast className='bg-white p-3'>
          <Container>
            <Row>
              <Toast className='bg-white pb-3' style={{ width: '320px' }}>
                <Row className='mt-3'>
                  <Col lg='3'>
                    <small className='text-left pl-3 font-weight-900'>
                      일시
                    </small>
                  </Col>
                  <Col lg='9'>
                    <small className='text-left font-weight-900'>
                      {`${dmkHallData.year}년 ${dmkHallData.month}월 ${dmkHallData.day}일 ${dmkHallData.week}요일 `}{' '}
                      {`${dmkHallData.reserveHour[0]}-${
                        dmkHallData.reserveHour[
                          dmkHallData.reserveHour.length - 1
                        ]
                      }`}
                      시({dmkHallData.reserveTime}시간)
                    </small>
                  </Col>
                </Row>
                <Row>
                  <Col lg='3'>
                    <small className='text-left pl-3 font-weight-900'>
                      인원
                    </small>
                  </Col>
                  <Col lg='9'>
                    <small className='text-left font-weight-900'>
                      {dmkHallData.people}명
                    </small>
                  </Col>
                </Row>
              </Toast>
            </Row>
            <Row className='justify-content-center mt-2'>
              {/* <Toast className="bg-white" style={{ width: "320px" }}> */}
              <Toast className='bg-white' style={{ width: '320px' }}>
                <Row className='mt-3'>
                  <Col lg='6'>
                    <p className='text-left pl-3 font-weight-900'>
                      총 공간사용료
                    </p>
                  </Col>
                  <Col lg='6'>
                    <p className='text-right pr-3 font-weight-900'>
                      {dmkHallData.totalPrice
                        .toString()
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                      원
                    </p>
                  </Col>
                </Row>
                <Row>
                  <Col lg='6'>
                    <p className='text-left pl-3 font-weight-900'>
                      총 할인금액
                    </p>
                  </Col>
                  <Col lg='6'>
                    <p className='text-right pr-3 font-weight-900'>
                      {dmkHallData.reserveTime >= 10
                        ? dmkHallData.discountPrice
                            .toString()
                            .replace(/\B(?=(\d{3})+(?!\d))/g, ',')
                        : '0'}
                      원
                    </p>
                  </Col>
                </Row>
                <div
                  style={{
                    height: 0,
                    margin: '0 1rem 1rem',
                    overflow: 'hidden',
                    borderTop: '2px solid #e9ecef',
                  }}
                />
                <div className='text-primary text-right font-weight-800 pr-3'>
                  {dmkHallData.reserveTime >= 10
                    ? '할인 적용!'
                    : `${
                        10 - dmkHallData.reserveTime
                      }시간 더 추가예약시, 10% 할인!`}
                </div>
                <Row className='mt-1'>
                  <Col lg='6'>
                    <p className='text-left pl-3 font-weight-900'>
                      결제예정금액
                    </p>
                  </Col>
                  <Col lg='6'>
                    <p className='text-right pr-3 font-weight-900'>
                      {dmkHallData.reserveTime >= 10
                        ? dmkHallData.lastPrice
                            .toString()
                            .replace(/\B(?=(\d{3})+(?!\d))/g, ',')
                        : dmkHallData.totalPrice
                            .toString()
                            .replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                      원
                    </p>
                  </Col>
                </Row>
              </Toast>
            </Row>
            <Row className='p-2 my-2'>
              {dmkHallData ? renderButton() : ''}
              {/* <Button
								type="button"
								color="primary"
								style={{ minWidth: "-webkit-fill-available" }}
								onClick={requestPay}>
								결 제 하 기
							</Button> */}
            </Row>
          </Container>
        </Toast>
      </div>
      {isSendInquiryErrorPopup && (
        <SendInquiryErrorModal
          text={errorMessage}
          handleInquiryErrorPopup={() => handleInquiryErrorPopup()}
        />
      )}
    </>
  );
}

// const Divier = {
//   height: 0,
//   margin: '0 1rem 1rem',
//   overflow: 'hidden',
//   borderTop: '2px solid #e9ecef',
// };
// const payFixed = {
//   position: 'sticky',
//   top: 80,
//   right: 0,
//   left: 0,
//   zIndex: 1,
//   border: '1px',
// };
