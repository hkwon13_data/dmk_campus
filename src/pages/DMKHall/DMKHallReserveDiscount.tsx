import React from 'react';

// reactstrap components
import { Container, Card, CardBody } from 'reactstrap';

// Core Components
// import imgA from 'assets/img/ill/p51.svg';

export default function DMKHallReserveDiscount() {
  return (
    <>
      <Container>
        <div className='mb-3'>
          <span className='text-lg font-weight-800 mb-3'>할인안내</span>
        </div>

        <Card className='card-frame'>
          <CardBody className='text-center font-weight-500'>
            10시간이상 대관 일괄결제시, 대관금액의{' '}
            <span className='font-weight-900'>10%할인 </span>적용
          </CardBody>
        </Card>
      </Container>
    </>
  );
}
