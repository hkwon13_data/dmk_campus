import React from 'react';
import { RootState } from 'store/modules/index';
import { Card, CardBody } from 'reactstrap';
// import Choices from "choices.js";
// import ButtonGroup from "antd/lib/button/button-group";
import { useDispatch, useSelector } from 'react-redux';
import * as action from '../../store/modules/dmkReserveManage';

export default function PeopleCountButton() {
  // const [nine, setNine] = useState(false);
  // const [selected, setSelected] = useState(1);
  const dispatch = useDispatch();
  const data = useSelector(
    (state: RootState) => state.dmkReserveManage.dmkHallReservation
  );
  const handleIncrease = (): void => {
    if (data.people < 25) {
      dispatch(action.setDmkHallReserveValue('people', data.people + 1));
    }
  };
  const handleDecrease = (): void => {
    if (data.people > 1) {
      dispatch(action.setDmkHallReserveValue('people', data.people + -1));
    }
  };

  const handleDoubleIncrease = (): void => {
    if (data.people < 25) {
      if (data.people > 15) {
        dispatch(action.setDmkHallReserveValue('people', 25));
        return;
      }
      dispatch(action.setDmkHallReserveValue('people', data.people + 10));
    }
  };
  const handleDoubleDecrease = (): void => {
    if (data.people > 1) {
      if (data.people < 11) {
        dispatch(action.setDmkHallReserveValue('people', 1));
        return;
      }
      dispatch(action.setDmkHallReserveValue('people', data.people - 10));
    }
  };

  return (
    <>
      <div className='text-primary text-sm font-weight-800 mt-4 mb-2'>
        인원 선택
      </div>
      <Card className='card-frame '>
        <CardBody className='py-2'>
          <div className='row px-4'>
            <div
              className='float-left icon icon-shape icon-md'
              onClick={() => handleDoubleDecrease}
              style={{ cursor: 'pointer' }}
              onKeyDown={() => {}}
              role='button'
              tabIndex={0}
            >
              <i className='ni ni-fat-delete' style={{ color: '#55626f' }} />
              <i className='ni ni-fat-delete' style={{ color: '#55626f' }} />
            </div>
            <div
              className='float-left icon icon-shape icon-md'
              onClick={handleDecrease}
              style={{ cursor: 'pointer' }}
              onKeyDown={() => {}}
              role='button'
              tabIndex={0}
            >
              <i className='ni ni-fat-delete' style={{ color: '#55626f' }} />
            </div>
            <div className='align-self-center mx-auto font-weight-700'>
              {data.people}
            </div>
            <div
              className='float-right icon icon-shape icon-md'
              onClick={handleIncrease}
              style={{ cursor: 'pointer' }}
              onKeyDown={() => {}}
              role='button'
              tabIndex={0}
            >
              <i className='ni ni-fat-add' style={{ color: '#55626f' }} />
            </div>
            <div
              className='float-right icon icon-shape icon-md'
              onClick={handleDoubleIncrease}
              style={{ cursor: 'pointer' }}
              onKeyDown={() => {}}
              role='button'
              tabIndex={0}
            >
              <i className='ni ni-fat-add' style={{ color: '#55626f' }} />
              <i className='ni ni-fat-add' style={{ color: '#55626f' }} />
            </div>
          </div>
        </CardBody>
      </Card>
    </>
  );
}
