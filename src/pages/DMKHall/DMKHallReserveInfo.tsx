/* eslint-disable no-sequences */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React from 'react';
import { useHistory } from 'react-router-dom';
import { RootState } from 'store/modules/index';
import { useDispatch, useSelector } from 'react-redux';
// reactstrap components
import {
  Col,
  Row,
  Container,
  Button,
  Card,
  CardBody,
  Collapse,
  CardHeader,
  CardTitle,
  CardText,
  FormGroup,
  Form,
  Input,
  Label,
} from 'reactstrap';

import classnames from 'classnames';
import validator from 'validator';

// import DMKHallReserveBasket from './DMKHallReserveBasket';
import CommonNavbar from 'components/NavBar/CommonNavbar';
import CommonFooter from 'components/NavBar/CommonFooter';

import Img1 from 'assets/img/dmkhall/reservation_wifi.png';
import Img2 from 'assets/img/dmkhall/reservation_led.png';
import Img3 from 'assets/img/dmkhall/reservation_speaker.png';
import Img4 from 'assets/img/dmkhall/reservation_mic.png';
import Img5 from 'assets/img/dmkhall/reservation_coffee.png';

import { setDmkHallReserveValue } from 'store/modules/dmkReserveManage';
import DMKHallReservePayment from './DMKHallReservePayment';

export default function DMKHallReserveInfo() {
  const dispatch = useDispatch();
  const history = useHistory();
  const dmkHallData = useSelector(
    (state: RootState) => state.dmkReserveManage.dmkHallReservation
  );
  const [openedCollapse, setOpenedCollapse] =
    React.useState<string>('collapse-1');
  const [openedCollapse2, setOpenedCollapse2] =
    React.useState<string>('collapse-2');

  React.useEffect(() => {
    if (dmkHallData.reserveHour[0] === '0') {
      history.push('/dmkHall/reservation/');
    }
    // let classList = document.body.classList;
    // document.body.classList.remove(...classList);
    // document.body.classList.add('index-page');
    window.scrollTo(0, 0);
    // return function cleanup() {
    //   document.body.classList.remove('index-page');
    // };
  }, []);

  const {
    username,
    groupname,
    phone,
    email,
    eventname,
    agreementKeep,
    keepTime,
    keepList,
    agreementPhoto,
    photoTime,
    agreementTotal,
    agreementCollectUserInfo,
    agreementHallRules,
  } = dmkHallData;

  // 변수 값 변경
  // const handleChange = (e) => {
  // 	const { name, checked } = e.target;
  // 	setValues({ ...values, [name]: checked });
  // };

  const [usernameFocus, setUsernameFocus] = React.useState<string>('');
  const [phoneFocus, setPhoneFocus] = React.useState<string>('');
  const [emailFocus, setEmailFocus] = React.useState<string>('');
  const [eventNameFocus, setEventNameFocus] = React.useState<string>('');

  const [usernameValid, setUsernameValid] = React.useState<string>('');
  const [phoneValid, setPhoneValid] = React.useState<string>('');
  const [emailValid, setEmailValid] = React.useState<string>('');
  const [eventNameValid, setEventNameValid] = React.useState<string>('');

  const [usernameState, setUsernameState] = React.useState<string | null>(null);
  const [phoneState, setPhoneState] = React.useState<string | null>(null);
  const [emailState, setEmailState] = React.useState<string | null>(null);
  const [eventNameState, setEventNameState] =
    React.useState<string | null>(null);

  // const [submitButtonState, setSubmitButtonState] = React.useState(true);

  // const [passwordUpdate, setPasswordUpdate] = React.useState(false);

  const checkValidate = (e: React.ChangeEvent<HTMLInputElement>) => {
    const success = 'has-success';
    const danger = 'has-danger';
    switch (e.target.name) {
      case 'username':
        if (
          validator.matches(e.target.value, '[ !@#$%^&*(),.?":{}|<>]') ||
          e.target.value === ''
        ) {
          setUsernameValid(danger);
          setUsernameState('invalid');
        } else {
          setUsernameValid(success);
          setUsernameState('valid');
        }

        break;
      case 'phone':
        if (!validator.isMobilePhone(e.target.value, 'ko-KR')) {
          setPhoneValid(danger);
          setPhoneState('invalid');
        } else {
          setPhoneValid(success);
          setPhoneState('valid');
        }

        break;

      case 'email':
        if (!validator.isEmail(e.target.value)) {
          setEmailValid(danger);
          setEmailState('invalid');
        } else {
          setEmailValid(success);
          setEmailState('valid');
        }

        break;

      case 'eventame':
        if (e.target.value === '') {
          setEventNameValid(danger);
          setEventNameState('invalid');
        } else {
          setEventNameValid(success);
          setEventNameState('valid');
        }

        break;

      // eslint-disable-next-line no-fallthrough
      default:
        break;
    }
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    dispatch(setDmkHallReserveValue(name, value));
    checkValidate(e);
  }; // 제목, 본문 input value change method

  const imgArr = [
    {
      img: Img1,
      alt: '와이파이',
    },
    {
      img: Img2,
      alt: '대형 LED',
    },
    {
      img: Img3,
      alt: '음향시설',
    },
    {
      img: Img4,
      alt: '마이크시설',
    },
    {
      img: Img5,
      alt: '카페테리아',
    },
  ];

  return (
    <>
      {/* 예약자정보 */}
      {/* <CommonNavbar type="primary" /> */}
      <CommonNavbar type='primary' />
      <Container className='py-8 '>
        <Row>
          <Col lg='8'>
            <div className='h3 font-weight-500 mb-2'>예약공간</div>
            <div className='mb-2'>
              대관 시 강연장, 카페테리아, 미팅룸 1,2 모두 이용하실 수 있습니다.
              <br /> 부분대관은 불가능합니다.
            </div>
            <Card className='bg-secondary p-3'>
              <CardBody>
                <Row>
                  {imgArr.map((item, index) => {
                    return (
                      // eslint-disable-next-line react/no-array-index-key
                      <Col className='text-center' key={index}>
                        <div className='mb-3'>
                          <img
                            src={item.img}
                            alt={item.alt}
                            width='70px'
                            height='70px'
                          />
                        </div>
                        <div className='font-weight-700'>{item.alt}</div>
                      </Col>
                    );
                  })}
                </Row>
              </CardBody>
            </Card>
            <div className='mb-3'>
              <span className='text-lg font-weight-800 mb-3'>예약자 정보</span>
            </div>
            <Card className='bg-secondary p-4'>
              <CardBody>
                <CardTitle>
                  <div className='h5 font-weight-700'>
                    필수 정보
                    <small className='float-right right-0 text-red font-weight-bold'>
                      *필수입력
                    </small>
                  </div>
                </CardTitle>
                <hr className='w-100 my-3 ' />
                {/* <CardText> */}
                <Form>
                  <FormGroup className='row'>
                    <Label
                      className='form-control-label font-weight-bold'
                      htmlFor='username'
                      md='2'
                    >
                      예약자명<span className='text-red'>*</span>
                    </Label>
                    <Col md='10' className={classnames(usernameValid)}>
                      <Input
                        // defaultValue={values.username}
                        className='form-control-alternative'
                        minLength={2}
                        defaultValue={username}
                        name='username'
                        type='text'
                        autoComplete='off'
                        onFocus={() => setUsernameFocus('focused')}
                        onBlur={() => setUsernameFocus('')}
                        onChange={handleChange}
                        valid={usernameState === 'valid'}
                        invalid={usernameState === 'invalid'}
                        placeholder='예약자 성함을 입력해주세요'
                      />
                      <div className='invalid-feedback ml-1'>
                        올바른 성명이 아닙니다
                      </div>
                    </Col>
                  </FormGroup>
                  <FormGroup className='row'>
                    <Label
                      className='form-control-label font-weight-bold'
                      htmlFor='username'
                      md='2'
                    >
                      단체명
                    </Label>
                    <Col md='10'>
                      <Input
                        // defaultValue={values.username}
                        className='form-control-alternative'
                        name='groupname'
                        type='text'
                        autoComplete='off'
                        placeholder='모임/기관 단체명을 입력해주세요'
                        value={groupname}
                        // onFocus={() => setUsernameFocus("focused")}
                        // onBlur={() => setUsernameFocus("")}
                        onChange={handleChange}
                        // valid={usernameState === "valid"}
                        // invalid={usernameState === "invalid"}
                      />
                    </Col>
                  </FormGroup>
                  <FormGroup className='row'>
                    <Label
                      className='form-control-label font-weight-bold'
                      htmlFor='username'
                      md='2'
                    >
                      연락처<span className='text-red'>*</span>
                    </Label>
                    <Col md='10' className={classnames(phoneValid)}>
                      <Input
                        // defaultValue={values.username}
                        className='form-control-alternative'
                        maxLength={11}
                        minLength={10}
                        name='phone'
                        type='text'
                        value={phone}
                        autoComplete='off'
                        onFocus={() => setPhoneFocus('focused')}
                        onBlur={() => setPhoneFocus('')}
                        onChange={handleChange}
                        valid={phoneState === 'valid'}
                        invalid={phoneState === 'invalid'}
                        placeholder="핸드폰 11자리를 '-' 없이 입력해주세요"
                      />
                      <div className='invalid-feedback ml-1'>
                        올바른 휴대폰 번호가 아닙니다
                      </div>
                    </Col>
                  </FormGroup>
                  <FormGroup className='row'>
                    <Label
                      className='form-control-label font-weight-bold'
                      htmlFor='email'
                      md='2'
                    >
                      이메일<span className='text-red'>*</span>
                    </Label>
                    <Col md='10' className={classnames(emailValid)}>
                      <Input
                        // defaultValue={values.username}
                        className='form-control-alternative'
                        name='email'
                        type='text'
                        autoComplete='off'
                        onFocus={() => setEmailFocus('focused')}
                        onBlur={() => setEmailFocus('')}
                        value={email}
                        onChange={handleChange}
                        valid={emailState === 'valid'}
                        invalid={emailState === 'invalid'}
                        placeholder='이메일 주소를 입력해주세요'
                      />
                      <div className='invalid-feedback ml-1'>
                        올바른 이메일 주소가 아닙니다.
                      </div>
                    </Col>
                  </FormGroup>
                  <FormGroup className='row'>
                    <Label
                      className='form-control-label font-weight-bold'
                      htmlFor='username'
                      md='2'
                    >
                      행사명<span className='text-red'>*</span>
                    </Label>
                    <Col md='10' className={classnames(eventNameValid)}>
                      <Input
                        // defaultValue={values.username}
                        className='form-control-alternative'
                        name='eventname'
                        type='text'
                        value={eventname}
                        autoComplete='off'
                        placeholder='행사명 or 사용목적을 입력해주세요'
                        onFocus={() => setEventNameFocus('focused')}
                        onBlur={() => setEventNameFocus('')}
                        onChange={handleChange}
                        valid={eventNameState === 'valid'}
                        invalid={eventNameState === 'invalid'}
                      />
                      <div className='invalid-feedback ml-1'>
                        행사명은 필수입력 사항 입니다.
                      </div>
                    </Col>
                  </FormGroup>
                  <Row>
                    <Col className='' md='8'>
                      {/* <div className="custom-control custom-checkbox mt-2">
												<input
													className="custom-control-input"
													id="customCheck"
													type="checkbox"
													name="consentCheck"
													// value={values.consentCheck}
													// onChange={handleChange}
													// onClick={() => setConsent(!consent)}
												/>
												<label
													className="custom-control-label pl-lg-3"
													htmlFor="customCheck">
													<div className="h5 font-weight-700">
														사전물품보관 신청{" "}
													</div>
												</label>
											</div> */}
                      <div className='mb-3 mt-3'>
                        <Row>
                          <Col md='4'>
                            <div className='h5 font-weight-700'>
                              사전물품보관{' '}
                            </div>
                          </Col>
                          <Col md='3'>
                            <div className='custom-control custom-radio'>
                              <input
                                // defaultValue = {false || ""}
                                // defaultChecked = {false}
                                type='radio'
                                className='custom-control-input'
                                id='customCheck'
                                name='agreementKeep'
                                checked={agreementKeep}
                                // defaultValue={agreementKeep}
                                // defaultChecked={agreementKeep}
                                onChange={(e) =>
                                  //   setdmkHallData((dmkHallData) => ({
                                  //     ...dmkHallData,
                                  //     agreementKeep: !agreementKeep,
                                  //   }))
                                  dispatch(
                                    setDmkHallReserveValue(
                                      e.target.name,
                                      !agreementKeep
                                    )
                                  )
                                }

                                // onChange={handleChange}
                              />
                              <label
                                className='custom-control-label pl-lg-3'
                                htmlFor='customCheck'
                              >
                                <div className='h5 font-weight-700'>신청</div>
                              </label>
                            </div>
                          </Col>
                          <Col md='4'>
                            <div className='custom-control custom-radio'>
                              <input
                                // defaultValue= {true || ""}
                                // defaultChecked = {true}
                                type='radio'
                                className='custom-control-input'
                                id='customCheck2'
                                name='agreementKeep'
                                checked={!agreementKeep}
                                // defaultValue={!agreementKeep}
                                // defaultChecked={!agreementKeep}
                                onChange={(e) => (
                                  dispatch(
                                    setDmkHallReserveValue(
                                      e.target.name,
                                      !agreementKeep
                                    )
                                    // eslint-disable-next-line no-sequences
                                  ),
                                  dispatch(
                                    setDmkHallReserveValue('keepTime', '')
                                  ),
                                  dispatch(
                                    setDmkHallReserveValue('keepList', '')
                                  )
                                )}
                              />
                              <label
                                className='custom-control-label pl-lg-3'
                                htmlFor='customCheck2'
                              >
                                <div className='h5 font-weight-700'>미신청</div>
                              </label>
                            </div>
                          </Col>
                        </Row>
                      </div>
                    </Col>
                    <Col md='10'>
                      <div className='font-weight-bold ml-lg-5 mb-3'>
                        배용 예상 시간
                      </div>
                    </Col>
                  </Row>
                  <FormGroup>
                    <Col md='12' className='ml-lg-4 px-lg-4'>
                      <Input
                        // defaultValue={values.username}
                        className='form-control-alternative'
                        name='keepTime'
                        type='text'
                        autoComplete='off'
                        placeholder='배용 예상시간을 입력해주세요'
                        disabled={!agreementKeep}
                        value={keepTime}
                        // onFocus={() => setUsernameFocus("focused")}
                        // onBlur={() => setUsernameFocus("")}
                        onChange={handleChange}
                        // valid={usernameState === "valid"}
                        // invalid={usernameState === "invalid"}
                      />
                    </Col>
                  </FormGroup>
                  <Row>
                    <Col md='10'>
                      <div className='font-weight-bold ml-lg-5 mb-3'>
                        보관 물품 내역
                      </div>
                    </Col>
                  </Row>
                  <FormGroup>
                    <Col md='12' className='ml-lg-4 px-lg-4'>
                      <Input
                        // defaultValue={values.username}
                        className='form-control-alternative'
                        name='keepList'
                        value={keepList}
                        type='textarea'
                        autoComplete='off'
                        placeholder='보관 물품 내역을 입력해주세요'
                        disabled={!agreementKeep}
                        // onFocus={() => setUsernameFocus("focused")}
                        // onBlur={() => setUsernameFocus("")}
                        onChange={handleChange}
                        // valid={usernameState === "valid"}
                        // invalid={usernameState === "invalid"}
                      />
                    </Col>
                  </FormGroup>
                  <Row>
                    <Col md='10' className='ml-lg-5 text-muted'>
                      <div className='font-weight-700 mb-2'>
                        [사전물품보관 규정안내]
                      </div>
                      <div className='ml-2'>
                        사전에 허가된 용품에 한해 보관이 가능합니다
                        <br />
                        사무용품, 다과 및 음료만 가능합니다(그 외 물품 및 음식
                        보관 불가)
                        <br />
                        조리 용품 반입 금지, 조리 불가합니다
                        <br />
                        대관 사용 후 나온 모든 쓰레기는 수거 및 처리 해야합니다
                        <br />
                        <br />
                      </div>
                    </Col>
                  </Row>
                  <Row>
                    <Col className='' md='8'>
                      {/* <div className="custom-control custom-checkbox mt-2">
												<input
													className="custom-control-input"
													id="customCheck"
													type="checkbox"
													name="consentCheck"
													// value={values.consentCheck}
													// onChange={handleChange}
													// onClick={() => setConsent(!consent)}
												/>
												<label
													className="custom-control-label pl-lg-3"
													htmlFor="customCheck">
													<div className="h5 font-weight-700">
														사전물품보관 신청{" "}
													</div>
												</label>
											</div> */}
                      <div className='mb-3 mt-3'>
                        <Row>
                          <Col md='4'>
                            <div className='h5 font-weight-700'>강의촬영 </div>
                          </Col>
                          <Col md='3'>
                            <div className='custom-control custom-radio'>
                              <input
                                type='radio'
                                className='custom-control-input'
                                id='customCheck3'
                                name='agreementPhoto'
                                checked={agreementPhoto}
                                onChange={(e) =>
                                  //   setdmkHallData((dmkHallData) => ({
                                  //     ...dmkHallData,
                                  //     agreementPhoto: !agreementPhoto,
                                  //   }))
                                  dispatch(
                                    setDmkHallReserveValue(
                                      e.target.name,
                                      !agreementPhoto
                                    )
                                  )
                                }
                              />
                              <label
                                className='custom-control-label pl-lg-3'
                                htmlFor='customCheck3'
                              >
                                <div className='h5 font-weight-700'>신청</div>
                              </label>
                            </div>
                          </Col>
                          <Col md='4'>
                            <div className='custom-control custom-radio'>
                              <input
                                type='radio'
                                className='custom-control-input'
                                id='customCheck4'
                                name='agreementPhoto'
                                checked={!agreementPhoto}
                                onChange={(e) => (
                                  //   setdmkHallData((dmkHallData) => ({
                                  //     ...dmkHallData,
                                  //     agreementPhoto: !agreementPhoto,
                                  //   }))
                                  dispatch(
                                    setDmkHallReserveValue(
                                      e.target.name,
                                      !agreementPhoto
                                    )
                                  ),
                                  dispatch(
                                    setDmkHallReserveValue('photoTime', '')
                                  )
                                )}
                              />
                              <label
                                className='custom-control-label pl-lg-3'
                                htmlFor='customCheck4'
                              >
                                <div className='h5 font-weight-700'>미신청</div>
                              </label>
                            </div>
                          </Col>
                        </Row>
                      </div>
                    </Col>
                    <Col md='10'>
                      <div className='font-weight-bold ml-lg-5 mb-3'>
                        촬영시간
                      </div>
                    </Col>
                  </Row>
                  <FormGroup>
                    <Col md='12' className='ml-lg-4 px-lg-4'>
                      <Input
                        // defaultValue={values.username}
                        className='form-control-alternative'
                        name='photoTime'
                        value={photoTime}
                        type='text'
                        autoComplete='off'
                        placeholder='예상 촬영 시간을 입력해주세요'
                        disabled={!agreementPhoto}
                        onChange={handleChange}
                        // onFocus={() => setUsernameFocus("focused")}
                        // onBlur={() => setUsernameFocus("")}
                        // onChange={handleChange}
                        // valid={usernameState === "valid"}
                        // invalid={usernameState === "invalid"}
                      />
                    </Col>
                  </FormGroup>
                  <Row>
                    <Col md='10' className='ml-lg-5 text-muted'>
                      <div className='font-weight-700 mb-2'>
                        [사전물품보관 규정안내]
                      </div>
                      <div className='ml-2'>
                        강의촬영 규정
                        <br />
                        강의촬영 규정
                        <br />
                        <br />
                      </div>
                    </Col>
                  </Row>
                </Form>
                {/* </CardText> */}
              </CardBody>
              <CardBody>
                <div className='h5 font-weight-700'>대관담당자</div>
                <hr className='w-100 my-2' />
                <CardText className='px-4 font-weight-500'>
                  김송강 선임
                  <span className='px-lg-3' />
                  010-8187-2381
                  <span className='px-lg-3' />
                  song@datamarketing.co.kr
                </CardText>
              </CardBody>
            </Card>
            <Card className='bg-secondary'>
              <CardBody className='p-5'>
                <CardTitle className='font-weight-700'>
                  예약 시 주의사항을 확인해주세요
                </CardTitle>
                -입실, 퇴실 시간을 준수해 주십시오. 행사 당일 30분 전에 미리
                오셔서 대관 담당자의 안내를 받으십시오.
                <br />
                (시설 세팅 및 정리를 위한 대관시간 전/후 각 30분씩 제공)
                <br />
                - 주차 공간은 제공하지 않습니다. 대중교통을 이용해 주시길
                바랍니다.
                <br />
                - 책상 의자 등 사전, 사후 세팅을 직접 해 주셔야 하며, 대관 종료
                후 사전 세팅 전 원상태로 정리해야 합니다.
                <br />
                - 현수박 등 안내 게시물은 사전 신청 시, 승인 후 허락된 곳에만
                부착할 수 있습니다.
                <br />
                - 다과와 음료 외 음식물 반입은 불가합니다. 필요 시 사전 문의 및
                승인을 받으셔야 합니다.
                <br />
                - 진행 중 발생한 쓰레기(일반/재활용/음식물 등)는 반드시 수거 및
                분리 배출해야 하며(쓰레기봉투 지원),
                <br />
                퇴실 시 모든 쓰레기는 수거해야 합니다.
                <br />
                - 퇴실 시 시설 이용에 대한 퇴실 체크리스트를 반드시 작성해야
                합니다.
                <br />
                - 취소 수수료는 행사 7일 전까지 대관료의 50%, 6일 전부터 행사
                당일까지는 전액 100% 입니다.
                <br />
                - 대관 예약을 변경하고자 하는 경우에는 기존 예약을 취소 후
                새롭게 에약을 진행해 주셔야 합니다.
                <br />
                <br />
                <div className='custom-control custom-checkbox mt-2 text-center'>
                  <input
                    className='custom-control-input'
                    id='reservationCheckbox'
                    type='checkbox'
                    name='agreementTotal'
                    onChange={(e) =>
                      //   setdmkHallData((dmkHallData) => ({
                      //     ...dmkHallData,
                      //     agreementTotal: !agreementTotal,
                      //   }))
                      dispatch(
                        setDmkHallReserveValue(e.target.name, !agreementTotal)
                      )
                    }
                    // value={values.consentCheck}
                    // onChange={handleChange}
                    // onClick={() => setConsent(!consent)}
                  />
                  <label
                    className='custom-control-label'
                    htmlFor='reservationCheckbox'
                  >
                    <div className='h6 font-weight-bold text-primary'>
                      모든 주의사항을 확인하였습니다.{' '}
                    </div>
                  </label>
                </div>
              </CardBody>
            </Card>
            <div className='mb-3'>
              <span className='text-lg font-weight-800 mb-3'>약관동의</span>
            </div>
            <div />
            <Card>
              <div className='accordion-1'>
                <Container>
                  <Row>
                    <Col className='ml-auto' md='12'>
                      <div
                        className='accordion my-1 custom-checkbox'
                        id='accordionExample'
                      >
                        <Card>
                          <CardHeader id='headingOne'>
                            <h5 className='mb-0'>
                              <Button
                                aria-expanded={openedCollapse === 'collapse-1'}
                                onClick={() =>
                                  setOpenedCollapse(
                                    openedCollapse === 'collapse-1'
                                      ? ''
                                      : 'collapse-1'
                                  )
                                }
                                className='w-100 text-primary text-left'
                                color='link'
                                type='button'
                              >
                                <input
                                  className='custom-control-input'
                                  id='agreement1'
                                  type='checkbox'
                                  name='agreementCollectUserInfo'
                                  checked={agreementCollectUserInfo}
                                  onChange={(e) =>
                                    // setdmkHallData((dmkHallData) => ({
                                    //   ...dmkHallData,
                                    //   agreementCollectUserInfo:
                                    //     !agreementCollectUserInfo,
                                    // }))
                                    dispatch(
                                      setDmkHallReserveValue(
                                        e.target.name,
                                        !agreementCollectUserInfo
                                      )
                                    )
                                  }
                                />
                                <label
                                  className='custom-control-label left--1'
                                  htmlFor='agreement1'
                                />
                                개인정보 수집 및 이용 동의서(필수){' '}
                                <i className='ni ni-bold-down float-right pt-1' />
                              </Button>
                            </h5>
                          </CardHeader>
                          <Collapse isOpen={openedCollapse === 'collapse-1'}>
                            <CardBody className='opacity-8'>
                              <span className='font-weight-700'>
                                [필수]개인정보보호법 제 15조 제1항의 의거
                                개인정보 수집 및 이용에 동의합니다.
                              </span>
                              <ul
                                className='pl-4'
                                style={{
                                  fontSize: '14px',
                                  lineHeight: '1.5rem',
                                }}
                              >
                                <li>
                                  개인 정보 수집 목적 및 이용 목적 : 교육 서비스
                                  제공에 관한 계약 이행 및 서비스 제공에 따른
                                  대관비 정산, 정보 및 콘텐츠 제공, 신청 확인,
                                  영수증 발급, 화면 이력관리
                                </li>
                                <li>
                                  수집하는 항목 : 회사명, 회사 주소/전화번호,
                                  사업자등록번호, 성명, 주민등록번호(수료증
                                  발급과정 시), 부서, 직위, 핸드폰, 이메일
                                </li>
                                <li>
                                  개인정보의 보유 및 이용기간 : 대관 신청자
                                  개인정보는 추후 이력관리 및 원활한 서비스
                                  지원을 위해 최소 3년간 보유합니다.
                                </li>
                                <li>
                                  다만, 이용자가 메일/전화 등을 통해 정보 삭제를
                                  요청한 경우 수집된 개인 정보는 재생할 수 없는
                                  방법에 의하여 완전히 삭제되면 어떠한 용도로도
                                  열람 또는 이용할 수 없도록 처리됩니다.
                                </li>
                              </ul>
                            </CardBody>
                          </Collapse>
                        </Card>
                        <Card>
                          <CardHeader id='headingTwo'>
                            <h5 className='mb-0'>
                              <Button
                                aria-expanded={openedCollapse2 === 'collapse-2'}
                                onClick={() =>
                                  setOpenedCollapse2(
                                    openedCollapse2 === 'collapse-2'
                                      ? ''
                                      : 'collapse-2'
                                  )
                                }
                                className='w-100 text-primary text-left collapsed'
                                color='link'
                                type='button'
                              >
                                <input
                                  className='custom-control-input'
                                  id='agreement2'
                                  type='checkbox'
                                  name='agreementHallRules'
                                  checked={agreementHallRules}
                                  onChange={(e) =>
                                    // setdmkHallData((dmkHallData) => ({
                                    //   ...dmkHallData,
                                    //   agreementHallRules: !agreementHallRules,
                                    // }))
                                    dispatch(
                                      setDmkHallReserveValue(
                                        e.target.name,
                                        !agreementHallRules
                                      )
                                    )
                                  }
                                />
                                <label
                                  className='custom-control-label left--1'
                                  htmlFor='agreement2'
                                />
                                대관이용수칙 확인 및 결제진행 동의(필수){' '}
                                <i className='ni ni-bold-down float-right pt-1' />
                              </Button>
                            </h5>
                          </CardHeader>
                          <Collapse isOpen={openedCollapse2 === 'collapse-2'}>
                            <CardBody className='opacity-8'>
                              <span className='font-weight-700'>
                                본인은 대관 이용 약관을 모두 이해하였으며, 해당
                                내용을 준수할 것에 동의합니다.
                              </span>
                              <ul
                                className='pl-4'
                                style={{
                                  fontSize: '14px',
                                  lineHeight: '1.5rem',
                                }}
                              >
                                <li>
                                  입실, 퇴실 시간을 준수해 주십시오. 행사 당일
                                  30분 전에 미리 오셔서 대관 담당자의 안내를
                                  받으십시오. (시설 세팅 및 정리를 위한 대관시간
                                  전/후 각 30분씩 제공)
                                </li>
                                <li>
                                  주차 공간은 제공하지 않습니다. 대중교통을
                                  이용해 주시길 바랍니다.
                                </li>
                                <li>
                                  책상 의자 등 사전, 사후 세팅을 직접 해 주셔야
                                  하며, 대관 종료 후 사전 세팅 전 원상태로
                                  정리해야 합니다.
                                </li>
                                <li>
                                  현수막 등 안내 게시물은 사전 신청 시, 승인 후
                                  허락된 곳에만 부착할 수 있습니다.
                                </li>
                                <li>
                                  다과와 음료 외 음식물 반입은 불가합니다. 필요
                                  시 사전 문의 및 승인을 받으셔아 합니다.
                                </li>
                                <li>
                                  진행 중 발생한 쓰레기(일반/재활용/음식물 등)는
                                  반드시 수거 및 분리 배출해야 하며(쓰레기봉투
                                  지원), 퇴실 시 모든 쓰레기는 수거해야 합니다.
                                </li>
                                <li>
                                  퇴실 시 시설 이용에 대한 퇴실 체크리스트를
                                  반드시 작성해야 합니다.
                                </li>
                                <li>
                                  퇴실 시 전등, 공기청정기, 환풍기, 배관 시설 등
                                  모든 전원이 오프 상태 임을 반드시 확인해야
                                  합니다.
                                </li>
                                <li>
                                  정치, 종교, 판촉 목적의 행사를 위한 대관을
                                  금지합니다
                                </li>
                                <li>
                                  취소 및 변경 수수료는 행사 7일 전까지
                                  대관료의50%, 6일 전부터 행사 당일까지는 전액
                                  100% 입니다.
                                </li>
                                <li>
                                  강의장 내 기자재와 시설 이상, 분실, 파손, 시
                                  공급 가액 기준으로 지불해야 하며, 이후 대관
                                  이용이 불가할 수 있습니다.
                                </li>
                                <li>
                                  사무용품 대여 및 인쇄는 별도로 지원하지
                                  않습니다. (사전 개별 준비)
                                </li>
                                <li>
                                  LED화면 사용 시 노트북 지참 바랍니다. (별도
                                  대여 불가)
                                </li>
                                <li>
                                  업무 시간 외 새벽, 저녁 시간 대관 등을 원할
                                  경우 원활한 출입을 위해 담당자에게 연락하시길
                                  바랍니다.
                                </li>
                                <li>
                                  대관 예약 전 사전 답사를 원할 경우 담당자에게
                                  연락해 시간을 조율하시길 바랍니다.
                                </li>
                                <li>
                                  강의장 기자재와 시설이상, 분실, 파손 시
                                  공급가액 기준으로 지불해야 하며 대관 이용이
                                  불가할 수 있습니다.
                                </li>
                              </ul>
                            </CardBody>
                          </Collapse>
                        </Card>
                      </div>
                    </Col>
                  </Row>
                </Container>
              </div>
            </Card>
          </Col>
          <Col lg='4' md='12'>
            {/* Todo: 고정 Column - 결제 예정금액 */}
            <DMKHallReservePayment
              emailState={emailState}
              phoneState={phoneState}
            />
          </Col>
          {/* Todo: 달력 및 인원 시간 로직 추가 */}
        </Row>
      </Container>
      <CommonFooter />
      {/* <CommonFooter /> */}
    </>
  );
}

const tableHeader = {
  fontSize: '2rem',
  lineHeight: 2,
};

const tableTRow = {
  fontWeight: 'bold',
};

const tableTitle = {
  fontWeight: 'bold',
  color: 'black',
};
