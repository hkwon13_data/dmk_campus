import React from 'react';

import CommonNavbar from 'components/NavBar/CommonNavbar';
import CommonFooter from 'components/NavBar/CommonFooter';
import { ScrollTopButton } from 'components/utils/ScrollTopButton';
import FloatingButton from '../../components/NavBar/FloatingButton';

import DMKHallMainCarousel from './DMKHallCarousel';
import DMKHallCmptt from './DMKHallCmptt';
import DMKHallDesc from './DMKHallDesc';
import DMKHallFeedback from './DMKHallFeedBack';
import DMKHallGoogleMap from './DMKHallGoogleMap';
import DMKHallReservePriceDesc from './DMKHallReservePriceDesc';

export default function DMKHallPage() {
  React.useEffect(() => {
    // let classList = document.body.classList;
    // document.body.classList.remove(...classList);
    // document.body.classList.add('index-page');
    window.scrollTo(0, 0);
    // return function cleanup() {
    //   document.body.classList.remove('index-page');
    // };
  }, []);

  return (
    <>
      <ScrollTopButton />
      <CommonNavbar type='primary' />
      <FloatingButton />
      <DMKHallDesc />
      <DMKHallMainCarousel />
      <hr className='w-100 my-9' />
      <DMKHallCmptt />
      <DMKHallFeedback />
      <DMKHallReservePriceDesc hidden />
      <DMKHallGoogleMap />
      <CommonFooter />
    </>
  );
}
