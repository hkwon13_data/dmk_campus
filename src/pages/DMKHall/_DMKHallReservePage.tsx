import React from 'react';
// import { Container, Row, Button } from "reactstrap";
import CommonNavbar from 'components/NavBar/CommonNavbar';
import CommonFooter from 'components/NavBar/CommonFooter';
import { setInitReserveValue } from 'store/modules/dmkReserveManage';
import { useDispatch, useSelector } from 'react-redux';
import { ScrollTopButton } from 'components/utils/ScrollTopButton';
import { RootState } from 'store/modules/index';
import FloatingButton from '../../components/NavBar/FloatingButton';

import DMKHallReserveHeader from './DMKHallReserveHeader';
import DMKHallReservePriceDesc from './DMKHallReservePriceDesc';

// import { format, getDay, addYears } from "date-fns";

export default function DMKHallReservePage() {
  // const [paymentState, setPaymentState] = useState(false);

  const dispatch = useDispatch();
  const dmkHallData = useSelector(
    (state: RootState) => state.dmkReserveManage.initReservation
  );
  React.useEffect(() => {
    dispatch(setInitReserveValue(dmkHallData)); // 페이지가 렌더링 시에 리덕스스토어에 스테이트를 초기화 하기 위한 dispatch
    // let classList = document.body.classList;
    // document.body.classList.remove(...classList);
    // document.body.classList.add('index-page');
    window.scrollTo(0, 0);
    // return function cleanup() {
    //   document.body.classList.remove('index-page');
    // };
  }, []);

  return (
    <>
      <ScrollTopButton />
      <>
        <CommonNavbar type='transparent' />
        <FloatingButton />
        <DMKHallReserveHeader />
        <DMKHallReservePriceDesc hidden />
      </>
      <CommonFooter />
    </>
  );
}
