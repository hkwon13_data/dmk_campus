import React from 'react';

// reactstrap components
import { Container, Row, Col } from 'reactstrap';

export default function DMKHallLocationDesc() {
  return (
    <>
      <Container className='my-5'>
        <Row>
          <Col lg='6'>
            <div className='info info-horizontal bg-white'>
              <div className='icon icon-shape icon-shape-info rounded-circle text-white'>
                <i className='ni ni-hat-3 text-info' />
              </div>
              <div className='description pl-4'>
                <h5 className='title text-info'>Modular Components</h5>
                <p>
                  The Arctic Ocean freezes every winter and much of the sea-ice
                  then thaws every summer, and that process will continue
                  whatever.
                </p>
                <a
                  href='#pablo'
                  onClick={(e) => e.preventDefault()}
                  className='text-info'
                >
                  Learn more
                </a>
              </div>
            </div>
          </Col>
          <Col lg='6'>
            <div className='info info-horizontal bg-white'>
              <div className='icon icon-shape icon-shape-info rounded-circle text-white'>
                <i className='ni ni-hat-3 text-info' />
              </div>
              <div className='description pl-4'>
                <h5 className='title text-info'>Modular Components</h5>
                <p>
                  The Arctic Ocean freezes every winter and much of the sea-ice
                  then thaws every summer, and that process will continue
                  whatever.
                </p>
                <a
                  href='#pablo'
                  onClick={(e) => e.preventDefault()}
                  className='text-info'
                >
                  Learn more
                </a>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </>
  );
}
