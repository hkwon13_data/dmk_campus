import React, { useState } from 'react';
import { RootState } from 'store/modules/index';
import { Container, Row, Toast, Spinner } from 'reactstrap';

// reactstrap components
import { format, addYears } from 'date-fns';
// import { axiosInstance, useAxios } from "api";

import ko from 'date-fns/locale/ko';
import { DatePickerCalendar } from 'react-nice-dates';

import { getHolidays } from 'store/modules/dmkHall';
import {
  setDmkHallReserveValue,
  setInitReserveValue,
} from 'store/modules/dmkReserveManage';
import { useSelector, useDispatch } from 'react-redux';
import TimeButton from './DMKHallReserveTimeRow';
import BasketButton from './DMKHallReserveBasket';
import PeopleCountButton from './DMKHallReservePplCount';

const Divier = {
  height: 0,
  margin: '0 1rem 1rem',
  overflow: 'hidden',
  borderTop: '2px solid #e9ecef',
};

export default function DMKHallReserveDatePicker() {
  const dispatch = useDispatch();
  const { data, loading, error } = useSelector(
    (state: RootState) => state.holidays.holidays
  );

  // const dmkHallData = useSelector(
  //   (state: RootState) => state.dmkReserveManage.dmkHallReservation
  // );
  const dmkHallInitData = useSelector(
    (state: RootState) => state.dmkReserveManage.initReservation
  );

  const [initDate, setInitDate] = useState(new Date());
  const [selectedDate, setSelectedDate] = useState('');
  const [isHoliday, setIsHoliday] = useState(false);

  // eslint-disable-next-line consistent-return
  const weekSetting = (date: any): string | undefined => {
    if (format(date, 'eee') === 'Mon') {
      return '월';
    }
    if (format(date, 'eee') === 'Tue') {
      return '화';
    }
    if (format(date, 'eee') === 'Wed') {
      return '수';
    }
    if (format(date, 'eee') === 'Thu') {
      return '목';
    }
    if (format(date, 'eee') === 'Fri') {
      return '금';
    }
    if (format(date, 'eee') === 'Sat') {
      return '토';
    }
    if (format(date, 'eee') === 'Sun') {
      return '일';
    }
  };

  const handleDayClick = (date: any) => {
    setInitDate(date);
    setSelectedDate(format(date, 'eee uuMMdd'));
    dispatch(setInitReserveValue(dmkHallInitData));
    dispatch(setDmkHallReserveValue('year', format(date, 'uu')));
    dispatch(setDmkHallReserveValue('month', format(date, 'MM')));
    dispatch(setDmkHallReserveValue('day', format(date, 'dd')));
    dispatch(setDmkHallReserveValue('week', weekSetting(date)));
    dispatch(setDmkHallReserveValue('responseData', ['10', '11', '12', '13']));
  };

  React.useEffect(() => {
    setSelectedDate(format(initDate, 'eee uuMMdd'));
  }, []); // didmount단게에서 selectedDate를 오늘 날짜로 설정. => 그래야 오늘 날짜가 주말/공휴일 일때 isHoliday가 밑에 useEffect에서 true로 설정 됨.

  React.useEffect(() => {
    if (data) {
      // eslint-disable-next-line @typescript-eslint/no-unused-expressions
      data.data.indexOf(selectedDate.split(' ')[0]) > 0 ||
      data.data.indexOf(selectedDate.split(' ')[1]) > 0
        ? setIsHoliday(true)
        : setIsHoliday(false);
      return;
    }
    dispatch(getHolidays(undefined));
  }, [dispatch, selectedDate, data]);

  // eslint-disable-next-line consistent-return
  const PostListContainer = () => {
    if (loading)
      return (
        <Row className='justify-content-center'>
          <Spinner style={{ width: '3rem', height: '3rem' }} />
        </Row>
      );
    if (error) console.log(error);
    if (data)
      return (
        <>
          {data && (
            <DatePickerCalendar
              date={initDate}
              minimumDate={new Date()}
              maximumDate={addYears(initDate, 1)}
              onDateChange={(e: any) => handleDayClick(e)}
              // format='dd MM yyyy'
              locale={ko}
            />
          )}
        </>
      );
  };

  return (
    <>
      <Container
        style={{
          position: 'sticky',
          top: 80,
          right: 0,
          left: 0,
          zIndex: 1,
          border: '1px',
        }}
        className='bg-white pb-5'
      >
        <Toast className='bg-white p-3'>
          <div className='text-primary font-weight-900 py-3'>날짜선택</div>
          <div>{PostListContainer()}</div>
          <div className='text-primary text-sm font-weight-800 mt-3'>
            시간 선택
          </div>
          <Row className='justify-content-left overflow-auto m-1'>
            <TimeButton isHoliday={isHoliday} />
          </Row>
          <div style={Divier} className='my-3' />
          <PeopleCountButton />
          <BasketButton />
        </Toast>
      </Container>
    </>
  );
  // const payFixed = {
  //   position: 'sticky',
  //   top: 80,
  //   right: 0,
  //   left: 0,
  //   zIndex: 1,
  //   border: '1px',
  // };
}
