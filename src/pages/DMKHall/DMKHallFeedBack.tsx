import React from 'react';

// reactstrap components
import { Container, Col, Card, Row, CardBody, CardTitle } from 'reactstrap';

// import imgA from "assets/img/faces/team-3.jpg";

// Core Components

export default function MainFeedBacks() {
  return (
    <>
      <section className='team-1 my-5 pt-7'>
        <Container>
          <Row>
            <Col className='ml-auto mr-auto text-center' md='8'>
              <p className='display-5'>수많은 기업들이 선택한 공간</p>
              <div className='h7'>
                넓은 수용공간과 접근성, 대형 LED와 음향시설, 세련된 인테리어로
                이루어진 공간
              </div>
            </Col>
          </Row>
        </Container>
      </section>
      <Container>
        <Row className='justify-content-center mt-5'>
          <Col lg='4' md='6'>
            <Card className='p-2 card-lift--hover'>
              <CardTitle className='pl-4 pt-3'>
                <div className='icon icon-sm'>
                  <i className='ni ni-like-2 align-text-bottom' />
                </div>
                <span className='h5 font-weight-900'>KMA(한국능률협회)</span>
              </CardTitle>
              <CardBody className='pt-0'>
                <h6 className='m-0 pb-3 font-weight-bold'>
                  최고 경영자조찬회, 리더스포럼
                </h6>
                <div className='font-weight-300'>
                  최고 경영자조찬회모임의
                  <br />
                  특성상 품격있는 행사 공간이 필요한데,
                  <br />
                  데마코홀은 고급스러운 인테리어와 함께
                  <br />
                  대형 LED스크린, 음향장비가 갖춰져 있어서
                  <br />
                  온라인 행사시, 저희가 원하는 수준으로 진행할 수 있었습니다
                </div>
              </CardBody>
            </Card>
          </Col>
          <Col lg='4' md='6'>
            <Card className='p-2 card-lift--hover'>
              <CardTitle className='pl-4 pt-3'>
                <div className='icon icon-sm'>
                  <i className='ni ni-like-2 align-text-bottom' />
                </div>
                <span className='h5 font-weight-900'>(주) 동아사이언스</span>
              </CardTitle>
              <CardBody className='pt-0'>
                <h6 className='m-0 pb-3 font-weight-bold'>
                  과학콘서트, GREEB 콘서트 온라인 송출
                </h6>
                <div className='font-weight-300'>
                  인플루언서들이 출연하는 행사가 많아서
                  <br />
                  완벽한 시설, 넓은 공간, 접근성을 고려하여
                  <br />
                  공간을 선정하는데, 데마코홀은 적정한 가격에 대형LED, 음향,
                  넓은 공간 접근성을 모두 갖추고
                  <br />
                  있어서 최적의 행사 장소였습니다.
                  <br />
                  <br />
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
}
