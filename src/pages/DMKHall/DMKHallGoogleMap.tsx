/* eslint-disable react/no-unescaped-entities */
import React from 'react';
// import style from "styled-components";
import { useHistory } from 'react-router-dom';

// reactstrap components
import { Button, Container, Row, Col } from 'reactstrap';

import imgA from 'assets/img/dmkhall/map_1.png';

export default function DMKHallGoogleMap() {
  const history = useHistory();

  return (
    <>
      <section className='team-1 my-5 pt-7'>
        <Container>
          <Row>
            <Col className='ml-auto mr-auto text-center' md='8'>
              <p className='display-5'>오시는 길</p>
              <div className='h7'>
                지하철 2호선과 경인분당선을 모두 이용할 수 있는 '선릉역' 도보3분
                <br />
                강남 지역 접근성이 가장 높은 모임 공간
              </div>
            </Col>
          </Row>
        </Container>
      </section>
      <Container className='mb-5'>
        <Row className='justify-content-md-center'>
          <Col lg='8' md='12' xs='12'>
            <div className='image'>
              <img src={imgA} className='img-thumbnail' alt='...' />
            </div>
          </Col>
        </Row>
        <Row>
          <Col className='text-center my-3'>
            <div>
              <span
                className='text-primary font-weight-700'
                style={{ fontSize: '1.1rem' }}
              >
                강남 선릉 데마코홀
              </span>
              <span className='px-4'>|</span>
              <span className='text-sm'>
                서울 강남구 테헤란로 53길 16 예안빌딩 B1층,
                선릉역(2호선/경인분당선) 7번 출구 도보 3분
              </span>
            </div>
          </Col>
        </Row>
        <Row className='justify-content-center mt-4'>
          <Button
            type='button'
            color='primary'
            className='btn p-3'
            onClick={() => history.push('/dmkHall/reservation/')}
          >
            대관 문의하기
          </Button>
        </Row>
      </Container>
    </>
  );
}
