import React from 'react';

// reactstrap components
import { Card, CardBody, CardTitle, Container, Row, Col } from 'reactstrap';

import imgA from 'assets/img/dmkhall/facility_1.png';
import imgB from 'assets/img/dmkhall/facility_2.png';
import imgC from 'assets/img/dmkhall/facility_3.png';
import imgD from 'assets/img/dmkhall/facility_4.png';

// Core Components

export default function DMKHallCmptt() {
  return (
    <>
      <div className='my-5'>
        <Container>
          <Row>
            <Col className='mx-auto text-center' lg='8'>
              <p className='display-5'>On/Off 행사에 최적화된 프리미엄 시설</p>
              <div className='h7'>
                언택트 시대에 맞는 행사 시설 및 장비 제공
                <br />
                모임, 강연, 촬영, 행사 등 다양한 목적을 충족하는 국내 유일의
                프리미엄 공간
              </div>
            </Col>
          </Row>
          <Row className='align-items-center mt-6'>
            <Col lg='6'>
              <Card
                className='card-blog card-background'
                data-animation='zooming'
              >
                <div
                  className='full-background'
                  style={{
                    backgroundImage: `url(${imgA})`,
                  }}
                />
                <CardBody>
                  <div className='content-bottom'>
                    <CardTitle tag='h5'>비대면 온라인 행사</CardTitle>
                    <h6 className='card-category text-white opacity-8'>
                      줌(Zoom), 웨비나(Webinar) 등 비대면 강연, 모임 진행시
                      <br />
                      230인치 대형LED를 활용한 고품격 온라인 행사 진행가능
                    </h6>
                  </div>
                </CardBody>
              </Card>
            </Col>
            <Col lg='6'>
              <Card
                className='card-blog card-background'
                data-animation='zooming'
              >
                <div
                  className='full-background'
                  style={{
                    backgroundImage: `url(${imgB})`,
                  }}
                />
                <CardBody>
                  <div className='content-bottom'>
                    <CardTitle tag='h5'>
                      디지털 촬영장비를 활용한 행사
                    </CardTitle>
                    <h6 className='card-category text-white opacity-8'>
                      프리미엄 음향 콘솔, 스피커, 무선 마이크 등을 활용해 <br />
                      기업 강의, 수료식, 기자 간담회 등 오프라인 행사 진행 가능
                    </h6>
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Row className='align-items-center'>
            <Col lg='6'>
              <Card
                className='card-blog card-background'
                data-animation='zooming'
              >
                <div
                  className='full-background'
                  style={{
                    backgroundImage: `url(${imgC})`,
                  }}
                />
                <CardBody>
                  <div className='content-bottom'>
                    <CardTitle tag='h5'>
                      프리미엄 부대 시설을 활용한 모임
                    </CardTitle>
                    <h6 className='card-category text-white opacity-8'>
                      두 개의 고품격 미팅룸, 아일랜드 바 테이블, 카페테리아,
                      <br />
                      이동식 책걸상 등을 무료로 제공
                    </h6>
                  </div>
                </CardBody>
              </Card>
            </Col>
            <Col lg='6'>
              <Card
                className='card-blog card-background'
                data-animation='zooming'
              >
                <div
                  className='full-background'
                  style={{
                    backgroundImage: `url(${imgD})`,
                  }}
                />
                <CardBody>
                  <div className='content-bottom'>
                    <CardTitle tag='h5'>쾌적한 모임의 장</CardTitle>
                    <h6 className='card-category text-white opacity-8'>
                      최대수용인원 50명(책상 제외시)의 넓은 홀
                      <br />
                      뿐만 아니라, 내부 회의실까지 통째로 사용 가능
                    </h6>
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
}
