/* eslint-disable react/no-array-index-key */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useState } from 'react';
// import styled from 'styled-components';
// import PaymentPagination from './PaymentPagination';
import classnames from 'classnames';
import validator from 'validator';
import { RootState } from 'store/modules/index';

import {
  Button,
  FormGroup,
  Form,
  Input,
  Label,
  Container,
  Row,
  Col,
} from 'reactstrap';

import { fieldValues, categoryList } from 'variables/updateUserInfo';
import { axiosAuth } from 'api';
import { useSelector, useDispatch } from 'react-redux';
import * as actions from 'store/modules/modalManages';
import UpdateUserPassword from './UserInfoPasswordUpdate';
import UserInfoPasswordUpdateSuccessModal from './UserInfoPasswordUpdateSuccessModal';
import SendInquiryErrorModal from '../Community/SendInquiryErrorModal';

export default function UserInfoHeader() {
  // const { jwtToken } = useSelector((state: RootState) => state.authentication);
  const {
    username: initUsername,
    email: initEmail,
    phone: initPhone,
    company: initCompany,
    department: initDepartment,
    position: initPosition,
    category: initCategory,
    agreementEmail: initAgreementEmail,
    agreementSNS: initAgreementSNS,
  } = useSelector((state: RootState) => state.authentication.userInfo);

  // initValue Setting
  const [values, setValues] = useState({
    username: initUsername,
    email: initEmail,
    phone: initPhone,
    company: initCompany,
    department: initDepartment,
    position: initPosition,
    category: initCategory ? '' : categoryList[0],
    agreementEmail: initAgreementEmail,
    agreementSNS: initAgreementSNS,
  });

  const [usernameFocus, setUsernameFocus] = React.useState('');
  const [phoneFocus, setPhoneFocus] = React.useState('');
  const [companyFocus, setCompanyFocus] = React.useState('');
  const [departmentFocus, setDepartmentFocus] = React.useState('');
  const [positionFocus, setPositionFocus] = React.useState('');
  const [categoryFocus, setCategoryFocus] = React.useState('');

  const [usernameValid, setUsernameValid] = React.useState('');
  const [phoneValid, setPhoneValid] = React.useState('');

  const [usernameState, setUsernameState] = React.useState<string | null>(null);
  const [phoneState, setPhoneState] = React.useState<string | null>(null);

  const [submitButtonState, setSubmitButtonState] = React.useState(true);

  const dispatch = useDispatch();

  const { isPasswordUpdatePopup, isPasswordUpdateSuccessPopup } = useSelector(
    (state: RootState) => state.modalManages.myPage
  );

  const { isSendInquiryErrorPopup } = useSelector(
    (state: RootState) => state.modalManages.community
  );

  const handleInquiryErrorPopup = () => {
    dispatch(actions.setSendInquiryErrorModal(!isSendInquiryErrorPopup));
  };

  const checkValidate = (e: React.ChangeEvent<HTMLInputElement>) => {
    const success = 'has-success';
    const danger = 'has-danger';
    switch (e.target.name) {
      case 'username':
        if (
          validator.matches(e.target.value, '[ !@#$%^&*(),.?":{}|<>]') ||
          e.target.value === ''
        ) {
          setUsernameValid(danger);
          setUsernameState('invalid');
        } else {
          setUsernameValid(success);
          setUsernameState('valid');
        }
        break;
      case 'phone':
        if (!validator.isMobilePhone(e.target.value, 'ko-KR')) {
          setPhoneValid(danger);
          setPhoneState('invalid');
        } else {
          setPhoneValid(success);
          setPhoneState('valid');
        }
        break;

      default:
        break;
    }
  };

  // 변수 값 변경
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    const { name, value } = e.target;
    setValues({ ...values, [name]: value });
    checkValidate(e); // Validation Check
  };

  // Checkbox 값 변경
  const handleCheckbox = (e: React.ChangeEvent<HTMLInputElement>): void => {
    const { name, checked } = e.target;
    setValues({ ...values, [name]: checked });
  };

  const handlePopup = (): void => {
    dispatch(actions.setPasswordUpdateModal(!isPasswordUpdatePopup));
  };

  // Form Submit
  const handleSubmitForm = (e: React.FormEvent<HTMLButtonElement>): void => {
    e.preventDefault();
    async function fn() {
      const {
        username,
        email,
        phone,
        company,
        department,
        position,
        category,
        agreementEmail,
        agreementSNS,
      } = values;

      const formData = new FormData();

      formData.append('username', username);
      formData.append('email', email);
      formData.append('phone', phone);
      formData.append('company', company);
      formData.append('department', department);
      formData.append('position', position);
      formData.append('category', category);
      formData.append('marketing_contract_email', agreementEmail.toString());
      formData.append('marketing_contract_sns', agreementSNS.toString());

      try {
        // TO-DO : 리덕스 구현 - 회원정보수정
        // console.log("formdata: ", formData);
        await axiosAuth.put('/accounts/user_update/', formData);
      } catch (error) {
        if (error.response) {
          console.log('error: ', error);
        }
      }
    }
    fn();
  };

  React.useEffect(() => {
    if (usernameValid === 'has-success' && phoneValid === 'has-success') {
      setSubmitButtonState(false);
    } else {
      setSubmitButtonState(true);
    }
  }, [usernameValid, phoneValid]);

  return (
    <>
      <Container className='pt-7'>
        <Row className='justify-content-center pt-5'>
          <Col lg='10'>
            <h3 className='pb-3 font-weight-700'>회원정보수정</h3>
            <h5 className='mt-3 mb--3 text-primary font-weight-700'>
              회원정보
            </h5>
            <hr className='w-100' />
            <Form>
              <FormGroup
                className={classnames('row', { focused: usernameFocus })}
              >
                <Label className='form-control-label' htmlFor='username' md='2'>
                  성명
                </Label>
                <Col md='10' className={classnames(usernameValid)}>
                  <Input
                    minLength={2}
                    defaultValue={values.username}
                    name='username'
                    type='text'
                    autoComplete='off'
                    onFocus={() => setUsernameFocus('focused')}
                    onBlur={() => setUsernameFocus('')}
                    onChange={handleChange}
                    valid={usernameState === 'valid'}
                    invalid={usernameState === 'invalid'}
                  />
                  <div className='invalid-feedback'>올바른 성명이 아닙니다</div>
                </Col>
              </FormGroup>
              <FormGroup className='row'>
                <Label className='form-control-label' htmlFor='email' md='2'>
                  이메일
                </Label>
                <Col md='10'>
                  <Input
                    defaultValue={values.email}
                    id='email'
                    type='email'
                    plaintext
                    disabled
                  />
                </Col>
              </FormGroup>
              <FormGroup className={classnames('row', { focused: phoneFocus })}>
                <Label className='form-control-label' htmlFor='phone' md='2'>
                  휴대폰번호
                </Label>
                <Col md='10' className={classnames(phoneValid)}>
                  <Input
                    maxLength={11}
                    minLength={10}
                    defaultValue={values.phone}
                    name='phone'
                    type='text'
                    autoComplete='off'
                    onFocus={() => setPhoneFocus('focused')}
                    onBlur={() => setPhoneFocus('')}
                    onChange={handleChange}
                    valid={phoneState === 'valid'}
                    invalid={phoneState === 'invalid'}
                  />
                  <div className='invalid-feedback'>
                    올바른 휴대폰 번호가 아닙니다
                  </div>
                </Col>
              </FormGroup>
              <FormGroup className='row'>
                <Label className='form-control-label' htmlFor='password' md='2'>
                  비밀번호
                </Label>
                <Col md='10'>
                  <Button
                    type='button'
                    outline
                    color='primary'
                    onClick={() => handlePopup()}
                  >
                    비밀번호 변경
                  </Button>
                </Col>
              </FormGroup>
              <hr className='w-100' />
              <FormGroup
                className={classnames('row', { focused: companyFocus })}
              >
                <Label className='form-control-label' htmlFor='company' md='2'>
                  소속
                </Label>
                <Col md='10'>
                  <Input
                    defaultValue={values.company}
                    placeholder='소속(회사, 학교, 연구소)을 입력해주세요.'
                    name='company'
                    type='text'
                    onChange={handleChange}
                    autoComplete='off'
                    onFocus={() => setCompanyFocus('focused')}
                    onBlur={() => setCompanyFocus('')}
                  />
                </Col>
              </FormGroup>
              <FormGroup
                className={classnames('row', { focused: departmentFocus })}
              >
                <Label
                  className='form-control-label'
                  htmlFor='department'
                  md='2'
                >
                  부서
                </Label>
                <Col md='10'>
                  <Input
                    defaultValue={values.department}
                    placeholder='소속 부서를 입력해주세요'
                    name='department'
                    type='text'
                    onChange={handleChange}
                    autoComplete='off'
                    onFocus={() => setDepartmentFocus('focused')}
                    onBlur={() => setDepartmentFocus('')}
                  />
                </Col>
              </FormGroup>
              <FormGroup
                className={classnames('row', { focused: positionFocus })}
              >
                <Label className='form-control-label' htmlFor='position' md='2'>
                  직급
                </Label>
                <Col md='10'>
                  <Input
                    defaultValue={values.position}
                    placeholder='직급을 입력해주세요'
                    id='position'
                    name='position'
                    type='text'
                    onChange={handleChange}
                    autoComplete='off'
                    onFocus={() => setPositionFocus('focused')}
                    onBlur={() => setPositionFocus('')}
                  />
                </Col>
              </FormGroup>
              <FormGroup
                className={classnames('row', { focused: categoryFocus })}
              >
                <Label className='form-control-label' htmlFor='category' md='2'>
                  업종
                </Label>
                <Col md='10'>
                  <Input
                    defaultValue={values.category}
                    id='category'
                    name='category'
                    type='select'
                    onChange={handleChange}
                    onFocus={() => setCategoryFocus('focused')}
                    onBlur={() => setCategoryFocus('')}
                  >
                    {categoryList.map((category, idx) => {
                      return <option key={idx}>{category}</option>;
                    })}
                  </Input>
                </Col>
              </FormGroup>
              <hr className='w-100' />
              <FormGroup className='row mt--3'>
                <Label
                  className='form-control-label'
                  htmlFor='example-text-input'
                  md='2'
                >
                  마케팅 수신동의
                </Label>
                <Col md='10'>
                  <div className='custom-control-inline custom-checkbox mx-3 p-2'>
                    <input
                      className='custom-control-input'
                      id='agreementEmail'
                      name='agreementEmail'
                      type='checkbox'
                      checked={values.agreementEmail}
                      onChange={handleCheckbox}
                    />
                    <label
                      className='custom-control-label'
                      htmlFor='agreementEmail'
                    >
                      이메일
                    </label>
                  </div>
                  <div className='custom-control-inline custom-checkbox mx-3 p-2'>
                    <input
                      className='custom-control-input'
                      id='agreementSNS'
                      name='agreementSNS'
                      type='checkbox'
                      checked={values.agreementSNS}
                      onChange={handleCheckbox}
                    />
                    <label
                      className='custom-control-label'
                      htmlFor='agreementSNS'
                    >
                      SNS
                    </label>
                  </div>
                </Col>
              </FormGroup>
              <div className='text-center my-5'>
                <Button
                  className='px-5 text-md'
                  type='submit'
                  color='primary'
                  size='lg'
                  onClick={handleSubmitForm}
                  disabled={submitButtonState}
                >
                  수정
                </Button>
                <Button
                  className='px-5'
                  type='button'
                  color='secondary'
                  size='lg'
                >
                  취소
                </Button>
              </div>
            </Form>
          </Col>
        </Row>
      </Container>
      {isPasswordUpdatePopup && (
        <UpdateUserPassword isModalOpen={() => handlePopup()} />
      )}
      {isPasswordUpdateSuccessPopup && <UserInfoPasswordUpdateSuccessModal />}
      {isSendInquiryErrorPopup && (
        <SendInquiryErrorModal
          text='비밀번호 입력이 잘못되었습니다.'
          handleInquiryErrorPopup={() => handleInquiryErrorPopup()}
        />
      )}
    </>
  );
}
