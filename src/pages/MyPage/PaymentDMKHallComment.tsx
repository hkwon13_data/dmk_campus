/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useState } from 'react';
import ReactStars from 'react-rating-stars-component';
import {
  Container,
  Row,
  Col,
  Button,
  Modal,
  FormGroup,
  Form,
  Input,
} from 'reactstrap';

interface CommentDataPropsType {
  data: boolean;
  isModalOpen: React.Dispatch<React.SetStateAction<boolean>>;
}

export default function PaymentDMKHallComment(props: CommentDataPropsType) {
  const { data, isModalOpen } = props;

  const [modalOpen, setModalOpen] = useState(data);
  // const [btnSubmit, setBtnSubmit] = useState(false);
  const [textAreaFocus, setTextAreaFocus] = useState('');
  const [starRating, setStarRating] = useState(0);

  const ratingChanged = (newRating: number) => {
    setStarRating(newRating);
  };

  const [values, setValues] = useState({
    textArea: '',
  });

  // const { textArea } = values;

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setValues({ ...values, [name]: value });
  };

  React.useEffect(() => {
    if (!modalOpen) isModalOpen(false);
  }, [modalOpen, props]);

  // const onSubmit = (e: any) => {
  //   e.preventDefault();
  //   const data = {
  //     textArea,
  //     starRating,
  //   };
  // };

  return (
    <>
      <Container>
        <Row>
          <Col md='4' sm='4' xs='4'>
            <Modal
              isOpen={modalOpen}
              toggle={() => setModalOpen(!modalOpen)}
              className='modal-md mt-9'
              modalClassName=' bd-example-modal-sm'
            >
              <div className='modal-header'>
                <div
                  className='modal-title font-weight-700 m-0'
                  id='exampleModalScrollableTitle'
                >
                  <div className='mb-3 px-3'>
                    <div className='h4 font-weight-900 m-0 mb-3'>대관 날짜</div>
                    <div className='h6 font-weight-900 m-0'>
                      2021년 2월 23일(목), 09:00-11:00, 총 2시간
                    </div>
                  </div>
                </div>
                <button
                  aria-label='Close'
                  className='close'
                  onClick={() => setModalOpen(!modalOpen)}
                  type='button'
                >
                  <span className='text-white font-weight-bold' aria-hidden>
                    ×
                  </span>
                </button>
              </div>
              <div className='modal-body bg-secondary'>
                <Row className=''>
                  <div className='mt-3 mb-0 pl-3 h5'>데마코홀은 어땠나요?</div>
                </Row>
                <Row className='justify-content-center mb-3'>
                  <ReactStars
                    count={5}
                    onChange={ratingChanged}
                    size={48}
                    isHalf
                    emptyIcon={<i className='far fa-star' />}
                    halfIcon={<i className='fa fa-star-half-alt' />}
                    fullIcon={<i className='fa fa-star' />}
                    activeColor='#ff5a45'
                  />
                </Row>
                <Row>
                  <Col md='12'>
                    <Form id='contact-form-1' method='post' role='form'>
                      <FormGroup className={textAreaFocus}>
                        <label className='h5 mb-1'>대관후기</label>
                        <Input
                          id='contact-us-message-1'
                          name='textArea'
                          rows='4'
                          type='textarea'
                          value={values.textArea}
                          onChange={handleChange}
                          onFocus={() => setTextAreaFocus('focused')}
                          onBlur={() => setTextAreaFocus('')}
                        />
                      </FormGroup>
                    </Form>
                  </Col>
                </Row>
                <div className='text-center'>
                  <Button
                    color='secondary'
                    onClick={() => setModalOpen(!modalOpen)}
                    type='button'
                  >
                    취소
                  </Button>
                  <Button color='primary' type='button'>
                    저장 하기
                  </Button>
                </div>
              </div>
            </Modal>
          </Col>
        </Row>
      </Container>
    </>
  );
}
