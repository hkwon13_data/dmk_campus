import React from 'react';
// import ReactStars from 'react-rating-stars-component';
import StarRatings from 'react-rating-stars-component';
import * as types from 'types/types';
import { Row, Col, Button } from 'reactstrap';

interface PaymentInfoPropsTypes {
  pagingData: {
    pagesCount: number;
    currentPage: number;
    pageSize: number;
  };
  data: types.MyPageInitial[];
  modalOpen: boolean;
  setModalOpen: React.Dispatch<React.SetStateAction<boolean>>;
}

function PaymentInfo({
  pagingData,
  data,
  modalOpen,
  setModalOpen,
}: PaymentInfoPropsTypes) {
  const { pageSize, currentPage } = pagingData;

  // const [starRating, setStarRating] = useState(0);

  // const ratingChanged = (newRating) => {
  //   setStarRating(newRating);
  // };
  return (
    <>
      {data
        .slice(currentPage * pageSize, (currentPage + 1) * pageSize)
        .map((item) => {
          return (
            <tr key={item.id}>
              <th
                className='text-center'
                scope='row'
                style={{ verticalAlign: 'middle' }}
              >
                {item.id}
              </th>
              <td className='pb-0' style={{ verticalAlign: 'middle' }}>
                {/* 대관정보 {`${item.year}년 ${item.month}월 ${item.day}일 ${item.week}요일 `}
                                <br />
                                결제정보 {`${item.reserveHour[0]}-${item.reserveHour[item.reserveHour.length-1]}`}시,{" "}
                                <span className="text-warning font-weight-700">
                                    {" "}
                                    총 {item.reserveTime}시간
                                </span>{" "}
                                <br /> 대관후기 {item.people}명 */}
                <Row className='align-items-center h6 pb-1'>
                  <Col
                    md='2'
                    className='font-weight-700'
                    style={{ color: '#7c8a99' }}
                  >
                    대관정보
                  </Col>
                  <Col
                    md='3'
                    className='font-weight-900'
                    style={{ color: '#1a1a1c' }}
                  >
                    {`총 ${item.reserveTime}시간`}
                  </Col>
                  <Col md='6'>
                    {`${item.year}년 ${item.month}월 ${item.day}일 (${item.week}) `}{' '}
                    {`${item.reserveHour[0]} : 00 ~ ${
                      item.reserveHour[item.reserveHour.length - 1]
                    } : 00`}
                    , {item.people}명
                  </Col>
                </Row>
                <Row className='align-items-center h6'>
                  <Col
                    md='2'
                    className='font-weight-700'
                    style={{ color: '#7c8a99' }}
                  >
                    결제정보
                  </Col>
                  <Col
                    md='3'
                    className='font-weight-900'
                    style={{ color: '#1a1a1c' }}
                  >
                    {item.lastPrice
                      .toString()
                      .replace(/\B(?=(\d{3})+(?!\d))/g, ',')}{' '}
                    원
                  </Col>
                  <Col md='6'>{`2021년 4월 13일 (목), ${
                    item.payMethod === 'card' ? '신용카드' : '실시간 계좌이체'
                  }`}</Col>
                </Row>
                <Row className='align-items-center h6'>
                  <Col
                    md='2'
                    className='font-weight-700'
                    style={{ color: '#7c8a99' }}
                  >
                    대관후기
                  </Col>
                  <Col md='3' style={{ pointerEvents: 'none' }}>
                    {/* <ReactStars
                      value={item.rating}
                      count={5}
                      size={24}
                      isHalf={true}
                      emptyIcon={<i className="far fa-star"></i>}
                      halfIcon={<i className="fa fa-star-half-alt"></i>}
                      fullIcon={<i className="fa fa-star"></i>}
                      activeColor="#ff5a45"
                    /> */}
                    <StarRatings
                      rating={item.rating}
                      starRatedColor='red'
                      numberOfStars={5}
                      name='rating'
                      starDimension='25px'
                      starSpacing='0px'
                    />
                  </Col>
                  <Col md='6' className='text-truncate'>
                    {item.hallComment === ''
                      ? '작성한 후기가 없습니다'
                      : item.hallComment}
                  </Col>
                </Row>
              </td>
              <td className='pt-2 pb-4' style={{ verticalAlign: 'middle' }}>
                <div
                  className='align-items-center'
                  style={{ flexDirection: 'column' }}
                >
                  <div className='my-3'>
                    <Button
                      outline
                      type='button'
                      color='primary'
                      style={{ width: '-webkit-fill-available' }}
                    >
                      영수증 발행
                    </Button>
                  </div>
                  <div>
                    {item.hallComment === '' ? (
                      <Button
                        outline
                        className='w-100'
                        type='button'
                        color='primary'
                        style={{ width: '-webkit-fill-available' }}
                        onClick={() => setModalOpen(!modalOpen)}
                      >
                        대관 후기 작성
                      </Button>
                    ) : (
                      <Button
                        outline
                        className='w-100'
                        type='button'
                        color='primary'
                        style={{ width: '-webkit-fill-available' }}
                        onClick={() => setModalOpen(!modalOpen)}
                      >
                        수정하기
                      </Button>
                    )}
                  </div>
                </div>
              </td>
            </tr>
          );
        })}
    </>
  );
}

export default PaymentInfo;
