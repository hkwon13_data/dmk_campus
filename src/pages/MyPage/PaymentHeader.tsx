/* eslint-disable consistent-return */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useState } from 'react';
import { Container, Table, Spinner } from 'reactstrap';
import { RootState } from 'store/modules/index';
// import PaymentPagination from './PaymentPagination';
import CommonPagination from 'components/utils/Pagination';
import { useSelector } from 'react-redux';
import PaymentDMKHallComment from './PaymentDMKHallComment';
import PaymentInfo from './PaymentInfo';

// import { getPaymentHeader } from 'store/modules/myPage';

interface PagingDataTypes {
  pagesCount: number;
  currentPage: number;
  pageSize: number;
}

export default function PaymentHeader() {
  const { data, loading, error } = useSelector(
    (state: RootState) => state.myPage.paymentHeader
  );

  const [pageSize, setPageSize] = useState(3);
  const dataLength = !data ? 1 : data.length;
  const pages = Math.ceil(dataLength / pageSize);
  const [pagesCount, setPageCount] = useState(pages);
  const [currentPage, setCurrentPage] = useState(0);
  const pagingData: PagingDataTypes = {
    pagesCount,
    currentPage,
    pageSize,
  };

  const [modalOpen, setModalOpen] = useState(false);

  React.useEffect(() => {
    if (dataLength > 3) {
      setPageCount(pages);
    }
  }, [dataLength, pages]);

  const handlePage = (e: number) => {
    setCurrentPage(e);
  };

  const PostListContainer = () => {
    if (loading)
      return (
        <>
          <Spinner style={{ width: '3rem', height: '3rem' }} />
        </>
      );
    if (error) return <>에러 발생!</>;
    if (data)
      return (
        <PaymentInfo
          pagingData={pagingData}
          data={data}
          modalOpen={modalOpen}
          setModalOpen={setModalOpen}
        />
      );
  };

  return (
    <>
      <Container className='container-xxl'>
        <div className='mt-lg mb-5 h2 font-weight-900'>결제 이력</div>
        <div className='table p-5' style={{ background: '#ffffff' }}>
          {/* <Card>
						<CardBody> */}
          <p className='h3 font-weight-900'>대관이력</p>
          <Table responsive hover style={{ background: '#ffffff' }}>
            <thead className='text-primary text-center'>
              <tr>
                <th className='text-lg' style={{ width: '12%' }}>
                  <div className='h6 mb-0'>No.</div>
                </th>
                <th>
                  <div className='h6 mb-0'>대관이력</div>
                </th>
                {/* <th>결제정보</th> */}
                <th>
                  <div className='h6 mb-0'>영수증 및 후기</div>
                </th>
              </tr>
            </thead>
            <tbody>
              {/* <PaymentInfo
                pagingData={pagingData}
                data={data.data}
                modalOpen={modalOpen}
                setModalOpen={setModalOpen}
              /> */}
              {PostListContainer()}
            </tbody>
          </Table>
          {/* </CardBody>
					</Card> */}
        </div>
        {data && (
          <CommonPagination
            pagingData={pagingData}
            handlePage={(e: number) => handlePage(e)}
          />
        )}
        {modalOpen && (
          <PaymentDMKHallComment data={modalOpen} isModalOpen={setModalOpen} />
        )}
      </Container>
    </>
  );
}
