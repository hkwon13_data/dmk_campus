import React from 'react';
// import { Container, Row, Button } from 'reactstrap';

import CommonNavbar from 'components/NavBar/CommonNavbar';
import CommonFooter from 'components/NavBar/CommonFooter';
import { ScrollTopButton } from 'components/utils/ScrollTopButton';

import UserInfoHeader from './UserInfoHeader';
import UserCheck from './UserCheck';

export default function UserInfoPage() {
  const [userCheck, setUserCheck] = React.useState<boolean>(false);
  return (
    <>
      <ScrollTopButton />
      <CommonNavbar type='primary' />
      {!userCheck ? (
        <UserCheck setUserCheck={(e: boolean) => setUserCheck(e)} />
      ) : (
        <UserInfoHeader />
      )}
      <CommonFooter />
    </>
  );
}
