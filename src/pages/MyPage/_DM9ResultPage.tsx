import React from 'react';
import { Container } from 'reactstrap';

import CommonNavbar from 'components/NavBar/CommonNavbar';
import CommonFooter from 'components/NavBar/CommonFooter';
import { ScrollTopButton } from 'components/utils/ScrollTopButton';

export default function DM9ResultPage() {
  return (
    <>
      <ScrollTopButton />
      <CommonNavbar type='primary' />
      <div className='section section-typography'>
        <Container>
          <h2 className='mt-lg mb-5'>DM9 Result Content Areas</h2>
        </Container>
      </div>
      <CommonFooter />
    </>
  );
}
