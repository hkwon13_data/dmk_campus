import React from 'react';
import { Container } from 'reactstrap';

import CommonNavbar from 'components/NavBar/CommonNavbar';
import CommonFooter from 'components/NavBar/CommonFooter';
import { ScrollTopButton } from 'components/utils/ScrollTopButton';

import { getPaymentHeader } from 'store/modules/myPage';
import { useDispatch } from 'react-redux';

import PaymentHeader from './PaymentHeader';

export default function PaymentHistPage() {
  const dispatch = useDispatch();
  React.useEffect(() => {
    dispatch(getPaymentHeader(undefined));
  }, [dispatch]);
  return (
    <>
      <ScrollTopButton />
      <CommonNavbar type='primary' />
      <div
        className='section section-typography'
        style={{ background: '#f9fafb' }}
      >
        <Container>
          <PaymentHeader />
        </Container>
      </div>
      <CommonFooter />
    </>
  );
}
