import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from 'store/modules/index';
import * as actions from 'store/modules/modalManages';
// reactstrap components
import { Button, Modal, Row } from 'reactstrap';

// Core Components

function UserInfoPasswordUpdateSuccessModal() {
  //   const [modalOpen, handleSignUpSuccessModal] = React.useState(false);

  const dispatch = useDispatch();

  const { isPasswordUpdateSuccessPopup } = useSelector(
    (state: RootState) => state.modalManages.myPage
  );

  return (
    <>
      <Modal
        isOpen={isPasswordUpdateSuccessPopup}
        // toggle={() => handleSignUpSuccessModal(!isOpenSignUpSuccess)}
        className='modal-dialog-centered modal-md'
      >
        <div className='modal-body'>
          <div className='font-weight-700 text-center h3 mt-3'>
            <i className='ni ni-bell-55' />
          </div>
          <div className='text-center mb-4 h5'>
            비밀번호 변경에 성공하였습니다.
          </div>
          <Row className='justify-content-center'>
            <div>
              <Button
                className='btn px-4'
                color='primary'
                onClick={() =>
                  dispatch(
                    actions.setPasswordUpdateSuccessModal(
                      !isPasswordUpdateSuccessPopup
                    )
                  )
                }
              >
                확인
              </Button>
            </div>
          </Row>
        </div>
      </Modal>
    </>
  );
}

export default UserInfoPasswordUpdateSuccessModal;
