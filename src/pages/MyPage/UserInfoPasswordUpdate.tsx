/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useState } from 'react';
import { RootState } from 'store/modules/index';
import classnames from 'classnames';
import validator from 'validator';
import { axiosInstance, axiosAuth } from 'api';
// import { useAppContext } from 'store';
import { getStorageItem } from 'utils/useLocalStorage';
import * as actions from 'store/modules/modalManages';
// reactstrap components
import {
  Button,
  Modal,
  Form,
  FormGroup,
  Label,
  Col,
  Input,
  Spinner,
} from 'reactstrap';
import { validatorPasswordOption as patternNewPassword } from 'utils/regex';
import { useSelector, useDispatch } from 'react-redux';

const PasswordFont = {
  fontFamily: 'sans-serif',
};

interface UpdateUserPasswordPropsTypes {
  isModalOpen: () => void;
}

// Core Components
export default function UpdateUserPassword(
  props: UpdateUserPasswordPropsTypes
) {
  const { isModalOpen } = props;
  const dispatch = useDispatch();
  const jwtToken = getStorageItem('jwtToken');
  const headers = { Authorization: `Bearer ${jwtToken}` };

  const { isPasswordUpdatePopup, isPasswordUpdateSuccessPopup } = useSelector(
    (state: RootState) => state.modalManages.myPage
  );

  const { isSendInquiryErrorPopup } = useSelector(
    (state: RootState) => state.modalManages.community
  );

  const handlePasswordUpdateSuccessPopup = () => {
    dispatch(
      actions.setPasswordUpdateSuccessModal(!isPasswordUpdateSuccessPopup)
    );
  };

  const [values, setValues] = useState({
    password: '',
    newPassword: '',
    newRePassword: '',
  });

  const [isLoading, setIsLoading] = useState(false);

  const [passwordFocus, setPasswordFocus] = useState('');
  const [newPasswordFocus, setNewPasswordFocus] = useState('');
  const [newRePasswordFocus, setNewRePasswordFocus] = useState('');

  const [passwordValid, setPasswordValid] = React.useState('');
  const [newPasswordValid, setNewPasswordValid] = React.useState('');
  const [newRePasswordValid, setNewRePasswordValid] = React.useState('');

  const [passwordState, setPasswordState] = React.useState<string | null>(null);
  const [newPasswordState, setNewPasswordState] = React.useState<string | null>(
    null
  );
  const [newRePasswordState, setNewRePasswordState] = React.useState<
    string | null
  >(null);

  const [submitButtonState, setSubmitButtonState] = React.useState(true);

  const checkValidate = (e: React.ChangeEvent<HTMLInputElement>) => {
    const success = 'has-success';
    const danger = 'has-danger';
    const valid = 'valid';
    const invalid = 'invalid';

    switch (e.target.name) {
      case 'password':
        // if (
        // 	validator.matches(e.target.value, '[ !@#$%^&*(),.?":{}|<>]') ||
        // 	e.target.value === ""
        // ) {
        // 	setPasswordValid(danger);
        // 	setPasswordState("invalid");
        // } else {
        // 	setPasswordValid(success);
        // 	setPasswordState("valid");
        // }
        break;

      case 'newPassword':
        if (!validator.isStrongPassword(e.target.value, patternNewPassword)) {
          setNewPasswordValid(danger);
          setNewPasswordState(invalid);
        } else {
          setNewPasswordValid(success);
          setNewPasswordState(valid);
        }
        break;

      case 'newRePassword':
        if (
          !validator.equals(e.target.value, values.newPassword) ||
          validator.isEmpty(e.target.value)
        ) {
          setNewRePasswordValid(danger);
          setNewRePasswordState(invalid);
        } else {
          setNewRePasswordValid(success);
          setNewRePasswordState(valid);
        }
        break;

      default:
        break;
    }
  };

  // 변수 값 변경
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setValues({ ...values, [name]: value });
    checkValidate(e); // Validation Chec/
  };

  // const [modalOpen, setModalOpen] = React.useState(props.data.data);

  // React.useEffect(() => {
  //   if (!modalOpen) props.data.isModalOpen(false);
  // }, [modalOpen, props.data]);

  React.useEffect(() => {
    if (
      newPasswordValid === 'has-success' &&
      newRePasswordValid === 'has-success'
    ) {
      setSubmitButtonState(false);
    } else {
      setSubmitButtonState(true);
    }
  }, [passwordValid, newPasswordValid, newRePasswordValid]);

  const submitPasswordChange = () => {
    const fn = async () => {
      const formData = new FormData();
      formData.append('old_password', values.password);
      formData.append('new_password', values.newPassword);
      try {
        setIsLoading(true);
        const response = await axiosInstance.put(
          'accounts/password/change/',
          formData,
          { headers }
        );
        handlePasswordUpdateSuccessPopup();
        // isModalOpen();
        // alert("비밀번호가 성공적으로 변경되었습니다.");
      } catch (error) {
        // return alert("비밀번호 변경이 실패했습니다.", error.response);
        // console.log("error: ", error);
        dispatch(actions.setSendInquiryErrorModal(!isSendInquiryErrorPopup));
        setIsLoading(false);
      }
    };
    fn();
  };

  return (
    <>
      <Modal
        isOpen={isPasswordUpdatePopup}
        toggle={() => isModalOpen()}
        className='modal-dialog-centered container-sm'
      >
        <div className='modal-header'>
          <h5
            className='modal-title text-muted font-weight-700'
            id='modal-title-default'
          >
            비밀번호 변경
          </h5>
          <button
            aria-label='Close'
            className='close'
            onClick={() => isModalOpen()}
            type='button'
          >
            <span aria-hidden>×</span>
          </button>
        </div>
        <div className='modal-body'>
          <Form>
            <FormGroup
              className={classnames('row', { focused: passwordFocus })}
            >
              <Label className='form-control-label' htmlFor='password' md='4'>
                기존 비밀번호
              </Label>
              <Col
                md='8'
                className={classnames('pl-1', passwordValid)}
                style={PasswordFont}
              >
                <Input
                  className='form-control-md'
                  minLength={8}
                  name='password'
                  type='password'
                  autoComplete='off'
                  placeholder='기존 비밀번호를 입력해주세요'
                  onFocus={() => setPasswordFocus('focused')}
                  onBlur={() => setPasswordFocus('')}
                  onChange={handleChange}
                  valid={passwordState === 'valid'}
                  invalid={passwordState === 'invalid'}
                />
                <div className='invalid-feedback'>기존 비밀번호가 틀립니다</div>
              </Col>
            </FormGroup>
            <FormGroup
              className={classnames('row', { focused: newPasswordFocus })}
            >
              <Label
                className='form-control-label'
                htmlFor='newPassword'
                md='4'
              >
                새 비밀번호
              </Label>
              <Col
                md='8'
                className={classnames('pl-1', newPasswordValid)}
                style={PasswordFont}
              >
                <Input
                  name='newPassword'
                  type='password'
                  autoComplete='off'
                  placeholder='영문, 숫자, 특수문자 모두 포함한 8자 입력해주세요'
                  onFocus={() => setNewPasswordFocus('focused')}
                  onBlur={() => setNewPasswordFocus('')}
                  onChange={handleChange}
                  valid={newPasswordState === 'valid'}
                  invalid={newPasswordState === 'invalid'}
                />
                <div className='invalid-feedback'>
                  8자 이상의 영문, 숫자, 특수문자를 조합해주세요
                </div>
              </Col>
            </FormGroup>
            <FormGroup
              className={classnames('row', { focused: newRePasswordFocus })}
            >
              <Label
                className='form-control-label'
                htmlFor='newRePasswordFocus'
                md='4'
              >
                새 비밀번호 확인
              </Label>
              <Col
                md='8'
                className={classnames('pl-1', newRePasswordValid)}
                style={PasswordFont}
              >
                <Input
                  defaultValue=''
                  name='newRePassword'
                  type='password'
                  autoComplete='off'
                  placeholder='새로운 비밀번호를 한번 더 입력해주세요'
                  onFocus={() => setNewRePasswordFocus('focused')}
                  onBlur={() => setNewRePasswordFocus('')}
                  onChange={handleChange}
                  valid={newRePasswordState === 'valid'}
                  invalid={newRePasswordState === 'invalid'}
                />
                <div className='invalid-feedback'>
                  비밀번호가 일치하지 않습니다.
                </div>
              </Col>
            </FormGroup>
          </Form>
        </div>
        <div className='modal-footer justify-content-center'>
          {isLoading ? (
            <Spinner />
          ) : (
            <Button
              color='primary'
              type='submit'
              outline
              disabled={submitButtonState}
              onClick={() => submitPasswordChange()}
            >
              비밀번호 변경하기
            </Button>
          )}
        </div>
      </Modal>
    </>
  );
}
