import React, { useState } from 'react';
import { RootState } from 'store/modules/index';
import { useHistory } from 'react-router-dom';
import { axiosAuth } from 'api';
import { notification } from 'antd';

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  Col,
} from 'reactstrap';

// Core Components

// 임시
import { useSelector, useDispatch } from 'react-redux';
import * as modalActions from 'store/modules/modalManages';
// import * as actions from 'store/modules/authentication';
import { getStorageItem } from 'utils/useLocalStorage';
import SearchPasswordPopup from '../../components/Auth/SearchPasswordPopup';

const PasswordFont = {
  fontFamily: 'sans-serif',
};

interface UserCheckPropsTypes {
  setUserCheck: (value: boolean) => void;
}

export default function UserCheck(props: UserCheckPropsTypes) {
  const { setUserCheck } = props;
  const history = useHistory();
  const dispatch = useDispatch();
  // const jwtToken = useSelector((state) => state.authentication.jwtToken);
  const [password, setPassword] = useState('');
  const jwtToken = getStorageItem('jwtToken');
  const headers = { Authorization: `Bearer ${jwtToken}` };

  const [passwordFocus, setPasswordFocus] = useState<string>('');

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.name === 'password') {
      setPassword(e.target.value);
    }
  };

  // 임시
  const { isSearchPasswordPopup } = useSelector(
    (state: RootState) => state.modalManages.authentication
  );
  const { email: userEmail } = useSelector(
    (state: RootState) => state.authentication.userInfo
  );
  const handleSearchPasswordPopup = () => {
    dispatch(modalActions.setSearchPasswordPopupModal(!isSearchPasswordPopup));
  };
  // 임시

  const handleSubmitForm = (event: React.FormEvent<HTMLButtonElement>) => {
    event.preventDefault();
    async function fn() {
      const formData = new FormData();
      formData.append('email', userEmail);
      formData.append('password', password);
      try {
        await axiosAuth.post('/accounts/user_check/', formData, {
          headers,
        });
        notification.open({
          message: '유저확인 성공',
          description: '회원정보 페이지로 이동합니다',
        });

        setUserCheck(true);
        history.push('/mypage/userinfo');
      } catch (error) {
        if (error.response) {
          notification.open({
            message: '유저확인 실패',
            description: '아이디/암호를 확인해주세요.',
          });
        }
      }
    }
    fn();
  };

  return (
    <>
      <Container className='pt-8'>
        <Col className='mx-auto ' lg='5' md='8'>
          <Card className='bg-secondary shadow border-0'>
            <CardHeader>
              <CardTitle className='text-center' tag='h4'>
                이메일/비밀번호 재확인
                <div className='text-center my-4'>
                  <small>
                    정보를 안전하게 보호하기 위해 <br /> 이메일과 비밀번호를
                    다시 한번 확인합니다.
                  </small>
                </div>
              </CardTitle>
            </CardHeader>
            <CardBody className='px-lg-5 py-lg-5 mt-lg-5'>
              <Form role='form'>
                <FormGroup className={passwordFocus}>
                  <InputGroup className='input-group-alternative'>
                    <InputGroupAddon addonType='prepend'>
                      <InputGroupText>
                        <i className='ni ni-lock-circle-open' />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input
                      style={PasswordFont}
                      placeholder='Password'
                      type='password'
                      name='password'
                      onChange={handleChange}
                      onFocus={() => setPasswordFocus('focused')}
                      onBlur={() => setPasswordFocus('')}
                    />
                  </InputGroup>
                </FormGroup>
                <div
                  className='text-right text-underline'
                  style={{ cursor: 'pointer' }}
                  onClick={() => handleSearchPasswordPopup()}
                  onKeyDown={() => {}}
                  role='button'
                  tabIndex={0}
                >
                  비밀번호 찾기
                </div>

                <div className='text-center'>
                  <Button
                    className='mt-4'
                    color='primary'
                    onClick={handleSubmitForm}
                  >
                    확인
                  </Button>
                </div>
              </Form>
            </CardBody>
          </Card>
        </Col>
        {isSearchPasswordPopup && (
          <SearchPasswordPopup
            handleSearchPasswordPopup={() => handleSearchPasswordPopup()}
          />
        )}
      </Container>
    </>
  );
}
