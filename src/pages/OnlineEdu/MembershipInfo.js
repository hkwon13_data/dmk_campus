import React from 'react'

import {
    Row,
	Col,
	Container,
	Button,
    Card,
    CardBody,
    CardTitle,
} from "reactstrap";

function MembershipInfo() {
    return (
        <div>
            <Container className="container-xl">
                <Card className="p-4">
                    <CardBody className="pb-0">
                        <Container className="container-md">
                            <Row>
                                <div className="h5 font-weight-900 mt-5">안내사항</div>
                                <div className="h5 mb-6">
                                    1. 본 세미나는 인공지능 마케팅 솔루션 <span className="font-weight-900">마대리 회원이라면 누구나 무료로</span> 신청할 수 있습니다.<br/>
                                    2. 아직 회원이 아니시라면, 마대리 홈페이지에서 회원가입을 한 후 신청해 주세요.<br/>
                                    3. 세미나 당일 이메일로 교육을 시청하실 수 있는 링크를 보내드립니다.<br/>
                                    4. 세미나는 유튜브 라이브로 진행되며 <span className="font-weight-900">녹화본은 제공되지 않습니다.</span><br/>
                                    5. 불법적인 녹화, 배포, 대여 시 저작권법에 따라 처벌대상이 될 수 있습니다.
                                </div>
                                <div className="h5 font-weight-900">문의처</div>
                                <div className="h5 mb-6">데이터마케팅코리아 마대리전략팀(02-6011-5409, maderi@datamarketing.co.kr)</div>
                            </Row>
                        </Container>
                    </CardBody>
                </Card>
            </Container>
        </div>
    )
}

export default MembershipInfo
