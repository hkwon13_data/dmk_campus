import React, { Component } from "react";
import { Container, Row, Button } from "reactstrap";

import CommonNavbar from "components/NavBar/CommonNavbar";
import CommonFooter from "components/NavBar/CommonFooter";
import FloatingButton from "../../components/NavBar/FloatingButton"
import { ScrollTopButton } from "components/utils/ScrollTopButton";

import MembershipHeader from "./MembershipHeader";
import MembershipTab from "./MembershipTab"

export default function MMCPage(props) {

	const selectedTabs = props.match.path;

	return (
		<>
			<ScrollTopButton />
			<CommonNavbar type="transparent" />
			<FloatingButton />
			<div className="wrapper">
				<MembershipHeader />
				<MembershipTab tabs = {selectedTabs}/>
			</div>
			<CommonFooter />
		</>
	);
}
