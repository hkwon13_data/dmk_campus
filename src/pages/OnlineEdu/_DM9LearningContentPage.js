import React, { Component } from "react";
import { Container, Row, Button } from "reactstrap";

import CommonNavbar from "components/NavBar/CommonNavbar";
import CommonFooter from "components/NavBar/CommonFooter";
import { ScrollTopButton } from "components/utils/ScrollTopButton";

export default function DM9LCPage() {
	return (
		<>
			<ScrollTopButton />
			<CommonNavbar type="primary" />
			<div className="section section-content-areas">
				<Container>
					<h2 className="mt-lg mb-5">
						<span>DM9 Learning Contents Areas</span>
					</h2>
				</Container>
			</div>
			<CommonFooter />
		</>
	);
}
