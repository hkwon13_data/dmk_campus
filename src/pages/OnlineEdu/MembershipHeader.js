import React from 'react'

// reactstrap components
import { Container, Row, Col, Badge, CardBody, CardTitle, Button } from "reactstrap";

// Core Components

import imgA from "assets/img/online_mamemclub/mamemclubHeader.png";

function MembershipHeader() {
    return (
		<>
			<div className="page-header header-filter">
				<div
					className="page-header-image"
					style={{
						backgroundImage: "url(" + imgA + ")",
					}}
				/>
				<Container>
					<Row>
						<Col className="mx-auto" lg="10">
							<CardBody className="mb-4 text-center">
								<div className="content-bottom">
									<CardTitle
										tag="h1"
										className="text-white font-weight-700">
										마대리 멤버십 클럽
									</CardTitle>
                                    <CardTitle
										tag="h1"
										className="text-white font-weight-700 mb-5">
										Ma:deri Membership Club
									</CardTitle>
									<div className="h7 text-white my-3">
										빠르게 진행되는 디지털 트랜스포메이션. 빠르게 변화하는 소비자들의 라이프스타일. 해결되지 않은
                                        마케터들의 오랜 고민과 어려움을 데이터로 해결해드릴 수 있도록 현업 업계 최고의 데이터마케터를 모셨습니다. 본 마대리 멤버십 클럽에서는 발전하는 데이터마케팅 산업을 주제로 최고의
                                        인사이트를 공유합니다. 데이터마케팅코리아 대표와 현업 업계 최고의 마케팅 전문가를 초청하여
                                        다양한 분야의 마케팅 트랜드와 인사이트를 제공합니다.
									</div>
								</div>
							</CardBody>
						</Col>
					</Row>
				</Container>
			</div>
		</>
	);
}

export default MembershipHeader
