import React from 'react'
import { Link, useHistory, useLocation } from "react-router-dom";

// reactstrap components
import {
    Row,
	Col,
	Container,
	Button,
	TabPane,
	TabContent,
	Nav,
	NavItem,
	NavLink,
    Card
} from "reactstrap";

import MembershipProcess from "./MembershipProcess";
import MembershipLineUp from "./MembershipLineUp"
import MembershipInfo from "./MembershipInfo"

function MembershipTab(props) {

    const history = useHistory();
	const handleClick = (path) => {
		history.push(path);
	};

	const selectedTabs = props.tabs;

    return (
        <Container className="container-lg">
            <div className="nav-wrapper">
                <Nav className="nav-fill flex-row" pills role="tablist">
                    <NavItem>
                        <NavLink
                            className={
                                "mb-sm-0 mb-md-0 " +
                                (selectedTabs === "/onlineEdu/mmc" ? "active" : "")
                            }
                            href="#pablo"
                            onClick={(e) => {
                                e.preventDefault();
                                handleClick("/onlineEdu/mmc");
                            }}>
                            진행방식
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink
                            className={
                                "mb-sm-0 mb-md-0 " +
                                (selectedTabs === "/onlineEdu/mmc/lineUp" ? "active" : "")
                            }
                            href="#pablo"
                            onClick={(e) => {
                                e.preventDefault();
                                handleClick("/onlineEdu/mmc/lineUp");
                            }}>
                            라인업
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink
                            className={
                                "mb-sm-0 mb-md-0 " +
                                (selectedTabs === "/onlineEdu/mmc/info" ? "active" : "")
                            }
                            href="#pablo"
                            onClick={(e) => {
                                e.preventDefault();
                                handleClick("/onlineEdu/mmc/info");
                            }}>
                            안내사항
                        </NavLink>
                    </NavItem>
                </Nav>
            </div>
            <TabContent id="myTabContent" activeTab={selectedTabs}>
                    {selectedTabs === "/onlineEdu/mmc" && (
                    <TabPane tabId="/onlineEdu/mmc" role="tabpanel">
                        <MembershipProcess />
                    </TabPane>
					)}
					{selectedTabs === "/onlineEdu/mmc/lineUp" && (
						<TabPane tabId="/onlineEdu/mmc/lineUp" role="tabpanel">
							<MembershipLineUp />
						</TabPane>
					)}
					{selectedTabs === "/onlineEdu/mmc/info" && (
						<TabPane tabId="/onlineEdu/mmc/info" role="tabpanel">
							<MembershipInfo />
						</TabPane>
					)}
            </TabContent>
            <Card className="px-4 py-3" style={{position : 'sticky', bottom : '30px', zIndex : '10000',}}>
                    <Row className="align-items-center">
                        <Col>
                            <div className="h4 font-weight-700"> 마케터가 데이터를 활용하는 법</div>
                            <div className="h5">3월 16일 화요일</div>
                        </Col>
                        <Col className="text-right mr-4">
                            <Button
                                className="btn"
                                color="primary"
                                onClick={() => console.log("Clicked Button")}>
                                <span className="h4 font-weight-bold text-white">마대리 멤버십 클럽 신청하기</span>
                            </Button>
                        </Col>
                    </Row>
            </Card>
        </Container>
    )
}

export default MembershipTab
