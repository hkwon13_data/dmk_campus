import React from "react";

// reactstrap components
import {
	Row,
	Col,
	Container,
	Button,
	Card,
	CardBody,
	CardTitle,
	CardImg,
} from "reactstrap";

import ImgA from "assets/img/online_mamemclub/mmc_lineup_img.png";
import Item from "antd/lib/list/Item";

// const style = { width: "18rem" };
import styled from "styled-components";

function MembershipLineUp() {
	const data = [
		{
			img: ImgA,
			name: "허원석",
			subName: "MLB코리아 이사",
			text: "데이터와 패션이\n 만나면 생기는 일",
			month: "3월",
		},
		{
			img: ImgA,
			name: "허원석",
			subName: "MLB코리아 이사",
			text: "데이터와 패션이\n 만나면 생기는 일",
			month: "3월",
		},
		{
			img: ImgA,
			name: "허원석",
			subName: "MLB코리아 이사",
			text: "데이터와 패션이\n 만나면 생기는 일",
			month: "3월",
		},
		{
			img: ImgA,
			name: "허원석",
			subName: "MLB코리아 이사",
			text: "데이터와 패션이\n 만나면 생기는 일",
			month: "3월",
		},
		{
			img: ImgA,
			name: "허원석",
			subName: "MLB코리아 이사",
			text: "데이터와 패션이\n 만나면 생기는 일",
			month: "3월",
		},
		{
			img: ImgA,
			name: "허원석",
			subName: "MLB코리아 이사",
			text: "데이터와 패션이\n 만나면 생기는 일",
			month: "3월",
		},
		{
			img: ImgA,
			name: "허원석",
			subName: "MLB코리아 이사",
			text: "데이터와 패션이\n 만나면 생기는 일",
			month: "3월",
		},
		{
			img: ImgA,
			name: "허원석",
			subName: "MLB코리아 이사",
			text: "데이터와 패션이\n 만나면 생기는 일",
			month: "3월",
		},
		{
			img: ImgA,
			name: "허원석",
			subName: "MLB코리아 이사",
			text: "데이터와 패션이\n 만나면 생기는 일",
			month: "3월",
		},
	];

	const renderLineupCard = (data) => {
		return data.map((item, index) => {
			return (
				<Col lg="4" md="8" xs="12" key={index}>
					<Card className="p-0 border-2">
						<CardImg alt="..." src={item.img} height="200px"></CardImg>
						<CardBody className="p-2">
							<CardTitle className="font-weight-900 mt-3 h4">
								{item.name}
							</CardTitle>
							<CardTitle className="font-weight-500 h6">
								{item.subName}
							</CardTitle>
							<div className="mb-4">
								<div className="font-weight-700 h5">{item.text}</div>
							</div>
						</CardBody>
						<RightToolTip
							className="font-weight-900 h6"
							style={{ color: "#171717" }}>
							{item.month}
						</RightToolTip>
					</Card>
				</Col>
			);
		});
	};

	return (
		<div>
			<Container className="container-xl">
				<Card className="p-4 text-center">
					<CardBody>
						<div className="h3 font-weight-900 text-center">
							Marketing Insight 라인업
						</div>
						<Row>{renderLineupCard(data)}</Row>
					</CardBody>
				</Card>
			</Container>
		</div>
	);
}

export default MembershipLineUp;

const RightToolTip = styled.div`
	display: inline-block;
	padding: 0.6rem 1.5rem;
	background: #3bd6ce;
	border-radius: 25px;
	position: absolute;
	right: 4%;
	top: 2%;
`;
