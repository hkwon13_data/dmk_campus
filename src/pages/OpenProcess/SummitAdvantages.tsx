import React from 'react';

import { Container, Card, CardBody, Row, Col } from 'reactstrap';

function SummitAdvantages() {
  return (
    <Container>
      <Card className='px-5 py-5'>
        <CardBody className='text-center'>
          <div className='h1'>Data Marketing Summit 2020</div>
          <div className='h3 font-weight-900 mb-4'>특장점 3가지</div>
          <Container className='container-lg'>
            <Row className='p-5 bg-secondary'>
              <Col>
                <div className='h3 font-weight-900'>1</div>
                <div className='h4 font-weight-700'>
                  쉽게 볼 수 없는
                  <br /> 라인업과 강의
                </div>
                <div />
              </Col>
              <Col>
                <div className='h3 font-weight-900'>2</div>
                <div className='h4 font-weight-700'>
                  여러 실무자들과의
                  <br />
                  네트워킹
                </div>
                <div />
              </Col>
              <Col>
                <div className='h3 font-weight-900'>3</div>
                <div className='h4 font-weight-700'>
                  최신 데이터마케팅
                  <br />
                  트렌드 강의
                </div>
                <div />
              </Col>
            </Row>
          </Container>
        </CardBody>
      </Card>
    </Container>
  );
}

export default SummitAdvantages;
