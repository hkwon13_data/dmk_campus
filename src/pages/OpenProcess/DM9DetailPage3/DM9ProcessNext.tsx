import React from 'react';

import { Container } from 'reactstrap';

import styled, { DefaultTheme } from 'styled-components';

const IconDiv = styled.div<DefaultTheme>`
  display: inline-block;
  position: relative;
  top: 4px;
`;

function DM9ProcessNext() {
  return (
    <div className='wrapper py-9'>
      <Container className='container-lg'>
        <div className='h2 font-weight-900 mb-5 text-center'>
          이 과정이 끝나면?
        </div>
        <div className='h4 pl-6'>
          <IconDiv>
            <i className='ni ni-check-bold' />
          </IconDiv>
          <span>
            <span className='font-weight-900'>
              Brand Identity와 Brrand iMAGE 사이의 GAP 파악
            </span>
            할 수 있습니다.
          </span>
        </div>
        <div className='h4 pl-6'>
          <IconDiv>
            <i className='ni ni-check-bold' />
          </IconDiv>
          <span>
            우리가 원하는{' '}
            <span className='font-weight-900'>Brand Image 전략 수립</span>이
            가능합니다.
          </span>
        </div>
        <div className='h4 pl-6'>
          <IconDiv>
            <i className='ni ni-check-bold' />
          </IconDiv>
          <span>
            우리{' '}
            <span className='font-weight-900'>
              브랜드와 소비자의 심리 Cycle 파악
            </span>
            이 가능합니다.
          </span>
        </div>
        <div className='h4 pl-6'>
          <IconDiv>
            <i className='ni ni-check-bold' />
          </IconDiv>
          <span>
            <span className='font-weight-900'>
              차별화 된 셀링 포인트를 도출
            </span>
            합니다.
          </span>
        </div>
      </Container>
    </div>
  );
}

export default DM9ProcessNext;
