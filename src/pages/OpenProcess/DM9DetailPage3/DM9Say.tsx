import React from 'react';

import { Container, Row, Col } from 'reactstrap';

import ImgA from 'assets/img/dm9_domain/say_img.png';

function DM9Say() {
  return (
    <Container className='my-9'>
      <Row className='justify-content-center'>
        <Col lg='3' className='px-0'>
          <div
            className='image'
            style={{
              height: '300px',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'flex-end',
              backgroundColor: '#8df9c5',
            }}
          >
            <img className=' img-fluid' src={ImgA} alt='이미지' />
          </div>
        </Col>
        <Col lg='8' className='p-5' style={{ border: '3px solid #8df9c5' }}>
          <div className='h1 font-weight-900'>마케터 says</div>
          <div className='h4'>
            1. 사람들은 우리 브랜드에 대해 어떠한 인식을 가지고 있을까요?
          </div>
          <div className='h4'>
            2. 우리 회사에서는 고객에게 어떠한 메세지를 전달해야 할까요?
          </div>
          <div className='h4'>
            3. 우리 브랜드의 긍정적인 이미지를 상승시키려면 어떻게 해야할까요?
          </div>
        </Col>
      </Row>
    </Container>
  );
}

export default DM9Say;
