import React from 'react';

import ImgA from 'assets/img/dm9_domain/headerImg3.png';

import { Container, Row, Col } from 'reactstrap';

function DM9DetailHeader() {
  return (
    <>
      {/* <div className="page-header header-filter page-header-small skew-separator skew-mini"> */}
      <div className='page-header header-filter'>
        <div
          className='page-header-image'
          style={{
            backgroundImage: `url(${ImgA})`,
            filter: 'brightness(0.50)',
          }}
        />
        <Container className='container-lg pt-5'>
          <Row className='justify-content-center align-items-center'>
            <Col lg='11'>
              <div className='h1 font-weight-900 text-white'>DM9 3영역</div>
              <div
                className='font-weight-900 text-white mb-3'
                style={{ fontSize: '3rem' }}
              >
                브랜드 경쟁력분석
              </div>
              <div className='h2 font-weight-500 text-white'>
                내가 생각하는 우리 브랜드 VS 고객이 생각하는 우리 브랜드
              </div>
              <div className='h2 font-weight-500 text-white'>
                Gap을 좁혀야 경쟁에서 승리한다!
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
}

export default DM9DetailHeader;
