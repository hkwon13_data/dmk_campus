import React from 'react';

import { Card, CardBody, Row, Col } from 'reactstrap';

function DM9EduObject() {
  return (
    <div className='wrapper py-9' style={{ background: '#dedffc' }}>
      <div className='h1 font-weight-900 mb-6 text-center'>교육 대상자</div>
      <Row className='justify-content-center'>
        <Col lg='3'>
          <Card>
            <CardBody>
              <div className='h5'>자사 브랜드 전략을 정비하고</div>
              <div className='font-weight-900 h5'>
                <span>싶은</span> 브랜드 담장자
              </div>
              <div className='font-weight-900 h5 mb-0'>&nbsp;</div>
              <div className='text-right'>
                <div className='icon-shape icon-xl mx-auto text-right'>
                  <i className='ni ni-circle-08' />
                </div>
              </div>
            </CardBody>
          </Card>
        </Col>
        <Col lg='3'>
          <Card>
            <CardBody>
              <div className='h5'>새로운 브랜드를 출시하기 위해</div>
              <div className='font-weight-900 h5'>
                브랜드 전략을 수깁해야 하는
              </div>
              <div className='font-weight-900 h5 mb-0'>마케팅 담당자</div>
              <div className='text-right'>
                <div className='icon-shape icon-xl mx-auto text-right'>
                  <i className='ni ni-circle-08' />
                </div>
              </div>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  );
}

export default DM9EduObject;
