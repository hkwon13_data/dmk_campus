import React from 'react';

import ImgA from 'assets/img/dm9_domain/bottom_banner_7.png';

import { Row } from 'reactstrap';

import { useHistory } from 'react-router-dom';

function DM9BottomBanner1() {
  const history = useHistory();
  return (
    <Row className='mb-9'>
      <div
        onClick={() => history.push('/community/inquiry/')}
        onKeyDown={() => {}}
        role='button'
        tabIndex={0}
      >
        <img src={ImgA} width='100%' alt='...' style={{ cursor: 'pointer' }} />
      </div>
    </Row>
  );
}

export default DM9BottomBanner1;
