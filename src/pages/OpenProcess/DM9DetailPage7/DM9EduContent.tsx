import React from 'react';

import { Container, Row, Col } from 'reactstrap';

function DM9EduContent() {
  return (
    <div className='wrapper pt-5' style={{ background: '#ecf1ff' }}>
      <div className='h1 font-weight-900 mb-6 text-center'>학습 내용</div>
      <Container>
        <Row className='justify-content-center align-items-center'>
          <Col
            lg='2'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderRight: 'none' }}
          >
            <div className='text-center text-primary h1 mb-0 p-2'>01</div>
          </Col>
          <Col
            lg='7'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderLeft: 'none' }}
          >
            <div className='p-1'>
              <div className='font-weight-700 h5'>데이터로 사고하기 : </div>
              <div className='h5 mb-0'>
                비즈니스 이슈 파악 및 데이터 분석 목적을 정의하고 분석 기준을
                구조화합니다
              </div>
            </div>
          </Col>
        </Row>
        <div className='h3 mb-0 text-center py-3'>
          <i className='ni ni-bold-down' />
        </div>
        <Row className='justify-content-center align-items-center'>
          <Col
            lg='2'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderRight: 'none' }}
          >
            <div className='text-center text-primary h1 mb-0 p-2'>02</div>
          </Col>
          <Col
            lg='7'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderLeft: 'none' }}
          >
            <div className='p-1'>
              <div className='font-weight-700 h5'>데이터 쓰기 : </div>
              <div className='h5 mb-0'>
                분석한 데이터의 Key Message를 강조하는 시각화 기법을 활용할 수
                있습니다
              </div>
            </div>
          </Col>
        </Row>
        <div className='h3 mb-0 text-center py-3'>
          <i className='ni ni-bold-down' />
        </div>
        <Row className='justify-content-center align-items-center'>
          <Col
            lg='2'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderRight: 'none' }}
          >
            <div className='text-center text-primary h1 mb-0 p-2'>03</div>
          </Col>
          <Col
            lg='7'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderLeft: 'none' }}
          >
            <div className='p-1'>
              <div className='font-weight-700 h5'>데이터 읽기 : </div>
              <div className='h5 mb-0'>
                두 개 이상의 데이터를 결합하여 분석하고 스토리텔링 할 수
                있습니다
              </div>
            </div>
          </Col>
        </Row>
        <div className='h3 mb-0 text-center py-3'>
          <i className='ni ni-bold-down' />
        </div>
        <Row className='justify-content-center align-items-center pb-6'>
          <Col
            lg='2'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderRight: 'none' }}
          >
            <div className='text-center text-primary h1 mb-0 p-2'>04</div>
          </Col>
          <Col
            lg='7'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderLeft: 'none' }}
          >
            <div className='p-1'>
              <div className='font-weight-700 h5'>데이터마케팅 전략도출 : </div>
              <div className='h5 mb-0'>
                데이터 분석 기획부터 분석 결과까지의 마케팅 전략 보고서를 도출할
                수 있습니다
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default DM9EduContent;
