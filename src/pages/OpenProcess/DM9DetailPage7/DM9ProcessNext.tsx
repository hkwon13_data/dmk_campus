import React from 'react';

import { Container } from 'reactstrap';

import styled, { DefaultTheme } from 'styled-components';

const IconDiv = styled.div<DefaultTheme>`
  display: inline-block;
  position: relative;
  top: 4px;
`;

function DM9ProcessNext() {
  return (
    <div className='wrapper py-9'>
      <Container className='container-lg'>
        <div className='h2 font-weight-900 mb-6 text-center'>
          이 과정이 끝나면?
        </div>
        <div className='h4 pl-9'>
          <IconDiv>
            <i className='ni ni-check-bold' />
          </IconDiv>
          <span>
            <span className='font-weight-900'>고객 데이터마케팅 거버넌스</span>
            (체계)를 파악 합니다
          </span>
        </div>
        <div className='h4 pl-9'>
          <IconDiv>
            <i className='ni ni-check-bold' />
          </IconDiv>
          <span>
            내/외부 데이터 연계 인사이트를 도축합니다{' '}
            <span className='font-weight-900' />
          </span>
        </div>
        <div className='h4 pl-9'>
          <IconDiv>
            <i className='ni ni-check-bold' />
          </IconDiv>
          <span>
            데이터{' '}
            <span className='font-weight-900'>의사결정 프로세스를 이해</span>
            하게 됩니다
          </span>
        </div>
        <div className='h4 pl-9'>
          <IconDiv>
            <i className='ni ni-check-bold' />
          </IconDiv>
          <span>
            소셜 Buzz의 수집, 분석,{' '}
            <span className='font-weight-900'>인사이트 도출 역량을 습득</span>
            하게 됩니다
          </span>
        </div>
      </Container>
    </div>
  );
}

export default DM9ProcessNext;
