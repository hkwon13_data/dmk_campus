import React from 'react';

import { Container, Row, Col } from 'reactstrap';

function DM9EduPoint() {
  return (
    <>
      <div className='my-8'>
        <Container>
          <Row className='justify-content-center'>
            <Col className='mx-auto text-center my-5' lg='8'>
              <div className='h1 font-weight-900'>교육포인트</div>
            </Col>
          </Row>
          <Row className='justify-content-center mb-4'>
            <Col lg='5' className='p-4' style={{ border: '1px solid #333333' }}>
              <div className='h1 text-right mb-0'>1</div>
              <div className='h4 font-weight-900'>IT 비전공저도 OK,</div>
              <div className='h4'>기획자/마케터 관점에서 데이터를</div>
              <div className='h4'>분석하는 방법 제공합니다</div>
            </Col>
            <Col
              lg='5'
              className='p-4 ml-4'
              style={{ border: '1px solid #333333' }}
            >
              <div className='h1 text-right mb-0'>2</div>
              <div className='h4 font-weight-900'>데이터 결합 인사이트.</div>
              <div className='h4'>데이터를 연게해 Data Creative 방법을</div>
              <div className='h4'>제공합니다</div>
            </Col>
          </Row>
          <Row className='justify-content-center'>
            <Col lg='5' className='p-4' style={{ border: '1px solid #333333' }}>
              <div className='h1 text-right mb-0'>3</div>
              <div className='h4 font-weight-900'>데이터마케팅 의사결정,</div>
              <div className='h4'>데이터리터러시를 통해 기업에 필요한</div>
              <div className='h4'>마케팅 의사결정 방향성을 제시합니다</div>
            </Col>
            <Col
              lg='5'
              className='p-4 ml-4'
              style={{ border: '1px solid #333333' }}
            >
              <div className='h1 text-right mb-0'>4</div>
              <div className='h4 font-weight-900'>소셜 Buzz 인사이트,</div>
              <div className='h4'>효과적인 채널 소비자들의 의견을</div>
              <div className='h4'>반영한 마케팅 전략을 도출합니다</div>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
}

export default DM9EduPoint;
