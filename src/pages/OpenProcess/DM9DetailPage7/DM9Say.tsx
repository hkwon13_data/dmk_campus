import React from 'react';

import { Container, Row, Col } from 'reactstrap';

import ImgA from 'assets/img/dm9_domain/say_img.png';

function DM9Say() {
  return (
    <Container className='my-9'>
      <Row className='justify-content-center'>
        <Col lg='3' className='px-0'>
          <div
            className='image'
            style={{
              height: '300px',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'flex-end',
              backgroundColor: '#8df9c5',
            }}
          >
            <img className=' img-fluid' src={ImgA} alt='이미지' />
          </div>
        </Col>
        <Col lg='8' className='p-5' style={{ border: '3px solid #8df9c5' }}>
          <div className='h1 font-weight-900'>마케터 says</div>
          <div className='h4'>
            1. 데이터를 결합하여 인사이트를 얻는 방법은 무엇인가요?
          </div>
          <div className='h4'>
            2. 데이터를 해석해 어떻게 마케팅의 의사결정을 내릴 수 있나요?
          </div>
          <div className='h4'>
            3. 소비자들의 의견을 어떻게 마케팅에 반영할 수 있나요?
          </div>
        </Col>
      </Row>
    </Container>
  );
}

export default DM9Say;
