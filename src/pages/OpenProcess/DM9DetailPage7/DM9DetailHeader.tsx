import React from 'react';

import ImgA from 'assets/img/dm9_domain/headerImg7.png';

import { Container, Row, Col } from 'reactstrap';

function DM9DetailHeader() {
  return (
    <>
      {/* <div className="page-header header-filter page-header-small skew-separator skew-mini"> */}
      <div className='page-header header-filter'>
        <div
          className='page-header-image'
          style={{
            backgroundImage: `url(${ImgA})`,
            filter: 'brightness(0.50)',
          }}
        />
        <Container className='container-lg pt-5'>
          <Row className='justify-content-center align-items-center'>
            <Col lg='11'>
              <div className='h1 font-weight-900 text-white'>DM9 7영역</div>
              <div
                className='font-weight-900 text-white mb-3'
                style={{ fontSize: '3rem' }}
              >
                데이터리터러시
              </div>
              <div className='h2 font-weight-700 text-white'>
                내게 필요한 데이터, 자유롭게 읽고 쓰고 활용하고 싶다면?
              </div>
              <div className='h2 font-weight-700 text-white mb-6'>
                데이터리터러시를 배우셔야 합니다
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
}

export default DM9DetailHeader;
