import React from 'react';

import { Card, CardBody, Row, Col } from 'reactstrap';

function DM9EduObject() {
  return (
    <div className='wrapper py-9' style={{ background: '#dedffc' }}>
      <div className='h1 font-weight-900 mb-6 text-center'>교육 대상자</div>
      <Row className='justify-content-center'>
        <Col lg='3'>
          <Card>
            <CardBody>
              <div className='h5'>데이터를 기반으로 근거 있는</div>
              <div className='h5'>마케팅을 기획하고 싶은 마케터</div>
              <div className='h5 mb-0'>&nbsp;</div>
              <div className='text-right'>
                <div className='icon-shape icon-xl mx-auto text-right'>
                  <i className='ni ni-circle-08' />
                </div>
              </div>
            </CardBody>
          </Card>
        </Col>
        <Col lg='3'>
          <Card>
            <CardBody>
              <div className='h5'>데이터에 숨어 있는 의미를 파악</div>
              <div className='h5'>하여 합리적인 의사결정을 내리고</div>
              <div className='h5 mb-0'>싶은 임원진</div>
              <div className='text-right'>
                <div className='icon-shape icon-xl mx-auto text-right'>
                  <i className='ni ni-circle-08' />
                </div>
              </div>
            </CardBody>
          </Card>
        </Col>
        <Col lg='3'>
          <Card>
            <CardBody>
              <div className='h5'>코딩, Tool습득 보다 데이터를 이해</div>
              <div className='h5'>하여 비지니스에 활용하고 싶은</div>
              <div className='h5 mb-0'>누구나</div>
              <div className='text-right'>
                <div className='icon-shape icon-xl mx-auto text-right'>
                  <i className='ni ni-circle-08' />
                </div>
              </div>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  );
}

export default DM9EduObject;
