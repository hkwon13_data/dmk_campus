import React, { useState } from 'react';
import styled from 'styled-components';
import { useHistory } from 'react-router-dom';

// reactstrap components
import { Card, CardBody, Container, Row, Col, CardTitle } from 'reactstrap';

// import iconA from 'assets/icons/intro_growth.png';
// import iconB from 'assets/icons/intro_sight.png';
// import iconC from 'assets/icons/intro_marketing.png';

const ToggleDiv1 = styled.div<{ toggleProps1: boolean }>`
  width: 100%;
  padding: 1rem;
  background: #f4f4f8;
  font-size: 1.5rem;
  position: absolute;
  left: 0;
  bottom: 0;
  text-align: center;
  cursor: pointer;
  display: ${(props) => (props.toggleProps1 === true ? 'block' : 'none')};
`;

const ToggleDiv2 = styled.div<{ toggleProps2: boolean }>`
  width: 100%;
  padding: 1rem;
  background: #f4f4f8;
  font-size: 1.5rem;
  position: absolute;
  left: 0;
  bottom: 0;
  text-align: center;
  cursor: pointer;
  display: ${(props) => (props.toggleProps2 === true ? 'block' : 'none')};
`;

const ToggleDiv3 = styled.div<{ toggleProps3: boolean }>`
  width: 100%;
  padding: 1rem;
  background: #f4f4f8;
  font-size: 1.5rem;
  position: absolute;
  left: 0;
  bottom: 0;
  text-align: center;
  cursor: pointer;
  display: ${(props) => (props.toggleProps3 === true ? 'block' : 'none')};
`;

const ToggleDiv4 = styled.div<{ toggleProps4: boolean }>`
  width: 100%;
  padding: 1rem;
  background: #f4f4f8;
  font-size: 1.5rem;
  position: absolute;
  left: 0;
  bottom: 0;
  text-align: center;
  cursor: pointer;
  display: ${(props) => (props.toggleProps4 === true ? 'block' : 'none')};
`;

const ToggleDiv5 = styled.div<{ toggleProps5: boolean }>`
  width: 100%;
  padding: 1rem;
  background: #f4f4f8;
  font-size: 1.5rem;
  position: absolute;
  left: 0;
  bottom: 0;
  text-align: center;
  cursor: pointer;
  display: ${(props) => (props.toggleProps5 === true ? 'block' : 'none')};
`;

const ToggleDiv6 = styled.div<{ toggleProps6: boolean }>`
  width: 100%;
  padding: 1rem;
  background: #f4f4f8;
  font-size: 1.5rem;
  position: absolute;
  left: 0;
  bottom: 0;
  text-align: center;
  cursor: pointer;
  display: ${(props) => (props.toggleProps6 === true ? 'block' : 'none')};
`;

const ToggleDiv7 = styled.div<{ toggleProps7: boolean }>`
  width: 100%;
  padding: 1rem;
  background: #f4f4f8;
  font-size: 1.5rem;
  position: absolute;
  left: 0;
  bottom: 0;
  text-align: center;
  cursor: pointer;
  display: ${(props) => (props.toggleProps7 === true ? 'block' : 'none')};
`;

const ToggleDiv8 = styled.div<{ toggleProps8: boolean }>`
  width: 100%;
  padding: 1rem;
  background: #f4f4f8;
  font-size: 1.5rem;
  position: absolute;
  left: 0;
  bottom: 0;
  text-align: center;
  cursor: pointer;
  display: ${(props) => (props.toggleProps8 === true ? 'block' : 'none')};
`;

const ToggleDiv9 = styled.div<{ toggleProps9: boolean }>`
  width: 100%;
  padding: 1rem;
  background: #f4f4f8;
  font-size: 1.5rem;
  position: absolute;
  left: 0;
  bottom: 0;
  text-align: center;
  cursor: pointer;
  display: ${(props) => (props.toggleProps9 === true ? 'block' : 'none')};
`;

function DM9Piloshopy() {
  const [toggleProps1, setToggleProps1] = useState<boolean>(false);
  const [toggleProps2, setToggleProps2] = useState<boolean>(false);
  const [toggleProps3, setToggleProps3] = useState<boolean>(false);
  const [toggleProps4, setToggleProps4] = useState<boolean>(false);
  const [toggleProps5, setToggleProps5] = useState<boolean>(false);
  const [toggleProps6, setToggleProps6] = useState<boolean>(false);
  const [toggleProps7, setToggleProps7] = useState<boolean>(false);
  const [toggleProps8, setToggleProps8] = useState<boolean>(false);
  const [toggleProps9, setToggleProps9] = useState<boolean>(false);

  const history = useHistory();

  return (
    <>
      <div className='project-2 mt-3 mb-8'>
        <Container>
          <Row>
            <Col className='mx-auto text-center my-5' lg='8'>
              <div className='display-5'>데이터마케팅의 9대 영역</div>
              <div className='h7 py-4 font-weight-700'>
                데이터마케팅 9대 영역을 알면 기획, 여행, 성과측정까지 직접 할 수
                있습니다
              </div>
            </Col>
          </Row>
          <Row className='justify-content-center'>
            <Col lg='3' md='6'>
              <Card
                className='card-project'
                onMouseOver={() => setToggleProps1(!toggleProps1)}
                onMouseOut={() => setToggleProps1(!toggleProps1)}
              >
                <a href='#pablo' onClick={(e) => e.preventDefault()}>
                  <div className='icon icon-lg icon-shape icon-shape-primary shadow rounded-circle mx-auto cursor-default'>
                    <div className='icon-numbers'>
                      <i className='fas fa-heart' />
                    </div>
                  </div>
                </a>
                <CardBody>
                  <CardTitle
                    className='mt-3 font-weight-700 text-muted '
                    tag='h6'
                  >
                    1영역
                  </CardTitle>
                  <CardTitle
                    className='mt-3 font-weight-900 text-dark '
                    tag='h4'
                  >
                    시장/트렌드 분석
                  </CardTitle>
                  {/* <p className="card-description text-muted text-lg font-weight-600 text-center pt-5"> */}
                  <div className=''>
                    자사 제품부터 라이프스타일 까지! 시장을 분석하고 예측 할 수
                    있다.
                  </div>
                  <ToggleDiv1
                    className='text-primary font-weight-700'
                    toggleProps1={toggleProps1}
                    onClick={() => history.push('/openProcess/dm9/detailPage1')}
                  >
                    강의보기
                  </ToggleDiv1>
                </CardBody>
              </Card>
            </Col>
            <Col lg='3' md='6'>
              <Card
                className='card-project'
                onMouseOver={() => setToggleProps2(!toggleProps2)}
                onMouseOut={() => setToggleProps2(!toggleProps2)}
              >
                <a href='#pablo' onClick={(e) => e.preventDefault()}>
                  <div className='icon icon-lg icon-shape icon-shape-primary shadow rounded-circle mx-auto cursor-default'>
                    <div className='icon-numbers text-green'>
                      <i className='ni ni-books' />
                    </div>
                  </div>
                </a>
                <CardBody>
                  <CardTitle
                    className='mt-3 font-weight-700 text-muted '
                    tag='h6'
                  >
                    2영역
                  </CardTitle>
                  <CardTitle
                    className='mt-3 font-weight-900 text-dark '
                    tag='h4'
                  >
                    고객 분석
                  </CardTitle>
                  {/* <p className="card-description text-muted text-lg font-weight-600 text-center pt-5"> */}
                  <div className=''>
                    고객이 남긴 디지털 발자국을 보면 진짜 내 고객을 알 수 있다.
                  </div>
                  <ToggleDiv2
                    className='text-primary font-weight-700'
                    toggleProps2={toggleProps2}
                    onClick={() => history.push('/openProcess/dm9/detailPage2')}
                  >
                    강의보기
                  </ToggleDiv2>
                </CardBody>
              </Card>
            </Col>
            <Col lg='3' md='6'>
              <Card
                className='card-project'
                onMouseOver={() => setToggleProps3(!toggleProps3)}
                onMouseOut={() => setToggleProps3(!toggleProps3)}
              >
                <a href='#pablo' onClick={(e) => e.preventDefault()}>
                  <div className='icon icon-lg icon-shape icon-shape-primary shadow rounded-circle mx-auto cursor-default'>
                    <div className='icon-numbers text-red'>
                      <i className='ni ni-trophy' />
                    </div>
                  </div>
                </a>
                <CardBody>
                  <CardTitle
                    className='mt-3 font-weight-700 text-muted '
                    tag='h6'
                  >
                    3영역
                  </CardTitle>
                  <CardTitle
                    className='mt-3 font-weight-900 text-dark '
                    tag='h4'
                  >
                    브랜드 경쟁력 분석
                  </CardTitle>
                  {/* <p className="card-description text-muted text-lg font-weight-600 text-center pt-5"> */}
                  <div className=''>
                    브랜드, 나와 고객 생각의 Gap을 좁혀야 경쟁에서 승리한다
                  </div>
                  <ToggleDiv3
                    className='text-primary font-weight-700'
                    toggleProps3={toggleProps3}
                    onClick={() => history.push('/openProcess/dm9/detailPage3')}
                  >
                    강의보기
                  </ToggleDiv3>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Row className='justify-content-center'>
            <Col lg='3' md='6'>
              <Card
                className='card-project'
                onMouseOver={() => setToggleProps4(!toggleProps4)}
                onMouseOut={() => setToggleProps4(!toggleProps4)}
              >
                <a href='#pablo' onClick={(e) => e.preventDefault()}>
                  <div className='icon icon-lg icon-shape icon-shape-primary shadow rounded-circle mx-auto cursor-default'>
                    <div className='icon-numbers'>
                      <i className='ni ni-tablet-button' />
                    </div>
                  </div>
                </a>
                <CardBody>
                  <CardTitle
                    className='mt-3 font-weight-700 text-muted '
                    tag='h6'
                  >
                    4영역
                  </CardTitle>
                  <CardTitle
                    className='mt-3 font-weight-900 text-dark '
                    tag='h4'
                  >
                    채널 경쟁력 분석
                  </CardTitle>
                  {/* <p className="card-description text-muted text-lg font-weight-600 text-center pt-5"> */}
                  <div className=''>
                    자사 마케팅 채널의 건강상태(DHC)를 파악할 수 있다.
                  </div>
                  <ToggleDiv4
                    className='text-primary font-weight-700'
                    toggleProps4={toggleProps4}
                    onClick={() => history.push('/openProcess/dm9/detailPage4')}
                  >
                    강의보기
                  </ToggleDiv4>
                </CardBody>
              </Card>
            </Col>
            <Col lg='3' md='6'>
              <Card
                className='card-project'
                onMouseOver={() => setToggleProps5(!toggleProps5)}
                onMouseOut={() => setToggleProps5(!toggleProps5)}
              >
                <a href='#pablo' onClick={(e) => e.preventDefault()}>
                  <div className='icon icon-lg icon-shape icon-shape-primary shadow rounded-circle mx-auto cursor-default'>
                    <div className='icon-numbers text-green'>
                      <i className='ni ni-headphones' />
                    </div>
                  </div>
                </a>
                <CardBody>
                  <CardTitle
                    className='mt-3 font-weight-700 text-muted '
                    tag='h6'
                  >
                    5영역
                  </CardTitle>
                  <CardTitle
                    className='mt-3 font-weight-900 text-dark '
                    tag='h4'
                  >
                    콘텐츠 경쟁력 분석
                  </CardTitle>
                  {/* <p className="card-description text-muted text-lg font-weight-600 text-center pt-5"> */}
                  <div className=''>
                    내 고객이 반응하는 대박 콘텐츠의 비밀, 콘텐츠 DPI
                    <span className='text-red'>*</span> 속에 있다.
                    <br />
                    <span className='text-red'>*DPI: Digital Power Index</span>
                  </div>
                  <ToggleDiv5
                    className='text-primary font-weight-700'
                    toggleProps5={toggleProps5}
                    onClick={() => history.push('/openProcess/dm9/detailPage5')}
                  >
                    강의보기
                  </ToggleDiv5>
                </CardBody>
              </Card>
            </Col>
            <Col lg='3' md='6'>
              <Card
                className='card-project'
                onMouseOver={() => setToggleProps6(!toggleProps6)}
                onMouseOut={() => setToggleProps6(!toggleProps6)}
              >
                <a href='#pablo' onClick={(e) => e.preventDefault()}>
                  <div className='icon icon-lg icon-shape icon-shape-primary shadow rounded-circle mx-auto cursor-default'>
                    <div className='icon-numbers text-red'>
                      <i className='ni ni-vector' />
                    </div>
                  </div>
                </a>
                <CardBody>
                  <CardTitle
                    className='mt-3 font-weight-700 text-muted '
                    tag='h6'
                  >
                    6영역
                  </CardTitle>
                  <CardTitle
                    className='mt-3 font-weight-900 text-dark '
                    tag='h4'
                  >
                    퍼포먼스 마케팅
                  </CardTitle>
                  {/* <p className="card-description text-muted text-lg font-weight-600 text-center pt-5"> */}
                  <div className=''>
                    적은 예산으로 최적의 광고를 기획해보자.
                  </div>
                  <ToggleDiv6
                    className='text-primary font-weight-700'
                    toggleProps6={toggleProps6}
                    onClick={() => history.push('/openProcess/dm9/detailPage6')}
                  >
                    강의보기
                  </ToggleDiv6>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Row className='justify-content-center'>
            <Col lg='3' md='6'>
              <Card
                className='card-project'
                onMouseOver={() => setToggleProps7(!toggleProps7)}
                onMouseOut={() => setToggleProps7(!toggleProps7)}
              >
                <a href='#pablo' onClick={(e) => e.preventDefault()}>
                  <div className='icon icon-lg icon-shape icon-shape-primary shadow rounded-circle mx-auto cursor-default'>
                    <div className='icon-numbers'>
                      <i className='ni ni-hat-3' />
                    </div>
                  </div>
                </a>
                <CardBody>
                  <CardTitle
                    className='mt-3 font-weight-700 text-muted '
                    tag='h6'
                  >
                    7영역
                  </CardTitle>
                  <CardTitle
                    className='mt-3 font-weight-900 text-dark '
                    tag='h4'
                  >
                    데이터리터러시
                  </CardTitle>
                  {/* <p className="card-description text-muted text-lg font-weight-600 text-center pt-5"> */}
                  <div className=''>
                    내게 필요한 데이터, 그 속에 숨겨진 의미를 해석하고 목적에
                    맞게 활용해보자.
                  </div>
                  <ToggleDiv7
                    className='text-primary font-weight-700'
                    toggleProps7={toggleProps7}
                    onClick={() => history.push('/openProcess/dm9/detailPage7')}
                  >
                    강의보기
                  </ToggleDiv7>
                </CardBody>
              </Card>
            </Col>
            <Col lg='3' md='6'>
              <Card
                className='card-project'
                onMouseOver={() => setToggleProps8(!toggleProps8)}
                onMouseOut={() => setToggleProps8(!toggleProps8)}
              >
                <a href='#pablo' onClick={(e) => e.preventDefault()}>
                  <div className='icon icon-lg icon-shape icon-shape-primary shadow rounded-circle mx-auto cursor-default'>
                    <div className='icon-numbers text-green'>
                      <i className='ni ni-key-25' />
                    </div>
                  </div>
                </a>
                <CardBody>
                  <CardTitle
                    className='mt-3 font-weight-700 text-muted '
                    tag='h6'
                  >
                    8영역
                  </CardTitle>
                  <CardTitle
                    className='mt-3 font-weight-900 text-dark '
                    tag='h4'
                    style={{ whiteSpace: 'nowrap' }}
                  >
                    데이터마케팅 방법론
                  </CardTitle>
                  {/* <p className="card-description text-muted text-lg font-weight-600 text-center pt-5"> */}
                  <div className=''>
                    어려운 머신러닝 인공지능? 간단한 개념만 알아도 데이터
                    기술자와 대화 할 수 있다.
                  </div>
                  <ToggleDiv8
                    className='text-primary font-weight-700'
                    toggleProps8={toggleProps8}
                    onClick={() => history.push('/openProcess/dm9/detailPage8')}
                  >
                    강의보기
                  </ToggleDiv8>
                </CardBody>
              </Card>
            </Col>
            <Col lg='3' md='6'>
              <Card
                className='card-project'
                onMouseOver={() => setToggleProps9(!toggleProps9)}
                onMouseOut={() => setToggleProps9(!toggleProps9)}
              >
                <a href='#pablo' onClick={(e) => e.preventDefault()}>
                  <div className='icon icon-lg icon-shape icon-shape-primary shadow rounded-circle mx-auto cursor-default'>
                    <div className='icon-numbers text-red'>
                      <i className='ni ni-paper-diploma' />
                    </div>
                  </div>
                </a>
                <CardBody>
                  <CardTitle
                    className='mt-3 font-weight-700 text-muted '
                    tag='h6'
                  >
                    9영역
                  </CardTitle>
                  <CardTitle
                    className='mt-3 font-weight-900 text-dark '
                    tag='h4'
                  >
                    데이터 윤리
                  </CardTitle>
                  {/* <p className="card-description text-muted text-lg font-weight-600 text-center pt-5"> */}
                  <div className=''>
                    디지털 상에 넘쳐나는 개인정보! 고객 데이터를 지키는 데이터
                    윤리가 필요하다
                  </div>
                  <ToggleDiv9
                    className='text-primary font-weight-700'
                    toggleProps9={toggleProps9}
                    onClick={() => history.push('/openProcess/dm9/detailPage9')}
                  >
                    강의보기
                  </ToggleDiv9>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
}

export default DM9Piloshopy;
