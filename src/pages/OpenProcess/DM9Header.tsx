import React from 'react';

// reactstrap components
import { Container, Row, Col, CardBody, CardTitle } from 'reactstrap';

// Core Components

import imgA from 'assets/img/oflline_dm9/offline_header.png';

function DM9Header() {
  return (
    <>
      {/* <div className="page-header header-filter page-header-small skew-separator skew-mini"> */}
      <div
        className='page-header-image d-flex align-items-center pt-5'
        style={{
          backgroundImage: `url(${imgA})`,
          backgroundSize: '100% 100%',
          height: '75vh',
          zIndex: -123,
        }}
      >
        <Container>
          <Row>
            <Col className='mx-auto' lg='10'>
              <CardBody className='mb-4 text-center'>
                <div className='content-bottom'>
                  <CardTitle
                    tag='h1'
                    className='text-white font-weight-700 mb-5'
                  >
                    마케팅 실무자들의 고민을 담아 만들었습니다.
                  </CardTitle>
                  <div className='h4 text-white font-weight-700 my-3'>
                    마케팅 건강검진부터 기획, 이행, 성과측정까지
                    <br />
                    데이터마케팅 전 영역에 걸쳐 체계적인 교육 커리큘럼을
                    제공합니다.
                  </div>
                </div>
              </CardBody>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
}

export default DM9Header;
