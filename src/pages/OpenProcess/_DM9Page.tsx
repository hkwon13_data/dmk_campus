import React from 'react';

import CommonNavbar from 'components/NavBar/CommonNavbar';
import CommonFooter from 'components/NavBar/CommonFooter';
import { ScrollTopButton } from 'components/utils/ScrollTopButton';
import FloatingButton from '../../components/NavBar/FloatingButton';

import DM9Header from './DM9Header';
import DM9Piloshopy from './DM9Piloshopy';
// import DM9LectureApply from './DM9LectureApply';
import HistoryInterview from '../Intro/HistoryInterview';
// import ProfileCard from '../Intro/HistoryInterviewProfileCard';
import DM9PersonRecommend from './DM9PersonRecommend';

export default function DM9Page() {
  React.useEffect(() => {
    // let classList = document.body.classList;
    // document.body.classList.remove(...classList);
    // document.body.classList.add('index-page');
    window.scrollTo(0, 0);
    // return function cleanup() {
    //   document.body.classList.remove('index-page');
    // };
  }, []);

  return (
    <>
      <ScrollTopButton />
      <CommonNavbar type='primary' />
      <FloatingButton />
      <div className='section section-typography'>
        <DM9Header />
        <HistoryInterview />
        <DM9PersonRecommend />
        <DM9Piloshopy />
        {/* <DM9LectureApply /> */}
      </div>
      <CommonFooter />
    </>
  );
}
