import React from 'react';

import { Container, Row, Col } from 'reactstrap';

function DM9EduContent() {
  return (
    <div className='wrapper pt-5' style={{ background: '#ecf1ff' }}>
      <div className='h1 font-weight-900 mb-6 text-center'>교육 대상자</div>
      <Container>
        <Row className='justify-content-center align-items-center'>
          <Col
            lg='2'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderRight: 'none' }}
          >
            <div className='text-center text-primary h1 mb-0 p-2'>01</div>
          </Col>
          <Col
            lg='7'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderLeft: 'none' }}
          >
            <div className='p-1'>
              <div className='font-weight-700 h5'>콘텐츠 마케팅의 이해 : </div>
              <div className='h5 mb-0'>
                MarTech 및 최신 콘텐츠 트렌드를 이해합니다
              </div>
            </div>
          </Col>
        </Row>
        <div className='h3 mb-0 text-center py-3'>
          <i className='ni ni-bold-down' />
        </div>
        <Row className='justify-content-center align-items-center'>
          <Col
            lg='2'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderRight: 'none' }}
          >
            <div className='text-center text-primary h1 mb-0 p-2'>02</div>
          </Col>
          <Col
            lg='7'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderLeft: 'none' }}
          >
            <div className='p-1'>
              <div className='font-weight-700 h5'>콘텐츠 트랜드 분석 : </div>
              <div className='h5 mb-0'>
                자사와 경쟁사의 콘텐츠 경쟁력을 비교하고, 타겟 고객의 선호
                콘텐츠를 분석합니다
              </div>
            </div>
          </Col>
        </Row>
        <div className='h3 mb-0 text-center py-3'>
          <i className='ni ni-bold-down' />
        </div>
        <Row className='justify-content-center align-items-center'>
          <Col
            lg='2'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderRight: 'none' }}
          >
            <div className='text-center text-primary h1 mb-0 p-2'>03</div>
          </Col>
          <Col
            lg='7'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderLeft: 'none' }}
          >
            <div className='p-1'>
              <div className='font-weight-700 h5'>
                콘텐츠 스토리텔링과 기획 :{' '}
              </div>
              <div className='h5 mb-0'>
                콘텐츠 목적과 메시지 전략을 수립하고, 해당 콘텐츠의 매채 전략을
                수립합니다
              </div>
            </div>
          </Col>
        </Row>
        <div className='h3 mb-0 text-center py-3'>
          <i className='ni ni-bold-down' />
        </div>
        <Row className='justify-content-center align-items-center pb-6'>
          <Col
            lg='2'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderRight: 'none' }}
          >
            <div className='text-center text-primary h1 mb-0 p-2'>04</div>
          </Col>
          <Col
            lg='7'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderLeft: 'none' }}
          >
            <div className='p-1'>
              <div className='font-weight-700 h5'>
                콘텐츠 운영 전략 및 성과측정 :{' '}
              </div>
              <div className='h5 mb-0'>
                콘텐츠 기획부터 성과 측정까지 운영 전략을 수립합니다
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default DM9EduContent;
