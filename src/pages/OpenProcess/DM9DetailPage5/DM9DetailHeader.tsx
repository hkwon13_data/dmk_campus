import React from 'react';

import ImgA from 'assets/img/dm9_domain/headerImg5.png';

import { Container, Row, Col } from 'reactstrap';

function DM9DetailHeader() {
  return (
    <>
      {/* <div className="page-header header-filter page-header-small skew-separator skew-mini"> */}
      <div className='page-header header-filter'>
        <div
          className='page-header-image'
          style={{
            backgroundImage: `url(${ImgA})`,
            filter: 'brightness(0.50)',
          }}
        />
        <Container className='container-lg pt-7'>
          <Row className='justify-content-center align-items-center'>
            <Col lg='8'>
              <div className='h1 font-weight-900 text-white'>DM9 5영역</div>
              <div
                className='font-weight-900 text-white mb-3'
                style={{ fontSize: '3rem' }}
              >
                콘텐츠 경쟁력 분석
              </div>
              <div className='h1 font-weight-500 text-white'>
                내 고객이 반응하는 대박 콘텐츠의 <br />
                비밀, 콘텐츠 DPI* 속에 있다
              </div>
              <div className='h5 font-weight-500 text-white'>
                *DPI : Digital Power Index
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
}

export default DM9DetailHeader;
