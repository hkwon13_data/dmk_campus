import React from 'react';

import { Container, Row, Col } from 'reactstrap';

import ImgA from 'assets/img/dm9_domain/say_img.png';

function DM9Say() {
  return (
    <Container className='my-9'>
      <Row className='justify-content-center'>
        <Col lg='3' className='px-0'>
          <div
            className='image'
            style={{
              height: '350px',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'flex-end',
              backgroundColor: '#8df9c5',
            }}
          >
            <img className=' img-fluid' src={ImgA} alt='이미지' />
          </div>
        </Col>
        <Col lg='9' className='p-5' style={{ border: '3px solid #8df9c5' }}>
          <div className='h1 font-weight-900'>마케터 says</div>
          <div className='h4'>1. 요즘 제일 인기있는 콘텐츠가 어떤 걸까요?</div>
          <div className='h4'>
            2. 고객의 관심을 끌어낼 수 있는 콘텐츠는 어떻게 발굴할 수 있을까요?
          </div>
          <div className='h4'>3. 늘 새로운 콘텐츠를 만드는게 부담스러워요</div>
          <div className='h4'>4. 우리 채널에 맞는 콘텐츠는 어떤 것일까요?</div>
        </Col>
      </Row>
    </Container>
  );
}

export default DM9Say;
