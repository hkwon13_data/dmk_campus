import React from 'react';

import { Container, Row, Col } from 'reactstrap';

function DM9EduPoint() {
  return (
    <>
      <div className='my-8'>
        <Container>
          <Row className='justify-content-center'>
            <Col className='mx-auto text-center my-5' lg='8'>
              <div className='h1 font-weight-900'>교육포인트</div>
            </Col>
          </Row>
          <Row className='justify-content-center mb-4'>
            <Col lg='5' className='p-4' style={{ border: '1px solid #333333' }}>
              <div className='h1 text-right mb-0'>1</div>
              <div className='h4 font-weight-900'>IT 비전공저도 OK,</div>
              <div className='h4'>기획자/마케터 관점에서 데이터를</div>
              <div className='h4'>분석하는 방법 제공합니다</div>
            </Col>
            <Col
              lg='5'
              className='p-4 ml-4'
              style={{ border: '1px solid #333333' }}
            >
              <div className='h1 text-right mb-0'>2</div>
              <div className='h4 font-weight-900'>뜨는 콘텐츠 파악,</div>
              <div className='h4'>콘텐츠의 조건과 우리 고객이 반응하는</div>
              <div className='h4'>콘텐츠 유형 파악합니다</div>
            </Col>
          </Row>
          <Row className='justify-content-center'>
            <Col lg='5' className='p-4' style={{ border: '1px solid #333333' }}>
              <div className='h1 text-right mb-0'>3</div>
              <div className='h4 font-weight-900'>통하는 콘텐츠 기획,</div>
              <div className='h4'>고객이 듣고 싶은 메세지를 도출하여</div>
              <div className='h4'>콘텐츠 스토리텔링 합니다</div>
            </Col>
            <Col
              lg='5'
              className='p-4 ml-4'
              style={{ border: '1px solid #333333' }}
            >
              <div className='h1 text-right mb-0'>4</div>
              <div className='h4 font-weight-900'>콘텐츠 운영 전략수립,</div>
              <div className='h4'>효과적인 콘텐츠 운영을 위해 채널별</div>
              <div className='h4'>콘텐츠 운영 전략 도출할 수 있습니다</div>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
}

export default DM9EduPoint;
