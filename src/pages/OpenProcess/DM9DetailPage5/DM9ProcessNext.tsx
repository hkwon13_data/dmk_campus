import React from 'react';

import { Container } from 'reactstrap';

import styled, { DefaultTheme } from 'styled-components';

const IconDiv = styled.div<DefaultTheme>`
  display: inline-block;
  position: relative;
  top: 4px;
`;

function DM9ProcessNext() {
  return (
    <div className='wrapper py-9'>
      <Container className='container-md'>
        <div className='h2 font-weight-900 mb-5 text-center'>
          이 과정이 끝나면?
        </div>
        <div className='h4 pl-6'>
          <IconDiv>
            <i className='ni ni-check-bold' />
          </IconDiv>
          <span>
            <span className='font-weight-900'>콘텐츠에 고객 Voice</span>를 담을
            수 있습니다.
          </span>
        </div>
        <div className='h4 pl-6'>
          <IconDiv>
            <i className='ni ni-check-bold' />
          </IconDiv>
          <span>
            <span className='font-weight-900'>
              이익이 되는 콘텐츠 기획과 제작
            </span>
            이 가능합니다.
          </span>
        </div>
        <div className='h4 pl-6'>
          <IconDiv>
            <i className='ni ni-check-bold' />
          </IconDiv>
          <span>
            <span className='font-weight-900'>콘텐츠 성과측정</span>을 할 수
            있습니다.
          </span>
        </div>
      </Container>
    </div>
  );
}

export default DM9ProcessNext;
