import React from 'react';

import { Card, CardBody, Row, Col } from 'reactstrap';

function DM9EduObject() {
  return (
    <div className='wrapper py-9' style={{ background: '#dedffc' }}>
      <div className='h1 font-weight-900 mb-6 text-center'>교육 대상자</div>
      <Row className='justify-content-center'>
        <Col lg='3'>
          <Card>
            <CardBody>
              <div className='h5'>조직 내 디지털 마케팅 전략을</div>
              <div className='h5'>수립하는 마케팅 담당자</div>
              <div className='font-weight-900 h5 mb-0'>&nbsp;</div>
              <div className='text-right'>
                <div className='icon-shape icon-xl mx-auto text-right'>
                  <i className='ni ni-circle-08' />
                </div>
              </div>
            </CardBody>
          </Card>
        </Col>
        <Col lg='3'>
          <Card>
            <CardBody>
              <div className='h5'>마케팅 성과측정 및 매체 최적화에</div>
              <div className='h5'>고민하는 마케팅 담당자</div>
              <div className='font-weight-900 h5 mb-0'>&nbsp;</div>
              <div className='text-right'>
                <div className='icon-shape icon-xl mx-auto text-right'>
                  <i className='ni ni-circle-08' />
                </div>
              </div>
            </CardBody>
          </Card>
        </Col>
        <Col lg='3'>
          <Card>
            <CardBody>
              <div className='h5'>마케팅 대행사를 활용하여</div>
              <div className=' h5'>마케팅 성과를 높이고자 하는</div>
              <div className=' h5 mb-0'>마케팅 담당자</div>
              <div className='text-right'>
                <div className='icon-shape icon-xl mx-auto text-right'>
                  <i className='ni ni-circle-08' />
                </div>
              </div>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  );
}

export default DM9EduObject;
