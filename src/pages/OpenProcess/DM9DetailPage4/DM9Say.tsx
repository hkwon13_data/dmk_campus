import React from 'react';

import { Container, Row, Col } from 'reactstrap';

import ImgA from 'assets/img/dm9_domain/say_img.png';

function DM9Say() {
  return (
    <Container className='my-9'>
      <Row className='justify-content-center'>
        <Col lg='3' className='px-0'>
          <div
            className='image'
            style={{
              height: '300px',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'flex-end',
              backgroundColor: '#8df9c5',
            }}
          >
            <img className=' img-fluid' src={ImgA} alt='이미지' />
          </div>
        </Col>
        <Col lg='9' className='p-5' style={{ border: '3px solid #8df9c5' }}>
          <div className='h1 font-weight-900'>마케터 says</div>
          <div className='h4'>
            1. 소비자들에게 영향력 있는 SNS채널을 만들기 위해서는 어떻게 해야
            하나요?
          </div>
          <div className='h4'>
            2. 효율적인 마케팅을 하기 위해 어떤 채널에 집중해야 할 지
            모르겠어요.
          </div>
          <div className='h4'>
            3. 웹사이트에 고객 트래픽을 높이고, 구매량을 높이려면 어떻게 해야
            할까요?
          </div>
        </Col>
      </Row>
    </Container>
  );
}

export default DM9Say;
