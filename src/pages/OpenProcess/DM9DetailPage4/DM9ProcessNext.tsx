import React from 'react';

import { Container } from 'reactstrap';

import styled, { DefaultTheme } from 'styled-components';

const IconDiv = styled.div<DefaultTheme>`
  display: inline-block;
  position: relative;
  top: 4px;
`;

function DM9ProcessNext() {
  return (
    <div className='wrapper py-9'>
      <Container className='container-lg'>
        <div className='h2 font-weight-900 mb-5 text-center'>
          이 과정이 끝나면?
        </div>
        <div className='h4 pl-6'>
          <IconDiv>
            <i className='ni ni-check-bold' />
          </IconDiv>
          <span>
            <span className='font-weight-900'>자사와 경쟁사 SNS채널 비교</span>
            를 통한 채널 경쟁력을 파악합니다
          </span>
        </div>
        <div className='h4 pl-6'>
          <IconDiv>
            <i className='ni ni-check-bold' />
          </IconDiv>
          <span>
            자사 웹사이트, MALL 분석을 통한{' '}
            <span className='font-weight-900'>고객 유입 방안을 분석</span>
            합니다.
          </span>
        </div>
        <div className='h4 pl-6'>
          <IconDiv>
            <i className='ni ni-check-bold' />
          </IconDiv>
          <span>
            자사 채널의
            <span className='font-weight-900'>
              디지털 영향력을 종합 분석(DHC)
            </span>
            해 효율적 운영 전략을 수립해봅니다
          </span>
        </div>
      </Container>
    </div>
  );
}

export default DM9ProcessNext;
