import React from 'react';
import { useHistory } from 'react-router-dom';

// reactstrap components
import {
  Row,
  Col,
  Container,
  Button,
  TabPane,
  TabContent,
  Nav,
  NavItem,
  NavLink,
  Card,
} from 'reactstrap';

import MembershipProcess from './MembershipProcess';
import MembershipLineUp from './MembershipLineUp';
import MembershipInfo from './MembershipInfo';

interface MembershipTabsTypes {
  tabs: string;
}

function MembershipTab(props: MembershipTabsTypes) {
  const { tabs } = props;
  const history = useHistory();
  const selectedTabs = tabs;
  const handleClick = (path: string) => {
    history.push(path);
  };

  return (
    <Container className='container-lg'>
      <div className='nav-wrapper'>
        <Nav className='nav-fill flex-row' pills role='tablist'>
          <NavItem>
            <NavLink
              className={`mb-sm-0 mb-md-0 ${
                selectedTabs === '/openProcess/mmc' ? 'active' : ''
              }`}
              href='#pablo'
              onClick={(e) => {
                e.preventDefault();
                handleClick('/openProcess/mmc');
              }}
            >
              진행방식
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={`mb-sm-0 mb-md-0 ${
                selectedTabs === '/openProcess/mmc/lineUp' ? 'active' : ''
              }`}
              href='#pablo'
              onClick={(e) => {
                e.preventDefault();
                handleClick('/openProcess/mmc/lineUp');
              }}
            >
              라인업
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={`mb-sm-0 mb-md-0 ${
                selectedTabs === '/openProcess/mmc/info' ? 'active' : ''
              }`}
              href='#pablo'
              onClick={(e) => {
                e.preventDefault();
                handleClick('/openProcess/mmc/info');
              }}
            >
              안내사항
            </NavLink>
          </NavItem>
        </Nav>
      </div>
      <TabContent id='myTabContent' activeTab={selectedTabs}>
        {selectedTabs === '/openProcess/mmc' && (
          <TabPane tabId='/openProcess/mmc' role='tabpanel'>
            <MembershipProcess />
          </TabPane>
        )}
        {selectedTabs === '/openProcess/mmc/lineUp' && (
          <TabPane tabId='/openProcess/mmc/lineUp' role='tabpanel'>
            <MembershipLineUp />
          </TabPane>
        )}
        {selectedTabs === '/openProcess/mmc/info' && (
          <TabPane tabId='/openProcess/mmc/info' role='tabpanel'>
            <MembershipInfo />
          </TabPane>
        )}
      </TabContent>
      <Card
        className='px-4 py-1'
        style={{ position: 'sticky', bottom: '30px', zIndex: 10000 }}
      >
        <Row className='align-items-center'>
          <Col>
            <div
              className='h4 font-weight-700 pt-2'
              style={{ fontSize: '1.3rem' }}
            >
              마케터가 데이터를 활용하는 법
            </div>
            <div className='h5' style={{ fontSize: '1rem' }}>
              3월 16일 화요일
            </div>
          </Col>
          <Col className='text-right mr-4'>
            <Button
              className='btn'
              color='primary'
              onClick={() => console.log('Clicked Button')}
            >
              <span className='h4 font-weight-bold text-white'>
                마대리 멤버십 클럽 신청하기
              </span>
            </Button>
          </Col>
        </Row>
      </Card>
    </Container>
  );
}

export default MembershipTab;
