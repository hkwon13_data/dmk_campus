import React from 'react';
import { RouteComponentProps } from 'react-router-dom';

import CommonNavbar from 'components/NavBar/CommonNavbar';
import CommonFooter from 'components/NavBar/CommonFooter';
import { ScrollTopButton } from 'components/utils/ScrollTopButton';
import FloatingButton from '../../components/NavBar/FloatingButton';

import MembershipHeader from './MembershipHeader';
import MembershipTab from './MembershipTab';

export default function MMCPage({ match }: RouteComponentProps) {
  const selectedTabs = match.path;

  // React.useEffect(() => {
  // 	let classList = document.body.classList;
  // 	document.body.classList.remove(...classList);
  // 	document.body.classList.add("index-page");
  // 	window.scrollTo(0, 0);
  // 	return function cleanup() {
  // 		document.body.classList.remove("index-page");
  // 	};
  // }, []);

  return (
    <>
      <ScrollTopButton />
      <CommonNavbar type='transparent' />
      <FloatingButton />
      <div className='wrapper'>
        <MembershipHeader />
        <MembershipTab tabs={selectedTabs} />
      </div>
      <CommonFooter />
    </>
  );
}
