import React from 'react';

import { Row, Col, Container, Card, CardBody } from 'reactstrap';

import ImgA from 'assets/img/online_mamemclub/processImg_left.jpg';
import ImgB from 'assets/img/online_mamemclub/processImg_right.jpg';

function MembershipProcess() {
  return (
    <div>
      <Container className='container-xl'>
        <Card className='p-4'>
          <CardBody>
            <Row className='mb-4'>
              <Col>
                <img
                  src={ImgA}
                  alt='left'
                  style={{ width: '100%', height: '90%' }}
                />
              </Col>
              <Col>
                <img
                  src={ImgB}
                  alt='right'
                  style={{ width: '100%', height: '90%' }}
                />
              </Col>
            </Row>
            <div className='text-center h4 mb-0'>
              마케터가 데이터를 사용하는 법!
            </div>
            <div className='text-center h4 font-weight-900 mb-6'>
              최고의 데이터마케터와 함께하는 세미나
            </div>
            <Row>
              <Col lg='6' className='text-center'>
                {' '}
                <span
                  style={{
                    borderRadius: '30px',
                    display: 'inline-block',
                    background: '#eceff2',
                  }}
                  className='font-weight-900 h4 px-4 py-2 mb-2'
                >
                  Session 1
                </span>
                <div className='font-weight-900 h4'>Data Insight</div>
                <div className='font-weight-700 h4 mb-6'>18:30 ~ 19:30</div>
              </Col>
              <Col lg='6' className='text-center'>
                {' '}
                <span
                  style={{
                    borderRadius: '30px',
                    display: 'inline-block',
                    background: '#eceff2',
                  }}
                  className='font-weight-900 h4 px-4 py-2 mb-2'
                >
                  Session 2
                </span>
                <div className='font-weight-900 h4 text-dark'>
                  Marketing Insight
                </div>
                <div className='font-weight-700 h4'>19:30 ~ 20:30</div>
              </Col>
            </Row>
            {/* <div className="text-dark mb-6 text-center"> */}
            {/* <Col className="text-center text-dark"> */}
            {/* </Col> */}
            {/* <Col className="text-center text-dark" style={{borderLeft :'1px solid black'}}> */}
            {/* </Col> */}
            {/* </div> */}
            {/* <Row className="mb-4">
                            <Col><img src={ImgA} alt="left" style={{width : '100%', height : '90%'}}/></Col>
                            <Col><img src={ImgB} alt="right" style={{width : '100%', height : '90%'}}/></Col>
                        </Row> */}
            {/* <div className="text-center"> */}
            {/* <div className="font-weight-900 h4 mb-6">신청대상</div> */}
            {/* <span style={{borderRadius : '30px', display : 'inline-block', background : '#eceff2'}} className="font-weight-900 h4 px-5 py-2 mb-2">신청대상</span>
                            <div className="font-weight-500 h4">온라인강의, <span className="font-weight-900"> 누구나!</span></div>
                            <div className="font-weight-500 h4 mb-3">오프라인, <span className="font-weight-900"> 마대리 유료고객 대상 (선착순)</span></div> */}
            {/* <div className="font-weight-900 h4 mb-6"><span style={{background : '#2fc7d1', color : '#111111'}}>누구나</span></div>
                            <div className="font-weight-500 h4 mb-3">오프라인</div>
                            <div className="font-weight-900 h4"><span style={{background : '#2fc7d1', color : '#111111'}}>마대리 유료고객 대상 (선착순)</span></div> */}
            {/* </div> */}
          </CardBody>
        </Card>
      </Container>
    </div>
  );
}

export default MembershipProcess;
