import React from 'react';

import { Container, Row, Col } from 'reactstrap';

function DM9EduContent() {
  return (
    <div className='wrapper pt-5' style={{ background: '#ecf1ff' }}>
      <div className='h1 font-weight-900 mb-6 text-center'>교육 대상자</div>
      <Container>
        <Row className='justify-content-center align-items-center'>
          <Col
            lg='2'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderRight: 'none' }}
          >
            <div className='text-center text-primary h1 mb-0 p-2'>01</div>
          </Col>
          <Col
            lg='9'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderLeft: 'none' }}
          >
            <div className='p-1'>
              <div className='font-weight-700 h5'>
                데이터로 보는 고객분석 :{' '}
              </div>
              <div className='h5 mb-0'>
                내부 고객 정보를 통합하여 효율적으로 관리할 수 있습니다
              </div>
            </div>
          </Col>
        </Row>
        <div className='h3 mb-0 text-center py-3'>
          <i className='ni ni-bold-down' />
        </div>
        <Row className='justify-content-center align-items-center'>
          <Col
            lg='2'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderRight: 'none' }}
          >
            <div className='text-center text-primary h1 mb-0 p-2'>02</div>
          </Col>
          <Col
            lg='9'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderLeft: 'none' }}
          >
            <div className='p-1'>
              <div className='font-weight-700 h5'>
                통합 데이터 기반 고객 분석 :{' '}
              </div>
              <div className='h5 mb-0'>
                인구통계 데이터(DEMO), CRM 데이터, 멤버십 정보 통합 등의 고객
                데이터 연계 분석을 진행합니다
              </div>
            </div>
          </Col>
        </Row>
        <div className='h3 mb-0 text-center py-3'>
          <i className='ni ni-bold-down' />
        </div>
        <Row className='justify-content-center align-items-center'>
          <Col
            lg='2'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderRight: 'none' }}
          >
            <div className='text-center text-primary h1 mb-0 p-2'>03</div>
          </Col>
          <Col
            lg='9'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderLeft: 'none' }}
          >
            <div className='p-1'>
              <div className='font-weight-700 h5'>
                고객 행동패턴 데이터 수집 :{' '}
              </div>
              <div className='h5 mb-0'>
                기존 고객 데이터에 외부 데이터를 결합해 고객 행동 패턴을
                분석합니다
              </div>
            </div>
          </Col>
        </Row>
        <div className='h3 mb-0 text-center py-3'>
          <i className='ni ni-bold-down' />
        </div>
        <Row className='justify-content-center align-items-center pb-6'>
          <Col
            lg='2'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderRight: 'none' }}
          >
            <div className='text-center text-primary h1 mb-0 p-2'>04</div>
          </Col>
          <Col
            lg='9'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderLeft: 'none' }}
          >
            <div className='p-1'>
              <div className='font-weight-700 h5'>타겟 고객 정의 : </div>
              <div className='h5 mb-0'>
                고객 행동 데이터를 통해 고객 페르소나를 정의하고 고객 CDJ를
                재설계 합니다
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default DM9EduContent;
