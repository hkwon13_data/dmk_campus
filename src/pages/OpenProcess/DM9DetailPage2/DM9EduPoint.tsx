import React from 'react';

import { Container, Row, Col } from 'reactstrap';

function DM9EduPoint() {
  return (
    <>
      <div className='my-8'>
        <Container>
          <Row className='justify-content-center'>
            <Col className='mx-auto text-center my-5' lg='8'>
              <div className='h1 font-weight-900'>교육포인트</div>
            </Col>
          </Row>
          <Row className='justify-content-center mb-4'>
            <Col lg='5' className='p-4' style={{ border: '1px solid #333333' }}>
              <div className='h1 text-right mb-0'>1</div>
              <div className='h4 font-weight-900'>IT 비전공저도 OK,</div>
              <div className='h4'>기획자/마케터 관점에서 데이터를</div>
              <div className='h4'>분석하는 방법 제공합니다</div>
            </Col>
            <Col
              lg='5'
              className='p-4 ml-4'
              style={{ border: '1px solid #333333' }}
            >
              <div className='h1 text-right mb-0'>2</div>
              <div className='h4 font-weight-900'>고객의 행동패턴 분석</div>
              <div className='h4'>데이터를 활용해 타겟을 정의하고,</div>
              <div className='h4'>그들의 행동패턴을 분석해봅니다</div>
            </Col>
          </Row>
          <Row className='justify-content-center'>
            <Col lg='5' className='p-4' style={{ border: '1px solid #333333' }}>
              <div className='h1 text-right mb-0'>3</div>
              <div className='h4 font-weight-900'>진성고객발굴,</div>
              <div className='h4'>고객 데이터를 통합하여</div>
              <div className='h4'>진성고객을 발굴 할 수 있습니다</div>
            </Col>
            <Col
              lg='5'
              className='p-4 ml-4'
              style={{ border: '1px solid #333333' }}
            >
              <div className='h1 text-right mb-0'>4</div>
              <div className='h4 font-weight-900'>CDJ 마케팅 메시지 정비,</div>
              <div className='h4'>고객의 니즈를 바탕으로</div>
              <div className='h4'>고객 구매 여정(CDJ)을 재설계합니다</div>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
}

export default DM9EduPoint;
