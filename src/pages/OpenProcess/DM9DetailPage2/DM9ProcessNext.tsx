import React from 'react';

import { Container } from 'reactstrap';

import styled, { DefaultTheme } from 'styled-components';

const IconDiv = styled.div<DefaultTheme>`
  display: inline-block;
  position: relative;
  top: 4px;
`;

function DM9ProcessNext() {
  return (
    <div className='wrapper py-9'>
      <Container className='container-lg'>
        <div className='h2 font-weight-900 mb-5 text-center'>
          이 과정이 끝나면?
        </div>
        <div className='h4 pl-9'>
          <IconDiv>
            <i className='ni ni-check-bold' />
          </IconDiv>
          <span>
            체계적/종합적인{' '}
            <span className='font-weight-900'>고객 데이터 분석 및 정의</span>할
            수 있습니다.
          </span>
        </div>
        <div className='h4 pl-9'>
          <IconDiv>
            <i className='ni ni-check-bold' />
          </IconDiv>
          <span>
            내/외부 데이터를 활용한{' '}
            <span className='font-weight-900'>고객 행동패턴 분석</span>이
            가능합니다.
          </span>
        </div>
        <div className='h4 pl-9'>
          <IconDiv>
            <i className='ni ni-check-bold' />
          </IconDiv>
          <span>
            데이터를 통한{' '}
            <span className='font-weight-900'>진성고객발굴 및 Needs 파악</span>
            을 할 수 있습니다.
          </span>
        </div>
        <div className='h4 pl-9'>
          <IconDiv>
            <i className='ni ni-check-bold' />
          </IconDiv>
          <span>
            이를 통해{' '}
            <span className='font-weight-900'>
              고객 맞춤형 최적화된 마케팅 전략 구사
            </span>
            가 가능합니다.
          </span>
        </div>
      </Container>
    </div>
  );
}

export default DM9ProcessNext;
