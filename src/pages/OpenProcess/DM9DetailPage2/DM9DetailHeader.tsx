import React from 'react';

import ImgA from 'assets/img/dm9_domain/headerImg2.png';

import { Container, Row, Col } from 'reactstrap';

function DM9DetailHeader() {
  return (
    <>
      {/* <div className="page-header header-filter page-header-small skew-separator skew-mini"> */}
      <div className='page-header header-filter'>
        <div
          className='page-header-image'
          style={{
            backgroundImage: `url(${ImgA})`,
            filter: 'brightness(0.50)',
          }}
        />
        <Container className='container-lg pt-5'>
          <Row className='justify-content-center align-items-center'>
            <Col lg='8'>
              <div className='h1 font-weight-900 text-white'>DM9 2영역</div>
              <div
                className='font-weight-900 text-white mb-3'
                style={{ fontSize: '3rem' }}
              >
                고객 분석
              </div>
              <div className='h1 font-weight-500 text-white'>
                고객이 남긴 디지털 발자국을 보면
              </div>
              <div className='h1 font-weight-500 text-white'>
                진짜 내 고객을 알 수 있다!
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
}

export default DM9DetailHeader;
