import React from 'react';

import { Container, Row, Col } from 'reactstrap';

import ImgA from 'assets/img/dm9_domain/say_img.png';

function DM9Say() {
  return (
    <Container className='my-9'>
      <Row className='justify-content-center'>
        <Col lg='3' className='px-0'>
          <div
            className='image'
            style={{
              height: '350px',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'flex-end',
              backgroundColor: '#8df9c5',
            }}
          >
            <img className=' img-fluid' src={ImgA} alt='이미지' />
          </div>
        </Col>
        <Col lg='8' className='p-5' style={{ border: '3px solid #8df9c5' }}>
          <div className='h1 font-weight-900'>마케터 says</div>
          <div className='h4'>
            1. 우리 제품/서비스를 구매하는 고객은 누구이며, 어떤 특성이
            있을까요?
          </div>
          <div className='h4'>
            2. 우리 고객 중 몇 %가 충성 고객이고 몇 %가 일회성 고객일까요?
          </div>
          <div className='h4'>3. 고객 데이터는 어떻게 확보할 수 있을까요?</div>
          <div className='h4'>
            4. 충성 고객을 만들기 위해서는 고객 데이터를 어떻게 활용해야 할까요?
          </div>
        </Col>
      </Row>
    </Container>
  );
}

export default DM9Say;
