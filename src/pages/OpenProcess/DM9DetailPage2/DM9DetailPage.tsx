import React from 'react';

import CommonNavbar from 'components/NavBar/CommonNavbar';
import CommonFooter from 'components/NavBar/CommonFooter';
import FloatingButton from 'components/NavBar/FloatingButton';
import { ScrollTopButton } from 'components/utils/ScrollTopButton';

import DM9DetailHeader from './DM9DetailHeader';
import DM9EduPoint from './DM9EduPoint';
import DM9Say from './DM9Say';
import DM9EduContent from './DM9EduContent';
import DM9EduObject from './DM9EduObject';
import DM9ProcessNext from './DM9ProcessNext';
import DM9BottomBanner1 from './DM9BottomBanner1';

// import { Container, Row, Button } from 'reactstrap';

// import ImgA from 'assets/img/dm9_domain/headerImg1.jpg';

function DM9DetailPage() {
  React.useEffect(() => {
    // let classList = document.body.classList;
    // document.body.classList.remove(...classList);
    // document.body.classList.add('index-page');
    window.scrollTo(0, 0);
    // return function cleanup() {
    //   document.body.classList.remove('index-page');
    // };
  }, []);

  return (
    <div>
      <ScrollTopButton />
      <CommonNavbar type='white' img='black' />
      <FloatingButton />
      <div className='wrapper'>
        <DM9DetailHeader />
        <DM9EduPoint />
        <DM9Say />
        <DM9EduContent />
        <DM9EduObject />
        <DM9ProcessNext />
        <DM9BottomBanner1 />
      </div>
      <CommonFooter />
    </div>
  );
}

export default DM9DetailPage;
