import React from 'react';

import { Card, CardBody, Row, Col } from 'reactstrap';

function DM9EduObject() {
  return (
    <div className='wrapper py-9' style={{ background: '#dedffc' }}>
      <div className='h1 font-weight-900 mb-6 text-center'>교육 대상자</div>
      <Row className='justify-content-center'>
        <Col lg='3'>
          <Card>
            <CardBody>
              <div className='h5'>고객특성을 찾아 개인 최적회된</div>
              <div className='font-weight-900 h5'>
                마케팅 메시지를 기획하고자
              </div>
              <div className='font-weight-900 h5 mb-0'>하는 마케팅담당자</div>
              <div className='text-right'>
                <div className='icon-shape icon-xl mx-auto text-right'>
                  <i className='ni ni-circle-08' />
                </div>
              </div>
            </CardBody>
          </Card>
        </Col>
        <Col lg='3'>
          <Card>
            <CardBody>
              <div className='h5'>새로운 멤버십 서비스나</div>
              <div className='font-weight-900 h5'>
                연계 상품을 기획하고자 하는
              </div>
              <div className='font-weight-900 h5 mb-0'>마케팅 담당자</div>
              <div className='text-right'>
                <div className='icon-shape icon-xl mx-auto text-right'>
                  <i className='ni ni-circle-08' />
                </div>
              </div>
            </CardBody>
          </Card>
        </Col>
        <Col lg='3'>
          <Card>
            <CardBody>
              <div className='h5'>새로운 상품/서비스 기획에</div>
              <div className='font-weight-900 h5'>
                <span>맞춰</span> 타겟 고객을 정의하고자
              </div>
              <div className='font-weight-900 h5 mb-0'>하는 기획자</div>
              <div className='text-right'>
                <div className='icon-shape icon-xl mx-auto text-right'>
                  <i className='ni ni-circle-08' />
                </div>
              </div>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  );
}

export default DM9EduObject;
