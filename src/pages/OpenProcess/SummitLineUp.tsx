import React from 'react';

import { Row, Container, Col } from 'reactstrap';

import ImgA from 'assets/img/offline_sumit/sinhosang.png';
import ImgB from 'assets/img/offline_sumit/choiwonjoon.png';
import ImgC from 'assets/img/offline_sumit/choijungyoon.png';
import ImgD from 'assets/img/offline_sumit/hwangbohyen.png';

function SummitLineUp() {
  return (
    <Container>
      <div className='wrapper bg-secondary p-5 py-6 text-center'>
        <div className='h1'>Data Marketing Summit 2020</div>
        <div className='font-weight-900 h3'>데이터는 사람을 행동하게 한다</div>
        <Row>
          <Col>
            <img src={ImgA} alt='img' width='100%' />
          </Col>
          <Col>
            <img src={ImgB} alt='img' width='100%' />
          </Col>
        </Row>
        <Row>
          <Col>
            <img src={ImgC} alt='img' width='100%' />
          </Col>
          <Col>
            <img src={ImgD} alt='img' width='100%' />
          </Col>
        </Row>
      </div>
    </Container>
  );
}

export default SummitLineUp;
