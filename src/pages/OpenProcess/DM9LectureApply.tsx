import React from 'react';

import {
  Button,
  Card,
  CardImg,
  CardBody,
  CardTitle,
  Container,
  Row,
  Col,
} from 'reactstrap';

import imgD from 'assets/img/sections/unsplashs.jpg';

const style = { width: '18rem' };

interface DM9LectureApplyDataTypes {
  subtitle: string;
  title: string;
  period: string;
  day: string;
  time: string;
}

function DM9LectureApply() {
  const onlineData: DM9LectureApplyDataTypes[] = [
    {
      subtitle: 'DM9_2영역 현업필수 트렌드 시장분석',
      title: '한 눈에 보는 브랜드트렌드 분석 - DM9필수영역',
      period: '2021.05.13 ~ 2021.05.30',
      day: '2021.06.11(금)',
      time: '오후 13:00 ~ 16:00',
    },
    {
      subtitle: 'DM9_2영역 현업필수 트렌드 시장분석',
      title: '한 눈에 보는 브랜드트렌드 분석 - DM9필수영역',
      period: '2021.05.13 ~ 2021.05.30',
      day: '2021.06.11(금)',
      time: '오후 13:00 ~ 16:00',
    },
    {
      subtitle: 'DM9_2영역 현업필수 트렌드 시장분석',
      title: '한 눈에 보는 브랜드트렌드 분석 - DM9필수영역',
      period: '2021.05.13 ~ 2021.05.30',
      day: '2021.06.11(금)',
      time: '오후 13:00 ~ 16:00',
    },
    {
      subtitle: 'DM9_2영역 현업필수 트렌드 시장분석',
      title: '한 눈에 보는 브랜드트렌드 분석 - DM9필수영역',
      period: '2021.05.13 ~ 2021.05.30',
      day: '2021.06.11(금)',
      time: '오후 13:00 ~ 16:00',
    },
  ];

  const offlineData: DM9LectureApplyDataTypes[] = [
    {
      subtitle: 'DM9_2영역 현업필수 트렌드 시장분석',
      title: '한 눈에 보는 브랜드트렌드 분석 - DM9필수영역',
      period: '2021.05.13 ~ 2021.05.30',
      day: '2021.06.11(금)',
      time: '오후 13:00 ~ 16:00',
    },
    {
      subtitle: 'DM9_2영역 현업필수 트렌드 시장분석',
      title: '한 눈에 보는 브랜드트렌드 분석 - DM9필수영역',
      period: '2021.05.13 ~ 2021.05.30',
      day: '2021.06.11(금)',
      time: '오후 13:00 ~ 16:00',
    },
    {
      subtitle: 'DM9_2영역 현업필수 트렌드 시장분석',
      title: '한 눈에 보는 브랜드트렌드 분석 - DM9필수영역',
      period: '2021.05.13 ~ 2021.05.30',
      day: '2021.06.11(금)',
      time: '오후 13:00 ~ 16:00',
    },
    {
      subtitle: 'DM9_2영역 현업필수 트렌드 시장분석',
      title: '한 눈에 보는 브랜드트렌드 분석 - DM9필수영역',
      period: '2021.05.13 ~ 2021.05.30',
      day: '2021.06.11(금)',
      time: '오후 13:00 ~ 16:00',
    },
  ];

  const renderEduCard = (data: DM9LectureApplyDataTypes[]) => {
    return data.map((item, index) => {
      return (
        // eslint-disable-next-line react/no-array-index-key
        <Col lg='3' md='6' xs='12' key={index}>
          <Card className='p-0' style={style}>
            <CardImg alt='...' src={imgD} top />
            <CardBody>
              <small className='font-weight-700 text-primary'>
                {item.subtitle}
              </small>
              <CardTitle className='font-weight-900 mt-3' tag='h5'>
                {item.title}
              </CardTitle>
              <div className='mb-4'>
                <Row>
                  <div className='text-muted float-left pl-3'>
                    <small>수강신청기간</small>
                  </div>
                  <div style={{ alignSelf: 'center' }}>
                    <small className='float-right pl-3'>{item.period}</small>
                  </div>
                </Row>
                <Row>
                  <div className='text-muted float-left pl-3'>
                    <small>교육일</small>
                  </div>
                  <div
                    className='float-right pl-5'
                    style={{ alignSelf: 'center' }}
                  >
                    <small style={{ paddingLeft: '2px' }}>{item.day}</small>
                  </div>
                </Row>
                <Row>
                  <div className='text-muted float-left pl-3'>
                    <small>교육 시간</small>
                  </div>
                  <div
                    style={{ alignSelf: 'center' }}
                    className='float-right pl-4'
                  >
                    <small style={{ paddingLeft: '10px' }}>{item.time}</small>
                  </div>
                </Row>
              </div>
              <Row className='justify-content-center'>
                <Button
                  className='col-10'
                  color='primary'
                  href='#pablo'
                  onClick={(e) => e.preventDefault()}
                >
                  오픈 알림 신청하기
                </Button>
              </Row>
            </CardBody>
          </Card>
        </Col>
      );
    });
  };

  return (
    <>
      <Container className='my-5'>
        <div className='mt-5 mb-4'>
          <div className='h7 font-weight-800 mb-3'>진행 중 과정리스트</div>
        </div>

        {/* <div className="card-deck"> */}
        {/* <Card>
					<CardImg alt="..." src={imgD} top />
					<CardBody>
						<div>
							<small className="text-primary font-weight-700">
								DM9_2영역 현업필수 트렌드 시장분석
							</small>
						</div>
						<CardTitle tag="h6" className="font-weight-800 py-2">
							한 눈에 보는 브랜드트렌드 분석-DM9필수영역
						</CardTitle>
						<Row>
							<div className="text-muted float-left pl-3">
								<small>수강신청기간</small>
							</div>
							<div style={{ alignSelf: "center" }}>
								<small className="float-right pl-3">
									{lecture_data.period}
								</small>
							</div>
						</Row>
						<Row>
							<div className="text-muted float-left pl-3">
								<small>교육일</small>
							</div>
							<div className="float-right pl-5" style={{ alignSelf: "center" }}>
								<small>{lecture_data.day}</small>
							</div>
						</Row>
						<Row>
							<div className="text-muted float-left pl-3">
								<small>교육 시간</small>
							</div>
							<div style={{ alignSelf: "center" }} className="float-right pl-4">
								<small style={{ paddingLeft: "10px" }}>
									{lecture_data.time}
								</small>
							</div>
						</Row>
					</CardBody>
					<Button color="primary" name="button" type="button">
						수강신청하기
					</Button>
				</Card> */}
        <Row className=''>
          {renderEduCard(onlineData)}
          {/* <Card style={style}>
						<CardImg alt="..." src={imgD} top></CardImg>
						<CardBody>
							<small className="font-weight-700 text-primary">{lecture_data.subtitle}</small>
							<CardTitle tag="h5">{lecture_data.title}</CardTitle>
							<CardText>
							<Row>
								<div className="text-muted float-left pl-3">
									<small>수강신청기간</small>
								</div>
								<div style={{ alignSelf: "center" }}>
									<small className="float-right pl-3">
										{lecture_data.period}
									</small>
								</div>
							</Row>
								<Row>
									<div className="text-muted float-left pl-3">
										<small>교육일</small>
									</div>
									<div className="float-right pl-5" style={{ alignSelf: "center" }}>
										<small>{lecture_data.day}</small>
									</div>
								</Row>
								<Row>
									<div className="text-muted float-left pl-3">
										<small>교육 시간</small>
									</div>
									<div style={{ alignSelf: "center" }} className="float-right pl-4">
										<small style={{ paddingLeft: "10px" }}>
											{lecture_data.time}
										</small>
									</div>
								</Row>
							</CardText>
							<Row className="justify-content-center">
								<Button
									className="col-10"
									color="primary"
									href="#pablo"
									onClick={(e) => e.preventDefault()}>
									Go somewhere
								</Button>
							</Row>
						</CardBody>
					</Card> */}
          {/* <Card style={style}>
						<CardImg alt="..." src={imgD} top></CardImg>
						<CardBody>
							<CardTitle>Card title</CardTitle>
							<CardText>
								Some quick example text to build on the card title and make up
								the bulk of the card's content.
							</CardText>
							<Button
								color="primary"
								href="#pablo"
								onClick={(e) => e.preventDefault()}>
								Go somewhere
							</Button>
						</CardBody>
					</Card>
					<Card style={style}>
						<CardImg alt="..." src={imgD} top></CardImg>
						<CardBody>
							<CardTitle>Card title</CardTitle>
							<CardText>
								Some quick example text to build on the card title and make up
								the bulk of the card's content.
							</CardText>
							<Button
								color="primary"
								href="#pablo"
								onClick={(e) => e.preventDefault()}>
								Go somewhere
							</Button>
						</CardBody>
					</Card>
					<Card style={style}>
						<CardImg alt="..." src={imgD} top></CardImg>
						<CardBody>
							<CardTitle>Card title</CardTitle>
							<CardText>
								Some quick example text to build on the card title and make up
								the bulk of the card's content.
							</CardText>
							<Button
								color="primary"
								href="#pablo"
								onClick={(e) => e.preventDefault()}>
								Go somewhere
							</Button>
						</CardBody>
					</Card>
					<Card style={style}>
						<CardImg alt="..." src={imgD} top></CardImg>
						<CardBody>
							<CardTitle>Card title</CardTitle>
							<CardText>
								Some quick example text to build on the card title and make up
								the bulk of the card's content.
							</CardText>
							<Button
								color="primary"
								href="#pablo"
								onClick={(e) => e.preventDefault()}>
								Go somewhere
							</Button>
						</CardBody>
					</Card> */}
        </Row>
        <div className='mt-5 mb-4'>
          <div className='h7 font-weight-800 mb-3'>출시 준비 중 과정리스트</div>
        </div>
        <Row>{renderEduCard(offlineData)}</Row>
      </Container>
    </>
  );
}

export default DM9LectureApply;
