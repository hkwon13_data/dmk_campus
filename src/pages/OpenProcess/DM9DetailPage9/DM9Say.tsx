import React from 'react';

import { Container, Row, Col } from 'reactstrap';

import ImgA from 'assets/img/dm9_domain/say_img.png';

function DM9Say() {
  return (
    <Container className='my-9'>
      <Row className='justify-content-center'>
        <Col lg='3' className='px-0'>
          <div
            className='image'
            style={{
              height: '350px',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'flex-end',
              backgroundColor: '#8df9c5',
            }}
          >
            <img className=' img-fluid' src={ImgA} alt='이미지' />
          </div>
        </Col>
        <Col lg='9' className='p-5' style={{ border: '3px solid #8df9c5' }}>
          <div className='h1 font-weight-900'>마케터 says</div>
          <div className='h4'>
            1. 데이터를 자의적으로 해석하고 있는 것은 아닌지 의구심이 들어요
          </div>
          <div className='h4'>
            2. 활용하는 데이터가 상표권, 저작권 등에 위반되지 않는지 모르겠어요
          </div>
          <div className='h4'>
            3. 개인정보를 활용해 마케팅을 하고 싶은데 어떤 기준으로 어떤
            정보까지 가능한지 모르겠어요
          </div>
        </Col>
      </Row>
    </Container>
  );
}

export default DM9Say;
