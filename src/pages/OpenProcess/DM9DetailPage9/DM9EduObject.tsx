import React from 'react';

import { Card, CardBody, Row, Col } from 'reactstrap';

function DM9EduObject() {
  return (
    <div className='wrapper py-9' style={{ background: '#dedffc' }}>
      <div className='h1 font-weight-900 mb-6 text-center'>교육 대상자</div>
      <Row className='justify-content-center'>
        <Col lg='3'>
          <Card>
            <CardBody>
              <div className='h5'>Fact에 기반하여 데이터를 해석</div>
              <div className='h5'>하고 있는지 궁금한 실무 담당자</div>
              <div className='h5 mb-0'>&nbsp;</div>
              <div className='text-right'>
                <div className='icon-shape icon-xl mx-auto text-right'>
                  <i className='ni ni-circle-08' />
                </div>
              </div>
            </CardBody>
          </Card>
        </Col>
        <Col lg='3'>
          <Card>
            <CardBody>
              <div className='h5'>개인정보를 활용해 마케팅 기획을</div>
              <div className='h5'>하고 싶은 기획자</div>
              <div className='h5 mb-0'>&nbsp;</div>
              <div className='text-right'>
                <div className='icon-shape icon-xl mx-auto text-right'>
                  <i className='ni ni-circle-08' />
                </div>
              </div>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  );
}

export default DM9EduObject;
