import React from 'react';

import { Container, Row, Col } from 'reactstrap';

function DM9EduContent() {
  return (
    <div className='wrapper pt-5' style={{ background: '#ecf1ff' }}>
      <div className='h1 font-weight-900 mb-6 text-center'>학습 내용</div>
      <Container>
        <Row className='justify-content-center align-items-center'>
          <Col
            lg='2'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderRight: 'none' }}
          >
            <div className='text-center text-primary h1 mb-0 p-2'>01</div>
          </Col>
          <Col
            lg='7'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderLeft: 'none' }}
          >
            <div className='p-1'>
              <div className='font-weight-700 h5'>Fact 기반 데이터 해석 : </div>
              <div className='h5 mb-0'>
                데이터를 조작하지 않고 Fact 기반으로 해석하기 위한 방법을
                학습합니다
              </div>
            </div>
          </Col>
        </Row>
        <div className='h3 mb-0 text-center py-3'>
          <i className='ni ni-bold-down' />
        </div>
        <Row className='justify-content-center align-items-center'>
          <Col
            lg='2'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderRight: 'none' }}
          >
            <div className='text-center text-primary h1 mb-0 p-2'>02</div>
          </Col>
          <Col
            lg='7'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderLeft: 'none' }}
          >
            <div className='p-1'>
              <div className='font-weight-700 h5'>
                데이터 활용 유형 별 법적 기준 :{' '}
              </div>
              <div className='h5 mb-0'>
                데이터 윤리를 준수하기 위한 법적 기준과 최신 데이터 정책을
                학습합니다
              </div>
            </div>
          </Col>
        </Row>
        <div className='h3 mb-0 text-center py-3'>
          <i className='ni ni-bold-down' />
        </div>
        <Row className='justify-content-center align-items-center'>
          <Col
            lg='2'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderRight: 'none' }}
          >
            <div className='text-center text-primary h1 mb-0 p-2'>03</div>
          </Col>
          <Col
            lg='7'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderLeft: 'none' }}
          >
            <div className='p-1'>
              <div className='font-weight-700 h5'>
                개인정보 활용과 마이데이터 :{' '}
              </div>
              <div className='h5 mb-0'>
                고객 데이터 활용이 가능한 법적 기준과 활용방안을 학습합니다
              </div>
            </div>
          </Col>
        </Row>
        <div className='h3 mb-0 text-center py-3'>
          <i className='ni ni-bold-down' />
        </div>
        <Row className='justify-content-center align-items-center pb-6'>
          <Col
            lg='2'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderRight: 'none' }}
          >
            <div className='text-center text-primary h1 mb-0 p-2'>04</div>
          </Col>
          <Col
            lg='7'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderLeft: 'none' }}
          >
            <div className='p-1'>
              <div className='font-weight-700 h5'>데이터 윤리의 활용 : </div>
              <div className='h5 mb-0'>
                다양한 사례 학습을 통해 윤리적이고 슬기로운 데이터 분석을 할 수
                있습니다
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default DM9EduContent;
