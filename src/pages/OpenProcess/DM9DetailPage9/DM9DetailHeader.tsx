/* eslint-disable react/no-unescaped-entities */
import React from 'react';

import ImgA from 'assets/img/dm9_domain/headerImg9.png';

import { Container, Row, Col } from 'reactstrap';

function DM9DetailHeader() {
  return (
    <>
      {/* <div className="page-header header-filter page-header-small skew-separator skew-mini"> */}
      <div className='page-header header-filter'>
        <div
          className='page-header-image'
          style={{
            backgroundImage: `url(${ImgA})`,
            filter: 'brightness(0.50)',
          }}
        />
        <Container className='container-lg pt-5'>
          <Row className='justify-content-center align-items-center'>
            <Col lg='11'>
              <div className='h1 font-weight-900 text-white'>DM9 9영역</div>
              <div
                className='font-weight-900 text-white mb-3'
                style={{ fontSize: '3rem' }}
              >
                데이터 윤리
              </div>
              <div className='h3 font-weight-500 text-white'>
                '아'다르고 '어'다른 데이터,
              </div>
              <div className='h3 font-weight-500 text-white'>
                데이터에 속지말고, 속이지도 않는 데이터 윤리를 배우셔야 합니다.
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
}

export default DM9DetailHeader;
