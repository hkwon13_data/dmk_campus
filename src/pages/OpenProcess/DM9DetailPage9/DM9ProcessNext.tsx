import React from 'react';

import { Container } from 'reactstrap';

import styled, { DefaultTheme } from 'styled-components';

const IconDiv = styled.div<DefaultTheme>`
  display: inline-block;
  position: relative;
  top: 4px;
`;

function DM9ProcessNext() {
  return (
    <div className='wrapper py-9'>
      <Container className='container-md'>
        <div className='h2 font-weight-900 mb-6 text-center'>
          이 과정이 끝나면?
        </div>
        <div className='h4 pl-6'>
          <IconDiv>
            <i className='ni ni-check-bold' />
          </IconDiv>
          <span>
            객관적으로{' '}
            <span className='font-weight-900'>
              데이터를 해석하는 기준 및 방법을 습득
            </span>
            합니다
          </span>
        </div>
        <div className='h4 pl-6'>
          <IconDiv>
            <i className='ni ni-check-bold' />
          </IconDiv>
          <span>
            <span className='font-weight-900'>
              데이터 활용 법적 기준 및 사례를 학습{' '}
            </span>
            합니다
          </span>
        </div>
        <div className='h4 pl-6'>
          <IconDiv>
            <i className='ni ni-check-bold' />
          </IconDiv>
          <span>
            <span className='font-weight-900'>
              개인정보 처리단계를 이해하고 활용
            </span>{' '}
            할 수 있습니다
          </span>
        </div>
      </Container>
    </div>
  );
}

export default DM9ProcessNext;
