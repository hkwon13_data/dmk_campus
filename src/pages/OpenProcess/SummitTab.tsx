import React from 'react';
import { useHistory } from 'react-router-dom';

import {
  Row,
  Col,
  Container,
  Button,
  TabPane,
  TabContent,
  Nav,
  NavItem,
  NavLink,
  Card,
} from 'reactstrap';

import SummitInfo from './SummitInfo';
import SummitLineUp from './SummitLineUp';
import SummitAdvantages from './SummitAdvantages';
import SummitInquiry from './SummitInquiry';

interface SummitTabsTypes {
  tabs: string;
}

function SummitTab(props: SummitTabsTypes) {
  const { tabs } = props;
  const history = useHistory();
  const handleClick = (path: string) => {
    history.push(path);
  };

  const selectedTabs = tabs;

  return (
    <Container>
      <div className='nav-wrapper'>
        <Nav className='nav-fill flex-row mb-3 mt-3' pills role='tablist'>
          <NavItem>
            <NavLink
              className={`mb-sm-0 mb-md-0 ${
                selectedTabs === '/openProcess/summit' ? 'active' : ''
              }`}
              href='#pablo'
              onClick={(e) => {
                e.preventDefault();
                handleClick('/openProcess/summit');
              }}
            >
              소개
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={`mb-sm-0 mb-md-0 ${
                selectedTabs === '/openProcess/summit/lineUp' ? 'active' : ''
              }`}
              href='#pablo'
              onClick={(e) => {
                e.preventDefault();
                handleClick('/openProcess/summit/lineUp');
              }}
            >
              라인업
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={`mb-sm-0 mb-md-0 ${
                selectedTabs === '/openProcess/summit/advantages'
                  ? 'active'
                  : ''
              }`}
              href='#pablo'
              onClick={(e) => {
                e.preventDefault();
                handleClick('/openProcess/summit/advantages');
              }}
            >
              특장점
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={`mb-sm-0 mb-md-0 ${
                selectedTabs === '/openProcess/summit/inquiry' ? 'active' : ''
              }`}
              href='#pablo'
              onClick={(e) => {
                e.preventDefault();
                handleClick('/openProcess/summit/inquiry');
              }}
            >
              문의
            </NavLink>
          </NavItem>
        </Nav>
      </div>
      <TabContent id='myTabContent' activeTab={selectedTabs}>
        {selectedTabs === '/openProcess/summit' && (
          <TabPane tabId='/openProcess/summit' role='tabpanel'>
            <SummitInfo />
          </TabPane>
        )}
        {selectedTabs === '/openProcess/summit/lineUp' && (
          <TabPane tabId='/openProcess/summit/lineUp' role='tabpanel'>
            <SummitLineUp />
          </TabPane>
        )}
        {selectedTabs === '/openProcess/summit/advantages' && (
          <TabPane tabId='/openProcess/summit/advantages' role='tabpanel'>
            <SummitAdvantages />
          </TabPane>
        )}
        {selectedTabs === '/openProcess/summit/inquiry' && (
          <TabPane tabId='/openProcess/summit/inquiry' role='tabpanel'>
            <SummitInquiry />
          </TabPane>
        )}
      </TabContent>
      <Card
        className='px-4 py-3 mt-6'
        style={{ position: 'sticky', bottom: '30px', zIndex: 10000 }}
      >
        <Row className='align-items-center'>
          <Col>
            <div className='h4 font-weight-700'>
              {' '}
              2020 DMS :: 데이터는 사람을 행동하게 한다
            </div>
            <div className='h5'>2020. 11. 05(목) 13:00~18:00</div>
          </Col>
          <Col className='text-right mr-4'>
            <Button
              className='btn'
              color='primary'
              disabled
              onClick={() => console.log('Clicked Button')}
            >
              <span className='h4 font-weight-bold text-white'>
                SUMMIT 신청하기
              </span>
            </Button>
          </Col>
        </Row>
      </Card>
    </Container>
  );
}

export default SummitTab;
