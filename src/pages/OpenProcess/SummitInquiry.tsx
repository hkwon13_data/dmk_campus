import React from 'react';

import { Container, Card, CardBody, Row, Col } from 'reactstrap';

function SummitInquiry() {
  return (
    <Container>
      <Card>
        <CardBody className='text-center py-5'>
          <div className='h2 font-weight-700 mb-5'>문의</div>
          <Container className='container-md'>
            <Row>
              <Col className='mb-4'>
                <a href='#pablo' onClick={(e) => e.preventDefault()}>
                  <div className='icon icon-shape icon-shape-primary icon-md rounded-circle text-white'>
                    <div className='icon-numbers text-blue'>
                      <i className='ni ni-email-83' />
                    </div>
                  </div>
                </a>
                <div className='mt-3'>Email</div>
                <div>dataedu@datamarketing.co.kr</div>
              </Col>
              <Col>
                <a href='#pablo' onClick={(e) => e.preventDefault()}>
                  <div className='icon icon-shape icon-shape-primary icon-md rounded-circle text-white'>
                    <div className='icon-numbers text-blue'>
                      <i className='ni ni-mobile-button' />
                    </div>
                  </div>
                </a>
                <div className='mt-3'>Phone</div>
                <div>02-6011-5407, 5418</div>
              </Col>
            </Row>
          </Container>
        </CardBody>
      </Card>
    </Container>
  );
}

export default SummitInquiry;
