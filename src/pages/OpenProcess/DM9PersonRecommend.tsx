import React from 'react';

import { Row, Col } from 'reactstrap';

import ImgB from 'assets/img/oflline_dm9/recommend_one.png';
import ImgC from 'assets/img/oflline_dm9/recommend_two.png';
import ImgD from 'assets/img/oflline_dm9/recommend_three.png';

// import ImgB from "assets/img/online_mamemclub/processImg_right.JPG"

function DM9PersonRecommend() {
  return (
    <div className='wrapper py-9' style={{ background: '#e9eeff' }}>
      <div className='h1 font-weight-900 mb-6 text-center'>
        이런 분께 추천 드립니다
      </div>
      <Row className='justify-content-center'>
        <Col lg='3' className='text-center'>
          <div className='mb-3'>
            <img src={ImgB} alt='...' width='50%' height='50%' />
          </div>
          <div className='text-dark h5'>
            <div>마케팅부터 기획까지 이행,</div>
            <div>성과측정까지 프로세스를</div>
            <div>
              배우고 싶은{' '}
              <span className='h4 font-weight-900 text-dark'>예비 마케터</span>
            </div>
          </div>
        </Col>
        <Col lg='3' className='text-center'>
          <div className='mb-3'>
            <img src={ImgC} alt='...' width='50%' height='50%' />
          </div>
          <div className='text-dark h5'>
            <div>마케팅 퍼포먼스를 높이고</div>
            <div>싶으나 어려움이 있는</div>
            <div className='h4 font-weight-900 text-dark'>
              Hard working 마케터
            </div>
          </div>
        </Col>
        <Col lg='3' className='text-center'>
          <div className='mb-3'>
            <img src={ImgD} alt='...' width='50%' height='50%' />
          </div>
          <div className='text-dark h5'>
            <div>전사 DT 역량 강화를 위한</div>
            <div className='h4 font-weight-900 text-dark mb-0'>
              교육 커리큘럼이
            </div>
            <div className='h4 font-weight-900 text-dark'>필요한 기업</div>
          </div>
        </Col>
      </Row>
    </div>
  );
}

export default DM9PersonRecommend;
