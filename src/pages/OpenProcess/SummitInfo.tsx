import React from 'react';

import { Container } from 'reactstrap';

import ImgA from 'assets/img/offline_sumit/infoImg.png';

function SummitInfo() {
  return (
    <Container>
      <div
        className='text-center py-7'
        style={{ background: '#150c48', color: '#ebedef' }}
      >
        <div className='h1' style={{ color: '#ebedef' }}>
          Data Marketing Summit 2020
        </div>
        <div className='h4 font-weight-900' style={{ color: '#ebedef' }}>
          데이터는 사람을 행동하게 한다
        </div>
        <div>
          <img src={ImgA} alt='이미지' width='60%' />
        </div>
        <div className='mt-5 h3' style={{ color: '#ebedef' }}>
          <div>일시 : 2020. 11. 05 (목) 13:00 ~ 18:00</div>
          <div>장소 : 서울시 강남구</div>
          <div>주관 : 데이터마케팅코리아</div>
          <div>후원 : 멀티캠퍼스, 한국문화정보원, DOPIX</div>
        </div>
      </div>
    </Container>
  );
}

export default SummitInfo;
