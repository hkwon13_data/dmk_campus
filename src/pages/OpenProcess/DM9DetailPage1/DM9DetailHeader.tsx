import React from 'react';

import ImgA from 'assets/img/dm9_domain/headerImg1.jpg';

import { Container, Row, Col } from 'reactstrap';

function DM9DetailHeader() {
  return (
    <>
      {/* <div className="page-header header-filter page-header-small skew-separator skew-mini"> */}
      <div className='page-header header-filter'>
        <div
          className='page-header-image'
          style={{
            backgroundImage: `url(${ImgA})`,
            filter: 'brightness(0.50)',
          }}
        />
        <Container className='container-lg pt-5'>
          <Row className='justify-content-center align-items-center'>
            <Col lg='8'>
              <div className='h1 font-weight-900 text-white'>DM9 1영역</div>
              <div
                className='font-weight-900 text-white mb-3'
                style={{ fontSize: '3rem' }}
              >
                시장 / 트렌드 분석
              </div>
              <div className='h1 font-weight-500 text-white'>
                자사 제품부터 라이프스타일까지 !
              </div>
              <div className='h1 font-weight-500 text-white'>
                시장을 분석하고 예측할 수 있습니다.
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
}

export default DM9DetailHeader;
