import React from 'react';

import { Container } from 'reactstrap';

import styled, { DefaultTheme } from 'styled-components';

const IconDiv = styled.div<DefaultTheme>`
  display: inline-block;
  position: relative;
  top: 4px;
`;

function DM9ProcessNext() {
  return (
    <div className='wrapper py-9'>
      <Container className='container-md'>
        <div className='h2 font-weight-900 mb-5 text-center'>
          이 과정이 끝나면?
        </div>
        <div className='h4 pl-6'>
          <IconDiv>
            <i className='ni ni-check-bold' />
          </IconDiv>
          <span>
            기존 시장 분석과{' '}
            <span className='font-weight-900'>차별화된 타겟 시장 정의</span>를
            할 수 있습니다.
          </span>
        </div>
        <div className='h4 pl-6'>
          <IconDiv>
            <i className='ni ni-check-bold' />
          </IconDiv>
          <span>
            빅데이터를 활용한{' '}
            <span className='font-weight-900'>상품, 산업, 경쟁사 분석</span>이
            가능합니다.
          </span>
        </div>
        <div className='h4 pl-6'>
          <IconDiv>
            <i className='ni ni-check-bold' />
          </IconDiv>
          <span>
            고객 Life Style을 반영한{' '}
            <span className='font-weight-900'>제품경쟁력 분석</span>합니다.
          </span>
        </div>
        <div className='h4 pl-6'>
          <IconDiv>
            <i className='ni ni-check-bold' />
          </IconDiv>
          <span>
            진입가능한{' '}
            <span className='font-weight-900'>신규 시장 정의/트렌드 예측</span>
            합니다.
          </span>
        </div>
      </Container>
    </div>
  );
}

export default DM9ProcessNext;
