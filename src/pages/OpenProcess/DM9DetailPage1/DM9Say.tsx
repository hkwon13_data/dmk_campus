import React from 'react';

import { Container, Row, Col } from 'reactstrap';

import ImgA from 'assets/img/dm9_domain/say_img.png';

function DM9Say() {
  return (
    <Container className='my-9'>
      <Row className='justify-content-center'>
        <Col lg='3' className='px-0'>
          <div
            className='image'
            style={{
              height: '300px',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'flex-end',
              backgroundColor: '#8df9c5',
            }}
          >
            <img className='img-fluid' src={ImgA} alt='이미지' />
          </div>
        </Col>
        <Col lg='7' className='p-5' style={{ border: '3px solid #8df9c5' }}>
          <div className='h1 font-weight-900'>마케터 says</div>
          <div className='h4'>
            1. 마케팅에 필요한 트렌드는 어떻게 분석할 수 있을까요?
          </div>
          <div className='h4'>
            2. 수많은 데이터에서 어떻게 인사이트를 도출할 수 있을까요?
          </div>
          <div className='h4'>
            3. 시장조사와 수요예측을 어떻게 할 수 있을까요?
          </div>
        </Col>
      </Row>
    </Container>
  );
}

export default DM9Say;
