import React from 'react';

import { Card, CardBody, Row, Col } from 'reactstrap';

function DM9EduObject() {
  return (
    <div className='wrapper py-9' style={{ background: '#dedffc' }}>
      <div className='h1 font-weight-900 mb-6 text-center'>교육 대상자</div>
      <Row className='justify-content-center'>
        <Col lg='3'>
          <Card>
            <CardBody>
              <div className='h5'>시장 트렌드 분석을 통해</div>
              <div className='font-weight-900 h5'>새로운 상품과 서비스를</div>
              <div className='font-weight-900 h5 mb-0'>기획하는 기획자</div>
              <div className='text-right'>
                <div className='icon-shape icon-xl mx-auto text-right'>
                  <i className='ni ni-circle-08' />
                </div>
              </div>
            </CardBody>
          </Card>
        </Col>
        <Col lg='3'>
          <Card>
            <CardBody>
              <div className='h5'>마케팅 전략을 수립하기 위해</div>
              <div className='font-weight-900 h5'>시장분석이 필요한</div>
              <div className='font-weight-900 h5 mb-0'>마케팅 담당자</div>
              <div className='text-right'>
                <div className='icon-shape icon-xl mx-auto text-right'>
                  <i className='ni ni-circle-08' />
                </div>
              </div>
            </CardBody>
          </Card>
        </Col>
        <Col lg='3'>
          <Card>
            <CardBody>
              <div className='h5'>경영전략 수립을 위해</div>
              <div className='font-weight-900 h5'>
                시장 현황분석을 하고자 하는
              </div>
              <div className='font-weight-900 h5 mb-0'>실무자</div>
              <div className='text-right'>
                <div className='icon-shape icon-xl mx-auto text-right'>
                  <i className='ni ni-circle-08' />
                </div>
              </div>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  );
}

export default DM9EduObject;
