import React from 'react';

import { Container, Row, Col } from 'reactstrap';

function DM9EduContent() {
  return (
    <div className='wrapper pt-5' style={{ background: '#ecf1ff' }}>
      <div className='h1 font-weight-900 mb-6 text-center'>교육 대상자</div>
      <Container>
        <Row className='justify-content-center align-items-center'>
          <Col
            lg='2'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderRight: 'none' }}
          >
            <div className='text-center text-primary h1 mb-0 p-2'>01</div>
          </Col>
          <Col
            lg='7'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderLeft: 'none' }}
          >
            <div className='p-1'>
              <div className='font-weight-700 h5'>
                데이터로 보는 시장분석 :{' '}
              </div>
              <div className='h5 mb-0'>
                전통적 시장분석 방법과 데이터를 활용한 분석을 연계합니다
              </div>
            </div>
          </Col>
        </Row>
        <div className='h3 mb-0 text-center py-3'>
          <i className='ni ni-bold-down' />
        </div>
        <Row className='justify-content-center align-items-center'>
          <Col
            lg='2'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderRight: 'none' }}
          >
            <div className='text-center text-primary h1 mb-0 p-2'>02</div>
          </Col>
          <Col
            lg='7'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderLeft: 'none' }}
          >
            <div className='p-1'>
              <div className='font-weight-700 h5'>
                데이터로 읽는 Product 분석 :{' '}
              </div>
              <div className='h5 mb-0'>
                Product Keyword 분석체계 구성하여 산업 내 자사 Product 역량을
                분석합니다
              </div>
            </div>
          </Col>
        </Row>
        <div className='h3 mb-0 text-center py-3'>
          <i className='ni ni-bold-down' />
        </div>
        <Row className='justify-content-center align-items-center'>
          <Col
            lg='2'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderRight: 'none' }}
          >
            <div className='text-center text-primary h1 mb-0 p-2'>03</div>
          </Col>
          <Col
            lg='7'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderLeft: 'none' }}
          >
            <div className='p-1'>
              <div className='font-weight-700 h5'>
                데이터로 보는 Industry 분석 :{' '}
              </div>
              <div className='h5 mb-0'>
                산업 트렌드 Keyword를 도출하고 산업 중장기 트렌드를 분석합니다
              </div>
            </div>
          </Col>
        </Row>
        <div className='h3 mb-0 text-center py-3'>
          <i className='ni ni-bold-down' />
        </div>
        <Row className='justify-content-center align-items-center pb-6'>
          <Col
            lg='2'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderRight: 'none' }}
          >
            <div className='text-center text-primary h1 mb-0 p-2'>04</div>
          </Col>
          <Col
            lg='7'
            className='p-3 bg-white'
            style={{ border: '1px solid #333333', borderLeft: 'none' }}
          >
            <div className='p-1'>
              <div className='font-weight-700 h5'>
                고객 Life Style 분석과 시장예측 :{' '}
              </div>
              <div className='h5 mb-0'>
                디지털상에 흔적으로 남는 고객 관심사 Data를 수집하고 신규시장을
                예측합니다
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default DM9EduContent;
