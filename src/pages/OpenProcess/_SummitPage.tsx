import React from 'react';

import CommonNavbar from 'components/NavBar/CommonNavbar';
import CommonFooter from 'components/NavBar/CommonFooter';
import { ScrollTopButton } from 'components/utils/ScrollTopButton';
import { RouteComponentProps } from 'react-router-dom';
import FloatingButton from '../../components/NavBar/FloatingButton';
import SummitHeader from './SummitHeader';
import SummitTab from './SummitTab';

export default function SummitPage({ match }: RouteComponentProps) {
  // React.useEffect(() => {
  // 	let classList = document.body.classList;
  // 	document.body.classList.remove(...classList);
  // 	document.body.classList.add("index-page");
  // 	window.scrollTo(0, 0);
  // 	return function cleanup() {
  // 		document.body.classList.remove("index-page");
  // 	};
  // }, []);

  const selectedTabs = match.path;

  return (
    <>
      <ScrollTopButton />
      <CommonNavbar type='transparent' />
      <FloatingButton />
      <div className='wrapper'>
        <SummitHeader />
        <SummitTab tabs={selectedTabs} />
      </div>
      <CommonFooter />
    </>
  );
}
