import React from 'react';

import { Container, Row, Col } from 'reactstrap';

function DM9EduPoint() {
  return (
    <>
      <div className='my-8'>
        <Container>
          <Row className='justify-content-center'>
            <Col className='mx-auto text-center my-5' lg='8'>
              <div className='h1 font-weight-900'>교육포인트</div>
            </Col>
          </Row>
          <Row className='justify-content-center mb-4'>
            <Col lg='5' className='p-4' style={{ border: '1px solid #333333' }}>
              <div className='h1 text-right mb-0'>1</div>
              <div className='h4 font-weight-900'>IT 비전공저도 OK,</div>
              <div className='h4'>기획자/마케터 관점에서 데이터를</div>
              <div className='h4'>분석하는 방법 제공합니다</div>
            </Col>
            <Col
              lg='5'
              className='p-4 ml-4'
              style={{ border: '1px solid #333333' }}
            >
              <div className='h1 text-right mb-0'>2</div>
              <div className='h4 font-weight-900'>데이터 수집 및 처리,</div>
              <div className='h4'>기업이 원하는 데이터를 수집하고</div>
              <div className='h4'>처리하는 방법 교육입니다</div>
            </Col>
          </Row>
          <Row className='justify-content-center'>
            <Col lg='5' className='p-4' style={{ border: '1px solid #333333' }}>
              <div className='h1 text-right mb-0'>3</div>
              <div className='h4 font-weight-900'>머신러닝과 AI ,</div>
              <div className='h4'>머신러닝과 딥러닝의 원리를 이해하고</div>
              <div className='h4'>AI에 적용된 사례를 피익헤봅니다</div>
            </Col>
            <Col
              lg='5'
              className='p-4 ml-4'
              style={{ border: '1px solid #333333' }}
            >
              <div className='h1 text-right mb-0'>4</div>
              <div className='h4 font-weight-900'>데이터 활용 및 시각화,</div>
              <div className='h4'>처리된 데이터를 활용하고 시각화하여</div>
              <div className='h4'>한 눈에 쉽게 바라볼 수 있습니다</div>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
}

export default DM9EduPoint;
