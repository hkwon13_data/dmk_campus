import React from 'react';

import { Container, Row, Col } from 'reactstrap';

import ImgA from 'assets/img/dm9_domain/say_img.png';

function DM9Say() {
  return (
    <Container className='my-9'>
      <Row className='justify-content-center'>
        <Col lg='3' className='px-0'>
          <div
            className='image'
            style={{
              height: '300px',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'flex-end',
              backgroundColor: '#8df9c5',
            }}
          >
            <img className=' img-fluid' src={ImgA} alt='이미지' />
          </div>
        </Col>
        <Col lg='7' className='p-5' style={{ border: '3px solid #8df9c5' }}>
          <div className='h1 font-weight-900'>마케터 says</div>
          <div className='h4'>
            1. 어떤 데이터를 어떻게 수집하고 분석해야 하나요??
          </div>
          <div className='h4'>
            2. 필요한 데이터만 간결하게 Cleansing 하는 법은 무엇인가요?
          </div>
          <div className='h4'>
            3. 분석된 데이터를 시각화 하는 방법은 무엇인가요?
          </div>
        </Col>
      </Row>
    </Container>
  );
}

export default DM9Say;
