import React from 'react';

import { Container } from 'reactstrap';

import styled, { DefaultTheme } from 'styled-components';

const IconDiv = styled.div<DefaultTheme>`
  display: inline-block;
  position: relative;
  top: 4px;
`;

function DM9ProcessNext() {
  return (
    <div className='wrapper py-9'>
      <Container className='container-md'>
        <div className='h2 font-weight-900 mb-6 text-center'>
          이 과정이 끝나면?
        </div>
        <div className='h4 pl-6'>
          <IconDiv>
            <i className='ni ni-check-bold' />
          </IconDiv>
          <span>
            데이터유형별{' '}
            <span className='font-weight-900'>수집/처리/분석 방법을 습득</span>
            합니다
          </span>
        </div>
        <div className='h4 pl-6'>
          <IconDiv>
            <i className='ni ni-check-bold' />
          </IconDiv>
          <span>
            <span className='font-weight-900'>
              정형/비정형 데이터 분석 원리를 이해
            </span>
            할 수 있습니다
          </span>
        </div>
        <div className='h4 pl-6'>
          <IconDiv>
            <i className='ni ni-check-bold' />
          </IconDiv>
          <span>
            데이터{' '}
            <span className='font-weight-900'>시각화 프로세스를 이해</span>
            합니다.
          </span>
        </div>
      </Container>
    </div>
  );
}

export default DM9ProcessNext;
