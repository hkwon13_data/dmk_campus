import React from 'react';

// reactstrap components
import { Container, Row, Col, CardBody, CardTitle } from 'reactstrap';

// Core Components

import imgA from 'assets/img/online_mamemclub/mamemclubHeader.png';

function MembershipHeader() {
  return (
    <>
      <div
        className='page-header-image d-flex align-items-center pt-5'
        style={{
          backgroundImage: `url(${imgA})`,
          backgroundSize: '100% 100%',
          height: '80vh',
          zIndex: -123,
        }}
      >
        <Container>
          <Row>
            <Col className='mx-auto' lg='10'>
              <CardBody className='mb-4 text-center'>
                <div className='content-bottom'>
                  <CardTitle tag='h1' className='text-white font-weight-700'>
                    마대리 멤버십 클럽
                  </CardTitle>
                  <CardTitle
                    tag='h1'
                    className='text-white font-weight-700 mb-5'
                  >
                    Ma:deri Membership Club
                  </CardTitle>
                  <div className='h7 text-white mt-3 mb-5'>
                    {/* 빠르게 진행되는 디지털 트랜스포메이션. 빠르게 변화하는 소비자들의 라이프스타일. 해결되지 않은
                                        마케터들의 오랜 고민과 어려움을 데이터로 해결해드릴 수 있도록 현업 업계 최고의 데이터마케터를 모셨습니다. 본 마대리 멤버십 클럽에서는 발전하는 데이터마케팅 산업을 주제로 최고의
                                        인사이트를 공유합니다. 데이터마케팅코리아 대표와 현업 업계 최고의 마케팅 전문가를 초청하여
                                        다양한 분야의 마케팅 트랜드와 인사이트를 제공합니다. */}
                    <p className='h7 text-white font-weight-500'>
                      데이터 마케팅, 실제 현업에서는 어떻게 하고 있을까요?
                    </p>
                    <p
                      className='h7 text-white font-weight-500'
                      style={{ lineHeight: '0.1' }}
                    >
                      마대리 멤버십 클럽은 발전하는 데이터 마케팅 산업을 주제로
                    </p>
                    <p className='h7 text-white font-weight-500'>
                      다양한 분야의 마케팅 전문가가 실제 사례에 기반하여 최고의
                      인사이트를 공유합니다
                    </p>
                  </div>
                  <div>
                    <button
                      type='button'
                      // color="primary"
                      className='btn btn-round bg-secondary px-4 text-dark'
                      style={{ pointerEvents: 'none' }}
                    >
                      <span className='h5 font-weight-900'>신청대상</span>
                    </button>
                  </div>
                  <div className='text-white font-weight-900 h5 mt-3'>
                    온라인 강의, 누구나!
                  </div>
                </div>
              </CardBody>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
}

export default MembershipHeader;
