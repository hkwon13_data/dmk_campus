import React from 'react';

import { Container } from 'reactstrap';

import styled, { DefaultTheme } from 'styled-components';

const IconDiv = styled.div<DefaultTheme>`
  display: inline-block;
  position: relative;
  top: 4px;
`;

function DM9ProcessNext() {
  return (
    <div className='wrapper py-9'>
      <Container className='container-lg'>
        <div className='h2 font-weight-900 mb-6 text-center'>
          이 과정이 끝나면?
        </div>
        <div className='h4 pl-6'>
          <IconDiv>
            <i className='ni ni-check-bold' />
          </IconDiv>
          <span>
            고객 행동 패턴을 분석해{' '}
            <span className='font-weight-900'>
              매체별 고객 유입률, 전환율을 파악
            </span>{' '}
            할 수 있습니다
          </span>
        </div>
        <div className='h4 pl-6'>
          <IconDiv>
            <i className='ni ni-check-bold' />
          </IconDiv>
          <span>
            고객 유입경로에 따른{' '}
            <span className='font-weight-900'>
              최적의 광고/캠페인 전략 수립
            </span>
            이 가능합니다.
          </span>
        </div>
        <div className='h4 pl-6'>
          <IconDiv>
            <i className='ni ni-check-bold' />
          </IconDiv>
          <span>
            ROI/ROAS/DV/VOI 개념을 통한{' '}
            <span className='font-weight-900'>광고/캠페인 성과 측정</span>을
            합니다.
          </span>
        </div>
      </Container>
    </div>
  );
}

export default DM9ProcessNext;
