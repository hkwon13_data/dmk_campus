import React from 'react';

import { Container, Row, Col } from 'reactstrap';

import ImgA from 'assets/img/dm9_domain/say_img.png';

function DM9Say() {
  return (
    <Container className='my-9'>
      <Row className='justify-content-center'>
        <Col lg='3' className='px-0'>
          <div
            className='image'
            style={{
              height: '300px',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'flex-end',
              backgroundColor: '#8df9c5',
            }}
          >
            <img className=' img-fluid' src={ImgA} alt='이미지' />
          </div>
        </Col>
        <Col lg='9' className='p-5' style={{ border: '3px solid #8df9c5' }}>
          <div className='h1 font-weight-900'>마케터 says</div>
          <div className='h4'>
            1. 매출에 영향을 미치는 광고를 기획하려면 어떤 것부터 봐야 하는지
            모르겠어요.
          </div>
          <div className='h4'>
            2. 적정한 광고 예산을 수립하려면 무엇부터 시작해야 하나요??
          </div>
          <div className='h4'>
            3. 광고/캠페인 성과를 분석하기 위해서는 무엇을 지표로 봐야 하나요?
          </div>
        </Col>
      </Row>
    </Container>
  );
}

export default DM9Say;
