import React from 'react';

import ImgA from 'assets/img/dm9_domain/headerImg6.png';

import { Container, Row, Col } from 'reactstrap';

function DM9DetailHeader() {
  return (
    <>
      {/* <div className="page-header header-filter page-header-small skew-separator skew-mini"> */}
      <div className='page-header header-filter'>
        <div
          className='page-header-image'
          style={{
            backgroundImage: `url(${ImgA})`,
            filter: 'brightness(0.50)',
          }}
        />
        <Container className='container-lg pt-0'>
          <Row className='justify-content-center align-items-center'>
            <Col lg='9'>
              <div className='h1 font-weight-900 text-white'>DM9 6영역</div>
              <div
                className='font-weight-900 text-white mb-3'
                style={{ fontSize: '3rem' }}
              >
                퍼포먼스 마케팅
              </div>
              <div className='h1 font-weight-500 text-white'>
                적은 예산으로 최적의 광고를 기획해보자!
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
}
export default DM9DetailHeader;
