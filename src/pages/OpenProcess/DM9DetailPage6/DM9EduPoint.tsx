import React from 'react';

import { Container, Row, Col } from 'reactstrap';

function DM9EduPoint() {
  return (
    <>
      <div className='my-8'>
        <Container>
          <Row className='justify-content-center'>
            <Col className='mx-auto text-center my-5' lg='8'>
              <div className='h1 font-weight-900'>교육포인트</div>
            </Col>
          </Row>
          <Row className='justify-content-center mb-4'>
            <Col lg='5' className='p-4' style={{ border: '1px solid #333333' }}>
              <div className='h1 text-right mb-0'>1</div>
              <div className='h4 font-weight-900'>IT 비전공저도 OK,</div>
              <div className='h4'>기획자/마케터 관점에서 데이터를</div>
              <div className='h4'>분석하는 방법 제공합니다</div>
            </Col>
            <Col
              lg='5'
              className='p-4 ml-4'
              style={{ border: '1px solid #333333' }}
            >
              <div className='h1 text-right mb-0'>2</div>
              <div className='h4 font-weight-900'>최적의 광고 기획,</div>
              <div className='h4'>고객 행동 패턴 분석을 통한 최적의</div>
              <div className='h4'>광고를 기획합니다</div>
            </Col>
          </Row>
          <Row className='justify-content-center'>
            <Col lg='5' className='p-4' style={{ border: '1px solid #333333' }}>
              <div className='h1 text-right mb-0'>3</div>
              <div className='h4 font-weight-900'>광고/캠페인 성과 측정,</div>
              <div className='h4'>광고가 매출에 얼마나 영향을 끼쳤는지</div>
              <div className='h4'>객관적인 수치로 확인합니다</div>
            </Col>
            <Col
              lg='5'
              className='p-4 ml-4'
              style={{ border: '1px solid #333333' }}
            >
              <div className='h1 text-right mb-0'>4</div>
              <div className='h4 font-weight-900'>Media Mix 전략 수립,</div>
              <div className='h4'>채널 별 광고 최적화 전략을 수립합니다</div>
              <div className='h4'>&nbsp;</div>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
}

export default DM9EduPoint;
