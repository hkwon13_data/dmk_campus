import React from 'react';

import { Card, CardBody, Row, Col } from 'reactstrap';

function DM9EduObject() {
  return (
    <div className='wrapper py-9' style={{ background: '#dedffc' }}>
      <div className='h1 font-weight-900 mb-6 text-center'>교육 대상자</div>
      <Row className='justify-content-center'>
        <Col lg='3'>
          <Card>
            <CardBody>
              <div className='h5'>적은 돈으로 디지털 광고를 진행</div>
              <div className='h5'>해야 하는 중소기업 마케터</div>
              <div className='h5 mb-0'>&nbsp;</div>
              <div className='text-right'>
                <div className='icon-shape icon-xl mx-auto text-right'>
                  <i className='ni ni-circle-08' />
                </div>
              </div>
            </CardBody>
          </Card>
        </Col>
        <Col lg='3'>
          <Card>
            <CardBody>
              <div className='h5'>효과적인 광고캠페인을 기획해야</div>
              <div className='h5'>하는 광고 대행사</div>
              <div className='h5 mb-0'>&nbsp;</div>
              <div className='text-right'>
                <div className='icon-shape icon-xl mx-auto text-right'>
                  <i className='ni ni-circle-08' />
                </div>
              </div>
            </CardBody>
          </Card>
        </Col>
        <Col lg='3'>
          <Card>
            <CardBody>
              <div className='h5'>광고캠페인 성과를 면밀히 분석</div>
              <div className='h5'>하고 싶은 의사결정자</div>
              <div className='h5 mb-0'>&nbsp;</div>
              <div className='text-right'>
                <div className='icon-shape icon-xl mx-auto text-right'>
                  <i className='ni ni-circle-08' />
                </div>
              </div>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  );
}

export default DM9EduObject;
