/* eslint-disable @typescript-eslint/no-shadow */
/* eslint-disable react/no-array-index-key */
import React from 'react';

// reactstrap components
import { Row, Col, Container, Card, CardBody, CardTitle } from 'reactstrap';

import heoWonSeok_03 from 'assets/img/online_mamemclub/heoWonSeok_03.jpg';
import leeSanHo_04 from 'assets/img/online_mamemclub/leeSanHo_04.png';
import hwangBoHyun_05 from 'assets/img/online_mamemclub/hwangBoHyun_05.jpg';
import koYungDae_06 from 'assets/img/online_mamemclub/koYungDae_06.jpg';
import shinHoSang_07 from 'assets/img/online_mamemclub/shinHoSang_07.png';
import jangJongHo_08 from 'assets/img/online_mamemclub/jangJongHo_08.jpg';
import naSeungYong_09 from 'assets/img/online_mamemclub/naSeungYong_09.jpg';
import kimYeonJeong_10 from 'assets/img/online_mamemclub/kimYeonJeong_10.jpg';
import joYongMin_11 from 'assets/img/online_mamemclub/joYongMin_11.jpg';

// import Item from 'antd/lib/list/Item';

// const style = { width: "18rem" };
import styled, { DefaultTheme } from 'styled-components';

const RightToolTip = styled.div<DefaultTheme>`
  display: inline-block;
  padding: 0.4rem 1rem;
  background: #3bd6ce;
  border-radius: 25px;
  position: absolute;
  right: 4%;
  top: 2%;
`;
const HeaderDeg = styled.div<DefaultTheme>`
  &::after {
    content: '';
    width: 24%;
    overflow: hidden;
    position: absolute;
    left: 32%;
    top: 18%;
    height: 90%;
    background-color: #fff;
    transform: rotate(80deg);
    z-index: 1;
  }
`;

interface CardDataTypes {
  img: string;
  name: string;
  subName: string;
  text: string;
  month: string;
}

function MembershipLineUp() {
  const cardData: CardDataTypes[] = [
    {
      img: heoWonSeok_03,
      name: '허원석',
      subName: 'MLB코리아 이사',
      text: `데이터와 패션이 \n 만나면 생기는 일`,
      month: '3월',
    },
    {
      img: leeSanHo_04,
      name: '이산호',
      subName: '요리조리 대표',
      text: '마케팅하지마세요\n 상품에 집중하세요',
      month: '4월',
    },
    {
      img: hwangBoHyun_05,
      name: '황보현',
      subName: '솔트룩스 부사장',
      text: '데이터크리에이티브 \n',
      month: '5월',
    },
    {
      img: koYungDae_06,
      name: '고영대',
      subName: '오리지널랩 대표',
      text: '콘텐츠 트렌드 \n',
      month: '6월',
    },
    {
      img: shinHoSang_07,
      name: '신호상',
      subName: '버거킹코리아 상무',
      text: '데이터와 패션이 \n 만나면 생기는 일',
      month: '7월',
    },
    {
      img: jangJongHo_08,
      name: '장중호',
      subName: '홈플러스 전무',
      text: '데이터마케팅방법론 \n',
      month: '8월',
    },
    {
      img: naSeungYong_09,
      name: '나승용',
      subName: '전 정훈병과장',
      text: '콘텐츠 제작 \n',
      month: '9월',
    },
    {
      img: kimYeonJeong_10,
      name: '허원석',
      subName: 'MLB코리아 이사',
      text: '데이터와 패션이\n 만나면 생기는 일',
      month: '10월',
    },
    {
      img: joYongMin_11,
      name: '허원석',
      subName: 'MLB코리아 이사',
      text: '데이터와 패션이\n 만나면 생기는 일',
      month: '11월',
    },
  ];

  const renderLineupCard = (data: CardDataTypes[]) => {
    return data.map((item, index) => {
      return (
        // <Col lg="4" md="6" xs="12" key={index}>
        // 	<Card className="card-profile p-0 border-2" data-image="profile-image">
        // 		{/* <CardImg alt="..." src={item.img}></CardImg> */}
        // 		<CardBody className="p-2">
        // 			<div style={{backgroundImage: "url(" + item.img + ")", backgroundSize : '100% 135%', backgroundRepeat : 'no-repeat', height : '300px'}}></div>
        // 			<CardTitle className="font-weight-900 h4 mb-0">
        // 				{item.name}
        // 			</CardTitle>
        // 			<CardTitle className="font-weight-500 h6">
        // 				{item.subName}
        // 			</CardTitle>
        // 			<div className="mb-4">
        // 			<HeaderDeg></HeaderDeg>
        // 				{/* <div className="font-weight-700 h5">{item.text}</div> */}
        // 				{/* {(item.text.split('\n').length > 1) ? item.text.split('\n').map((line, index) => {
        // 					return <div className="font-weight-700 h5 mb-0" key={index}>{line}<br/></div>
        // 				}) : item.text.split('\n').map((line, index) => {
        // 					return <div className="font-weight-700 h5 mb-0" key={index}>{line}</div>
        // 				})} */}
        // 				{item.text.split('\n').map((line, index) => {
        // 					return <div className="font-weight-700 h5 mb-0" key={index}>{line}<br/></div>
        // 				})}
        // 			</div>
        // 		</CardBody>
        // 		<RightToolTip
        // 			className="font-weight-900 h6"
        // 			style={{ color: "#171717" }}>
        // 			{item.month}
        // 		</RightToolTip>
        // 	</Card>
        // </Col>
        <Col lg='4' md='6' xs='12'>
          <Card className='card-profile' data-image='profile-image' key={index}>
            <div className='card-image'>
              <img alt='...' className='img rounded' src={item.img} />
            </div>
            <HeaderDeg />
            <CardBody className='pt-0 pb-0' style={{ zIndex: 2 }}>
              <CardTitle className='font-weight-900 h4 mb-0'>
                {item.name}
              </CardTitle>
              <CardTitle className='font-weight-500 h6'>
                {item.subName}
              </CardTitle>
              <div className='mb-4'>
                {/* <div className="font-weight-700 h5">{item.text}</div> */}
                {/* {(item.text.split('\n').length > 1) ? item.text.split('\n').map((line, index) => {
							return <div className="font-weight-700 h5 mb-0" key={index}>{line}<br/></div>
						}) : item.text.split('\n').map((line, index) => {
							return <div className="font-weight-700 h5 mb-0" key={index}>{line}</div>
						})} */}
                {item.text.split('\n').map((line, index) => {
                  return (
                    <div className='font-weight-700 h5 mb-0' key={index}>
                      {line}
                      <br />
                    </div>
                  );
                })}
              </div>
            </CardBody>
            <RightToolTip
              className='font-weight-900 h6'
              style={{ color: '#171717' }}
            >
              {item.month}
            </RightToolTip>
          </Card>
        </Col>
      );
    });
  };

  return (
    <div>
      <Container className='container-xl'>
        <Card className='p-4 text-center'>
          <CardBody>
            <div className='h3 font-weight-900 text-center'>
              Marketing Insight 라인업
            </div>
            <Row>{renderLineupCard(cardData)}</Row>
          </CardBody>
        </Card>
      </Container>
    </div>
  );
}

export default MembershipLineUp;
