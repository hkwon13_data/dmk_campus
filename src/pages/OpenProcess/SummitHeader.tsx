import React from 'react';

// reactstrap components

import imgA from 'assets/img/offline_sumit/headerImg.png';

function SummitHeader() {
  return (
    <>
      <div
        className='page-header-image d-flex align-items-center pt-5'
        style={{
          backgroundImage: `url(${imgA})`,
          backgroundSize: '100% 100%',
          height: '80vh',
          zIndex: -123,
        }}
      />
    </>
  );
}

export default SummitHeader;
