/* eslint-disable react/no-array-index-key */
/* eslint-disable @typescript-eslint/no-shadow */
import React from 'react';
// react library for routing
import { Route, Switch, Redirect, RouteComponentProps } from 'react-router-dom';

// Routes components
import routes from 'routes/CustomEduRoutes';
import types from 'types/types';

const CommunityLayout: React.FC<RouteComponentProps> = () => {
  const mainContentRef = React.useRef(null);

  const getRoutes = (
    routes: types.RoutesTypes[]
  ): React.ReactNode | Function | null => {
    return routes.map((prop: any, key: number) => {
      if (prop.collapse) {
        return getRoutes(prop.views);
      }
      if (prop.layout === '/customEdu') {
        return (
          <Route
            exact
            path={prop.layout + prop.path}
            component={prop.component}
            key={key}
          />
        );
      }
      return null;
    });
  };

  return (
    <>
      <div className='main-content' ref={mainContentRef}>
        <Switch>
          {getRoutes(routes)}
          <Redirect from='*' to='/' />
        </Switch>
      </div>
    </>
  );
};

export default CommunityLayout;
