/* eslint-disable react/no-array-index-key */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable @typescript-eslint/no-shadow */
import React from 'react';
// react library for routing
import { useLocation, Route, Switch, Redirect } from 'react-router-dom';
import { RootState } from 'store/modules/index';
// import * as types from 'types/types';

// core components
import AdminNavbar from 'components/NavBar/AdminNavbar';
import AdminFooter from 'components/NavBar/AdminFooter';
// import CommonFooter from 'components/NavBar/CommonFooter';
// import CommonNavbar from 'components/NavBar/CommonNavbar';
import Sidebar from 'components/NavBar/AdminSidebar';

import routes from 'routes/AdminRoutes';

// import { useAppContext } from 'store';
import { useSelector, useDispatch } from 'react-redux';
import { getNotices, getLectureMaterials } from 'store/modules/community';

import imgA from 'assets/img/DataMarketingCampus_Logo.png';
// import imgB from 'assets/img/brand/blue.png';

export default function AdminLayout() {
  const dispatch = useDispatch();
  const {
    userInfo: { isAdmin },
    isLoggedIn,
  } = useSelector((state: RootState) => state.authentication);

  const [sidenavOpen, setSidenavOpen] = React.useState(true);
  const location = useLocation();
  const mainContentRef = React.useRef(null);
  React.useEffect(() => {
    document.documentElement.scrollTop = 0;
    // document.scrollingElement && document.scrollingElement.scrollTop = 0;
    // mainContentRef.current?.scrollTop = 0;
  }, [location]);

  React.useEffect(() => {
    dispatch(getNotices(undefined));
    dispatch(getLectureMaterials(undefined));
  });

  const getRoutes = (routes: any): React.ReactNode | Function | null => {
    return routes.map((prop: any, key: number) => {
      if (prop.collapse) {
        return getRoutes(prop.views);
      }

      if (prop.layout === '/admin' && isAdmin && isLoggedIn) {
        return (
          <Route
            exact
            path={prop.layout + prop.path}
            component={prop.component}
            key={key}
          />
        );
      }
      return null;
    });
  };
  const getBrandText = (): string => {
    for (let i = 0; i < routes.length; i += 1) {
      if (location.pathname.indexOf(routes[i].layout + routes[i].path) !== -1) {
        return routes[i].name;
      }
    }
    return 'Brand';
  };

  // toggles collapse between mini sidenav and normal
  const toggleSidenav = () => {
    if (document.body.classList.contains('g-sidenav-pinned')) {
      document.body.classList.remove('g-sidenav-pinned');
      document.body.classList.add('g-sidenav-hidden');
    } else {
      document.body.classList.add('g-sidenav-pinned');
      document.body.classList.remove('g-sidenav-hidden');
    }
    setSidenavOpen(!sidenavOpen);
  };

  return (
    <>
      <Sidebar
        routes={routes}
        toggleSidenav={toggleSidenav}
        sidenavOpen={sidenavOpen}
        logo={{
          innerLink: '/home',
          imgSrc: imgA,
          imgAlt: '...',
        }}
      />
      <div className='main-content' ref={mainContentRef}>
        <AdminNavbar
          toggleSidenav={toggleSidenav}
          sidenavOpen={sidenavOpen}
          //   brandText={getBrandText(location.pathname)}
          brandText={getBrandText()}
        />
        <Switch>
          {getRoutes(routes)}
          <Redirect from='*' to='/login' />
        </Switch>
        <AdminFooter />
      </div>
      {sidenavOpen ? (
        // eslint-disable-next-line jsx-a11y/no-static-element-interactions
        <div className='backdrop d-xl-none' onClick={toggleSidenav} />
      ) : null}
    </>
  );
}
