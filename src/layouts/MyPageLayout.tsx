import React from 'react';
// react library for routing
import { Route, Switch, Redirect, RouteComponentProps } from 'react-router-dom';

// Routes components
import routes from 'routes/MyPageRoutes';
import { useSelector } from 'react-redux';
import types from 'types/types';
import { RootState } from 'store/modules/index';

const MyPageLayout: React.FC<RouteComponentProps> = () => {
  const {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    userInfo: { isAdmin },
    isLoggedIn,
  } = useSelector((state: RootState) => state.authentication);

  const mainContentRef = React.useRef(null);

  const getRoutes = (
    // eslint-disable-next-line @typescript-eslint/no-shadow
    routes: types.RoutesTypes[]
  ): React.ReactNode | Function | null => {
    return routes.map((prop: any, key: number) => {
      if (prop.collapse) {
        return getRoutes(prop.views);
      }
      // if (prop.layout === "/mypage" && !isAdmin && isLoggedIn) {
      if (prop.layout === '/mypage' && isLoggedIn) {
        return (
          <Route
            exact
            path={prop.layout + prop.path}
            component={prop.component}
            // eslint-disable-next-line react/no-array-index-key
            key={key}
          />
        );
      }
      return null;
    });
  };

  return (
    <>
      <div className='main-content' ref={mainContentRef}>
        <Switch>
          {getRoutes(routes)}
          <Redirect from='*' to='/' />
        </Switch>
      </div>
    </>
  );
};

export default MyPageLayout;
