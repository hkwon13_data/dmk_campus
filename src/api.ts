import Axios from 'axios';
import { makeUseAxios } from 'axios-hooks';
import { getStorageItem } from 'utils/useLocalStorage';
import { API_HOST } from './utils/Constant';

// Axios.defaults.withCredentials = true/

const jwtToken = getStorageItem('jwtToken');
const headers = { Authorization: `Bearer ${jwtToken}` };

export const axiosInstance = Axios.create({
  baseURL: API_HOST,
});

export const axiosAuth = Axios.create({
  baseURL: API_HOST,
  headers,
});

export const useAxios = makeUseAxios({
  axios: axiosInstance,
});
