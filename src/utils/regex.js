export function RegexUsername() {}

export function RegexPassword() {}

export function RegexNewPassword() {}

export function RegexNewRePassword() {}

// 최소 8자, 최소 하나의 문자 및 하나의 숫자:
export const pattern1 = '^(?=.*[A-Za-z])(?=.*d)[A-Za-zd]{8,}$';

// 최소 8 자, 최소 하나의 문자, 하나의 숫자 및 하나의 특수 문자 :
export const pattern2 =
  '^(?=.*[A-Za-z])(?=.*d)(?=.*[$@$!%*#?&])[A-Za-zd$@$!%*#?&]{8,}$';

// 최소 8 자, 대문자 하나 이상, 소문자 하나 및 숫자 하나 :
export const pattern3 = '^(?=.*[a-z])(?=.*[A-Z])(?=.*d)[a-zA-Zd]{8,}$';

// 최소 8 자, 대문자 하나 이상, 소문자 하나, 숫자 하나 및 특수 문자 하나 이상 :
export const pattern4 =
  '^(?=.*[a-z])(?=.*[A-Z])(?=.*d)(?=.*[$@$!%*?&])[A-Za-zd$@$!%*?&]{8,}';

// 최소 8 자 및 최대 10 자, 대문자 하나 이상, 소문자 하나, 숫자 하나 및 특수 문자 하나 이상 :
export const pattern5 =
  '^(?=.*[a-z])(?=.*[A-Z])(?=.*d)(?=.*[$@$!%*?&])[A-Za-zd$@$!%*?&]{8,10}';

export const validatorPasswordOption = {
  minLength: 8,
  minLowercase: 1,
  minUppercase: 0,
  minNumbers: 1,
  minSymbols: 1,
};
