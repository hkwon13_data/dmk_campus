export const API_HOST =
  process.env.REACT_APP_API_HOST || 'http://127.0.0.1:8000';

export const IAMPORT_IMP = 'imp04331181'; // maderi.ailab1@gmail.com
export const JWT_EXPIRY_TIME = 24 * 3600 * 1000; // 만료 시간 (24시간 밀리 초로 표현)
