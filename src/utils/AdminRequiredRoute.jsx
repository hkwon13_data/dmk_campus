import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { useSelector } from 'react-redux';
// import * as actions from 'store/modules/authentication';

export default function AdminRequiredRoute({
  // eslint-disable-next-line react/prop-types
  component: Component,
  ...kwargs
}) {
  const { isAuthenticated } = useSelector(
    (state) => state.authentication.authInfo
  );

  return (
    <Route
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...kwargs}
      render={(props) => {
        if (isAuthenticated) {
          // eslint-disable-next-line react/jsx-props-no-spreading
          return <Component {...props} />;
        }
        return (
          <Redirect
            to={{
              pathname: '/login',
              state: { from: props.location },
            }}
          />
        );
      }}
    />
  );
}
