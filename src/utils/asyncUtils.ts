import types from 'types/types';
import { AxiosResponse } from 'axios';

export const createPromiseThunk = (
  type: string,
  promiseCreator: (
    param?: any
  ) => Promise<AxiosResponse<any>> | Promise<unknown>
) => {
  const [SUCCESS, ERROR] = [`${type}_SUCCESS`, `${type}_ERROR`];
  return (param: any) => async (dispatch: Function) => {
    dispatch({ type, param });
    try {
      const payload = await promiseCreator(param);
      dispatch({ type: SUCCESS, payload }); // 성공
    } catch (e) {
      dispatch({ type: ERROR, payload: e, error: true }); // 실패
    }
  };
};

export const reducerUtils = {
  initial: <T, E = any>(initialData?: T): types.ReducerUtils<T, E> => ({
    loading: false,
    data: initialData || null,
    error: null,
  }),
  loading: <T, E = any>(data?: T): types.ReducerUtils<T, E> => ({
    loading: true,
    data: data || null,
    error: null,
  }),
  success: <T, E = any>(payload: T): types.ReducerUtils<T, E> => ({
    loading: false,
    data: payload,
    error: null,
  }),
  error: <T, E>(error: E): types.ReducerUtils<T, E> => ({
    loading: false,
    data: null,
    error,
  }),
};

// 비동기 관련 액션들을 처리하는 리듀서를 만들어줍니다.
// type 은 액션의 타입, key 는 상태의 key (예: posts, post) 입니다.
export const handleAsyncActions = (
  type: string,
  key: string,
  keepData: boolean = false
) => {
  const [SUCCESS, ERROR] = [`${type}_SUCCESS`, `${type}_ERROR`];
  return (state: any, action: any) => {
    switch (action.type) {
      case type:
        return {
          ...state,
          [key]: reducerUtils.loading(keepData ? state[key].data : null),
        };
      case SUCCESS:
        return {
          ...state,
          [key]: reducerUtils.success(action.payload),
        };
      case ERROR:
        return {
          ...state,
          [key]: reducerUtils.error(action.error),
        };
      default:
        return state;
    }
  };
};
