/* eslint-disable react/no-unescaped-entities */
import React from 'react';

import styled from 'styled-components';

import { Table, Container, Row, Col } from 'reactstrap';

const ScrollDiv = styled.div`
  overflow-y: scroll;
  height: 400px;
  &::-webkit-scrollbar {
    width: 5px;
    height: 0;
    /* background-color: #8C8F94; */
    border-radius: 1.5px;
  }
  &::-webkit-scrollbar-thumb {
    background-color: #8c8f94;
    border-radius: 6px;
  }
`;

function PersnalInfo() {
  return (
    <ScrollDiv className='pr-3'>
      <p className='font-weight-900 h5'>1. 목적</p>
      <p className='font-weight-700'>
        ㈜데이터마케팅코리아(이하 '회사')는 다음의 목적으로
        데이터마케팅캠퍼스(이하 ‘서비스’)의 개인 정보를 수집하고 있습니다.
      </p>
      <p className='font-weight-900'>
        (1) 본인 확인 및 가입 의사 확인, 회원에 대한 고지 사항 전달, ID 관리,
        고객센터 운영, 불량회원 부정이용 방지 및 비인가 사용 방지, 기타 서비스
        제공
      </p>
      <p className='font-weight-700 mb-5'>
        (2) 콘텐츠 제공, 추천 서비스 제공, 사은/판촉행사 등 각종 이벤트,
        행사/교육 관련 정보 안내 및 제반 마케팅 활동, 회사 및 제휴사 상품/서비스
        안내 및 권유, 혜택 및 서비스 개발을 위한 통계분석 및 연구조사
      </p>
      <p className='font-weight-900 h5'>2. 개인정보 수집 항목</p>
      <p className='font-weight-700'>
        (1) ‘회사’는 다음과 같이 ‘회원’의 개인정보를 수집하고 있습니다. 단
        ‘회원’의 기본적 인권 침해 우려가 있는 민감한 개인정보(인종, 민족, 사상
        및 신조, 출신지 및 본적지, 정치적 성향 및 범죄기록, 건강상태 및 성생활
        등)는 법령상 근거 없이 불필요하게 수집하지 않습니다.
      </p>
      <p className='font-weight-700'>
        (2) 개인정보는 원활한 서비스 제공을 위하여 활용하며, 해당 정보는 서비스
        개선을 위한 통계•분석에 이용될 수 있습니다.
      </p>
      <Container className='mb-5'>
        <Row>
          <Col lg='12'>
            <Table bordered className='text-wrap'>
              <thead>
                <tr>
                  <th style={{ width: '5%' }}>분류</th>
                  <th style={{ width: '30%' }}>수집/이용목적</th>
                  <th style={{ width: '35%' }}>수집/이용항목</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th rowSpan={7} className='align-middle'>
                    회원 관리
                  </th>
                  <td className='text-wrap'>
                    가입, 본인여부 확인, 부정이용 확인 및 방지
                  </td>
                  <td className='text-wrap'>
                    성명, 소속단체명, 회사명, 소속부서명, 직종, 회사 주소, 직급,
                    연락처, 휴대폰번호, 중복가입확인정보(DI), 이메일 주소,
                    암호화된 동일인 식별정보(CI)
                  </td>
                </tr>
                <tr>
                  <td className='text-wrap'>
                    계약이행 및 약관변경 등의 고지를 위한 연락, 본인의사확인 및
                    민원 등의 고객불만처리
                  </td>
                  <td className='text-wrap'>
                    아이디, 비밀번호, 이름, 직급, 이메일, 연락처, 휴대폰번호,
                    소속단체명, 회사명, 소속부서명, 직종, 회사 주소
                  </td>
                </tr>
                <tr>
                  <td className='text-wrap'>
                    부정이용방지, 비인가사용방지, 서비스 제공 및 계약의 이행{' '}
                  </td>
                  <td className='text-wrap'>
                    접속일시, 서비스 이용 기록 및 기기정보{' '}
                  </td>
                </tr>
                <tr>
                  <td className='text-wrap'>
                    부정거래의 배제 (가입 후 부정거래가 확인된 경우만)
                  </td>
                  <td className='text-wrap'>
                    아이디, 회사명, 소속부서명, 휴대폰번호, 이메일주소,
                    전화번호, 부정거래사유, 탈퇴 시 회원 상태값
                  </td>
                </tr>
                <tr>
                  <td>결제</td>
                  <td>
                    신용카드 : 카드사명, 카드번호, 유효기간
                    <br />
                    휴대전화 : 휴대전화번호, 통신사명
                    <br />
                    계좌이체 : 은행명, 계좌번호
                    <br />
                    무통장입금 : 은행명, 입금자명
                    <br />
                    현금영수증정보
                  </td>
                </tr>
                <tr>
                  <td className='text-wrap'>
                    서비스 이용 및 상담, 부정이용 확인∙방지 안내{' '}
                  </td>
                  <td className='text-wrap'>
                    쿠키, 서비스 이용기록(방문일시, IP, 불량 이용 기록 등),
                    기기정보(고유기기식별값, OS버전){' '}
                  </td>
                </tr>
                <tr>
                  <td className='text-wrap'>취소∙환불 시 제공 정보</td>
                  <td className='text-wrap'>은행명, 계좌번호</td>
                </tr>
                <tr>
                  <th rowSpan={2} className='text-wrap align-middle'>
                    마케팅 및 광고에 활용
                  </th>
                  <td className='text-wrap'>마케팅</td>
                  <td className='text-wrap'>
                    문자 수신동의, 메일 수신동의를 통한 이벤트 등 광고성 정보
                    전달
                  </td>
                </tr>
                <tr>
                  <td className='text-wrap'>분석</td>
                  <td className='text-wrap'>
                    사용자의 소속 회사 및 개인 로고를 서비스 이용 계정 소개에
                    활용)(만약, 해당 정보의 활용을 원치 않는 경우 회사에 별도
                    요청을 통해 거부할 수 있음)
                  </td>
                </tr>
                <tr>
                  <th rowSpan={2} className='text-wrap align-middle'>
                    고객사 리스트 관리
                  </th>
                  <td className='text-wrap'>사용 계정 소개</td>
                  <td className='text-wrap'>
                    사용자의 소속 회사 및 개인 로고를 서비스 이용 계정 소개에
                    활용)(만약, 해당 정보의 활용을 원치 않는 경우 회사에 별도
                    요청을 통해 거부할 수 있음)
                  </td>
                </tr>
              </tbody>
            </Table>
          </Col>
        </Row>
      </Container>
      <p className='font-weight-900 h5'>3. 개인정보 수집방법</p>
      <p className='font-weight-700'>
        '회사'는 다음 각 호의 방법으로 개인정보를 수집합니다.
      </p>
      <p className='font-weight-700'>① 홈페이지를 통한 회원가입</p>
      <p className='font-weight-700'>② 고객센터 상담 이용</p>
      <p className='font-weight-700'>③ 로그 분석 프로그램을 통해 생성된 정보</p>
      <p className='font-weight-700'>④ 쿠키(cookie)에 의한 정보 수집</p>
      <p className='font-weight-700'>⑤ 구매를 위한 주문과 결제</p>
      <p className='font-weight-700 mb-5'>⑥ 이벤트 참여 및 당첨</p>
      <p className='font-weight-900 h5'>4. 개인정보 보유 및 이용기간</p>
      <p className='font-weight-700'>
        (1)‘회사’는 개인정보의 처리 및 목적이 달성될 때까지 개인정보를
        보유합니다. 단, 정보주체로부터 사전에 동의를 받은 경우 또는 상법 등 관련
        법령에 의하여 보존할 필요성이 있는 다음의 경우에는 개인정보를 일정기간
        보유합니다.
      </p>
      <Container>
        <Row>
          <Col lg='12'>
            <Table bordered className='text-wrap'>
              <thead>
                <tr>
                  <th style={{ width: '40%' }}>보유정보</th>
                  <th style={{ width: '20%' }}>보유기간</th>
                  <th style={{ width: '35%' }}>근거법령</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td className='align-middle'>
                    대금결제 및 재화 등의 공급에 관한 기록
                  </td>
                  <td className='text-wrap'>5년 </td>
                  <td className='text-wrap align-middle'>
                    전자상거래 등에서의 소비자보호에 관한 법률
                  </td>
                </tr>
                <tr>
                  <td className='align-middle'>
                    계약 또는 청약철회 등에 관한 기록
                  </td>
                  <td className='text-wrap'>5년</td>
                  <td rowSpan={3} className='text-wrap align-middle'>
                    전자상거래 등에서의 소비자보호에 관한 법률
                  </td>
                </tr>
                <tr>
                  <td className='align-middle'>
                    소비자의 불만 또는 분쟁처리에 관한 기록{' '}
                  </td>
                  <td className='text-wrap'>3년</td>
                </tr>
                <tr>
                  <td className='align-middle'>표시, 광고에 관한 기록</td>
                  <td className='text-wrap'>6 개월</td>
                </tr>
                <tr>
                  <td className='align-middle'>
                    웹사이트 방문 기록, 로그기록자료, IP주소 등{' '}
                  </td>
                  <td className='text-wrap'>3개월</td>
                  <td className='text-wrap align-middle'>통신비밀보호법</td>
                </tr>
                <tr>
                  <td className='align-middle'>
                    전기통신 개시/종료시간, 통신번호 등 상대방의 가입자번호,
                    통신망에 접속된 정보통신기기의 위치추적자료
                  </td>
                  <td className='text-wrap'>12 개월</td>
                  <td className='text-wrap align-middle'>통신비밀보호법</td>
                </tr>
              </tbody>
            </Table>
          </Col>
        </Row>
      </Container>
      <p className='font-weight-700'>
        (2) ‘회사’는 회원이 동의를 얻어 회원탈퇴 시 1년 간 개인정보를 보유하며,
        기간종료 후 즉시 폐기합니다.
      </p>
      <p className='font-weight-700 mb-5'>
        (3) 부정 이용으로 탈퇴한 회원의 경우 부정 이용 방지 및 비인가 사용
        방지를 위해 회원가입 등의 제한조치 목적으로 아이핀 CI는 2년간
        보관합니다.
      </p>
      <p className='font-weight-700 h5'>
        5. 장기 미이용 회원 개인정보 분리 보관
      </p>
      <p className='font-weight-700'>
        (1) “회사”는 관련 법령에 의거하여 휴면 회원의 개인정보를 접근이 불가능한
        별도의 보관장치에 분리 보관합니다.
      </p>
      <p className='font-weight-700'>1) 시행일자: 2021년 04월 01일</p>
      <p className='font-weight-700'>
        2) 관련 법령: 정보통신망 이용촉진 및 정보보호 등에 관한 법률 제29조 및
        동법 시행령 제16조
      </p>
      <p className='font-weight-700'>
        3) 휴면 회원: 회사 서비스를 1년 이상 이용하지 않은 회원
      </p>
      <p className='font-weight-700'>
        (2) 다음 조건을 모두 만족하는 경우에 휴면 회원으로 전환됩니다.
      </p>
      <p className='font-weight-700'>
        - 서비스 홈페이지(PC/모바일웹 포함) 로그인 이력이 1년 이상 없는 회원
      </p>
      <p className='font-weight-700'>
        (3) “회사”는 휴면 전환 30일 전까지 휴면 예정 회원에게 별도 분리 보관되는
        사실 및 휴면 예정일, 별도 분리 보관하는 개인정보 항목을 이메일, 서면,
        모사전송, 전화 또는 이와 유사한 방법 중 어느 하나의 방법으로 회원에게
        알립니다. 해당 통지 수단에 대한 정보가 부재 또는 오류인 경우에는
        홈페이지 공지사항 게시로 대신합니다.
      </p>
      <p className='font-weight-700'>
        (4) 휴면 상태 이전에 발급되었던 쿠폰은 해당 유효기간 만료 시 자동
        소멸되며, 기간이 만료된 쿠폰은 재발행되지 아니합니다.
      </p>
      <p className='font-weight-700 mb-5'>
        (5) 휴면 회원이 홈페이지 로그인, 고객센터 상담 등 서비스를 재이용하는
        경우 휴면계정은 일반 회원 계정으로 복구됩니다.
      </p>
      <p className='font-weight-900 h5'>6. 개인정보의 파기 절차 및 방법</p>
      <p className='font-weight-700'>(1) 파기절차</p>
      <p className='font-weight-700'>
        수집•이용목적이 달성된 개인정보의 경우 별도의 DB에 옮겨져 내부규정 및
        관련 법령을 준수하여 안전하게 보관되며, 정해진 기간이 종료되었을 때,
        지체없이 파기됩니다. 이 때, 별도의 DB로 옮겨진 개인정보는 정보주체가
        동의한 목적을 초과하여 이용되지 않으며, 법률에 정한 경우가 아니고서는
        다른 목적으로 이용되지 않습니다.
      </p>
      <p className='font-weight-700'>(2) 파기방법 </p>
      <p className='font-weight-700 mb-5'>
        - 전자적 파일 형태의 정보는 기록을 재생할 수 없는 기술적 방법을
        사용합니다. 이용자의 개인정보는 개인정보의 보유기간이 경과된 경우에는
        보유기간의 종료일로부터 5일 이내에, 개인정보의 처리 목적 달성, 해당
        서비스의 폐지, 사업의 종료 등 그 개인정보가 불필요하게 되었을 때에는
        개인정보의 처리가 불필요한 것으로 인정되는 날로부터 5일 이내에 그
        개인정보를 파기합니다.
      </p>
      <p className='font-weight-900 h5'>7. 개인정보 열람 및 수정</p>
      <p className='font-weight-700'>
        회사의 서비스 이용자는 다음과 같은 권리를 행사할 수 있습니다. 이용자 및
        법정대리인은 언제든지 수집 정보에 대하여 수정, 동의 철회, 삭제, 열람요청
        등을 요청할 수 있습니다. 다만, 동의 철회•삭제 시 서비스의 일부 또는 전부
        이용이 제한될 수 있습니다.
      </p>
      <p className='font-weight-700'>(1) 개인정보 열람 요청</p>
      <p className='font-weight-700'>
        회사에서 처리하고 있는 이용자의 개인정보에 대한 열람을 요구할 수
        있습니다. 아래 나열한 정보에 대해서는 직접 회사에 접속해 확인이
        가능합니다.
      </p>
      <p className='font-weight-700'>
        ① 회원가입시 제공한 개인정보: ‘MY PAGE&gt;회원정보수정’
      </p>
      <p className='font-weight-700'>
        ② 구매 시 제공 개인정보 및 판매자에게 제공한 개인정보: ‘MY
        PAGE&gt;결제정보’
      </p>
      <p className='font-weight-700'>
        (2) 서비스 이용을 하면서 시스템을 통하여 자동적으로 생성된 개인정보를
        통하여 직접 확인하지 못하는 개인정보를 열람하고자 하는 경우에는 고객센터
        연락을 통해 처리되며, 특별한 사유가 없는 한 10일 이내(근무일 기준)에
        조치를 합니다. 여기서 특별한 사유란 제공 시 서비스 또는 보안상 중대한
        문제가 생길 경우, 다른 사람의 생명∙신체를 해할 우려가 있거나 다른 사람의
        재산과 그 밖의 이익을 부당하게 침해할 우려가 있는 경우를 말합니다{' '}
      </p>
      <p className='font-weight-700'>(3) 개인정보의 수정</p>
      <p className='font-weight-700'>
        ‘MY PAGE&gt;회원정보수정’ 또는 1:1 상담, 메일주소를 이용하여 처리할 수
        있습니다. 이를 위하여, 본인확인 등의 절차가 진행될 수 있습니다.
      </p>
      <p className='font-weight-700'>(4) 개인정보 동의 철회 및 삭제, 처리 정</p>
      <p className='font-weight-700'>
        1:1 상담, 메일주소를 이용하여 수집 및 이용된 정보에 대한 동의 철회 및
        삭제, 처리 정지를 요청하실 수 있습니다. 다만, 요청 시 서비스의 일부 또는
        전부 이용이 제한될 수 있으며, 다른 법령에 따라 수집하는 정보의 경우에는
        동의 철회, 삭제, 처리 정지가 어려울 수 있습니다. ‘회사’는 처리가 완료될
        때까지 해당 정보를 이용하거나 타인에게 제공하지 않을 것입니다. 또한
        합리적인 사유로 잘못된 개인정보를 제3자에게 이미 제공한 경우, 그 결과를
        지체 없이 제3자에게 통지하여 동의 철회, 삭제, 처리 정지하도록
        조치합니다.
      </p>
      <p className='font-weight-700'>(5) 회원 탈퇴</p>
      <p className='font-weight-700'>
        회원탈퇴는 고객센터이메일을 통하여 진행할 수 있습니다.
      </p>
      <p className='font-weight-700'>(6) 서비스 재이용</p>
      <p className='font-weight-700'>
        ‘아이디 찾기’를 통하여 미이용자 여부를 확인할 수 있으며, 반드시
        비밀번호를 변경하신 후 계정 재이용이 가능합니다. 문의사항은 1:1 상담,
        메일주소를 이용하시기 바랍니다.
      </p>
      <p className='font-weight-900 h5'>
        8. 개인정보의 자동 수집 장치(쿠키)의 설치, 운영 및 그 거부 방법
      </p>
      <p className='font-weight-700'>
        (1) “회사”는 개인화되고 맞춤화된 서비스를 제 공하기 위하여 “회원”의
        정보를 저장하고 수시로 불러오는 “쿠키(Cookie)”를 사용합니다.
      </p>
      <p className='font-weight-700'>
        (2) 쿠키는 웹사이트를 운영하는데 이용되는 서버가 “회원”이 이용하는
        브라우저에게 보내는 아주 작은 텍스트 파일로 “회원”이 이용하는
        단말기(PC/스마트폰/태플릿PC 등)의 하드디스크에 저장됩니다. 이후 “회원”이
        웹사이트에 방문할 경우 웹사이트 서버는 “회원”이 이용하는 단말기의 하드
        디스크에 저장되어 있는 쿠키의 내용을 읽어 “회원”의 환경설정을 유지하고
        맞춤화 된 서비스를 제공하기 위해 이용됩니다.
      </p>
      <p className='font-weight-700'>
        (3) “회사”의 쿠키 사용 목적은 “회원”이 “서비스’에 접속을 하면 “회원”이
        이용한 콘텐츠나, 접속 빈도, 방문 시간 및 횟수 등을 분석하고 “회원”의
        이용 행태, 이벤트 참여 정보 등을 파악하여 “회원”을 위 한 마케팅이나
        개인화 서비스의 제공 등을 위하여 사용합니다. 쿠키는 브라우저의 종료 시나
        로그아웃 시 만료됩니다.
      </p>
      <p className='font-weight-700'>
        (4) “회원”은 언제든지 쿠키의 설치를 거부하거나 삭제할 수 있습니다. 이에
        따라 “회원”은 웹브라우저에서 옵션을 설정함으로써 모든 쿠키를 허용하거나,
        쿠키가 저장될 때마다 확인을 거치거나, 아니면 모든 쿠키의 저장을 거부할
        수도 있습니다. 단, 쿠키의 저장을 거부할 경우 로그인이 필요한 일부
        서비스는 이용에 어려움이 있을 수 있습니다. 쿠키 설치 허용 여부를
        지정하는 방법(Internet Explorer의 경우)은 다음의 각 호와 같습니다.
      </p>
      <p className='font-weight-700'>
        ① 웹브라우저 상단의 [도구] 메뉴에서 [인터넷 옵션]을 클릭
      </p>
      <p className='font-weight-700'>② [개인정보] 탭을 클릭</p>
      <p className='font-weight-700'>③ [개인정보 처리 수준]을 설정</p>
      <p className='font-weight-900 h5'>
        9. 이용자 및 법정대리인의 권리와 의무
      </p>
      <p className='font-weight-700'>
        (1) “회원(만 14세 미만 아동 회원의 경우 그 법정대리인 포함)”은 다음
        각호의 방법에 따라 “회원” 본인의 개인정보를 열람 또는
        정정·삭제·처리정지를 요구하거나 개인정보 제공에 관한 동의철회, 회원가입
        해지를 요청할 수 있습니다. “회사”는 “회원”의 요구를 개인정보 관련 법령에
        따라 지체없이 처리할 것입니다.
      </p>
      <p className='font-weight-700'>① 홈페이지를 통한 ‘회원정보수정’</p>
      <p className='font-weight-700'>② 고객센터를 통한 요청</p>
      <p className='font-weight-700'>
        ③ 개인정보 담당부서 및 담당자(본 “개인정보처리방침” 상 “13. 개인정보
        보호 담당부서 및 연락처” 조항 참조)에게 서면 또는 이메일로 요청
      </p>
      <p className='font-weight-700'>
        (2) “회원”은 개인정보를 최신의 상태로 정확하 게 입력하여 불의의 사고를
        예방할 수 있도록 협조해야 합니다. “회원”의 부정확한 정보 제공으로
        발생하는 사고의 책임은 “회원”에게 있으며, 타인 정보의 도용 등 허위
        정보를 입력할 경우 “회원” 자격이 상실될 수 있습니다.
      </p>
      <p className='font-weight-700 mb-5'>
        (3) “회원”은 개인정보를 보호받을 권리와 함께 스스로를 보호하고 타인의
        정보를 침해하지 않을 의무도 가지고 있습니다. 비밀번호를 포함한 “회원”의
        개인정보가 유출되지 않도록 조심하시고 게시물을 포함한 타인의 개인정보를
        훼손하지 않도록 유의하시기 바랍니다. 만약 이 같은 책임을 다하지 못하고
        타인의 정보를 훼손할 시에는 ‘개인정보 보호법’ 등 관련 법령에 의해 처벌될
        수 있습니다.
      </p>
      <p className='font-weight-900 h5'>
        10. 개인정보보호를 위한 기술적, 관리적 대책
      </p>
      <p className='font-weight-700'>
        (1) “회사”는 “회원”의 개인정보를 처리함에 있어 개인정보가 분실, 도난,
        유출, 변조 또는 훼손되지 않도록 최선을 다하고 있으며, 다음의 각 호와
        같은 기술적 대책을 강구하고 있습니다.
      </p>
      <p className='font-weight-700'>
        ① “회원”의 개인정보는 아이디와 비밀번호에 의해 관리되고 있으며,
        비밀번호를 포함하는 파일 및 전송 데이터 등의 중요한 데이터는 암호화 등
        별도의 보안기능을 통해 보호되고 있습니다. “회사”는 암호화 알고리즘을
        이용하여 네트워크 상의 개인정보를 안전하게 전송할 수 있는 보안 장치를
        채택하고 있습니다
      </p>
      <p className='font-weight-700'>
        ② “회원”의 개인정보를 저장하고 처리하는 시스템은 방화벽과
        침입차단시스템을 통하여 보호되며, 24시간 감시를 통하여 외부로부터의
        위협에 대해 관리되고 있습니다.
      </p>
      <p className='font-weight-700'>
        ③ “회사”는 백신프로그램을 이용하여 컴퓨터 바이러스에 의한 피해를
        방지하기 위한 조치를 취하고 있습니다. 백신프로그램은 주기적으로
        업데이트되며 갑작스러운 바이러스가 출현할 경우 백신이 나오는 즉시 이를
        제공함으로써 개인정보가 침해되는 것을 방지하고 있습니다.
      </p>
      <p className='font-weight-700'>
        (2) “회사”는 “회원”의 개인정보를 처리함에 있어 다음의 각 호와 같은
        관리적 대책을 강구하고 있습니다.
      </p>
      <p className='font-weight-700'>
        ① “회원”의 개인정보에 대한 접근 권한은 업무 역할에 따라 필요한 최소한의
        인원으로 제한하고 있습니다.
      </p>
      <p className='font-weight-700'>
        ② “회사”의 임직원이 업무를 함에 있어 “회원”의 개인정보를 보호하기 위해
        지켜야 할 사내 규정을 마련하여 준수하고 있습니다.
      </p>
      <p className='font-weight-700'>
        ③ 회사”의 임직원이 “회원 ”의 개인정보를 처리하기 이전에 ‘개인정보
        처리서약서’를 통하여 “회원”의 개인정보에 대한 정보 유출을 사 전에
        방지하고 사내 규정에 대한 이행사항 및 준수 여부를 감시하기 위한 내부
        절차를 마련하고 있습니다.
      </p>
      <p className='font-weight-700'>
        ④ “회사” 임직원의 개인정보보호 실천을 위한 정기적인 개인정보보호 교육을
        실시하고 있습니다.
      </p>
      <p className='font-weight-700'>
        ⑤ “회사”가 전산실, 자료보관실 등의 장소를 둘 경우 출입통제 절차를 수립
        및 운영하고, 개인정보가 포함된 서류, 보조저장매체 등은 잠금 장치가 있는
        안전한 장소에 보관하는 등 보안대책를 마련하였습니다.
      </p>
      <p className='font-weight-700'>
        ⑥ “회사”는 단전 등 재해, 재난 발생시 개인정보처리시스템 보호를 위한
        위기대응 매뉴얼을 만들고 이를 정기적으로 점검하며 시스템 백업 및 복구를
        위한 방안을 마련하였습니다.
      </p>
      <p className='font-weight-700 mb-5'>
        (3) “회사”는 상기의 기술적, 관리적 대책에도 불구하고 “회원” 개인의
        실수나 사고 발생 당시의 기술로 예방, 조치할 수 없는 불가항력 사유로
        인하여 발생한 개인정보의 분실, 도난, 유출, 변조 또는 훼손에 대해서는
        “회사”가 책임을 지지 않습니다.
      </p>
      <p className='font-weight-900 h5'>11. 의견 수렴 및 불만 처리</p>
      <p className='font-weight-700'>
        (1) “회사”는 “회원”의 의견을 소중하게 생각하며, “회원”과의 원활한
        의사소통을 위해 고객센터를 운영하고 있습니다. “회사”의 고객센터 연락처
        및 상담 가능 시간은 다음의 각 호와 같습니다.
      </p>
      <p className='font-weight-700'>① 고객센터 전화번호: 02-6011-540</p>
      <p className='font-weight-700'>
        ② 고객센터 전화상담 가능시간: 오전 10시 ~ 오후 5시
      </p>
      <p className='font-weight-700'>
        (2) 기타 개인정보에 관한 상담이 필요한 경우 개인정보침해신고 센터,
        경찰청 사이버테러대응센터 등으로 문의하실 수 있습니다.
      </p>
      <p className='font-weight-700'>
        ①[개인정보침해신고센터] 전화번호: 118 / URL : http://privacy.kisa.or.kr
      </p>
      <p className='font-weight-700 mb-5'>
        ②[경찰청사이버테러대응센터] 전화번호: 182 / URL :
        http://cyberbureau.police.go.kr
      </p>
      <p className='font-weight-900 h5'>12. 개인정보 보호 담당부서 및 연락처</p>
      <p className='font-weight-700'>
        (1) 관련한 이용자의 불만처리 및 피해구제 등을 위하여 아래와 같이
        개인정보 보호책임자를 지정하고 있습니다.
      </p>
      <p className='font-weight-700'>▶ 개인정보 보호책임자</p>
      <p className='font-weight-700'>성명 : 양성열</p>
      <p className='font-weight-700'>직책 : 팀장</p>
      <p className='font-weight-700'>연락처 : 02-6011-5411</p>
      <p className='font-weight-700'>이메일 : maderi@datamarketing.co.kr</p>
      <p className='font-weight-700'>팩스 : 070-8270-0017</p>
      <p className='font-weight-700'>※ 개인정보 보호 담당부서로 연결됩니다</p>
      <p className='font-weight-700'>▶ 개인정보 보호 담당부서</p>
      <p className='font-weight-700'>부서명 : 데이터기술연구소</p>
      <p className='font-weight-700'>담당자 : 양성열</p>
      <p className='font-weight-700'>연락처 : 02-6011-5411</p>
      <p className='font-weight-700'>이메일 : maderi@datamarketing.co.kr,</p>
      <p className='font-weight-700'>팩스 : 070-8270-0017</p>
      <p className='font-weight-700'>
        (2) 회사가 제공하는 서비스(또는 사업)를 이용하시면서 발생한 모든
        개인정보 보호 관련 문의, 불만처리, 피해구제 등에 관한 사항을 개인정보
        보호책임자 및 고객지원센터로 문의하실 수 있으며, 회사는 이런 문의에 대해
        지체 없이 답변 및 처리 할 것입니다.
      </p>
      <p className='font-weight-700 mb-5'>
        (3) 개인정보가 침해 신고나 상담이 필요하신 경우에는 아래 기관에
        문의하시기 바랍니다. <br />
        - 개인정보 침해신고센터 (한국인터넷진흥원 운영) : (국번없이) 118 /
        privacy.kisa.or.kr <br />
        - 대검찰청 사이버범죄수사단 : (국번없이) 1301 / www.spo.go.kr <br />-
        경찰청 사이버안전국 : (국번없이) 182 / www.cyber.go.kr
      </p>
      <p className='font-weight-900 h5'>회사의 정보보호 노력</p>
      <p className='font-weight-700'>
        (1)회사는 고객님의 개인정보를 안전하게 관리하기 위해 다양한 정보보호
        활동을 수립·이행하고 있습니다.
      </p>
      <p className='font-weight-700'>①정기적인 자체 감사 실시</p>
      <p className='font-weight-700'>
        개인정보 취급 관련 안정성 확보를 위해 정기적(분기 1회)으로 자체 감사를
        실시하고 있습니다.
      </p>
      <p className='font-weight-700'>②개인정보 취급 직원의 최소화 및 교육</p>
      <p className='font-weight-700'>
        개인정보를 취급하는 직원을 지정하고 담당자에 한정시켜 최소화 하여
        개인정보를 관리하는 대책을 시행하고 있습니다.
      </p>
      <p className='font-weight-700'>③내부관리계획의 수립 및 시행</p>
      <p className='font-weight-700'>
        개인정보의 안전한 처리를 위하여 내부관리계획을 수립하고 시행하고
        있습니다.
      </p>
      <p className='font-weight-700'>④해킹 등에 대비한 기술적 대책</p>
      <p className='font-weight-700'>
        회사는 해킹이나 컴퓨터 바이러스 등에 의한 개인정보 유출 및 훼손을 막기
        위하여 보안프로그램을 설치하고 주기적인 갱신·점검을 하며 외부로부터
        접근이 통제된 구역에 시스템을 설치하고 기술적/물리적으로 감시 및
        차단하고 있습니다.
      </p>
      <p className='font-weight-700'>⑤개인정보의 암호화</p>
      <p className='font-weight-700'>
        이용자의 개인정보는 비밀번호는 암호화 되어 저장 및 관리되고 있어,
        본인만이 알 수 있으며 중요한 데이터는 파일 및 전송 데이터를 암호화
        하거나 파일 잠금 기능을 사용하는 등의 별도 보안기능을 사용하고 있습니다.
      </p>
      <p className='font-weight-700'>⑥접속기록의 보관 및 위변조 방지</p>
      <p className='font-weight-700'>
        개인정보처리시스템에 접속한 기록을 최소 2년 이상 보관, 관리하고 있으며,
        접속 기록이 위변조 및 도난, 분실되지 않도록 보안기능 사용하고 있습니다.
      </p>
      <p className='font-weight-700'>⑦문서보안을 위한 잠금장치 사용</p>
      <p className='font-weight-700'>
        개인정보가 포함된 서류, 보조저장매체 등을 잠금장치가 있는 안전한 장소에
        보관하고 있습니다. ⑧비인가자에 대한 출입 통제
      </p>
      <p className='font-weight-700 mb-5'>
        개인정보를 보관하고 있는 물리적 보관 장소를 별도로 두고 이에 대해
        출입통제 절차를 수립, 운영하고 있습니다.
      </p>
      <p className='font-weight-900 h5'>14. 고지의 의무</p>
      <p className='font-weight-700'>
        (1) 본 “개인정보처리방침”을 포함한 기타 개인 정보보호에 대한 상세한
        내용은 “회사”가 운영하는 서비스(www.marketingcampus.co.kr) 첫 화면
        하단에 공개함으로써 “회원”들이 언제나 용이하게 볼 수 있도록 조치하고
        있습니다. 본 “개인정보처리방침”의 내용은 수시로 변경될 수 있으므로
        홈페이지를 방문하실 때마다 이를 확인하시기 바랍니다.
      </p>
      <p className='font-weight-700'>
        (2) 본 “개인정보처리방침”이 관련 법령 및 정책 또는 보안기술의 변경에
        따라 내용의 추가 삭제 및 수정이 있을 경우 변경되는 “개인정보처리방침”을
        시행하기 전에 서비스(www.marketingcampus.co.kr)를 통해 변경 사유 및 내용
        등을 공지합니다.
      </p>
      <p className='font-weight-700'>
        (3) 본 개인정보처리방침은 2021년 04월 01일부터 시행됩니다.
      </p>
      <p className='font-weight-900 h5 text-center'>서비스 이용약관</p>
      <p className='font-weight-700 mb-3'>
        개인정보보호법 제22조 제4항에 의해 선택정보 사항에 대해서는 기재하지
        않아도 서비스를 이용하실 수 있습니다.
      </p>
      <div className='font-weight-700'>① 마케팅 및 광고에의 활용</div>
      <p className='font-weight-700'>
        – 신규 기능 개발 및 맞춤 서비스 제공 <br />
        – 새로운 기능(제품)의 안내 <br />– 할인 및 쿠폰 등 이벤트 등 광고성 정보
        제공
      </p>
      <p className='font-weight-700'>
        ② 서비스를 운용함에 있어 각종 정보를 서비스 화면, SMS, 이메일 등의
        방법으로 회원에게 제공할 수 있으며, 결제 안내 등 의무적으로 안내되어야
        하는 정보성 내용 및 일부 혜택성 정보는 수신동의 여부와 무관하게
        제공합니다.
      </p>
    </ScrollDiv>
  );
}

export default PersnalInfo;
